﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Oss.Hrms.Models;
using Oss.Hrms.Models.Services;
using Oss.Hrms.Models.ViewModels.HRMS;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using Oss.Hrms.Helper;

namespace Oss.Hrms.Controllers
{
    public class FormController : Controller
    {   DropDownData d = new DropDownData();
        HrmsContext db = new HrmsContext();
        VMApplications vmApplication;
        CurrentUser user;

        public FormController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
            this.user = sessionHandler.CurrentUser;
        }

        
        #region Employee Details

        public ActionResult IndexEmployeeDetails()
        {
            if (user != null)
            {
                ViewBag.Emp = new SelectList(d.GetActiveEmployeeListforForm(), "Value", "Text");
                return View();
            }

            return RedirectToAction("IndexEmployeeDetails", "Form");
        }

        [HttpPost]
        public async Task<ActionResult>  IndexEmployeeDetails(int id=0)
        {
            if (user != null && id > 0)
            {
                VMEmployee model = new VMEmployee();
                await Task.Run(() => model = model.DetailsVmEmployeesIdWise(id));
                return View("IndexSpecificEmployeeDetails", model);
            }
            return RedirectToAction("IndexEmployeeDetails", "Form");
        }

        public ActionResult IndexSpecificEmployeeDetails()
        {
            try
            {
               // ViewBag.Emp = new SelectList(dd.GetApplicationForms(), "Value", "Text");
                VMEmployee model = new VMEmployee();
                return View(model);
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("IndexEmployeeDetails", "Form");
            }
        }

        #endregion

        public ActionResult ApplicationForm(int id, int bid)
        {
            ViewBag.Grade = new SelectList(d.GetEmployeeGradeList(), "Value", "Text");
            ViewBag.Designation = new SelectList(d.GetEmployeeDesignationsList(), "Value", "Text");
            ViewBag.Section = new SelectList(d.GetEmployeeSectionsList(), "Value", "Text");
            ViewBag.Gender = new SelectList(d.GetChildGender(), "Value", "Text");
            ViewBag.LawStep = new SelectList(d.GetMaternityLawStep(), "Value", "Text");
            ViewBag.PE = new SelectList(d.PhysicalEligibility(), "Value", "Text");

            vmApplication = new VMApplications();
            vmApplication.ID = id;
            vmApplication.ButtonID = bid;
            vmApplication.ApplicationDate = DateTime.Now;
            vmApplication.FromDate = DateTime.Now;
            vmApplication.ToDate = DateTime.Now;
            vmApplication.StartDate = DateTime.Now;
            vmApplication.EndDate= DateTime.Now;
            vmApplication.EDD = DateTime.Now;
            vmApplication.LMP = DateTime.Now;

            #region MaternityFrom
            if (bid == 1)
            {
                ViewBag.Title = "Maternity Leave";
            }
            else if (bid == 2)
            {
                ViewBag.Title = "Maternity welfare Leave-B";
            }
            else if (bid == 3)
            {
                ViewBag.Title = "Maternity welfare Leave-C";
            }
            else if (bid == 4)
            {
                ViewBag.Title = "Maternity Leave Certificate-1 ";
            }
            else if (bid == 5)
            {
                ViewBag.Title = "Maternity Leave Certificate-2";
            }
            else if (bid == 6)
            {
                ViewBag.Title = "Maternity Welfare Facilities-D";
            }
            else if (bid == 7)
            {
                ViewBag.Title = "Maternity Welfare Allowance";
            }
            else if (bid == 8)
            {
                ViewBag.Title = " ALL Maternity From";
            }
            #endregion
         
            #region ApplicationFrom
            else if (bid == 31)
            {
                ViewBag.Title = "Promotion and Wage Increment";
            }
            else if (bid == 32)
            {
                ViewBag.Title = "Age Eligibility Testimonial";
            }
            else if (bid == 33)
            {
                ViewBag.Title = "Yearly Wage Increment";
            }
            else if (bid == 34)
            {
                ViewBag.Title = "Rejoin form-1";
            }
            else if (bid == 35)
            {
                ViewBag.Title = "Rejoin form-2";
            }
            else if (bid == 36)
            {
                ViewBag.Title = "Resign Letter-3";
            }
            else if (bid == 37)
            {
                ViewBag.Title = "Resign Letter";
            }
            else if (bid == 38)
            {
                ViewBag.Title = "Appoinment Letter";
            }
            else if (bid == 39)
            {
                ViewBag.Title = "Wage Increment";
            }
            else if (bid == 40)
            {
                ViewBag.Title = "Change Section and Wage Increment";
            }
            else if (bid == 41)
            {
                ViewBag.Title = "Job Applicant Details Information";
            }
            else if (bid == 42)
            {
                ViewBag.Title = "Back Ground Check";
            }
            else if (bid == 43)
            {
                ViewBag.Title = "Age Vefification Certificate";
            }
         
            else if (bid == 44)
            {
                ViewBag.Title = "Final Settlement";
            }
            #endregion

            return View(vmApplication);
        }

        [HttpPost]
        public ActionResult ApplicationForm(VMApplications model)
        {
            ReportClass cr = new ReportClass();
            string title = String.Empty;
            #region MaternityForm
            if (model.ButtonID == 1)
            {
                model.LoadData(model);
                cr.FileName = Server.MapPath("~/Views/Reports/MaternityFrom.rpt");
                title = "Maternity Leave Application From";
            }
            else if (model.ButtonID == 2)
            {
                model.LoadData(model);
               
                cr.FileName = Server.MapPath("~/Views/Reports/MaternityBedRestFormB.rpt");
                title = "Maternity Bed Rest FromB";
             }

            else if (model.ButtonID == 3)
            {
                model.LoadData(model);
                cr.FileName = Server.MapPath("~/Views/Reports/MaternityLeaveFormC.rpt");
                title = "Maternity Bed Rest Application FromC";
            }
            else if (model.ButtonID == 4)
            {
                model.LoadData(model);
                cr.FileName = Server.MapPath("~/Views/Reports/MedicalCertificateUpdate.rpt");
                title = "Maternity Medical Certificate Update From";
            }
            else if (model.ButtonID == 5)
            {
                model.LoadData(model);
                cr.FileName = Server.MapPath("~/Views/Reports/MedicalCertificate.rpt");
                title = "Maternity Medical Certificate From";
            }
            else if (model.ButtonID == 6)
            {
                model.LoadData(model);
                cr.FileName = Server.MapPath("~/Views/Reports/MaternityFacilities.rpt");
                title = "Maternity Facilities Application From";
            }
            else if (model.ButtonID == 7)
            {
                model.LoadData(model);
                cr.FileName = Server.MapPath("~/Views/Reports/MaternityAllowance.rpt");
                title = "Maternity Allowance Application From";
            }
            else if (model.ButtonID == 8)
            {
                model.LoadData(model);
                cr.FileName = Server.MapPath("~/Views/Reports/Master.rpt");
                title = "All Maternity Application From";
            }
            #endregion
         
            #region ApplicationFrom
            else if (model.ButtonID == 31)
            {
                model.LoadData(model);
                cr.FileName = Server.MapPath("~/Views/Reports/PromotionAndWageIncrement.rpt");
                title = "Promotion And Wage Increment";
            }
            else if (model.ButtonID == 32)
            {
                model.LoadData(model);
                cr.FileName = Server.MapPath("~/Views/Reports/AgeEligibilityTestimonial.rpt");
                title = "Age Eligibility Testimonial";
            }
            else if (model.ButtonID == 33)
            {
                model.LoadData(model);
                cr.FileName = Server.MapPath("~/Views/Reports/IncrementLetterYearly.rpt");
                title = "Increment Letter Yearly";
            }
            else if (model.ButtonID == 34)
            {
                model.LoadData(model);
                cr.FileName = Server.MapPath("~/Views/Reports/RejoinForm1.rpt");
                title = "Rejoin Form1";

            }
            else if (model.ButtonID == 35)
            {
                model.LoadData(model);
                cr.FileName = Server.MapPath("~/Views/Reports/RejoinForm2.rpt");
                title = "Rejoin Form2";

            }
            else if (model.ButtonID == 36)
            {
                model.LoadData(model);
                cr.FileName = Server.MapPath("~/Views/Reports/ResignLetter3.rpt");
                title = "Resign Letter3";
            }
            else if (model.ButtonID == 37)
            {
                model.LoadData(model);
                cr.FileName = Server.MapPath("~/Views/Reports/ResignLetter.rpt");
                title = "Resign Letter";
            }
            else if (model.ButtonID == 38)
            {
                model.LoadData(model);
                cr.FileName = Server.MapPath("~/Views/Reports/AppoinmentLetter.rpt");
                title = "Appoinment Letter From";


            }
            else if (model.ButtonID == 39)
            {
                model.LoadData(model);
                cr.FileName = Server.MapPath("~/Views/Reports/Demo.rpt");
                title = "Wage Increment From";

            }
            else if (model.ButtonID == 40)
            {
                model.LoadData(model);
                cr.FileName = Server.MapPath("~/Views/Reports/Demo.rpt");
                title = "Change Section and Wage Increment From";

            }
            else if (model.ButtonID == 41)
            {
                model.LoadData(model);
                cr.FileName = Server.MapPath("~/Views/Reports/Demo.rpt");
                title = "Job Applicant Details Information From";

            }
            else if (model.ButtonID == 42)
            {
                model.LoadData(model);
                cr.FileName = Server.MapPath("~/Views/Reports/Demo.rpt");
                title = "Back Ground Check From";
            }
            else if (model.ButtonID == 43)
            {
                model.LoadData(model);
                cr.FileName = Server.MapPath("~/Views/Reports/Demo.rpt");
                title = "Age Vefification Certificate From"; ;

            }
            else if (model.ButtonID == 44)
            {
                //model.LoadData(model);
                model.LoadFinalSettlementData(model);
                cr.FileName = Server.MapPath("~/Views/Reports/FinalSettlement.rpt");
                title = "Final Settlement";


            }
            #endregion

            cr.Load();
            cr.SetDataSource(model.ReportSource);
            cr.SummaryInfo.ReportTitle = title;
            Stream stream = 
               cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }
        #region
   
        public ActionResult JobDescriptionForm(int id,int bid)
        {
            VMApplications model = new VMApplications();
            model.ID = id;
            model.ButtonID = bid;
            ReportClass cr = new ReportClass();
            string title = String.Empty;
            model.JobLoadData(model);

            #region JobDescription
            if (model.ButtonID == 11)
            {
               cr.FileName = Server.MapPath("~/Views/Reports/FoldingManPacking.rpt");
                title = "Folding Man Packing";
            }
            else if (model.ButtonID == 12)
            {
              
                cr.FileName = Server.MapPath("~/Views/Reports/FoldingMan.rpt");
                title = "Folding Man";
            }
            else if (model.ButtonID == 13)
            {
           
                cr.FileName = Server.MapPath("~/Views/Reports/Inputman.rpt");
                title = "Input Man";
            }
            else if (model.ButtonID == 14)
            {
              
                cr.FileName = Server.MapPath("~/Views/Reports/KnittingOperator .rpt");
                title = "Knitting Operator";
            }
            else if (model.ButtonID == 15)
            {
              
                cr.FileName = Server.MapPath("~/Views/Reports/StoreLoader.rpt");
                title = "Store Loader/Helper";
            }
            else if (model.ButtonID == 16)
            {
              
                cr.FileName = Server.MapPath("~/Views/Reports/PackingManDescription.rpt");
                title = "Packing Man Description";

            }
            else if (model.ButtonID == 17)
            {
       
                cr.FileName = Server.MapPath("~/Views/Reports/PackingManResponsibility.rpt");
                title = "Packing Man Responsibility";
            }
            else if (model.ButtonID == 18)
            {
              
                cr.FileName = Server.MapPath("~/Views/Reports/PolyManDescription.rpt");
                title = "Poly Man Description";
            }
            else if (model.ButtonID == 19)
            {
             
                cr.FileName = Server.MapPath("~/Views/Reports/PolyManResponsibility.rpt");
                title = "Poly Man Responsibility";
            }
            else if (model.ButtonID == 20)
            {
           
                cr.FileName = Server.MapPath("~/Views/Reports/PrintAssistant.rpt");
                title = "Print Assistant";
            }
            else if (model.ButtonID == 21)
            {
               
                cr.FileName = Server.MapPath("~/Views/Reports/SecurityIncharge.rpt");
                title = "Security Incharge";
            }
            else if (model.ButtonID == 22)
            {
              
                cr.FileName = Server.MapPath("~/Views/Reports/SecurityIncharge.rpt");
                title = "Security Supervisor";


            }
            else if (model.ButtonID == 23)
            {
            
                cr.FileName = Server.MapPath("~/Views/Reports/SecurityGuard.rpt");
                title = "Security Guard";

            }
            else if (model.ButtonID == 24)
            {
            
                cr.FileName = Server.MapPath("~/Views/Reports/SewingOperator.rpt");
                title = "Sewing Operator Description";
            }
            else if (model.ButtonID == 25)
            {
              
                cr.FileName = Server.MapPath("~/Views/Reports/SewingOperatorResponsibility.rpt");
                title = "Sewing Operator Responsibility";

            }
            else if (model.ButtonID == 26)
            {
               
                cr.FileName = Server.MapPath("~/Views/Reports/SpotMan.rpt");
                title = "Spot Man";

            }
            else if (model.ButtonID == 27)
            {
             
                cr.FileName = Server.MapPath("~/Views/Reports/CutterMan.rpt");
                title = "Cutter Man";

            }
            else if (model.ButtonID == 28)
            {
              
                cr.FileName = Server.MapPath("~/Views/Reports/CuttingAssistant.rpt");
                title = "Cutting Assistant ";

            }
            else if (model.ButtonID == 29)
            {
                cr.FileName = Server.MapPath("~/Views/Reports/FinishingAssistant.rpt");
                title = "Finishing Assistant";
            }
            #endregion


            cr.Load();
            cr.SetDataSource(model.ReportSource);
            cr.SummaryInfo.ReportTitle = title;
            Stream stream =
               cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }

        #endregion
        public ActionResult ChangeOfDesignation(int id)
        {
            vmApplication = new VMApplications();
            ViewBag.Title = "Change Of Designation";
            vmApplication.ID = id;
            vmApplication.ApplicationDate = DateTime.Now;
            vmApplication.FromDate = DateTime.Now;
            vmApplication.ToDate = DateTime.Now;
            
            ViewBag.Grade = new SelectList(d.GetEmployeeGradeList(), "Value", "Text");
            ViewBag.Designation = new SelectList(d.GetEmployeeDesignationsList(), "Value", "Text");
            ViewBag.Section = new SelectList(d.GetEmployeeSectionsList(), "Value", "Text");

            return View("ApplicationForm", vmApplication);
        }

        [HttpPost]
        public ActionResult ChangeOfDesignation(VMApplications model)
        {
            model.LoadData(model);
            ReportClass cr = new ReportClass();

            cr.FileName = Server.MapPath("~/Views/Reports/ChangeOfDesignation.rpt");
            cr.Load();
            cr.SetDataSource(model.ReportSource);
            cr.SummaryInfo.ReportTitle = "Application Form Of Change Designation";
            Stream stream = cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }
        
        #region IDCard
        public ActionResult EmployeeIdCard(VMApplications model)
        {
           DtPayroll ds = new DtPayroll();
           ds = model.IDCardGenarate();
           ReportClass cr = new ReportClass();
          
            cr.FileName = Server.MapPath("~/Views/Reports/IDCard.rpt");
            cr.Load();
            cr.SetDataSource(ds);
            cr.SummaryInfo.ReportTitle = "Employee ID Card";
            Stream stream = cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");

        }


        #endregion
        
    }
}
