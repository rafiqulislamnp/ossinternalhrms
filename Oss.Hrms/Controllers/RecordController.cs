﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Oss.Hrms.Models;
using Oss.Hrms.Models.Entity;
using Oss.Hrms.Models.Entity.Common;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;
using Oss.Hrms.Models.ViewModels.HRMS;
namespace Oss.Hrms.Controllers
{
    public class RecordController : Controller
    {
        DropDownData d = new DropDownData();
        HrmsContext db = new HrmsContext();
        CurrentUser user;
        public RecordController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
            this.user = sessionHandler.CurrentUser;
        }

        // GET: Record
      public ActionResult Index()
        {
            return View();
        }

        // GET: Record/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Record/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Record/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Record/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Record/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Record/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Record/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
