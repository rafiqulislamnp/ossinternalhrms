﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Oss.Hrms.Models;
using Oss.Hrms.Models.Entity;
using Oss.Hrms.Models.Entity.Common;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;
using Oss.Hrms.Models.ViewModels.HRMS;
using System.Data;
using Oss.Hrms.Models.ViewModels.Reports;
using System.Web.Helpers;
using Oss.Hrms.Models.ViewModels;
using Oss.Hrms.Helper;
using CrystalDecisions.CrystalReports.Engine;
using System.Media;
using System.Web.UI;
using System.Windows.Media.Imaging;
using System.Windows;

namespace Oss.Hrms.Controllers
{
    public class ReportController : Controller
    {
        DropDownData d = new DropDownData();
        HrmsContext db = new HrmsContext();
        VMReportPayroll reportPayroll = new VMReportPayroll();
        CurrentUser user;

        public ReportController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
            this.user = sessionHandler.CurrentUser;
        }

        // GET: Report
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> AllReport()
        {
            ViewBag.ReportList = new SelectList(d.GetReportList(), "Value", "Text");
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AllReport(VM_Report model)
        {
            DataTable table = new DataTable();
            var filename = string.Empty;

            if (model.ReportId == 1)
            {
                filename = "Present Employee Information";
                table = GetPresentEmployeeReport();
            }
            else if (model.ReportId == 2)
            {
                filename = "Employee Address Information-01";
                table = GetEmployeeAddressReport01();
            }
            else if (model.ReportId == 3)
            {
                filename = "Employee Address Information-02";
                table = GetEmployeeAddressReport02();
            }
            else if (model.ReportId == 4)
            {
                filename = "Employee Gender Information";
                table = GetGenderReport();
            }
            else if (model.ReportId == 5)
            {
                filename = "Preodic Joining List";
                table = GetJoiningListReport(model);
            }
            else
            {
                return Content("No One");
            }

            GenerateReport(table, filename);
            
            return RedirectToAction("AllReport");
        }
        [HttpGet]
        public async Task<ActionResult> LeaveReport()
        {
            ViewBag.ReportList = new SelectList(d.GetLeaveReportList(), "Value", "Text");
            ViewBag.SectionList = new SelectList(d.GetEmployeeSectionsListWithAll(), "Value", "Text");
            ViewBag.EmployeeList = new SelectList(d.GetEmployeeListforLeaveApplicationwithAll(), "Value", "Text");
            ViewBag.ResignEmployeeList = new SelectList(d.GetEResignEmployeeListforEarnLeaveReport(), "Value", "Text");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> LeaveReport(VM_Report model)
        {
            DataTable table = new DataTable();
            var filename = string.Empty;
            if (model.ReportId == 20)
            {
                table = model.MonthlyLeaveReprt(model.Fromdate, model.Todate, model.EmployeeId);
                GenerateReport(table, "Monthly Leave Report");
            }
            else if (model.ReportId == 21)
            {
                table = model.SectionWiseMonthlyLeaveReprt(model.Fromdate, model.Todate, model.SectionID);
                GenerateReport(table, "Section Wise Leave Report");
            }
            else if (model.ReportId == 22)
            {
                table = model.MeternityMonthlyLeaveReprt(model.Fromdate, model.Todate);
                GenerateReport(table, "Meternity Leave Report");
            }
            else if (model.ReportId == 23)
            {
               // table = model.MeternityMonthlyLeaveReprt(model.Fromdate, model.Todate);
                 table = model.ResignationEarnLeaveReprt(model.EmployeeId1);
                GenerateReport(table, "Resignation Leave Report");
            }
            else
            {
                return Content("No One");
            }
            GenerateReport(table, filename);
            return RedirectToAction("LeaveReport");
        }


        public async Task<ActionResult> AttendanceReport()
        {
            ViewBag.ReportList = new SelectList(d.GetAttendanceReportList(), "Value", "Text");
            ViewBag.SectionList = new SelectList(d.GetEmployeeSectionsListWithAll(), "Value", "Text");
            ViewBag.EmployeeList = new SelectList(d.GetEmployeeListforLeaveApplicationwithAll(), "Value", "Text");
            ViewBag.ResignEmployeeList = new SelectList(d.GetEResignEmployeeListforEarnLeaveReport(), "Value", "Text");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AttendanceReport(VM_Report model)
        {
            DataTable table = new DataTable();
            var filename = string.Empty;
            if (model.ReportId == 7)
            {
               // filename = "Daily Attendance Summery";
                var dt = DateTime.Today;
                if (model.Fromdate == dt)
                {
                    table = model.InstantDailyAttendanceSummery(model.Fromdate);
                }
                else
                {
                    table = model.DailyAttendanceSummery(model.Fromdate);
                }
               GenerateReport(table, "Daily Attendance Summery");
            }
            else if (model.ReportId == 8)
            {
                table = model.PersonalMonthlyAttendanceSummery(model.Fromdate, model.Todate, model.EmployeeId);
                GenerateReport(table, "Personal Attendance Summery");
            }






            //else if (model.ReportId == 9)
            //{
            //    table = model.DailyInvalidAttendance(model.Fromdate);
            //    GenerateReport(table, "Daily Invalid Attendance");
            //}
            else if (model.ReportId == 10)
            {
               var dt = DateTime.Today;
                if (model.Fromdate == dt)
                {
                    table = model.InstantDailyAttendanceReport(model.Fromdate);
                }
                else
                {
                    table = model.DailyAttendanceReport(model.Fromdate);
                }
                GenerateReport(table, "Daily Attendance Report");

            }
            else if (model.ReportId == 11)
            {
               var dt = DateTime.Today;
                if (model.Fromdate == dt)
                {
                    table = model.InstantDailyLateAttendanceReport(model.Fromdate);
                }
                else
                {
                    table = model.DailyLateAttendanceReport(model.Fromdate);
                }
                GenerateReport(table, "Daily Late Report");
            }
            else if (model.ReportId == 12)
            {
                var dt = DateTime.Today;
                if (model.Fromdate == dt)
                {
                    table = model.InstantDailyAbsentAttendanceReport(model.Fromdate);
                }
                else
                {
                    table = model.DailyAbsentAttendanceReport(model.Fromdate);
                }
                GenerateReport(table, "Daily Absent Report");
            }
            else if (model.ReportId == 13)
            {
                var dt = DateTime.Today;
                if (model.Fromdate == dt)
                {
                    table = model.InstantDailyInvalidAttendanceReport(model.Fromdate);
                }
                else
                {
                    table = model.DailyInvalidAttendanceReport(model.Fromdate);
                }
                GenerateReport(table, "Daily Invalid Report");
            }
            else if (model.ReportId == 14)
            {
                table = model.SectionWiseTiffinList(model.Fromdate);
                GenerateReport(table, "Section Wise Tiffin List Report");
            }
            else if (model.ReportId == 15)
            {
                table = model.PersonalMonthlyAttendanceSummeryBuyerReprt(model.Fromdate, model.Todate, model.EmployeeId);
                GenerateReport(table, "Personal Monthly Attendance Summery");
            }
            else if (model.ReportId == 16)
            {
                table = model.OvertimeReport(model.Fromdate);
                GenerateReport(table, "Overtime Report");
            }
            else if (model.ReportId == 17)
            {
                table = model.TotalMonthlyAttendnanceReprt(model.Fromdate, model.Todate);
                GenerateReport(table, "Total Monthly Attendance Report With In Out and Overtime");
            }
            else if (model.ReportId ==18)
            {
                table = model.SectionWiseTiffinListSummery(model.Fromdate);
                GenerateReport(table, "Section Wise Summery Tiffin List Report");
            }
            else
            {
                return Content("No One");
            }
            GenerateReport(table, filename);
            return RedirectToAction("LeaveReport");
        }
        
        [HttpGet]
        public ActionResult PayrollReport()
        {
            ViewBag.ReportList = new SelectList(d.GetPayrollReportList(), "Value", "Text");
            ViewBag.PayrollList = new SelectList(d.GetPayrollList(), "Value", "Text");
            ViewBag.SectionList = new SelectList(d.GetSectionList(), "Value", "Text");
            ViewBag.TitleList = new SelectList(d.GetCardTitleList(), "Value", "Text");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PayrollReport(VM_Report model)
        {
            DataTable table = new DataTable();
            var filename = string.Empty;
            
            if (model.ReportId == 1)
            {
                if (model.SectionID > 0)
                {
                    filename = "Salary Sheet"; 
                    table = reportPayroll.GetExcelSalarySheet(model.SectionID, model.PayrollID);
                }
            }
            else if (model.ReportId == 7)
            {
                filename = "Summary Salary Sheet";
                table = reportPayroll.GetSalarySummarySheet(model.PayrollID);
            }
            else if (model.ReportId==2)
            {
                filename = "Salary and Extra OT Sheet";
                table = reportPayroll.GetMonthlyOTSalary(model.PayrollID,model.SectionID);
            }
            else if (model.ReportId == 3)
            {
                filename = "Night Payable Sheet";
                table = reportPayroll.GetNightPayable(model);
            }
            else if (model.ReportId == 4)
            {
                filename = "Salary Increment Report";
                table = reportPayroll.GetSalaryIncrement(model);
            }
            else if (model.ReportId == 5)
            {
                filename = "Night and Holiday Bill Report";
                table = reportPayroll.GetNightAndHDBill(model);
            }
            else if (model.ReportId == 6)
            {
                filename = "Extra OT Salary Payable";
                table = reportPayroll.GetExtraOTSalary(model);
            }
            else if (model.ReportId == 8)
            {
                filename = "Lefty Extra OT Salary Payable";
                table = reportPayroll.GetExtraOTForLeftSalary(model);
            }
            GenerateReport(table, filename);
            return RedirectToAction("PayrollReport");
        }

        [HttpGet]
        public ActionResult GenerateIDCard()
        {
            ViewBag.SectionList = new SelectList(d.GetSectionList(), "Value", "Text");
            ViewBag.DesignationList = new SelectList(d.GetDesignationList(), "Value", "Text");
            VMPersonalReport model = new VMPersonalReport();
            model.DataList = new List<VMPersonalReport>();
            model.Counter = 1;
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> GenerateIDCard(VMPersonalReport model)
        {
            ViewBag.SectionList = new SelectList(d.GetSectionList(), "Value", "Text");
            ViewBag.DesignationList = new SelectList(d.GetDesignationList(), "Value", "Text");
            await Task.Run(() => model.GetEmployeeListForID(model));
            model.Counter = 1;
            model.CardIssueDate = DateTime.Today;
            return View(model);
        }
        
        [HttpPost]
        public ActionResult GenerateCard(VMPersonalReport model)
        {
            DsEmployeeReport ds = new DsEmployeeReport();
            ds = GetGenerateIDCard(model);
            ReportClass cr = new ReportClass();
            cr.FileName = Server.MapPath("~/Views/Reports/CrpEmployeeID.rpt");
            cr.Load();
            cr.SetDataSource(ds);
            cr.SummaryInfo.ReportTitle = "IDCard";
            Stream stream = cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }

        #region PersonalReport

        private DataTable GetPresentEmployeeReport()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Serial", typeof(string));
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Card No", typeof(string));
            table.Columns.Add("Card Title", typeof(string));
            table.Columns.Add("Employee Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Line", typeof(string));
            table.Columns.Add("Gender", typeof(string));
            table.Columns.Add("Father/Husband Name", typeof(string));
            table.Columns.Add("Father/Husband", typeof(string));
            table.Columns.Add("Joining Date", typeof(string));
            table.Columns.Add("Date of Birth", typeof(string));

            var vData = (from o in db.Employees
                         join p in db.Designations on o.Designation_Id equals p.ID
                         join q in db.Sections on o.Section_Id equals q.ID
                         join r in db.Lines on o.LineId equals r.ID
                         where o.Present_Status == 1
                         select new
                         {
                             empID = o.ID,
                             mstatus = o.Marital_Status,
                             SectionName = q.SectionName,
                             CardNo = o.EmployeeIdentity,
                             CardTitle = o.CardTitle,
                             Name = o.Name,
                             DesignationName = p.Name,
                             Line = r.Line,
                             Gender = o.Gender,
                             FatherName = o.Father_Name,
                             JoinDate = o.Joining_Date,
                             BirthDate = o.DOB
                         }).OrderBy(a => a.SectionName).ThenBy(a => a.CardNo).ToList();
            int index = 0;
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    string FatherORHusbandName = string.Empty;
                    string FatherORHusband = string.Empty;
                    if (v.Gender == "Female" && !string.IsNullOrEmpty(v.Gender) && v.mstatus == "Married" && !string.IsNullOrEmpty(v.mstatus))
                    {
                        FatherORHusband = "H";
                        var getHusband = db.HrmsSpouses.Where(a => a.Employee_Id == v.empID && a.Status == true);
                        if (getHusband.Any())
                        {
                            FatherORHusbandName = getHusband.FirstOrDefault().SpouseName;
                        }
                    }
                    else
                    {
                        FatherORHusbandName = v.FatherName;
                        FatherORHusband = "F";
                    }


                    ++index;
                    table.Rows.Add(
                        index,
                        v.SectionName,
                        v.CardNo,
                        v.CardTitle,
                        v.Name,
                        v.DesignationName,
                        v.Line,
                        v.Gender,
                        FatherORHusbandName,
                        FatherORHusband,
                        Helpers.DateString(v.JoinDate),
                        Helpers.DateString(v.BirthDate)
                        );
                }
            }

            return table;
        }

        private DataTable GetEmployeeAddressReport01()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Serial", typeof(string));
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Card No", typeof(string));
            table.Columns.Add("Card Title", typeof(string));
            table.Columns.Add("Employee Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Father Name", typeof(string));
            table.Columns.Add("Gender", typeof(string));
            table.Columns.Add("Permanent Address", typeof(string));
            table.Columns.Add("Joining Date", typeof(string));
            table.Columns.Add("Mobile", typeof(string));
            table.Columns.Add("Birth Date", typeof(string));

            var vData = (from o in db.Employees
                         join p in db.Designations on o.Designation_Id equals p.ID
                         join q in db.Sections on o.Section_Id equals q.ID
                         where o.Present_Status == 1
                         select new
                         {
                             SectionName = q.SectionName,
                             CardNo = o.EmployeeIdentity,
                             CardTitle = o.CardTitle,
                             Name = o.Name,
                             DesignationName = p.Name,
                             FatherName = o.Father_Name,
                             Gender = o.Gender,
                             Address = o.Permanent_Address,
                             JoinDate = o.Joining_Date,
                             ModileNo = o.Mobile_No,
                             BirthDate = o.DOB
                         }).OrderBy(a => a.SectionName).ThenBy(a => a.CardNo).ToList();
            int index = 0;
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    ++index;
                    table.Rows.Add(
                        index,
                        v.SectionName,
                        v.CardNo,
                        v.CardTitle,
                        v.Name,
                        v.DesignationName,
                        v.FatherName,
                        v.Gender,
                        v.Address,
                        Helpers.DateString(v.JoinDate),
                        v.ModileNo,
                        Helpers.DateString(v.BirthDate)
                        );
                }
            }

            return table;
        }

        private DataTable GetEmployeeAddressReport02()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Serial", typeof(string));
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Card No", typeof(string));
            table.Columns.Add("Card Title", typeof(string));
            table.Columns.Add("Employee Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Present Address", typeof(string));
            table.Columns.Add("Permanent Address", typeof(string));
            table.Columns.Add("Phone", typeof(string));
            table.Columns.Add("Mobile", typeof(string));
            table.Columns.Add("Email", typeof(string));

            var vData = (from o in db.Employees
                         join p in db.Designations on o.Designation_Id equals p.ID
                         join q in db.Sections on o.Section_Id equals q.ID
                         where o.Present_Status == 1
                         select new
                         {
                             SectionName = q.SectionName,
                             CardNo = o.EmployeeIdentity,
                             CardTitle = o.CardTitle,
                             Name = o.Name,
                             DesignationName = p.Name,
                             PresentAddress = o.Present_Address,
                             PermanentAddress = o.Permanent_Address,
                             Phone = o.Photo,
                             ModileNo = o.Mobile_No,
                             Email = o.Email
                         }).OrderBy(a => a.SectionName).ThenBy(a => a.CardNo).ToList();
            int index = 0;
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    ++index;
                    table.Rows.Add(
                        index,
                        v.SectionName,
                        v.CardNo,
                        v.CardTitle,
                        v.Name,
                        v.DesignationName,
                        v.PresentAddress,
                        v.PermanentAddress,
                        v.Phone,
                        v.ModileNo,
                        v.Email
                        );
                }
            }

            return table;
        }

        private DataTable GetGenderReport()
        {
            DataTable table = new DataTable();

            table.Columns.Add("Serial", typeof(Int32));
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Card No", typeof(string));
            table.Columns.Add("Card Title", typeof(string));
            table.Columns.Add("Employee Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Gender", typeof(string));
            table.Columns.Add("Blood Group", typeof(string));
            table.Columns.Add("Present Address", typeof(string));
            table.Columns.Add("Permanent Address", typeof(string));


            var vData = (from o in db.Employees
                         join p in db.Designations on o.Designation_Id equals p.ID
                         join q in db.Sections on o.Section_Id equals q.ID
                         where o.Present_Status == 1
                         select new
                         {
                             SectionName = q.SectionName,
                             CardNo = o.EmployeeIdentity,
                             CardTitle = o.CardTitle,
                             Name = o.Name,
                             DesignationName = p.Name,
                             Gender = o.Gender,
                             PresentAddress = o.Present_Address,
                             PermanentAddress = o.Permanent_Address,
                             BloodGroup = o.Blood_Group
                         }).OrderBy(a => a.SectionName).ThenBy(a => a.CardNo).ToList();

            int index = 0;
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    ++index;
                    table.Rows.Add(
                        index,
                        v.SectionName,
                        v.CardNo,
                        v.CardTitle,
                        v.Name,
                        v.DesignationName,
                        v.Gender,
                        v.BloodGroup,
                        v.PresentAddress,
                        v.PermanentAddress
                        );
                }
            }

            return table;
        }

        private DataTable GetJoiningListReport(VM_Report model)
        {
            var vData = (from o in db.Employees
                         join p in db.Designations on o.Designation_Id equals p.ID
                         join q in db.Sections on o.Section_Id equals q.ID
                         where o.Joining_Date >= model.Fromdate && o.Joining_Date <= model.Todate && o.Present_Status==1
                         select new
                         {
                             empId = o.ID,
                             CardTitle = o.CardTitle,
                             CardNo = o.EmployeeIdentity,
                             PunchCardNo = o.CardNo,
                             Name = o.Name,
                             DesignationName = p.Name,
                             SectionName = q.SectionName,
                             Grade = o.Grade,
                             JoinDate = o.Joining_Date
                         }).OrderBy(a => a.SectionName).ThenBy(a => a.CardNo).ThenBy(a => a.JoinDate).ToList();

            DataTable table = new DataTable();
            table.Columns.Add("Serial", typeof(Int32));
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("CardNo", typeof(string));
            table.Columns.Add("Punch CardNo", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Grade", typeof(string));
            table.Columns.Add("JoinDate", typeof(string));
            table.Columns.Add("Joining Salary", typeof(string));
            table.Columns.Add("Current Salary", typeof(string));

            decimal joinBasicSalary = decimal.Zero;
            decimal currentBasicSalary = decimal.Zero;
            decimal joinSalary = decimal.Zero;
            decimal currentSalary = decimal.Zero;
            int index = 0;
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    joinBasicSalary = decimal.Zero;
                    currentBasicSalary = decimal.Zero;
                    joinSalary = decimal.Zero;
                    currentSalary = decimal.Zero;

                    var getSalary = db.HrmsEodRecords.Where(a => a.EmployeeId == v.empId && a.Eod_RefFk==1);
                    if (getSalary.Any())
                    {
                        joinBasicSalary = getSalary.OrderBy(a=>a.ID).FirstOrDefault().ActualAmount;
                        currentBasicSalary = getSalary.OrderByDescending(a => a.ID).FirstOrDefault().ActualAmount;
                        joinSalary = joinBasicSalary + (joinBasicSalary * (decimal)0.4) + 1100;
                        currentSalary = currentBasicSalary + (currentBasicSalary * (decimal)0.4) + 1100;
                    }
                    ++index;
                    table.Rows.Add(
                        index,
                        v.CardTitle,
                        v.CardNo,
                        v.PunchCardNo,
                        v.Name,
                        v.DesignationName,
                        v.SectionName,
                        v.Grade,
                        v.JoinDate,
                        Math.Round(joinSalary,0,MidpointRounding.AwayFromZero),
                        Math.Round(currentSalary,0,MidpointRounding.AwayFromZero)
                        );
                }
            }

            return table;
        }


        //Response.Clear();
        //Response.AddHeader("content-disposition","attachment;filename=Test.xls");   
        //Response.ContentType = "application/ms-excel";

        //Response.ContentEncoding = System.Text.Encoding.Unicode;
        //Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

        //System.IO.StringWriter sw = new System.IO.StringWriter();
        //System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);

        //FormView1.RenderControl(hw);

        //Response.Write(sw.ToString());
        //Response.End(); 

        private void GenerateReport(DataTable table, string filename)
        {
            string attachment = "attachment; filename=" + "" + filename + ".xls";


            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";

            Response.ContentEncoding = System.Text.Encoding.Unicode;
            Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

           // System.IO.StringWriter sw = new System.IO.StringWriter();
           // System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);


            string tab = "";

            foreach (DataColumn dc in table.Columns)
            {
                Response.Write(tab + dc.ColumnName);
                tab = "\t";
            }

            Response.Write("\n");
            int i;

            foreach (DataRow dr in table.Rows)
            {
                tab = "";
                for (i = 0; i < table.Columns.Count; i++)
                {
                    Response.Write(tab + dr[i].ToString());
                    tab = "\t";
                }
                Response.Write("\n");
            }

            Response.Flush();
            Response.End();
        }

        #endregion
        
        private void CalculateOthers(HRMS_Payroll model)
        {
            var tempDate = DateTime.Today;
            var activeEmployeeList = db.Employees.Where(a => a.Present_Status == 1).ToList();
            var allRef = db.HrmsEodReferences.Where(a => a.Finish == 31).OrderBy(a => a.priority).ToList();
            var AttendenceRecord = db.HrmsAttendanceHistory.Where(a => a.Date >= model.FromDate && a.Date <= model.ToDate).ToList();
            foreach (var emp in activeEmployeeList)
            {
                //Overtime 
                int TotalOverTime = 0;
                decimal OverTimeRate = 0;
                decimal OverTimeAmount = 0;
                decimal PerDayPay = 0;
                decimal AttendanceBonus = 0;
                decimal BasicPay = 0;
                decimal TotalDeduction = 0;
                decimal TotalPayableSalary = 0;
                decimal TotalSalaryPay = 0;
                decimal StampDeduction = 0;

                //----
                int TotalPresentDays = 0;
                int TotalLateDays = 0;
                int TotalAdsentDays = 0;
                int TotalLeaveDays = 0;
                int OffDays = 0;
                int HolyDays = 0;
                int TotalUnpaidLeave = 0;
                int TotalPaidLeave = 0;

                var empSalaryRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == emp.ID && a.Status == true).ToList();
                var employeeAttendenceRecord = AttendenceRecord.Where(a => a.Date >= model.FromDate && a.Date <= model.ToDate && a.EmployeeId == emp.ID);
                if (empSalaryRecord.Any())
                {
                    var OverTimeRate1 = empSalaryRecord.Where(a => a.Eod_RefFk == 11);
                    if (OverTimeRate1.Any())
                    {
                        OverTimeRate = OverTimeRate1.FirstOrDefault().ActualAmount;
                    }
                    //OverTimeRate = empSalaryRecord.Where(a => a.Eod_RefFk == 11).FirstOrDefault().ActualAmount;

                    var PerDayPay1 = empSalaryRecord.Where(a => a.Eod_RefFk == 1);
                    if (PerDayPay1.Any())
                    {
                        PerDayPay = PerDayPay1.FirstOrDefault().ActualAmount / 30;
                    }

                    //PerDayPay = empSalaryRecord.Where(a => a.Eod_RefFk == 1).FirstOrDefault().ActualAmount / 30;

                    var AttendanceBonus1 = empSalaryRecord.Where(a => a.Eod_RefFk == 7);
                    if (AttendanceBonus1.Any())
                    {
                        AttendanceBonus = AttendanceBonus1.FirstOrDefault().ActualAmount;
                    }
                    //AttendanceBonus = empSalaryRecord.Where(a => a.Eod_RefFk == 7).FirstOrDefault().ActualAmount;


                    BasicPay = empSalaryRecord.Where(a => a.Eod_RefFk == 1 || a.Eod_RefFk == 2 || a.Eod_RefFk == 3).Sum(a => a.ActualAmount);

                    var StampDeduction1 = empSalaryRecord.Where(a => a.Eod_RefFk == 12);
                    if (StampDeduction1.Any())
                    {
                        StampDeduction = StampDeduction1.FirstOrDefault().ActualAmount;
                    }
                    //StampDeduction = empSalaryRecord.Where(a => a.Eod_RefFk == 12).FirstOrDefault().ActualAmount;
                }

                if (employeeAttendenceRecord.Any())
                {
                    TotalOverTime = employeeAttendenceRecord.Sum(a => a.PayableOverTime).Value;


                    TotalPresentDays = employeeAttendenceRecord.Where(a => a.AttendanceStatus == 1).Count();
                    TotalLateDays = employeeAttendenceRecord.Where(a => a.AttendanceStatus == 2).Count();
                    TotalAdsentDays = employeeAttendenceRecord.Where(a => a.AttendanceStatus == 3).Count();
                    TotalLeaveDays = employeeAttendenceRecord.Where(a => a.AttendanceStatus == 4).Count();
                    OffDays = employeeAttendenceRecord.Where(a => a.AttendanceStatus == 5).Count();
                    HolyDays = employeeAttendenceRecord.Where(a => a.AttendanceStatus == 6).Count();
                    //int TotalMonthDays = DateTime.DaysInMonth(model.FromDate.Year, model.FromDate.Month);
                    //int TotalWorkDays = TotalMonthDays - OffDays - HolyDays;

                    //TotalUnpaidLeave = employeeAttendenceRecord.Where(a => a.LeavePaidType == 2).Count();
                    //TotalPaidLeave = employeeAttendenceRecord.Where(a => a.LeavePaidType == 1).Count();

                }
                OverTimeAmount = TotalOverTime * OverTimeRate;
                TotalDeduction = TotalAdsentDays * PerDayPay;
                TotalPayableSalary = BasicPay - TotalDeduction;

                TotalSalaryPay = (TotalPayableSalary + OverTimeAmount) - StampDeduction;

                List<EODReference> payrollInfolist = new List<EODReference>();

                foreach (var v in allRef)
                {
                    EODReference item = new EODReference();
                    item.EODRefID = v.ID;
                    item.SalaryHead = v.Name;
                    if (v.ID == 30)
                    {
                        item.Amount = BasicPay;
                        item.Note = "Total Salary";
                    }
                    else if (v.ID == 31)
                    {
                        item.Amount = TotalPresentDays;
                        item.Note = "Total Present Days";
                    }
                    else if (v.ID == 32)
                    {
                        item.Amount = OffDays + HolyDays;
                        item.Note = "Total OffDays and HolyDays";
                    }
                    else if (v.ID == 33)
                    {
                        item.Amount = TotalPaidLeave;
                        item.Note = "Total Leave Approved";
                    }
                    else if (v.ID == 34)
                    {
                        item.Amount = TotalAdsentDays;
                        item.Note = "Total Adsent Days";
                    }
                    else if (v.ID == 35)
                    {
                        item.Amount = TotalDeduction;
                        item.Note = "Total Deduction";
                    }
                    else if (v.ID == 28)
                    {
                        item.Amount = TotalOverTime;
                        item.Note = "Total OverTime Hours";
                    }
                    else if (v.ID == 11)
                    {
                        item.Amount = OverTimeRate;
                        item.Note = "OverTime Rate";
                    }
                    else if (v.ID == 39)
                    {
                        item.Amount = OverTimeAmount;
                        item.Note = "Total OverTime Value";
                    }
                    else if (v.ID == 7)
                    {
                        item.Amount = AttendanceBonus;
                        item.Note = "Attendance Bonus";
                    }
                    else if (v.ID == 36)
                    {
                        item.Amount = TotalPayableSalary;
                        item.Note = "Total Payable Salary";
                    }
                    else if (v.ID == 40)
                    {
                        item.Amount = TotalSalaryPay;
                        item.Note = "Total Payable Salary + OT";
                    }
                    payrollInfolist.Add(item);
                }

                foreach (var v in payrollInfolist)
                {
                    var updateInfo = db.HrmsPayrollInfos.Where(a=>a.PayrollFK==model.ID && a.EmployeeIdFk==v.EmpID && a.Eod_ReferenceFk==31);

                    if (v.EODRefID == 31)
                    {
                        var update = updateInfo.FirstOrDefault();
                        update.Amount = v.Amount;
                        
                    }
                    
                }
                db.SaveChanges();
            }
        }

        #region WillbeDeedCode
        private DataTable GetSalarySectionSheet(int SectionId, int PayrollId)
        {

            var vData = (from t1 in db.HrmsPayrolls
                         join t2 in db.HrmsPayrollInfos on t1.ID equals t2.PayrollFK
                         join a in db.Employees on t2.EmployeeIdFk equals a.ID
                         join b in db.Designations on a.Designation_Id equals b.ID
                         join c in db.Departments on a.Department_Id equals c.ID
                         join d in db.Sections on a.Section_Id equals d.ID
                         join e in db.Lines on a.LineId equals e.ID
                         join p in db.HrmsEodReferences on t2.Eod_ReferenceFk equals p.ID
                         where t2.Status == true
                         && t1.ID == PayrollId
                         && a.Section_Id == SectionId
                         select new
                         {
                             PayrollId = t1.ID,
                             PayMonth = t1.FromDate,
                             InfoID = t2.ID,
                             EmpID = a.ID,
                             name = a.Name_BN,
                             CardNo = a.EmployeeIdentity,
                             Grade = a.Grade,
                             Line = e.Line,
                             Designation = b.DesigNameBangla,
                             Department = c.DeptNameBangla,
                             Section = d.SectionNameBangla,
                             RefId = p.ID,
                             HeadName = p.Name,
                             Amount = t2.Amount,
                             IsAmountValue = p.IsHour,
                             Eod = p.Eod,
                             IsRegular = p.Regular,
                             Type = p.TypeBangla,
                             priority = p.priority

                         }).OrderBy(a => a.CardNo).ToList();

            var getGroup = (from o in vData
                            group o by new { o.EmpID, o.name, o.CardNo, o.Department, o.Section, o.Designation, o.Grade, o.PayMonth, o.Line } into all
                            select new
                            {
                                MonthName = all.Key.PayMonth,
                                EmpID = all.Key.EmpID,
                                Name = all.Key.name,
                                CardNo = all.Key.CardNo,
                                Depertment = all.Key.Department,
                                Section = all.Key.Section,
                                Designation = all.Key.Designation,
                                Grade = all.Key.Grade,
                                Line = all.Key.Line,
                                HeadItem = (from p in all
                                            where p.EmpID == all.Key.EmpID && p.PayrollId == PayrollId
                                            select new
                                            {
                                                InfoID = p.InfoID,
                                                RefId = p.RefId,
                                                HeadName = p.HeadName,
                                                Amount = p.Amount,
                                                IsAmountValue = p.IsAmountValue,
                                                Type = p.Type,
                                                priority = p.priority,
                                                IsDeductedEod = p.Eod,
                                                IsRegular = p.IsRegular,
                                            }).OrderBy(a => a.priority).ToList()
                            }).OrderBy(a => a.CardNo).ToList();
            DataTable ds = new DataTable();
            ds.Columns.Add("Serial", typeof(string));
            ds.Columns.Add("Name", typeof(string));
            ds.Columns.Add("Card No", typeof(string));
            ds.Columns.Add("Card Title", typeof(string));
            ds.Columns.Add("Designation", typeof(string));
            ds.Columns.Add("Section", typeof(string));
            ds.Columns.Add("Grade", typeof(string));
            ds.Columns.Add("Month", typeof(string));
            ds.Columns.Add("PaymentDate", typeof(string));
            ds.Columns.Add("Line", typeof(string));
            ds.Columns.Add("BasicPay", typeof(string));
            ds.Columns.Add("House", typeof(string));
            ds.Columns.Add("HealthFoodTravel", typeof(string));
            ds.Columns.Add("TotalSalary", typeof(string));
            ds.Columns.Add("WorkDays", typeof(string));
            ds.Columns.Add("HolyDays", typeof(string));
            ds.Columns.Add("PresentDays", typeof(string));
            ds.Columns.Add("LeaveApproved", typeof(string));
            ds.Columns.Add("AdsentDays", typeof(string));
            ds.Columns.Add("DeductionValue", typeof(string));
            ds.Columns.Add("AdjustSalary", typeof(string));
            ds.Columns.Add("OTHours", typeof(string));
            ds.Columns.Add("OTRate", typeof(string));
            ds.Columns.Add("OTValue", typeof(string));
            ds.Columns.Add("StampDeduction", typeof(string));
            ds.Columns.Add("PayableSalary", typeof(string));
            ds.Columns.Add("Trasport", typeof(string));
            ds.Columns.Add("Health", typeof(string));
            ds.Columns.Add("Food", typeof(string));
            ds.Rows.Clear();
            int index = 0;
            if (getGroup.Any())
            {
                int count = 0;
                foreach (var v in getGroup)
                {
                    ++index;
                    ds.Rows.Add(
                        index,
                        v.Name,
                        v.CardNo,
                        v.CardNo,
                        v.Designation,
                        v.Section,
                        v.Grade,
                        v.MonthName,
                        DateTime.Today,
                        v.Line,
                        0,//BasicPay10
                        0,//House11
                        0,//HealthFoodTravel/12
                        0,//TotalSalary13
                        0,//WorkDays14
                        0,//HolyDays15
                        0,//PresentDays16
                        0,//LeaveApproved17
                        0,//AdsentDays18
                        0,//DeductionValue19
                        0,//AdjustSalary20
                        0,//OTHours21
                        0,//OTRate22
                        0,//OTValue23
                        0,//StampDeduction24
                        0,//PayableSalary25
                        0,//Trasport26
                        0,//Health27
                        0//Food28
                        );
                    if (v.HeadItem.Any())
                    {
                        for (int i = 0; i < v.HeadItem.Count(); i++)
                        {
                            ds.Rows[count][i + 11] = v.HeadItem[i].Amount;
                        }
                    }
                    ++count;
                }
            }

            return ds;
        }

        private DataTable GetSalaryAllSheet(int PayrollId)
        {

            var vData = (from t1 in db.HrmsPayrolls
                         join t2 in db.HrmsPayrollInfos on t1.ID equals t2.PayrollFK
                         join a in db.Employees on t2.EmployeeIdFk equals a.ID
                         join b in db.Designations on a.Designation_Id equals b.ID
                         join c in db.Departments on a.Department_Id equals c.ID
                         join d in db.Sections on a.Section_Id equals d.ID
                         join e in db.Lines on a.LineId equals e.ID
                         join p in db.HrmsEodReferences on t2.Eod_ReferenceFk equals p.ID
                         where t2.Status == true
                         && t1.ID == PayrollId
                         //&& a.Section_Id == SectionId
                         select new
                         {
                             PayrollId = t1.ID,
                             PayMonth = t1.FromDate,
                             InfoID = t2.ID,
                             EmpID = a.ID,
                             name = a.Name_BN,
                             CardNo = a.EmployeeIdentity,
                             Grade = a.Grade,
                             Line = e.Line,
                             Designation = b.DesigNameBangla,
                             Department = c.DeptNameBangla,
                             Section = d.SectionNameBangla,
                             RefId = p.ID,
                             HeadName = p.Name,
                             Amount = t2.Amount,
                             IsAmountValue = p.IsHour,
                             Eod = p.Eod,
                             IsRegular = p.Regular,
                             Type = p.TypeBangla,
                             priority = p.priority

                         }).OrderBy(a => a.CardNo).ToList();

            var getGroup = (from o in vData
                            group o by new { o.EmpID, o.name, o.CardNo, o.Department, o.Section, o.Designation, o.Grade, o.PayMonth, o.Line } into all
                            select new
                            {
                                MonthName = all.Key.PayMonth,
                                EmpID = all.Key.EmpID,
                                Name = all.Key.name,
                                CardNo = all.Key.CardNo,
                                Depertment = all.Key.Department,
                                Section = all.Key.Section,
                                Designation = all.Key.Designation,
                                Grade = all.Key.Grade,
                                Line = all.Key.Line,
                                HeadItem = (from p in all
                                            where p.EmpID == all.Key.EmpID && p.PayrollId == PayrollId
                                            select new
                                            {
                                                InfoID = p.InfoID,
                                                RefId = p.RefId,
                                                HeadName = p.HeadName,
                                                Amount = p.Amount,
                                                IsAmountValue = p.IsAmountValue,
                                                Type = p.Type,
                                                priority = p.priority,
                                                IsDeductedEod = p.Eod,
                                                IsRegular = p.IsRegular,
                                            }).OrderBy(a => a.priority).ToList()
                            }).OrderBy(a => a.CardNo).ToList();
            DataTable ds = new DataTable();
            ds.Columns.Add("Serial", typeof(string));
            ds.Columns.Add("Name", typeof(string));
            ds.Columns.Add("Card No", typeof(string));
            ds.Columns.Add("Card Title", typeof(string));
            ds.Columns.Add("Designation", typeof(string));
            ds.Columns.Add("Section", typeof(string));
            ds.Columns.Add("Grade", typeof(string));
            ds.Columns.Add("Month", typeof(string));
            ds.Columns.Add("PaymentDate", typeof(string));
            ds.Columns.Add("Line", typeof(string));
            ds.Columns.Add("BasicPay", typeof(string));
            ds.Columns.Add("House", typeof(string));
            ds.Columns.Add("HealthFoodTravel", typeof(string));
            ds.Columns.Add("TotalSalary", typeof(string));
            ds.Columns.Add("WorkDays", typeof(string));
            ds.Columns.Add("HolyDays", typeof(string));
            ds.Columns.Add("PresentDays", typeof(string));
            ds.Columns.Add("LeaveApproved", typeof(string));
            ds.Columns.Add("AdsentDays", typeof(string));
            ds.Columns.Add("DeductionValue", typeof(string));
            ds.Columns.Add("AdjustSalary", typeof(string));
            ds.Columns.Add("OTHours", typeof(string));
            ds.Columns.Add("OTRate", typeof(string));
            ds.Columns.Add("OTValue", typeof(string));
            ds.Columns.Add("StampDeduction", typeof(string));
            ds.Columns.Add("PayableSalary", typeof(string));
            ds.Columns.Add("Trasport", typeof(string));
            ds.Columns.Add("Health", typeof(string));
            ds.Columns.Add("Food", typeof(string));
            ds.Rows.Clear();
            int index = 0;
            if (getGroup.Any())
            {
                int count = 0;
                foreach (var v in getGroup)
                {
                    ++index;
                    ds.Rows.Add(
                        index,
                        v.Name,
                        v.CardNo,
                        v.CardNo,
                        v.Designation,
                        v.Section,
                        v.Grade,
                        v.MonthName,
                        DateTime.Today,
                        v.Line,
                        0,//BasicPay10
                        0,//House11
                        0,//HealthFoodTravel/12
                        0,//TotalSalary13
                        0,//WorkDays14
                        0,//HolyDays15
                        0,//PresentDays16
                        0,//LeaveApproved17
                        0,//AdsentDays18
                        0,//DeductionValue19
                        0,//AdjustSalary20
                        0,//OTHours21
                        0,//OTRate22
                        0,//OTValue23
                        0,//StampDeduction24
                        0,//PayableSalary25
                        0,//Trasport26
                        0,//Health27
                        0//Food28
                        );
                    if (v.HeadItem.Any())
                    {
                        for (int i = 0; i < v.HeadItem.Count(); i++)
                        {
                            ds.Rows[count][i + 11] = v.HeadItem[i].Amount;
                        }
                    }
                    ++count;
                }
            }

            return ds;
        }
        #endregion

        #region EmployeeReport

        [HttpGet]
        public ActionResult StatusReport()
        {
            DateTime today = DateTime.Today;
            DateTime previousMonth = DateTime.Today.AddMonths(-1);
            ViewBag.StatusList = new SelectList(d.GetEmployeeStatus(), "Value", "Text");
            VMPersonalReport model = new VMPersonalReport();
            model.DataList = new List<VMPersonalReport>();
            model.Fromdate = new DateTime(previousMonth.Year, previousMonth.Month, 1);
            model.ToDate = new DateTime(previousMonth.Year, previousMonth.Month, DateTime.DaysInMonth(previousMonth.Year,previousMonth.Month));
            return View(model);
        }

        [HttpPost]
        public ActionResult StatusReport(VMPersonalReport model)
        {
            ViewBag.StatusList = new SelectList(d.GetEmployeeStatus(), "Value", "Text");
            if (model.StatusId == 1)
            {
                model.GetResign(model.Fromdate, model.ToDate);
            }
            else if (model.StatusId == 2)
            {
                model.GetLefty(model.Fromdate, model.ToDate);
            }
            else if (model.StatusId == 3)
            {
                model.GetTerminate(model.Fromdate, model.ToDate);
            }
            else if (model.StatusId == 4)
            {
                model.GetJoin(model.Fromdate, model.ToDate);
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult StatusDetails(VMPersonalReport model)
        {
            if (model.StatusId==2)
            {
                model.GetLefty(model.Fromdate,model.ToDate);
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Upload()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase[] files)
        {
            int Counter = 0;
            if (ModelState.IsValid)
            {    
                foreach (HttpPostedFileBase file in files)
                {
                    if (file != null)
                    {
                        var InputFileName = Path.GetFileName(file.FileName);
                        var ServerSavePath = Server.MapPath("~/Assets/Images/EmpImages/") + InputFileName;
                        //var ServerSavePath = Path.Combine(Server.MapPath("~/UploadedFiles/") + InputFileName);
                        //Save file to server folder  
                        //file.SaveAs(ServerSavePath);
                        string CardNo = file.FileName.Substring(0, file.FileName.LastIndexOf("."));
                        
                        var getEmployee = db.Employees.Where(a => a.EmployeeIdentity == CardNo);
                        if (getEmployee.Any())
                        {
                            file.SaveAs(ServerSavePath);
                            var update = getEmployee.FirstOrDefault();
                            update.Photo = InputFileName;
                            ++Counter;
                        }
                        
                        ViewBag.UploadStatus = Counter.ToString() +"of"+ files.Count().ToString() + " files uploaded successfully.";
                    }
                }
                db.SaveChanges();
            }
            
            return View();
        }
        #endregion
        
        public byte[] GetImage(string imgName)
        {
            byte[] data;
            
            string imgPath = string.Empty;
            string img = "Profile1.jpg";

            imgPath = Path.Combine(Server.MapPath("~/Assets/Images/EmpImages/"), imgName);

            if (System.IO.File.Exists(imgPath))
            {
                BitmapImage bitImg = new BitmapImage(new Uri(imgPath));
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bitImg));
                using (MemoryStream ms = new MemoryStream())
                {
                    encoder.Save(ms);
                    data = ms.ToArray();
                    return data;
                }
            }
            else
            {
                imgPath = Path.Combine(Server.MapPath("~/Assets/Images/EmpImages/"), img);
                BitmapImage bitImg = new BitmapImage(new Uri(imgPath));
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bitImg));
                using (MemoryStream ms = new MemoryStream())
                {
                    encoder.Save(ms);
                    data = ms.ToArray();
                    return data;
                }
            }
        }
        
        public byte[] GetSaveImage(string imgName,string ImagePath)
        {
            byte[] data;

            string imgPath = string.Empty;
            string img = "Profile1.jpg";

        // imgPath = Path.Combine(Server.MapPath("~/Assets/Images/EmpImages/"), imgName);
        
         imgPath = Path.Combine(Server.MapPath("E:/Romo Documents/Picture/Picture/RFCT"), imgName);

            if (System.IO.File.Exists(imgPath))
            {
                BitmapImage bitImg = new BitmapImage(new Uri(imgPath));
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bitImg));
                using (MemoryStream ms = new MemoryStream())
                {
                    encoder.Save(ms);
                    data = ms.ToArray();
                    return data;
                }
            }
            else
            {
                imgPath = Path.Combine(Server.MapPath("~/Assets/Images/EmpImages/"), img);
                BitmapImage bitImg = new BitmapImage(new Uri(imgPath));
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bitImg));
                using (MemoryStream ms = new MemoryStream())
                {
                    encoder.Save(ms);
                    data = ms.ToArray();
                    return data;
                }
            }
        }

        public DsEmployeeReport GetGenerateIDCard(VMPersonalReport model)
        {
            DsEmployeeReport ds = new DsEmployeeReport();
            DsEmployeeReport ds1 = new DsEmployeeReport();
            ds1.DtEmployeeIDCard.Rows.Clear();

            if (model.DataList.Any(a => a.IsChecked == true))
            {
                string imageName = string.Empty;
                //string imagePath = "~/Assets/Images/EmpImages/";
                string address = string.Empty;

                foreach (var v in model.DataList.Where(a => a.IsChecked == true))
                {
                    imageName = string.Empty;
                    ds.DtEmployeeIDCard.Rows.Clear();
                    var vData = from o in db.Employees
                                join p in db.Sections on o.Section_Id equals p.ID
                                join q in db.Designations on o.Designation_Id equals q.ID
                                where o.ID == v.EmployeeId
                                select new
                                {
                                    EmpID = o.ID,
                                    empName = o.Name_BN,
                                    CardNo = o.EmployeeIdentity,
                                    CardTitle = o.CardTitle,
                                    Designation = q.DesigNameBangla,
                                    Section = p.SectionName,
                                    Joindate = o.Joining_Date,
                                    NationalID = o.NID_No,
                                    EmergencyPhone = o.Emergency_Contact_No,
                                    BloodGroup = o.Blood_Group,
                                    PresentAddress = o.Present_Address,
                                    ImageName = o.Photo,
                                    perVillage = db.Village.Any(a => a.ID == o.PermanentVillage_ID)==true ? db.Village.FirstOrDefault(a => a.ID == o.PermanentVillage_ID).VillageName : "",
                                    perDistrict=db.District.Any(a=>a.ID==o.PermanentDistrict_ID)==true ? db.District.FirstOrDefault(a => a.ID == o.PermanentDistrict_ID).DistrictName: "",
                                    perPoliceStation=db.PoliceStation.Any(a=>a.ID==o.PermanentPoliceStation_ID)==true? db.PoliceStation.FirstOrDefault(a => a.ID == o.PermanentPoliceStation_ID).PoliceStationName : "",
                                    perPostOffice=db.PostOffice.Any(a=>a.ID==o.PermanentPostOffice_ID)==true? db.PostOffice.FirstOrDefault(a => a.ID == o.PermanentPostOffice_ID).PostOfficeName:""
                                };

                    if (vData.Any())
                    {
                        var takeSingle = vData.FirstOrDefault();

                        address = takeSingle.perVillage + ", " + takeSingle.perPoliceStation + ", " + takeSingle.perPostOffice + ", " + takeSingle.perDistrict;
                        

                        byte[] imageData;
                        imageName = takeSingle.ImageName;
                        if (string.IsNullOrEmpty(imageName))
                        {
                            imageData = GetImage("demo.jpg");
                        }
                        else
                        {
                            imageData = GetImage(imageName);
                        }
                        
                        ds.DtEmployeeIDCard.Rows.Add(
                            takeSingle.EmpID,
                            takeSingle.CardTitle + " " + takeSingle.CardNo,
                            takeSingle.CardTitle,
                            takeSingle.Designation,
                            takeSingle.Section,
                            model.CardIssueDate,
                            takeSingle.Joindate,
                            takeSingle.NationalID,
                            takeSingle.EmergencyPhone,
                            takeSingle.BloodGroup,
                            //Helpers.ShortDateString(DateTime.Today.AddYears(1)),
                            string.Empty,
                            address,
                            imageData,
                            takeSingle.empName
                            );

                        if (model.Counter >= 1)
                        {
                            for (int take = 1; take <= model.Counter; take++)
                            {
                                ds1.DtEmployeeIDCard.ImportRow(ds.DtEmployeeIDCard.Rows[0]);
                            }
                        }
                    }
                }
            }
            return ds1;
        }
    }
}
