﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Oss.Hrms.Models;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Controllers
{
    public class ConnectController : Controller
    {
        CurrentUser user;
        public ConnectController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
            this.user = sessionHandler.CurrentUser;
        }

        // GET: Connect
        HrmsContext db = new HrmsContext();
        public async Task<ActionResult> Index()
        {

            using (var webClient = new System.Net.WebClient())
            {
                try
                {
                    var lastSourceID = db.CheckInOut.Select(x => x.SourceId).Max();
                    var json = webClient.DownloadString("http://49.0.35.196/TempHRM/HRM/GetData/" + lastSourceID);
                    JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                    List<CheckInOut> checkInOut = jsonSerializer.Deserialize<List<CheckInOut>>(json);

                    foreach (var v in checkInOut)
                    {
                        v.SourceId = v.ID;
                        v.CheckTime = v.CheckTime.AddHours(6);
                        db.CheckInOut.Add(v);

                    }
                    db.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                }
            }
            return RedirectToAction("Index", "EmployeeRecord");
           // return RedirectToAction("Index", "EmployeeRecord");

        }
    }
}
