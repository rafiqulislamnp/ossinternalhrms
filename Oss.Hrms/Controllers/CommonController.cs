﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Oss.Hrms.Models;
using Oss.Hrms.Models.Entity.Common;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;
using Oss.Hrms.Models.ViewModels.Attendance;
using Oss.Hrms.Models.ViewModels.HRMS;

namespace Oss.Hrms.Controllers
{
    public class CommonController : Controller
    {
        // private HrmsContext db = new HrmsContext();
        HrmsContext db = new HrmsContext();
        CurrentUser user;
        public CommonController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
            this.user = sessionHandler.CurrentUser;
        }

        // Below Action used to view HRMS Home Page
        public ActionResult Index()
        {
            return View();
        }

        #region Line Number

        public async Task<ActionResult> IndexLine()
        {
            try
            {
                VMLine vmLine = new VMLine();
                await Task.Run(() => vmLine.GetLineNos());
                return View(vmLine);
            }
            catch (Exception ex)
            {

                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("IndexLine", "Common");
            }
        }

        /// <summary>
        /// Load Form for Adding Lines.... 
        /// </summary>
        /// <returns></returns>
        public ActionResult AddLine()
        {
            try
            {
                VMLine vmLine = new VMLine();
                return View(vmLine);
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("IndexLine", "Common");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddLine(VMLine vmLine)
        {
            try
            {
                LineNo ln = new LineNo()
                {
                    Line = vmLine.Line,
                    Entry_By = "1",
                    Entry_Date = DateTime.Now
                };
                db.Lines.Add(ln);
                await db.SaveChangesAsync();

                return RedirectToAction("IndexLine", "Common");
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("IndexLine", "Common");
            }
        }


        /// <summary>
        /// Delete Lines-----
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> DeleteLine(int id)
        {
            if (id != 0)
            {
                try
                {
                    LineNo ln = db.Lines.Find(id);
                    if (ln != null)
                    {
                        db.Lines.Remove(ln);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Deleted.";
                        return RedirectToAction("IndexLine", "Common");
                    }
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Not Deleted.";
                    return RedirectToAction("IndexLine", "Common");
                }
                catch (Exception e)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Unable to Delete" + e.Message;
                    return RedirectToAction("IndexLine", "Common");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "No Id found to delete";
            return RedirectToAction("IndexLine", "Common");
        }

        /// <summary>
        /// Load form with data-----for editing information
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> EditLine(int id)
        {
            if (id != 0)
            {
                try
                {
                    var lines = db.Lines.FirstOrDefault(x => x.ID == id);
                    VMLine vln = new VMLine()
                    {
                        ID = lines.ID,
                        Line = lines.Line
                    };
                    return View(vln);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    throw;
                }
            }
            return RedirectToAction("IndexLine", "Common");
        }

        /// <summary>
        /// Submit edited data----
        /// </summary>
        /// <param name="vmLine"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> EditLine(VMLine vmLine)
        {
            try
            {
                LineNo ln = new LineNo()
                {
                    ID = vmLine.ID,
                    Line = vmLine.Line,
                    Update_By = "1",
                    Update_Date = DateTime.Now
                };
                db.Entry(ln).State = EntityState.Modified;
                await db.SaveChangesAsync();
                Session["success_div"] = "true";
                Session["success_msg"] = "Updated.";
                return RedirectToAction("IndexLine", "Common");
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("IndexLine", "Common");
            }
        }
        #endregion

        #region Country


        //[HttpGet]
        //public async Task<ActionResult> ViewCountryList()
        //{

        //    if (Session["Role"] != null)
        //    {
        //        try
        //        {
        //            DropDownData ddd = new DropDownData();
        //            ViewBag.CountryListall = new SelectList(ddd.GetCountrylistAll(), "Text", "Value");
        //            VMCountry model = new VMCountry();
        //            model.DataList = from t1 in db.Countries
        //                             select new VMCountry
        //                             {
        //                                 ID = t1.ID,
        //                                 Name = t1.Name,
        //                             };
        //            return View(model);
        //        }
        //        catch (Exception ex)
        //        {

        //            Session["warning_div"] = "true";
        //            Session["warning_msg"] = "Error Happened." + ex.Message;
        //            return RedirectToAction("Index", "HRMS");
        //        }
        //    }
        //    Session["warning_div"] = "true";
        //    Session["warning_msg"] = "Please Login to continue..!";
        //    return RedirectToAction("Login", "Account");
        //}


        //// Below Action used to Add Country Information
        //// Database: CrimsonERP
        //// Table Name : Country
        //// Domain Model Name: Country
        //// View Model Name: VMCountry
        //// Developed by Anis
        //// Date: 2018-03-15

        //[HttpGet]
        //public async Task<ActionResult> AddCountry()
        //{

        //    if (Session["Role"] != null)
        //    {
        //        DropDownData ddd = new DropDownData();
        //        ViewBag.CountryListall = new SelectList(ddd.GetCountrylistAll(), "Value", "Text");
        //        VMCountry model = new VMCountry();
        //        model.DataList = from t1 in db.Countries
        //                         select new VMCountry
        //                         {
        //                             Name = t1.Name,

        //                         };
        //        return View(model);
        //    }
        //    Session["warning_div"] = "true";
        //    Session["warning_msg"] = "Please Login to continue..!";
        //    return RedirectToAction("Login", "Account");
        //}


        //// Below Action used to Add Country Information
        //// Database: CrimsonERP
        //// Table Name : Country
        //// Domain Model Name: Country
        //// View Model Name: VMCountry
        //// Developed by Anis
        //// Date: 2018-03-15

        //[HttpPost]
        //public async Task<ActionResult> AddCountry(VMCountry model)
        //{

        //    if (Session["Role"] != null)
        //    {

        //        try
        //        {
        //            Country country = new Country { Name = model.Name, Status = model.Status, Entry_By = Session["EmpID"].ToString(), Entry_Date = DateTime.Now };
        //            db.Countries.Add(country);
        //            await db.SaveChangesAsync();

        //            Session["success_div"] = "true";
        //            Session["success_msg"] = "Country Added Successfully.";
        //            return RedirectToAction("ViewCountryList", "HRMS");
        //        }
        //        catch (Exception ex)
        //        {
        //            if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
        //            {
        //                Session["warning_div"] = "true";
        //                Session["warning_msg"] = "Country Can't Add, It is Already Exist!";
        //                return RedirectToAction("ViewCountryList", "HRMS");
        //            }
        //            Session["warning_div"] = "true";
        //            Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
        //            return RedirectToAction("ViewCountryList", "HRMS");
        //        }
        //    }
        //    Session["warning_div"] = "true";
        //    Session["warning_msg"] = "Please Login to continue..!";
        //    return RedirectToAction("Login", "Account");
        //}


        //// Below Action used to Delete Country Information
        //// Database: CrimsonERP
        //// Table Name : Country
        //// Domain Model Name: Country
        //// View Model Name: VMCountry
        //// Developed by Anis
        //// Date: 2018-03-15

        //[HttpGet]
        //public async Task<ActionResult> DeleteCountry(int? id)
        //{

        //    if (Session["Role"] != null)
        //    {
        //        if (id == null)
        //        {
        //            //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //            Session["warning_div"] = "true";
        //            Session["warning_msg"] = "Country ID Not Found!";
        //            return RedirectToAction("ViewCountryList", "HRMS");
        //        }
        //        Country country = await db.Countries.FindAsync(id);
        //        if (country != null)
        //        {
        //            try
        //            {
        //                db.Countries.Remove(country);
        //                await db.SaveChangesAsync();
        //                Session["success_div"] = "true";
        //                Session["success_msg"] = "Country Deleted Successfully.";
        //                return RedirectToAction("ViewCountryList", "HRMS");
        //            }
        //            catch (Exception ex)
        //            {
        //                if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
        //                {
        //                    Session["warning_div"] = "true";
        //                    Session["warning_msg"] = "Country Can't Delete, It is Already in Use!";
        //                    return RedirectToAction("ViewCountryList", "HRMS");
        //                }
        //                Session["warning_div"] = "true";
        //                Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
        //                return RedirectToAction("ViewCountryList", "HRMS");
        //            }
        //            //return HttpNotFound();
        //        }
        //        Session["warning_div"] = "true";
        //        Session["warning_msg"] = "Something Went Wrong!";
        //        return RedirectToAction("ViewCountryList", "HRMS");
        //    }
        //    Session["warning_div"] = "true";
        //    Session["warning_msg"] = "Please Login to continue..!";
        //    return RedirectToAction("Login", "Account");
        //}

        //// Below Action used to Edit Country Information
        //// Database: CrimsonERP
        //// Table Name : Country
        //// Domain Model Name: Country
        //// View Model Name: VMCountry
        //// Developed by Anis
        //// Date: 2018-03-15
        //public async Task<ActionResult> EditCountry(int? id)
        //{
        //    if (Session["Role"] != null)
        //    {
        //        if (id != null)
        //        {
        //            DropDownData ddd = new DropDownData();
        //            ViewBag.CountryListall = new SelectList(ddd.GetCountrylistAll(), "Value", "Text");
        //            var a = db.Countries.FirstOrDefault(x => x.ID == id);
        //            VMCountry model = new VMCountry
        //            {
        //                ID = a.ID,
        //                Name = a.Name
        //            };
        //            return View(model);
        //        }
        //        Session["warning_div"] = "true";
        //        Session["warning_msg"] = "Country Not Found!";
        //        return RedirectToAction("ViewCountryList", "HRMS");
        //    }
        //    Session["warning_div"] = "true";
        //    Session["warning_msg"] = "Please Login to continue..!";
        //    return RedirectToAction("Login", "Account");
        //}

        //// Below Action used to Edit Country Information
        //// Database: CrimsonERP
        //// Table Name : Country
        //// Domain Model Name: Country
        //// View Model Name: VMCountry
        //// Developed by Anis
        //// Date: 2018-03-15
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> EditCountry(VMCountry model)
        //{
        //    if (Session["Role"] != null)
        //    {
        //        try
        //        {
        //            Country country = new Country { Name = model.Name, ID = model.ID };
        //            db.Entry(country).State = System.Data.Entity.EntityState.Modified;
        //            await db.SaveChangesAsync();
        //            Session["success_div"] = "true";
        //            Session["success_msg"] = "Country Updated Successfully.";
        //            return RedirectToAction("ViewCountryList", "HRMS");
        //        }
        //        catch (Exception ex)
        //        {
        //            Session["warning_div"] = "true";
        //            Session["warning_msg"] = "Error Happened." + ex.Message;
        //            return RedirectToAction("ViewCountryList", "HRMS");
        //        }
        //    }
        //    Session["warning_div"] = "true";
        //    Session["warning_msg"] = "Please Login to continue..!";
        //    return RedirectToAction("Login", "Account");
        //}




        #endregion

        #region Business Unit


        [HttpGet]
        public async Task<ActionResult> ViewBusinessUnitList()
        {
            {
                try
                {
                    BusinessUnit businessUnit = new BusinessUnit();
                    Country country = new Country();
                    VMBusinessUnit model = new VMBusinessUnit();
                    model.DataList = (from t1 in db.BusinessUnits
                                      join c in db.Countries on t1.Country_ID equals c.ID
                                      where t1.Status == true
                                      select new VMBusinessUnit
                                      {
                                          ID = t1.ID,
                                          Country_Name = c.Name,
                                          Name = t1.Name
                                      }).AsEnumerable();

                    return View(model);
                }
                catch (Exception ex)
                {

                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewBusinessUnitList", "Common");
                }
            }
        }


        // Below Action used to Add Business Unit Information
        // Database: CrimsonERP
        // Table Name : BusinessUnit
        // Domain Model Name: BusinessUnit
        // View Model Name: VMBusinessUnit
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-15
        [HttpGet]
        public async Task<ActionResult> AddBusinessUnit()
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    DropDownData ddd = new DropDownData();
                    ViewBag.CountryList = new SelectList(ddd.GetCountryList(), "Value", "Text");
                    VMBusinessUnit model = new VMBusinessUnit();
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewBusinessUnitList", "Common");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }



        // Below Action used to Add Business Unit Information
        // Database: CrimsonERP
        // Table Name : BusinessUnit
        // Domain Model Name: BusinessUnit
        // View Model Name: VMBusinessUnit
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-15
        [HttpPost]
        public async Task<ActionResult> AddBusinessUnit(VMBusinessUnit model)
        {

            //if (Session["Role"] != null)
            {
                try
                {
                    BusinessUnit businessUnit = new BusinessUnit
                    {
                        Name = model.Name,
                        Country_ID = model.Country_SL,
                        Status = model.Status,
                        Entry_By = "1",
                        Entry_Date = DateTime.Now
                    };
                    db.BusinessUnits.Add(businessUnit);
                    await db.SaveChangesAsync();

                    Session["success_div"] = "true";
                    Session["success_msg"] = "Business Unit Added Successfully.";
                    return RedirectToAction("ViewBusinessUnitList", "Common");
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Business Unit Can't Add, It is Already Exist!";
                        return RedirectToAction("ViewBusinessUnitList", "Common");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ViewBusinessUnitList", "Common");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }



        // Below Action used to Edit Business Unit Information
        // Database: CrimsonERP
        // Table Name : BusinessUnit
        // Domain Model Name: BusinessUnit
        // View Model Name: VMBusinessUnit
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-15
        public async Task<ActionResult> EditBusinessUnit(int? id)
        {
            //if (Session["Role"] != null)
            {
                if (id != null)
                {
                    try
                    {
                        DropDownData ddd = new DropDownData();
                        ViewBag.CountryList = new SelectList(ddd.GetCountryList(), "Value", "Text");
                        VMBusinessUnit model = (from t1 in db.BusinessUnits
                                                join c in db.Countries on t1.Country_ID equals c.ID
                                                where t1.ID == id
                                                select new VMBusinessUnit
                                                {
                                                    ID = t1.ID,
                                                    Country_Name = c.Name,
                                                    Name = t1.Name
                                                }).FirstOrDefault();
                        return View(model);
                    }
                    catch (Exception ex)
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Error Happened. " + ex.Message;
                        return RedirectToAction("ViewBusinessUnitList", "Common");
                    }
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Business Unit Not Found!";
                return RedirectToAction("ViewBusinessUnitList", "Common");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        // Below Action used to Edit Business Unit Information
        // Database: CrimsonERP
        // Table Name : BusinessUnit
        // Domain Model Name: BusinessUnit
        // View Model Name: VMBusinessUnit
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-15
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditBusinessUnit(VMBusinessUnit model)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    BusinessUnit businessUnit = new BusinessUnit
                    {
                        Name = model.Name,
                        Country_ID = model.Country_SL,
                        ID = model.ID
                    };
                    db.Entry(businessUnit).State = System.Data.Entity.EntityState.Modified;
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Business Unit Updated Successfully.";
                    return RedirectToAction("ViewBusinessUnitList", "Common");
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewBusinessUnitList", "Common");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }



        // Below Action used to Delete Business Unit Information
        // Database: CrimsonERP
        // Table Name : BusinessUnit
        // Domain Model Name: BusinessUnit
        // View Model Name: VMBusinessUnit
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-15
        [HttpGet]
        public async Task<ActionResult> DeleteBusinessUnit(int? id)
        {

            //if (Session["Role"] != null)
            {
                if (id == null)
                {
                    //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Business Unit Not Found!";
                    return RedirectToAction("ViewBusinessUnitList", "Common");
                }
                BusinessUnit businessUnit = await db.BusinessUnits.FindAsync(id);
                if (businessUnit != null)
                {
                    try
                    {
                        db.BusinessUnits.Remove(businessUnit);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Business Unit Deleted Successfully.";
                        return RedirectToAction("ViewBusinessUnitList", "Common");
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Business Unit Can't Delete, It is Already in Use!";
                            return RedirectToAction("ViewBusinessUnitList", "Common");
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("ViewBusinessUnitList", "Common");
                    }
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("ViewBusinessUnitList", "Common");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }



        #endregion

        #region Designation

        // Below Action used to View Designation Information
        // Database: CrimsonERP
        // Table Name : Designations
        // Domain Model Name: Designation
        // View Model Name: VMDesignation

        [HttpGet]
        public async Task<ActionResult> ViewDesignationList()
        {

            //if (Session["Role"] != null)
            {
                try
                {
                    VMDesignation model = new VMDesignation();
                    model.DataList = (from t1 in db.Designations
                                      select new VMDesignation
                                      {
                                          ID = t1.ID,
                                          Name = t1.Name,
                                          DesigNameBangla = t1.DesigNameBangla,
                                      }).AsEnumerable();
                    return await Task.Run(() => View(model));
                }
                catch (Exception ex)
                {

                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("Index", "Common");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        // Below Action used to Add Designation Information
        // Database: CrimsonERP
        // Table Name : Designations
        // Domain Model Name: Designation
        // View Model Name: VMDesignation
        [HttpGet]
        public ActionResult AddDesignation()
        {

            //if (Session["Role"] != null)
            {
                try
                {
                    VMDesignation model = new VMDesignation();
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewDesignationList", "Common");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        // Below Action used to Add Designation Information
        // Database: CrimsonERP
        // Table Name : Designations
        // Domain Model Name: Designation
        // View Model Name: VMDesignation
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-15
        [HttpPost]
        public async Task<ActionResult> AddDesignation(VMDesignation model)
        {
            //if (Session["Role"] != null)
            {

                try
                {
                    Designation designation = new Designation
                    {
                        Name = model.Name,
                        DesigNameBangla = model.DesigNameBangla,
                        Status = model.Status,
                        Entry_By = "1",
                        Entry_Date = DateTime.Now
                    };
                    db.Designations.Add(designation);
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Designation Added Successfully.";
                    return RedirectToAction("ViewDesignationList", "Common");
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Designation Can't Add. It is Already in Exist!";
                        return RedirectToAction("ViewDesignationList", "Common");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewDesignationList", "Common");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to Add Designation Information
        // Database: CrimsonERP
        // Table Name : Designations
        // Domain Model Name: Designation
        // View Model Name: VMDesignation
        [HttpGet]
        public async Task<ActionResult> DeleteDesignation(int? id)
        {

            //if (Session["Role"] != null)
            {
                if (id == null)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Designation ID Not Found!";
                    return RedirectToAction("ViewDesignationList", "Common");
                }
                Designation designation = await db.Designations.FindAsync(id);
                if (designation != null)
                {
                    try
                    {
                        db.Designations.Remove(designation);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Designation Deleted Successfully.";
                        return RedirectToAction("ViewDesignationList", "Common");
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Designation Can't Delete, It is Already in Use!";
                            return RedirectToAction("ViewDesignationList", "Common");
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("ViewDesignationList", "Common");
                    }
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("ViewDesignationList", "Common");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }





        // Below Action used to Edit Designation Information
        // Database: CrimsonERP
        // Table Name : Designations
        // Domain Model Name: Designation
        // View Model Name: VMDesignation
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-15
        public async Task<ActionResult> EditDesignation(int? id)
        {
            // if (Session["Role"] != null)
            {
                if (id != null)
                {
                    var a = db.Designations.FirstOrDefault(x => x.ID == id);
                    VMDesignation model = new VMDesignation
                    {
                        ID = a.ID,
                        Name = a.Name,
                        DesigNameBangla = a.DesigNameBangla
                    };
                    return View(model);
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Designation Not Found!";
                return RedirectToAction("ViewDesignationList", "Common");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        // Below Action used to Edit Designation Information
        // Database: CrimsonERP
        // Table Name : Designations
        // Domain Model Name: Designation
        // View Model Name: VMDesignation
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-15
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditDesignation(VMDesignation model)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    Designation designation = new Designation
                    {
                        Name = model.Name,
                        DesigNameBangla = model.DesigNameBangla,
                        ID = model.ID
                    };
                    db.Entry(designation).State = System.Data.Entity.EntityState.Modified;
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Designation Updated Successfully.";
                    return RedirectToAction("ViewDesignationList", "Common");
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewDesignationList", "Common");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }





        #endregion

        #region   Unit


        //// Below Action used to View Unit Information
        //// Database: CrimsonERP
        //// Table Name : Units
        //// Domain Model Name: Unit
        //// View Model Name: VMUnit

        public async Task<ActionResult> ViewUnit()
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    VMUnit model = new VMUnit();
                    model.DataListUnit = (from u in db.Units
                                          join b in db.BusinessUnits on u.BusinessUnit_ID equals b.ID
                                          select new VMUnit
                                          {
                                              ID = u.ID,
                                              UnitName = u.UnitName,
                                              BusinessUnit_Name = b.Name
                                          }).AsEnumerable();
                    return await Task.Run(() => View(model));
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened. " + ex.Message;
                    return RedirectToAction("Index", "Common");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to Add Unit Information
        // Database: CrimsonERP
        // Table Name : Units
        // Domain Model Name: Unit
        // View Model Name: VMUnit
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-15

        [HttpGet]
        public ActionResult AddUnit()
        {

            // if (Session["Role"] != null)
            {
                DropDownData ddd = new DropDownData();
                ViewBag.BusinessUnitall = new SelectList(ddd.GetBusinessUnitList(), "Value", "Text");
                VMUnit model = new VMUnit();
                return View(model);
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to Add Unit Information
        // Database: CrimsonERP
        // Table Name : Units
        // Domain Model Name: Unit
        // View Model Name: VMUnit
        [HttpPost]
        public async Task<ActionResult> AddUnit(VMUnit model)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    Unit unit = new Unit
                    {
                        UnitName = model.UnitName,
                        BusinessUnit_ID = model.BusinessUnit_SL,
                        Entry_By = "1",
                    };
                    db.Units.Add(unit);
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Unit Added Successfully.";
                    return RedirectToAction("ViewUnit", "Common");
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Unit Can't Add, It is Already Exist!";
                        return RedirectToAction("ViewUnit", "Common");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ViewUnit", "Common");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        // Below Action used to Delete Unit Information
        // Database: CrimsonERP
        // Table Name : Units
        // Domain Model Name: Unit
        // View Model Name: VMUnit
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-15
        [HttpGet]
        public async Task<ActionResult> DeleteUnit(int? id)
        {

            //if (Session["Role"] != null)
            {
                if (id == null)
                {
                    //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Business Unit Not Found!";
                    return RedirectToAction("ViewUnit", "Common");
                }
                Unit unit = await db.Units.FindAsync(id);
                if (unit != null)
                {
                    try
                    {
                        db.Units.Remove(unit);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Unit Deleted Successfully.";
                        return RedirectToAction("ViewUnit", "Common");
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Unit Can't Delete, It is Already in Use!";
                            return RedirectToAction("ViewUnit", "Common");
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("ViewUnit", "Common");
                    }
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("ViewUnit", "Common");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }



        // Below Action used to Edit Unit Information
        // Database: CrimsonERP
        // Table Name : Units
        // Domain Model Name: Unit
        // View Model Name: VMUnit
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-15
        public async Task<ActionResult> EditUnit(int? id)
        {
            //if (Session["Role"] != null)
            {
                if (id != null)
                {
                    try
                    {
                        DropDownData ddd = new DropDownData();
                        ViewBag.BusinessUnitall = new SelectList(ddd.GetBusinessUnitList(), "Value", "Text");
                        VMUnit model = (from u in db.Units
                                        join b in db.BusinessUnits on u.BusinessUnit_ID equals b.ID
                                        where u.ID == id
                                        select new VMUnit
                                        {
                                            ID = u.ID,
                                            UnitName = u.UnitName,
                                            BusinessUnit_SL = b.ID
                                        }).FirstOrDefault();
                        return await Task.Run(() => View(model));
                    }
                    catch (Exception ex)
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Error Happened. " + ex.Message;
                        return RedirectToAction("ViewUnit", "Common");
                    }

                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Unit ID Not Found!";
                return RedirectToAction("ViewUnit", "Common");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to Edit Unit Information
        // Database: CrimsonERP
        // Table Name : Units
        // Domain Model Name: Unit
        // View Model Name: VMUnit
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-15
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditUnit(VMUnit model)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    Unit unit = new Unit
                    {
                        UnitName = model.UnitName,
                        BusinessUnit_ID = model.BusinessUnit_SL,
                        ID = model.ID
                    };
                    db.Entry(unit).State = System.Data.Entity.EntityState.Modified;
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Unit Updated Successfully.";
                    return RedirectToAction("ViewUnit", "Common");
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewUnit", "Common");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        #endregion

        #region   Department

        // Below Action used to View Department Information
        // Database: CrimsonERP
        // Table Name : Departments
        // Domain Model Name: Department
        // View Model Name: VMDepartment
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-15

        public async Task<ActionResult> ViewDepartment()
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    VMDepartment model = new VMDepartment();
                    model.DatalistDepartments = (from d in db.Departments
                                                 join u in db.Units on d.Unit_ID equals u.ID
                                                 select new VMDepartment()
                                                 {
                                                     ID = d.ID,
                                                     DeptName = d.DeptName,
                                                     DeptNameBangla = d.DeptNameBangla,
                                                     Unit_Name = u.UnitName
                                                 }).AsEnumerable();
                    return await Task.Run(() => View(model));
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("index", "Common");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        // Below Action used to Add Department Information
        // Database: CrimsonERP
        // Table Name : Departments
        // Domain Model Name: Department
        // View Model Name: VMDepartment

        [HttpGet]
        public ActionResult AddDepartment()
        {

            //if (Session["Role"] != null)
            {
                DropDownData ddd = new DropDownData();
                ViewBag.Unitall = new SelectList(ddd.GetUnitList(), "Value", "Text");
                VMDepartment model = new VMDepartment();
                return View(model);
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }



        // Below Action used to View Department Information
        // Database: CrimsonERP
        // Table Name : Departments
        // Domain Model Name: Department
        // View Model Name: VMDepartment

        [HttpPost]
        public async Task<ActionResult> AddDepartment(VMDepartment model)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    Department department = new Department
                    {
                        DeptName = model.DeptName,
                        DeptNameBangla = model.DeptNameBangla,
                        Unit_ID = model.Unit_SL,
                        Entry_By = "1",
                        Entry_Date = DateTime.Now
                    };
                    db.Departments.Add(department);
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Department Added Successfully.";
                    return RedirectToAction("ViewDepartment", "Common");
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Department Can't Add, It is Already Exist!";
                        return RedirectToAction("ViewDepartment", "Common");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ViewDepartment", "Common");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }



        // Below Action used to Delete Department Information
        // Database: CrimsonERP
        // Table Name : Departments
        // Domain Model Name: Department
        // View Model Name: VMDepartment
        [HttpGet]
        public async Task<ActionResult> DeleteDepartment(int? id)
        {

            // if (Session["Role"] != null)
            {
                if (id == null)
                {
                    //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Department Not Found!";
                    return RedirectToAction("ViewDepartment", "Common");
                }
                Department department = await db.Departments.FindAsync(id);
                if (department != null)
                {
                    try
                    {
                        db.Departments.Remove(department);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Department Deleted Successfully.";
                        return RedirectToAction("ViewDepartment", "Common");
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Department Can't Delete, It is Already Use!";
                            return RedirectToAction("ViewDepartment", "Common");
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("ViewDepartment", "Common");
                    }
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("ViewDepartment", "Common");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        // Below Action used to Edit Department Information
        // Database: CrimsonERP
        // Table Name : Departments
        // Domain Model Name: Department
        // View Model Name: VMDepartment
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-15
        [HttpGet]
        public async Task<ActionResult> EditDepartment(int? id)
        {
            //if (Session["Role"] != null)
            {
                if (id != null)
                {

                    DropDownData ddd = new DropDownData();
                    ViewBag.Unitall = new SelectList(ddd.GetUnitList(), "Value", "Text");
                    var u = db.Departments.FirstOrDefault(x => x.ID == id);
                    VMDepartment model = new VMDepartment
                    {
                        ID = u.ID,
                        DeptName = u.DeptName,
                        DeptNameBangla = u.DeptNameBangla,
                        Unit_SL = u.Unit_ID
                    };
                    return View(model);
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Department Not Found!";
                return RedirectToAction("ViewDepartment", "Common");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to Edit Department Information
        // Database: CrimsonERP
        // Table Name : Departments
        // Domain Model Name: Department
        // View Model Name: VMDepartment

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditDepartment(VMDepartment model)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    Department department = new Department
                    {
                        DeptName = model.DeptName,
                        DeptNameBangla = model.DeptNameBangla,
                        Unit_ID = model.Unit_SL,
                        ID = model.ID
                    };
                    db.Entry(department).State = System.Data.Entity.EntityState.Modified;
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Department Updated Successfully.";
                    return RedirectToAction("ViewDepartment", "Common");
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewDepartment", "Common");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        #endregion

        #region   Section

        // Below Action used to View Section Information
        // Database: CrimsonERP
        // Table Name :Sections
        // Domain Model Name: Section
        // View Model Name: VMSection

        public async Task<ActionResult> ViewSection()
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    VMSection model = new VMSection();
                    model.DataListSection = (from s in db.Sections
                                             join d in db.Departments on s.Dept_ID equals d.ID
                                             select new VMSection()
                                             {
                                                 ID = s.ID,
                                                 SectionName = s.SectionName,
                                                 DeptName = d.DeptName
                                             }).AsEnumerable();
                    return await Task.Run(() => View(model));
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("Index", "Common");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to Add Section Information
        // Database: CrimsonERP
        // Table Name :Sections
        // Domain Model Name: Section
        // View Model Name: VMSection

        [HttpGet]
        public ActionResult AddSection()
        {
            //if (Session["Role"] != null)
            {
                DropDownData ddd = new DropDownData();
                ViewBag.DepartmentList = new SelectList(ddd.GetDepartmentList(), "Value", "Text");
                VMSection model = new VMSection();

                return View(model);
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to Add Section Information
        // Database: CrimsonERP
        // Table Name :Sections
        // Domain Model Name: Section
        // View Model Name: VMSection

        [HttpPost]
        public async Task<ActionResult> AddSection(VMSection model)
        {
            // if (Session["Role"] != null)
            {
                try
                {
                    Section section = new Section
                    {
                        SectionName = model.SectionName,
                        Dept_ID = model.Dept_ID,
                        Entry_By = "1",
                        Entry_Date = DateTime.Now
                    };
                    db.Sections.Add(section);
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Section Added Successfully.";
                    return RedirectToAction("ViewSection", "Common");
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Section Can't Add, It is Already Exist!";
                        return RedirectToAction("ViewSection", "Common");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ViewSection", "Common");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        // Below Action used to Delete Section Information
        // Database: CrimsonERP
        // Table Name :Sections
        // Domain Model Name: Section
        // View Model Name: VMSection
        [HttpGet]
        public async Task<ActionResult> DeleteSection(int? id)
        {

            //if (Session["Role"] != null)
            {
                if (id == null)
                {
                    //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Section Not Found!";
                    return RedirectToAction("ViewSection", "Common");
                }
                Section section = await db.Sections.FindAsync(id);
                if (section != null)
                {
                    try
                    {
                        db.Sections.Remove(section);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Section Deleted Successfully.";
                        return RedirectToAction("ViewSection", "Common");
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Section Can't Delete, It is Already Use!";
                            return RedirectToAction("ViewSection", "Common");
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("ViewSection", "Common");
                    }
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("ViewSection", "Common");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to Edti Section Information
        // Database: CrimsonERP
        // Table Name :Sections
        // Domain Model Name: Section
        // View Model Name: VMSection

        public async Task<ActionResult> EditSection(int? id)
        {
            //if (Session["Role"] != null)
            {
                if (id != null)
                {
                    DropDownData ddd = new DropDownData();
                    ViewBag.DepartmentList = new SelectList(ddd.GetDepartmentList(), "Value", "Text");
                    VMSection model = new VMSection();
                    model = (from s in db.Sections
                             join d in db.Departments on s.Dept_ID equals d.ID
                             where s.ID == id
                             select new VMSection
                             {
                                 ID = s.ID,
                                 Dept_ID = s.Dept_ID,
                                 SectionName = s.SectionName
                             }).FirstOrDefault();
                    return View(model);
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Section Not Found!";
                return RedirectToAction("ViewSection", "Common");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to Edit Section Information
        // Database: CrimsonERP
        // Table Name :Sections
        // Domain Model Name: Section
        // View Model Name: VMSection

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditSection(VMSection model)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    Section section = new Section
                    {
                        SectionName = model.SectionName,
                        ID = model.ID,
                        Dept_ID = model.Dept_ID,
                    };
                    db.Entry(section).State = System.Data.Entity.EntityState.Modified;
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Section Updated Successfully.";
                    return RedirectToAction("ViewSection", "Common");
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewSection", "Common");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        #endregion

        #region   Notification user

        // Below Action used to View Notification User Information
        // Database: CrimsonERP
        // Table Name :NotificationUsers
        // Domain Model Name: NotificationUser
        // View Model Name: VMNotificationUser

        public async Task<ActionResult> ViewNotificationUser()
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    VMNotificationUser model = new VMNotificationUser();
                    await Task.Run(() => model.LoadNotificationUser());
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("Index", "Common");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to Add Notification User Information
        // Database: CrimsonERP
        // Table Name :NotificationUsers
        // Domain Model Name: NotificationUser
        // View Model Name: VMNotificationUser

        [HttpGet]
        public async Task<ActionResult> AddNotificationUser()
        {

            //if (Session["Role"] != null)
            {
                try
                {
                    DropDownData ddd = new DropDownData();
                    ViewBag.BusinessUnitList = new SelectList(ddd.GetBusinessUnitList(), "Value", "Text");
                    ViewBag.EmployeeIDlist = new SelectList(ddd.GetActiveEmployeeList(), "Value", "Text");
                    VMNotificationUser model = new VMNotificationUser();
                    await Task.Run(() => model.LoadNotificationUser());
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happend. " + ex.Message;
                    return RedirectToAction("ViewNotificationUser", "Common");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to Add Notification User Information
        // Database: CrimsonERP
        // Table Name :NotificationUsers
        // Domain Model Name: NotificationUser
        // View Model Name: VMNotificationUser

        [HttpPost]
        public async Task<ActionResult> AddNotificationUser(VMNotificationUser model)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    NotificationUser notificationUser = new NotificationUser
                    {
                        Employee_Id = model.Employee_Id,
                        Name = model.Name,
                        Email = model.Email,
                        BusinessUnit_ID = model.BusinessUnit_ID,
                        Subject = model.Subject
                    };
                    db.NotificationUsers.Add(notificationUser);
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Notification User Added Successfully.";
                    return RedirectToAction("ViewNotificationUser", "Common");
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Notification User Can't Add, It is Already Exist!";
                        return RedirectToAction("ViewNotificationUser", "Common");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                    return RedirectToAction("ViewNotificationUser", "Common");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        // Below Action used to Delete Notification User Information
        // Database: CrimsonERP
        // Table Name :NotificationUsers
        // Domain Model Name: NotificationUser
        // View Model Name: VMNotificationUser

        [HttpGet]
        public async Task<ActionResult> DeleteNotificationUser(int? id)
        {

            //if (Session["Role"] != null)
            {
                if (id == null)
                {
                    //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Notification User Not Found!";
                    return RedirectToAction("ViewNotificationUser", "Common");
                }
                NotificationUser notificationUser = await db.NotificationUsers.FindAsync(id);
                if (notificationUser != null)
                {
                    try
                    {
                        db.NotificationUsers.Remove(notificationUser);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Notification User Deleted Successfully.";
                        return RedirectToAction("ViewNotificationUser", "Common");
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Notification User Can't Delete, It is Already Use!";
                            return RedirectToAction("ViewNotificationUser", "Common");
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("ViewNotificationUser", "Common");
                    }
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("ViewNotificationUser", "Common");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to Edit Notification User Information
        // Database: CrimsonERP
        // Table Name :NotificationUsers
        // Domain Model Name: NotificationUser
        // View Model Name: VMNotificationUser

        public async Task<ActionResult> EditNotificationUser(int? id)
        {
            //if (Session["Role"] != null)
            {
                if (id != null)
                {
                    DropDownData ddd = new DropDownData();
                    ViewBag.BusinessUnitList = new SelectList(ddd.GetBusinessUnitList(), "Value", "Text");
                    ViewBag.EmployeeIDlist = new SelectList(ddd.GetActiveEmployeeList(), "Value", "Text");
                    VMNotificationUser model = new VMNotificationUser();
                    await Task.Run(() => model = model.LoadSpecificNotificationUser(id.Value));
                    return View(model);
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Notification User Not Found!";
                return RedirectToAction("ViewNotificationUser", "Common");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        // Below Action used to Edit Notification User Information
        // Database: CrimsonERP
        // Table Name :NotificationUsers
        // Domain Model Name: NotificationUser
        // View Model Name: VMNotificationUser

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditNotificationUser(VMNotificationUser model)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    NotificationUser notificationUser = new NotificationUser
                    {
                        Employee_Id = model.Employee_Id,
                        Name = model.Name,
                        Email = model.Email,
                        BusinessUnit_ID = model.BusinessUnit_ID,
                        Subject = model.Subject,
                        ID = model.ID,
                        Update_By = "1",
                        Update_Date = DateTime.Now
                    };
                    db.Entry(notificationUser).State = System.Data.Entity.EntityState.Modified;
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Notification User Updated Successfully.";
                    return RedirectToAction("ViewNotificationUser", "Common");
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewNotificationUser", "Common");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");

        }

        #endregion

        #region SalaryGrade

        [HttpGet]
        public async Task<ActionResult> ViewSalaryGradeList()
        {

            //if (Session["Role"] != null)
            {
                try
                {
                    VMSalaryGrade model = new VMSalaryGrade();
                    model.DataList = (from t1 in db.SalaryGrade
                                      select new VMSalaryGrade
                                      {
                                          ID = t1.ID,
                                          Name = t1.Name,
                                          Grade_Amount = t1.Grade_Amount,
                                      }).AsEnumerable();
                    return await Task.Run(() => View(model));
                }
                catch (Exception ex)
                {

                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("Index", "Common");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        public ActionResult AddSalaryGrade()
        {

            //if (Session["Role"] != null)
            {
                try
                {
                    VMSalaryGrade model = new VMSalaryGrade();
                    return View(model);
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewSalaryGradeList", "Common");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        public async Task<ActionResult> AddSalaryGrade(VMSalaryGrade model)
        {
            //if (Session["Role"] != null)
            {

                try
                {
                    SalaryGrade Grade = new SalaryGrade
                    {
                        Name = model.Name,
                        Grade_Amount = model.Grade_Amount,
                        Status = model.Status,
                        Entry_By = "1",
                        Entry_Date = DateTime.Now
                    };
                    db.SalaryGrade.Add(Grade);
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Salary Grade Added Successfully.";
                    return RedirectToAction("ViewSalaryGradeList", "Common");
                }
                catch (Exception ex)
                {
                    if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                    {
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Designation Can't Add. It is Already in Exist!";
                        return RedirectToAction("ViewDesignationList", "Common");
                    }
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewDesignationList", "Common");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        [HttpGet]
        public async Task<ActionResult> DeleteSalaryGrade(int? id)
        {

            //if (Session["Role"] != null)
            {
                if (id == null)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Salary Grade ID Not Found!";
                    return RedirectToAction("ViewSalaryGradeList", "Common");
                }
                SalaryGrade grade = await db.SalaryGrade.FindAsync(id);
                if (grade != null)
                {
                    try
                    {
                        db.SalaryGrade.Remove(grade);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Salary Grade Deleted Successfully.";
                        return RedirectToAction("ViewSalaryGradeList", "Common");
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Salary Grade Can't Delete, It is Already in Use!";
                            return RedirectToAction("ViewDesignationList", "Common");
                        }
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Something Went Wrong!" + ex.Message;
                        return RedirectToAction("ViewDesignationList", "Common");
                    }
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Something Went Wrong!";
                return RedirectToAction("ViewDesignationList", "Common");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }


        public async Task<ActionResult> EditSalaryGrade(int? id)
        {
            // if (Session["Role"] != null)
            {
                if (id != null)
                {
                    var a = db.SalaryGrade.FirstOrDefault(x => x.ID == id);
                    VMSalaryGrade model = new VMSalaryGrade
                    {
                        ID = a.ID,
                        Name = a.Name,
                        Grade_Amount = a.Grade_Amount
                    };
                    return View(model);
                }
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Salary Grade Not Found!";
                return RedirectToAction("ViewSalaryGradeList", "Common");
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditSalaryGrade(VMSalaryGrade model)
        {
            //if (Session["Role"] != null)
            {
                try
                {
                    SalaryGrade Grade = new SalaryGrade
                    {
                        Name = model.Name,
                        Grade_Amount = model.Grade_Amount,
                        ID = model.ID
                    };
                    db.Entry(Grade).State = System.Data.Entity.EntityState.Modified;
                    await db.SaveChangesAsync();
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Salary Grade Updated Successfully.";
                    return RedirectToAction("ViewSalaryGradeList", "Common");
                }
                catch (Exception ex)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Error Happened." + ex.Message;
                    return RedirectToAction("ViewSalaryGradeList", "Common");
                }
            }
            //Session["warning_div"] = "true";
            //Session["warning_msg"] = "Please Login to continue..!";
            //return RedirectToAction("Login", "Account");
        }

        #endregion

        #region District Manage

        public async Task<ActionResult> IndexDistrict()
        {
            try
            {
                VM_District vmDistrict = new VM_District();
                await Task.Run(() => vmDistrict.GetDistrictName());
                return View(vmDistrict);
            }
            catch (Exception ex)
            {

                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("Index", "Common");
            }
        }

        /// <summary>
        /// Load Form for Adding Lines.... 
        /// </summary>
        /// <returns></returns>
        public ActionResult AddDistrict()
        {
            try
            {
                VM_District vmDistrict = new VM_District();
                return View(vmDistrict);
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("IndexDistrict", "Common");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddDistrict(VM_District vmDistrict)
        {
            try
            {
                District d = new District()
                {
                    DistrictName = vmDistrict.DistrictName,
                    Entry_By = "1",
                    Entry_Date = DateTime.Now
                };
                db.District.Add(d);
                await db.SaveChangesAsync();
                return RedirectToAction("IndexDistrict", "Common");
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("IndexDistrict", "Common");
            }
        }


        /// <summary>
        /// Delete Lines-----
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> DeleteDistrict(int id)
        {
            if (id != 0)
            {
                try
                {
                    District d = db.District.Find(id);
                    if (d != null)
                    {
                        db.District.Remove(d);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Deleted.";
                        return RedirectToAction("IndexDistrict", "Common");
                    }
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Not Deleted.";
                    return RedirectToAction("IndexDistrict", "Common");
                }
                catch (Exception e)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Unable to Delete" + e.Message;
                    return RedirectToAction("IndexDistrict", "Common");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "No Id found to delete";
            return RedirectToAction("IndexDistrict", "Common");
        }

        /// <summary>
        /// Load form with data-----for editing information
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> EditDistrict(int id)
        {
            if (id != 0)
            {
                try
                {
                    var d = db.District.FirstOrDefault(x => x.ID == id);
                    VM_District vd = new VM_District()
                    {
                        ID = d.ID,
                        DistrictName = d.DistrictName
                    };
                    return View(vd);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    throw;
                }
            }
            return RedirectToAction("IndexDistrict", "Common");
        }

        /// <summary>
        /// Submit edited data----
        /// </summary>
        /// <param name="vmLine"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> EditDistrict(VM_District vd)
        {
            try
            {
                District d = new District()
                {
                    ID = vd.ID,
                    DistrictName = vd.DistrictName,
                    Update_By = "1",
                    Update_Date = DateTime.Now
                };
                db.Entry(d).State = EntityState.Modified;
                await db.SaveChangesAsync();
                Session["success_div"] = "true";
                Session["success_msg"] = "Updated.";
                return RedirectToAction("IndexDistrict", "Common");
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("IndexDistrict", "Common");
            }
        }

        #endregion


        #region Police Station Manage

        public async Task<ActionResult> IndexPoliceStation()
        {
            try
            {
                VM_PoliceStation vmPoliceStation = new VM_PoliceStation();
                await Task.Run(() => vmPoliceStation.GetPoliceStationList());
                return View(vmPoliceStation);
            }
            catch (Exception ex)
            {

                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("Index", "Common");
            }
        }

        /// <summary>
        /// Load Form for Adding Lines.... 
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> AddPoliceStation(int id)
        {
            try
            {
                //DropDownData d = new  DropDownData();
                //ViewBag.ActiveDistrictList = new SelectList(d.GetActiveDistrictList(), "Value", "Text");
                //VM_PoliceStation vmPoliceStation = new VM_PoliceStation();
                //return View(vmPoliceStation);
                VM_PoliceStation model = new VM_PoliceStation();
                await Task.Run(() => model = model.GetSpecificDistrictInfo(id));
                return View(model);
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("IndexPoliceStation", "Common");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddPoliceStation(VM_PoliceStation vmPoliceStation)
        {
            try
            {
                PoliceStation p = new PoliceStation()
                {
                    PoliceStationName = vmPoliceStation.PoliceStationName,
                    District_ID = vmPoliceStation.District_ID,
                    Entry_By = "1",
                    Entry_Date = DateTime.Now
                };
                db.PoliceStation.Add(p);
                await db.SaveChangesAsync();
                return RedirectToAction("IndexPoliceStation", "Common");
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("IndexPoliceStation", "Common");
            }
        }


        /// <summary>
        /// Delete Lines-----
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> DeletePoliceStation(int id)
        {
            if (id != 0)
            {
                try
                {
                    PoliceStation p = db.PoliceStation.Find(id);
                    if (p != null)
                    {
                        db.PoliceStation.Remove(p);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Deleted.";
                        return RedirectToAction("IndexPoliceStation", "Common");
                    }
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Not Deleted.";
                    return RedirectToAction("IndexPoliceStation", "Common");
                }
                catch (Exception e)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Unable to Delete" + e.Message;
                    return RedirectToAction("IndexPoliceStation", "Common");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "No Id found to delete";
            return RedirectToAction("IndexPoliceStation", "Common");
        }
        //GetSpecificDistrictInfo
        /// <summary>
        /// Load form with data-----for editing information
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> EditPoliceStation(int id)
        {
            if (id != 0)
            {
                try
                {
                    DropDownData d = new DropDownData();
                    ViewBag.ActiveDistrictList = new SelectList(d.GetActiveDistrictList(), "Value", "Text");
                    VM_PoliceStation model = new VM_PoliceStation();
                    await Task.Run(() => model = model.GetSpecificPoliceStation(id));
                    return View(model);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    throw;
                }
            }
            return RedirectToAction("IndexPoliceStation", "Common");
        }

        /// <summary>
        /// Submit edited data----
        /// </summary>
        /// <param name="vmLine"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> EditPoliceStation(VM_PoliceStation p)
        {
            try
            {
                var po = db.PoliceStation.Where(x => x.ID == p.ID).FirstOrDefault();
                po.PoliceStationName = p.PoliceStationName;
                po.Update_By = "1";
                po.Update_Date = DateTime.Now;
                //PoliceStation d = new PoliceStation()
                //{
                //    ID = p.ID,
                //    PoliceStationName = p.PoliceStationName,
                //    District_ID = p.District_ID,
                //    Update_By = "1",
                //    Update_Date = DateTime.Now
                //};
                //db.Entry(d).State = EntityState.Modified;
                await db.SaveChangesAsync();
                Session["success_div"] = "true";
                Session["success_msg"] = "Updated.";
                return RedirectToAction("IndexPoliceStation", "Common");
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("IndexPoliceStation", "Common");
            }
        }

        #endregion

        #region Post Office Manage

        public async Task<ActionResult> IndexPostOffice()
        {
            try
            {
                VM_PostOffice vmPostOffice = new VM_PostOffice();
                await Task.Run(() => vmPostOffice.GetPostOfficeList());
                return View(vmPostOffice);
            }
            catch (Exception ex)
            {

                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("Index", "Common");
            }
        }

        /// <summary>
        /// Load Form for Adding Lines.... 
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> AddPostOffice(int id)
        {
            try
            {
                //DropDownData d = new DropDownData();
                //ViewBag.ActiveDistrictList = new SelectList(d.GetActiveDistrictList(), "Value", "Text");
                //ViewBag.ActivePoliceStationList = new SelectList(d.GetActivePoliceStationList(), "Value", "Text");
                VM_PostOffice model = new VM_PostOffice();
                await Task.Run(() => model = model.GetSpecificPostOfficeforAddPostOffice(id));
                return View(model);
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("IndexPostOffice", "Common");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddPostOffice(VM_PostOffice vmPostOffice)
        {
            try
            {
                PostOffice p = new PostOffice()
                {
                    PostOfficeName = vmPostOffice.PostOfficeName,
                    PoliceStation_ID = vmPostOffice.PoliceStation_ID,
                    Entry_By = "1",
                    Entry_Date = DateTime.Now
                };
                db.PostOffice.Add(p);
                await db.SaveChangesAsync();
                return RedirectToAction("IndexPostOffice", "Common");
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("IndexPostOffice", "Common");
            }
        }


        /// <summary>
        /// Delete Lines-----
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> DeletePostOffice(int id)
        {
            if (id != 0)
            {
                try
                {
                    PostOffice p = db.PostOffice.Find(id);
                    if (p != null)
                    {
                        db.PostOffice.Remove(p);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Deleted.";
                        return RedirectToAction("IndexPostOffice", "Common");
                    }
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Not Deleted.";
                    return RedirectToAction("IndexPostOffice", "Common");
                }
                catch (Exception e)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Unable to Delete" + e.Message;
                    return RedirectToAction("IndexPostOffice", "Common");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "No Id found to delete";
            return RedirectToAction("IndexPostOffice", "Common");
        }

        /// <summary>
        /// Load form with data-----for editing information
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> EditPostOffice(int id, int did)
        {
            if (id != 0)
            {
                try
                {
                    DropDownData d = new DropDownData();
                    //ViewBag.ActiveDistrictList = new SelectList(d.GetActiveDistrictList(), "Value", "Text");
                    ViewBag.ActivePoliceStationList = new SelectList(d.GetSpecificActivePoliceStationList(did), "Value", "Text");
                    VM_PostOffice model = new VM_PostOffice();
                    await Task.Run(() => model = model.GetSpecificPostOffice(id));
                    return View(model);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    throw;
                }
            }
            return RedirectToAction("IndexPostOffice", "Common");
        }

        /// <summary>
        /// Submit edited data----
        /// </summary>
        /// <param name="vmLine"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> EditPostOffice(VM_PostOffice p)
        {
            try
            {
                var po = db.PostOffice.Where(x => x.ID == p.ID).FirstOrDefault();
                po.PostOfficeName = p.PostOfficeName;
                po.Update_By = "1";
                po.Update_Date = DateTime.Now;
                //PostOffice d = new PostOffice()
                //{
                //    ID = p.ID,
                //    PostOfficeName = p.PostOfficeName,
                //    PoliceStation_ID = p.PoliceStation_ID,
                //    Update_By = "1",
                //    Update_Date = DateTime.Now
                //};
                //db.Entry(d).State = EntityState.Modified;
                await db.SaveChangesAsync();
                Session["success_div"] = "true";
                Session["success_msg"] = "Updated.";
                return RedirectToAction("IndexPostOffice", "Common");
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("IndexPostOffice", "Common");
            }
        }

        #endregion


        #region Village Manage

        public async Task<ActionResult> IndexVillage()
        {
            try
            {
                VM_Village vmVillage = new VM_Village();
                await Task.Run(() => vmVillage.GetVillageList());
                return View(vmVillage);
            }
            catch (Exception ex)
            {

                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("Index", "Common");
            }
        }

        /// <summary>
        /// Load Form for Adding Lines.... 
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> AddVillage(int id)
        {
            try
            {
                //DropDownData d = new DropDownData();
                //ViewBag.ActiveDistrictList = new SelectList(d.GetActiveDistrictList(), "Value", "Text");
                //ViewBag.ActivePoliceStationList = new SelectList(d.GetActivePoliceStationList(), "Value", "Text");
                VM_Village model = new VM_Village();
                await Task.Run(() => model = model.GetinformationforVillageAdd(id));
                return View(model);
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("IndexVillage", "Common");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddVillage(VM_Village vmVillage)
        {
            try
            {
                Village v = new Village()
                {
                    VillageName = vmVillage.VillageName,
                    PostOffice_ID = vmVillage.PostOffice_ID,
                    Entry_By = "1",
                    Entry_Date = DateTime.Now
                };
                db.Village.Add(v);
                await db.SaveChangesAsync();
                return RedirectToAction("IndexVillage", "Common");
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("IndexVillage", "Common");
            }
        }


        /// <summary>
        /// Delete Lines-----
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> DeleteVillage(int id)
        {
            if (id != 0)
            {
                try
                {
                    Village v = db.Village.Find(id);
                    if (v != null)
                    {
                        db.Village.Remove(v);
                        await db.SaveChangesAsync();
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Deleted.";
                        return RedirectToAction("IndexVillage", "Common");
                    }
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Not Deleted.";
                    return RedirectToAction("IndexVillage", "Common");
                }
                catch (Exception e)
                {
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Unable to Delete" + e.Message;
                    return RedirectToAction("IndexVillage", "Common");
                }
            }
            Session["warning_div"] = "true";
            Session["warning_msg"] = "No Id found to delete";
            return RedirectToAction("IndexVillage", "Common");
        }

        /// <summary>
        /// Load form with data-----for editing information
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> EditVillage(int id, int did)
        {
            if (id != 0)
            {
                try
                {
                    // DropDownData d = new DropDownData();
                    //ViewBag.ActivePoliceStationList = new SelectList(d.GetSpecificActivePostOfficeList(did), "Value", "Text");
                    VM_Village model = new VM_Village();
                    await Task.Run(() => model = model.GetSpecificVillage(id));
                    return View(model);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    throw;
                }
            }
            return RedirectToAction("IndexVillage", "Common");
        }

        /// <summary>
        /// Submit edited data----
        /// </summary>
        /// <param name="vmLine"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> EditVillage(VM_Village v)
        {
            try
            {


                //Village hrmVillage = new Village();
                var village = db.Village.Where(x => x.ID == v.ID).FirstOrDefault();
                village.VillageName = v.VillageName;
                //village.PostOffice_ID = v.PostOffice_ID;
                village.Update_By = "1";
                village.Update_Date = DateTime.Now;
                await db.SaveChangesAsync();
                Session["success_div"] = "true";
                Session["success_msg"] = "Updated.";
                return RedirectToAction("IndexVillage", "Common");
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;
                return RedirectToAction("IndexVillage", "Common");
            }
        }

        #endregion


        #region Notification

        public ActionResult NotificationIndex()
        {
            return View();
        }
        [HttpPost]
        public ActionResult NotificationIndex(Notification model)
        {
            try
            {
                db = new HrmsContext();
                model.Entry_By = user.ID.ToString();
                model.Entry_Date = DateTime.Now;
                db.Notification.Add(model);
                db.SaveChanges();

            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened. " + ex.Message;
                return RedirectToAction("NotificationIndex", "Common");
            }
            return View();
        }

        public ActionResult NotificationList()
        {
            try
            {
                db = new HrmsContext();

                var data = db.Notification.Where(x => x.Status == true).OrderByDescending(x => x.Date).ToList();
                return View(data);
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened. " + ex.Message;
                return RedirectToAction("NotificationIndex", "Common");
            }
        }

        public ActionResult NotificationEdit(int id)
        {
            try
            {
                db = new HrmsContext();
                var model = db.Notification.Single(x=>x.ID==id);
                return View(model);
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened. " + ex.Message;
                return RedirectToAction("NotificationIndex", "Common");
            }
        }

        public ActionResult DeleteNotification(int id)
        {
            db = new HrmsContext();
            var model = db.Notification.Single(x => x.ID == id);
            model.Status = false;
            db.SaveChanges();
            return RedirectToAction("NotificationList", "Common");
        }

        public ActionResult NotificationDetails(int id)
        {
            db = new HrmsContext();
            var model = db.Notification.Single(x => x.ID == id);
            return View(model);
        }
        #endregion

        #region JSON

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetPostOfficeList(int id)
        {
            var list = db.PostOffice.ToList().Where(u => u.PoliceStation_ID == id);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetPoliceStationList(int id)
        {
            var list = db.PoliceStation.ToList().Where(u => u.District_ID == id);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult IsEmployeeIdentityExists(string employeeid)
        {
            //check if any of the UserName matches the UserName specified in the Parameter using the ANY extension method.  
            return Json(!db.Employees.Any(x => x.EmployeeIdentity == employeeid), JsonRequestBehavior.AllowGet);
        }


        // Below Action used to check Country exist or not in the system
        // Database: CrimsonERP
        // Table Name :Countries
        // Domain Model Name: Countries
        // View Model Name: 
        // Developed by: Anis and Fazle Rabbi
        // Date: 2018-03-15
        public JsonResult IsCountryExists(string country)
        {
            //check if any of the UserName matches the UserName specified in the Parameter using the ANY extension method.  
            return Json(!db.Countries.Any(x => x.Name == country), JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="Name"></param>
        /// <returns></returns>
        public JsonResult IsBusinessUnitExists(string Name)
        {
            //check if any of the UserName matches the UserName specified in the Parameter using the ANY extension method.  
            return Json(!db.BusinessUnits.Any(x => x.Name == Name), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bUnitId"></param>
        /// <returns></returns>
        public JsonResult GetUnitList(int bUnitId)
        {
            var list = db.Units.ToList().Where(u => u.BusinessUnit_ID == bUnitId);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="unitId"></param>
        /// <returns></returns>
        public JsonResult GetDeptList(int unitId)
        {
            if (Session["Role"] != null)//&& Session["Role"].ToString() == "Admin")
            {
                var list = new List<object>();
                foreach (var List in db.Departments.ToList().Where(u => u.Unit_ID == unitId))
                {
                    list.Add(new { Text = List.DeptName, Value = List.ID });
                }
                var retval = Json(new { list }, JsonRequestBehavior.AllowGet);
                return retval;
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deptId"></param>
        /// <returns></returns>
        public JsonResult GetSectionList(int deptId)
        {
            if (Session["Role"] != null)
            {
                var list = new List<object>();
                foreach (var List in db.Sections.ToList().Where(u => u.Dept_ID == deptId))
                {
                    list.Add(new { Text = List.SectionName, Value = List.ID });
                }
                var retval = Json(new { list }, JsonRequestBehavior.AllowGet);
                return retval;
            }
            return null;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sectionid"></param>
        /// <returns></returns>
        public JsonResult GetEmployeeIDforEmployeeShiftAssign(int sectionid)
        {
            if (Session["Role"] != null)
            {
                var list = db.Employees.ToList().Where(u => u.Section_Id == sectionid);
                var retval = Json(new { list }, JsonRequestBehavior.AllowGet);
                return retval;
            }
            return null;
        }



        #endregion


    }
}