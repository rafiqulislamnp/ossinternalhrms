﻿using Oss.Hrms.Models.ViewModels.User;
using Oss.Hrms.Models.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;
using System.Windows.Media.Imaging;
using Oss.Hrms.Models.Services;
using Oss.Hrms.Models.ViewModels;
using Oss.Hrms.Models;
using Oss.Hrms.Models.Entity.User;

namespace Oss.Hrms.Controllers
{
    [SoftwareAdmin]
    [HandleError]
    //[OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
    public class UserController : Controller
    {
        VmForDropDown VmForDropDown;
        VmUser_User VmUser_User;
        VmUser_Role VmUser_Role;
        VmUser_Team VmUser_Team;
        //VmControllerHelper VmControllerHelper;
        private VmUser_MenuItem VmUser_MenuItem;
        private VmUser_Department VmUser_Department;
        private HrmsContext db;
        Int32 UserID = 0;


        public UserController()
        {
            Models.Services.SessionHandler sessionHandler = new Models.Services.SessionHandler();
            sessionHandler.Adjust();
        }


        #region User Role
        public async Task<ActionResult> VmUser_RoleIndex()
        {
            //Oss.Hrms.Models.ViewModels.User.VmUser_User u = (Oss.Hrms.Models.ViewModels.User.VmUser_User)Session["One"];
            //  if (u == null)
            //  {
            //      return RedirectToAction("Login", "Home");
            //  }
            VmUser_Role = new VmUser_Role();
            await Task.Run(() => VmUser_Role.InitialDataLoad());
            return View(VmUser_Role);
        }
        [HttpGet]
        public ActionResult VmUser_RoleCreate()
        {
            //Oss.Hrms.Models.ViewModels.User.VmUser_User user = (Oss.Hrms.Models.ViewModels.User.VmUser_User)Session["One"];
            // if (user == null)
            // {
            //     return RedirectToAction("Login", "Home");
            // }

            return View();
        }
        //......//
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmUser_RoleCreate(VmUser_Role vmUser_Role)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => vmUser_Role.Add(UserID));
                return RedirectToAction("VmUser_RoleIndex");
            }
            return RedirectToAction("VmUser_RoleIndex");
        }
        [HttpGet]
        public async Task<ActionResult> VmUser_RoleEdit(int id)
        {
            //Oss.Hrms.Models.ViewModels.User.VmUser_User u = (Oss.Hrms.Models.ViewModels.User.VmUser_User)Session["One"];
            // if (u == null)
            // {
            //     return RedirectToAction("Login", "Home");
            // }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmUser_Role = new VmUser_Role();
            await Task.Run(() => VmUser_Role.SelectSingle(id));
            if (VmUser_Role.User_Role == null)
            {
                return HttpNotFound();
            }
            return View(VmUser_Role);
        }

        [HttpPost]

        public async Task<ActionResult> VmUser_RoleEdit(VmUser_Role a)
        {

            await Task.Run(() => a.Edit(UserID));

            return RedirectToAction("VmUser_RoleIndex");

        }

        public ActionResult VmUser_RoleDelete(int id)
        {
            //Oss.Hrms.Models.ViewModels.User.VmUser_User u = (Oss.Hrms.Models.ViewModels.User.VmUser_User)Session["One"];
            // if (u == null)
            // {
            //     return RedirectToAction("Login", "Home");
            // }
            //int roleId = 0;
            ////VmControllerHelper = new VmControllerHelper();
            ////id = VmControllerHelper.Decrypt(id);
            ////Int32.TryParse(id, out roleId);
            //User_Role user_Role = new User_Role();
            //user_Role.ID = roleId;
            //user_Role.Delete();
            VmUser_Role = new VmUser_Role();
            VmUser_Role.Delete(id);
            return RedirectToAction("VmUser_RoleIndex");

        }
        #endregion

        #region Access Control
        [HttpGet]
        public async Task<ActionResult> AccessControl(int id)
        {
            //Oss.Hrms.Models.ViewModels.User.VmUser_User u = (Oss.Hrms.Models.ViewModels.User.VmUser_User)Session["One"];
            //if (u == null)
            //{
            //    return RedirectToAction("Login", "Home");
            //}



            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetUserRoleForDropDown();
            SelectList userRole = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.UserRole = userRole;

            VmUser_User = new VmUser_User();
            await Task.Run(() => VmUser_User.SelectSingle(id));

            if (VmUser_User.User_User == null)
            {
                return HttpNotFound();
            }
            return View(VmUser_User);
        }
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> AccessControl(VmUser_User x)
        //{

        //    await Task.Run(() => x.Edit(0));
        //    return RedirectToAction("VmUser_UserIndex");

        //}
        #endregion

        #region User
        [HttpGet]
        public async Task<ActionResult> VmUser_UserIndex()
        {
            //Oss.Hrms.Models.ViewModels.User.VmUser_User u = (Oss.Hrms.Models.ViewModels.User.VmUser_User)Session["One"];
            //if (u == null)
            //{
            //    return RedirectToAction("Login", "Home");
            //}
            VmUser_User = new VmUser_User();
            await Task.Run(() => VmUser_User.InitialDataLoad());
            return View(VmUser_User);
        }

        public ActionResult VmUser_UserCreate()
        {
            //Oss.Hrms.Models.ViewModels.User.VmUser_User u = (Oss.Hrms.Models.ViewModels.User.VmUser_User)Session["One"];
            //if (u == null)
            //{
            //    return RedirectToAction("Login", "Home");
            //}
            //................DropDown For User_Team....................
            //VmForDropDown = new VmForDropDown();
            //VmForDropDown.DDownData = VmForDropDown.GetUser_TeamForDropDown();
            //ViewBag.UserTeam = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            //................DropDown For User_Department....................
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetUser_DepartmentForDropDown();
            ViewBag.UserDepartment = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.UserRoles = new SelectList(VmForDropDown.GetUserRoleForDropDown(), "Value", "Text");
            //................DropDown For User_AccessLevel....................
            //VmForDropDown = new VmForDropDown();
            //VmForDropDown.DDownData = VmForDropDown.GetUser_AccessLevelForDropDown();
            //ViewBag.UserAccessLevel = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmForDropDown.DDownData = VmForDropDown.GetEmployee();
            ViewBag.EmployeeName = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            VmUser_User = new VmUser_User();
            return View(VmUser_User);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmUser_UserCreate(VmUser_User a)
        {


            a.User_User.IsActive = true;
            //await Task.Run(() => a.Add(UserID, Server.MapPath("~/CustomeFile/ProfilePic/")));

            await Task.Run(() => a.Add(UserID));
            return RedirectToAction("VmUser_UserIndex");
            //}
            //return RedirectToAction("VmUser_UserIndex");
        }

        //  [HttpPost]
        //  public JsonResult CheckName(FormCollection form)
        //{
        //      VmUser_User VmUser_User = new VmUser_User();

        //      string name = form["User_User.UserName"];
        //      HrmsContext db = new HrmsContext();
        //      var v = (from u in db.User_User
        //               select u.UserName).AsEnumerable();

        //      var s = v.Contains(name);

        //      if (s.Equals(name))
        //          return Json(true);
        //      else
        //      {
        //          return Json(false);
        //      }

        //      //return Json(!v.Contains(name));

        //  }


        [HttpGet]
        public async Task<ActionResult> VmUser_UserEdit(int id)
        {
            //Oss.Hrms.Models.ViewModels.User.VmUser_User u = (Oss.Hrms.Models.ViewModels.User.VmUser_User)Session["One"];
            //if (u == null)
            //{
            //    return RedirectToAction("Login", "Home");
            //}
            //................DropDown For User_Team....................
            //VmForDropDown = new VmForDropDown();
            //VmForDropDown.DDownData = VmForDropDown.GetUser_TeamForDropDown();
            //ViewBag.UserTeam = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            //................DropDown For User_Department....................
            VmForDropDown = new VmForDropDown();
            VmForDropDown.DDownData = VmForDropDown.GetUser_DepartmentForDropDown();
            ViewBag.UserDepartment = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.UserRoles = new SelectList(VmForDropDown.GetUserRoleForDropDown(), "Value", "Text");
            //................DropDown For User_AccessLevel....................
            //VmForDropDown = new VmForDropDown();
            //VmForDropDown.DDownData = VmForDropDown.GetUser_AccessLevelForDropDown();
            //ViewBag.UserAccessLevel = new SelectList(VmForDropDown.DDownData, "Value", "Text");

            VmUser_User = new VmUser_User();
            await Task.Run(() => VmUser_User.SelectSingle(id));

            if (VmUser_User.User_User == null)
            {
                return HttpNotFound();
            }

            return View(VmUser_User);
        }
        [HttpPost]
        public async Task<ActionResult> VmUser_UserEdit(VmUser_User a)
        {
            //string s = MvcApplication.FilePrefix;
            await Task.Run(() => a.EditUser(UserID, Server.MapPath(/*MvcApplication.FilePrefix+*/"~/Assets/Images/EmpImages/")));
            return RedirectToAction("VmUser_UserIndex");
        }
        #endregion

        #region User Activation
        [HttpGet]
        public async Task<ActionResult> Active(int id)
        {
            //Oss.Hrms.Models.ViewModels.User.VmUser_User u = (Oss.Hrms.Models.ViewModels.User.VmUser_User)Session["One"];
            //if (u == null)
            //{
            //    return RedirectToAction("Login", "Home");
            //}

            VmUser_User = new VmUser_User();
            await Task.Run(() => VmUser_User.SelectSingle(id));

            if (VmUser_User.User_User == null)
            {
                return HttpNotFound();
            }

            return View(VmUser_User);
        }

        [HttpPost]

        public async Task<ActionResult> Active(VmUser_User a)
        {

            await Task.Run(() => a.Edit(a.User_User.ID, a.User_User.IsActive));
            return RedirectToAction("VmUser_UserIndex");

        }
        #endregion

        #region Change Password
        [HttpGet]
        public async Task<ActionResult> PasswordEdit()
        {
            //Oss.Hrms.Models.ViewModels.User.VmUser_User u = (Oss.Hrms.Models.ViewModels.User.VmUser_User)Session["One"];
            //if (u == null)
            //{
            //    return RedirectToAction("Login", "Home");
            //}

            int userId = 0;

            VmUser_User = new VmUser_User();

            await Task.Run(() => VmUser_User.SelectSingle(userId));

            if (VmUser_User.User_User == null)
            {
                return HttpNotFound();
            }
            VmUser_User.User_User.Password = "";

            return View(VmUser_User);
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> PasswordEdit(VmUser_User VmUser_User)
        //{

        //    await Task.Run(() => VmUser_User.EditSelfPassword(VmUser_User));
        //    return RedirectToAction("CommonProfile","User");
        //}

        //..........PasswordConfirm for Admin............//
        [HttpGet]
        public async Task<ActionResult> AdminPasswordEdit(int id)
        {
            //Oss.Hrms.Models.ViewModels.User.VmUser_User user = (Oss.Hrms.Models.ViewModels.User.VmUser_User)Session["One"];
            //if (user == null)
            //{
            //    return RedirectToAction("Login", "Home");
            //}
            VmUser_User = new VmUser_User();
            await Task.Run(() => VmUser_User.SelectSingle(id));

            if (VmUser_User.User_User == null)
            {
                return HttpNotFound();
            }

            VmUser_User.User_User.Password = "";
            return View(VmUser_User);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AdminPasswordEdit(VmUser_User a)
        {
            await Task.Run(() => a.EditPassword(0, a.IsSupperUser));
            return RedirectToAction("VmUser_UserIndex");
        }

        [HttpPost]
        public ActionResult ChangeUserPassword(int userid, string password)
        {
            db = new HrmsContext();
            VmUser_User uservm = new VmUser_User();
            string pass = uservm.GetHashedPassword(password);
            var user = db.User_User.SingleOrDefault(x=>x.ID==userid);
            if (user!=null)
            {
                user.Password = pass;
                db.SaveChanges();
            }
            return RedirectToAction("SignOut", "Home");
        }
        #endregion

        #region User Profile
        [HttpGet]
        public async Task<ActionResult> CommonProfile()
        {
            //Oss.Hrms.Models.ViewModels.User.VmUser_User user = (Oss.Hrms.Models.ViewModels.User.VmUser_User)Session["One"];
            //if (user == null)
            //{
            //    return RedirectToAction("Login", "Home");
            //}

            int userId = 0;



            VmUser_User = new VmUser_User();
            await Task.Run(() => VmUser_User.SelectSingle(userId));

            if (VmUser_User.User_User == null)
            {
                return HttpNotFound();
            }
            return View(VmUser_User);
        }
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> CommonProfile(VmUser_User VmUser_User)
        //{

        //    await Task.Run(() => VmUser_User.Edit(0));
        //    return RedirectToAction("VmUser_UserIndex");
        //}
        #endregion

        #region Role Assign
        public async Task<ActionResult> VmUser_RoleAssign(int id)
        {
            //Oss.Hrms.Models.ViewModels.User.VmUser_User user = (Oss.Hrms.Models.ViewModels.User.VmUser_User)Session["One"];
            //if (user == null)
            //{
            //    return RedirectToAction("Login", "Home");
            //}

            VmUser_Role = new VmUser_Role();

            VmUser_Role = VmUser_Role.SelectSingleRole(id);

            await Task.Run(() => VmUser_Role.GetRoleDetailsById(id));



            return View(VmUser_Role);
        }

        //......//
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmUser_RoleAssign(VmUser_Role x)
        {
            //if (ModelState.IsValid)
            //{
            //await Task.Run(() => x.DefineRoleMenu(UserID));
            return RedirectToAction("VmUser_RoleAssign/" + x.DataListVmRoleDetails[1].RoleID);
            //}

            //return RedirectToAction("VmUser_RoleAssign/" + VmControllerHelper.Encrypt(x.DataListVmRoleDetails[1].RoleID.ToString()));
        }

        public ActionResult DeleteUser(int id)
        {
            db = new HrmsContext();
            User_User delete = new User_User() { ID = id };
            db.User_User.Attach(delete);
            db.User_User.Remove(delete);
            db.SaveChanges();
            Session["success_div"] = "true";
            Session["success_msg"] = "User Deleted Successfully.";
            return RedirectToAction("VmUser_UserIndex", "User");
        }
        #endregion

        #region User Team
        public async Task<ActionResult> VmUser_TeamIndex()
        {
            //Oss.Hrms.Models.ViewModels.User.VmUser_User u = (Oss.Hrms.Models.ViewModels.User.VmUser_User)Session["One"];
            //if (u == null)
            //{
            //    return RedirectToAction("Login", "Home");
            //}
            VmUser_Team = new VmUser_Team();
            await Task.Run(() => VmUser_Team.InitialDataLoad());
            return View(VmUser_Team);
        }

        public ActionResult VmUser_TeamCreate()
        {
            Oss.Hrms.Models.ViewModels.User.VmUser_User u = (Oss.Hrms.Models.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmUser_Team = new VmUser_Team();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmUser_TeamCreate(VmUser_Team a)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));
                return RedirectToAction("VmUser_TeamIndex");
            }
            return RedirectToAction("VmUser_TeamIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmUser_TeamEdit(int? id)
        {
            //Oss.Hrms.Models.ViewModels.User.VmUser_User u = (Oss.Hrms.Models.ViewModels.User.VmUser_User)Session["One"];
            //if (u == null)
            //{
            //    return RedirectToAction("Login", "Home");
            //}
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmUser_Team = new VmUser_Team();
            await Task.Run(() => VmUser_Team.SelectSingle(id.Value));

            if (VmUser_Team.User_Team == null)
            {
                return HttpNotFound();
            }
            return View(VmUser_Team);
        }
        // To update Buyer Information. -Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmUser_TeamEdit(User_Team User_Team)
        {

            if (ModelState.IsValid)
            {
                await Task.Run(() => User_Team.Edit(0));
                return RedirectToAction("VmUser_TeamIndex");
            }
            return RedirectToAction("VmUser_TeamIndex");
        }
        public ActionResult VmUser_TeamDelete(int id)
        {
            //Oss.Hrms.Models.ViewModels.User.VmUser_User u = (Oss.Hrms.Models.ViewModels.User.VmUser_User)Session["One"];
            //if (u == null)
            //{
            //    return RedirectToAction("Login", "Home");
            //}
            User_Team b = new User_Team();
            b.ID = id;
            b.Delete(0);
            return RedirectToAction("VmUser_TeamIndex");
        }

        #endregion

        #region User Menu Item
        public async Task<ActionResult> VmUser_MenuItemIndex()
        {
            Oss.Hrms.Models.ViewModels.User.VmUser_User u = (Oss.Hrms.Models.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmUser_MenuItem = new VmUser_MenuItem();
            await Task.Run(() => VmUser_MenuItem.InitialDataLoad());
            return View(VmUser_MenuItem);
        }

        public ActionResult VmUser_MenuItemCreate()
        {
            Oss.Hrms.Models.ViewModels.User.VmUser_User u = (Oss.Hrms.Models.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }

            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetUserMenuListForDropDown();
            SelectList userMenu = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.UserMenu = userMenu;

            VmForDropDown.DDownData = VmForDropDown.GetUserSubMenuForDropDown();
            SelectList userSubMenu = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.UserSubMenu = userSubMenu;

            VmUser_MenuItem = new VmUser_MenuItem();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmUser_MenuItemCreate(VmUser_MenuItem a)
        {
            int userMenuItemID = 0;

            if (ModelState.IsValid)
            {
                await Task.Run(() => userMenuItemID = a.Add(UserID));

                VmUser_Role vmUserRole = new VmUser_Role();
                List<VmUser_Role> vmUserRolesList = new List<VmUser_Role>();
                await Task.Run(() => vmUserRolesList = vmUserRole.GetUserRoles());
                foreach (var userRole in vmUserRolesList)
                {
                    User_RoleMenuItem userRoleMenuItem = new User_RoleMenuItem();
                    userRoleMenuItem.IsAllowed = false;
                    userRoleMenuItem.User_RoleFK = userRole.User_Role.ID;
                    userRoleMenuItem.User_MenuItemFk = userMenuItemID;
                    userRoleMenuItem.FirstCreatedBy = 0;
                    userRoleMenuItem.LastEditeddBy = 0;

                    userRoleMenuItem.AddReady(0);
                    await Task.Run(() => userRoleMenuItem.Add());
                }
                return RedirectToAction("VmUser_MenuItemIndex");
            }
            return RedirectToAction("VmUser_MenuItemIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmUser_MenuItemEdit(int? id)
        {
            Oss.Hrms.Models.ViewModels.User.VmUser_User u = (Oss.Hrms.Models.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VmForDropDown = new VmForDropDown();

            VmForDropDown.DDownData = VmForDropDown.GetUserMenuListForDropDown();
            SelectList userMenu = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.UserMenu = userMenu;

            VmForDropDown.DDownData = VmForDropDown.GetUserSubMenuForDropDown();
            SelectList userSubMenu = new SelectList(VmForDropDown.DDownData, "Value", "Text");
            ViewBag.UserSubMenu = userSubMenu;

            VmUser_MenuItem = new VmUser_MenuItem();
            await Task.Run(() => VmUser_MenuItem.SelectSingle(id.Value));

            if (VmUser_MenuItem.User_MenuItem == null)
            {
                return HttpNotFound();
            }
            return View(VmUser_MenuItem);
        }
        // To update Buyer Information. -Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmUser_MenuItemEdit(VmUser_MenuItem VmUser_MenuItem)
        {

            if (ModelState.IsValid)
            {
                await Task.Run(() => VmUser_MenuItem.Edit(0));
                return RedirectToAction("VmUser_MenuItemIndex");
            }
            return View(VmUser_MenuItem);
        }
        public ActionResult VmUser_MenuItemDelete(int id)
        {
            Oss.Hrms.Models.ViewModels.User.VmUser_User u = (Oss.Hrms.Models.ViewModels.User.VmUser_User)Session["One"];
            if (u == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VmUser_MenuItem b = new VmUser_MenuItem();
            b.User_MenuItem = new User_MenuItem();
            b.User_MenuItem.ID = id;
            b.Delete(0);
            return RedirectToAction("VmUser_MenuItemIndex");
        }
        #endregion

        #region User Depertment
        public async Task<ActionResult> VmUser_DepartmentIndex()
        {
            //Oss.Hrms.Models.ViewModels.User.VmUser_User u = (Oss.Hrms.Models.ViewModels.User.VmUser_User)Session["One"];
            //if (u == null)
            //{
            //    return RedirectToAction("Login", "Home");
            //}
            VmUser_Department = new VmUser_Department();
            await Task.Run(() => VmUser_Department.InitialDataLoad());
            return View(VmUser_Department);
        }

        public ActionResult VmUser_DepartmentCreate()
        {
            //Oss.Hrms.Models.ViewModels.User.VmUser_User u = (Oss.Hrms.Models.ViewModels.User.VmUser_User)Session["One"];
            //if (u == null)
            //{
            //    return RedirectToAction("Login", "Home");
            //}

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmUser_DepartmentCreate(VmUser_Department a)
        {
            int userMenuItemID = 0;

            if (ModelState.IsValid)
            {
                await Task.Run(() => a.Add(UserID));



                return RedirectToAction("VmUser_DepartmentIndex");
            }
            return RedirectToAction("VmUser_DepartmentIndex");
        }

        [HttpGet]
        public async Task<ActionResult> VmUser_DepartmentEdit(int? id)
        {
            //Oss.Hrms.Models.ViewModels.User.VmUser_User u = (Oss.Hrms.Models.ViewModels.User.VmUser_User)Session["One"];
            //if (u == null)
            //{
            //    return RedirectToAction("Login", "Home");
            //}
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            VmUser_Department = new VmUser_Department();
            await Task.Run(() => VmUser_Department.SelectSingle(id.Value));

            if (VmUser_Department.User_Department == null)
            {
                return HttpNotFound();
            }
            return View(VmUser_Department);
        }
        // To update Buyer Information. -Post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VmUser_DepartmentEdit(VmUser_Department VmUser_Department)
        {

            if (ModelState.IsValid)
            {
                await Task.Run(() => VmUser_Department.Edit(0));
                return RedirectToAction("VmUser_DepartmentIndex");
            }
            return View(VmUser_MenuItem);
        }
        public ActionResult VmUser_DepartmentDelete(int id)
        {
            //Oss.Hrms.Models.ViewModels.User.VmUser_User u = (Oss.Hrms.Models.ViewModels.User.VmUser_User)Session["One"];
            //if (u == null)
            //{
            //    return RedirectToAction("Login", "Home");
            //}
            VmUser_Department b = new VmUser_Department();
            b.User_Department.ID = id;
            b.Delete(0);
            return RedirectToAction("VmUser_DepartmentIndex");
        }
        #endregion

        #region Get Image For HRMS
        public byte[] GetImage(string imgName)
        {
            byte[] data;

            string imgPath = string.Empty;
            string img = "Profile1.jpg";

            imgPath = Path.Combine(Server.MapPath("~/Assets/Images/EmpImages/"), imgName);

            if (System.IO.File.Exists(imgPath))
            {
                BitmapImage bitImg = new BitmapImage(new Uri(imgPath));
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bitImg));
                using (MemoryStream ms = new MemoryStream())
                {
                    encoder.Save(ms);
                    data = ms.ToArray();
                    return data;
                }
            }
            else
            {
                imgPath = Path.Combine(Server.MapPath("~/Assets/Images/EmpImages/"), img);
                BitmapImage bitImg = new BitmapImage(new Uri(imgPath));
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bitImg));
                using (MemoryStream ms = new MemoryStream())
                {
                    encoder.Save(ms);
                    data = ms.ToArray();
                    return data;
                }
            }
        }
        #endregion

        #region AssignUser
        public async Task<ActionResult> VmAssignUser(int id)
        {
            //Oss.Hrms.Models.ViewModels.User.VmUser_User u = (Oss.Hrms.Models.ViewModels.User.VmUser_User)Session["One"];
            //if (u == null)
            //{
            //    return RedirectToAction("Login", "Home");
            //}
            VmUser_User = new VmUser_User();
            await Task.Run(() => VmUser_User = VmUser_User.GetEmployeeRole(id));
            await Task.Run(() => VmUser_User.GetRoleDetailsById(VmUser_User.User_User.User_RoleFK == null ? 0 : (int)VmUser_User.User_User.User_RoleFK));

            return View(VmUser_User);
        }
        #endregion
    }
}