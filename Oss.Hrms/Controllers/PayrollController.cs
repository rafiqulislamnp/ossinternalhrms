﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Oss.Hrms.Models;
using Oss.Hrms.Models.Entity;
using Oss.Hrms.Models.Entity.Common;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;
using Oss.Hrms.Models.ViewModels.HRMS;
using System.Globalization;
using CrystalDecisions.CrystalReports.Engine;
using Oss.Hrms.Helper;
using Oss.Hrms.Models.ViewModels.Reports;
using Oss.Hrms.Models.ViewModels.Bonus;
using Oss.Hrms.Models.ViewModels.Salary;
using CrystalDecisions.Shared;
using System.Data.Entity.Infrastructure;

namespace Oss.Hrms.Controllers
{
    public class PayrollController : Controller
    {
        DropDownData d = new DropDownData();
        HrmsContext db = new HrmsContext();
        private VM_HRMS_Payroll VM_HRMS_Payroll;
        VMEmployee VMEmployees;
        CurrentUser user;

        public PayrollController()
        {
            SessionHandler sessionHandler = new SessionHandler();
            sessionHandler.Adjust();
            VMEmployees = new VMEmployee();
            this.user = sessionHandler.CurrentUser;
        }

        public ActionResult Index()
        {
            return View();
        }

        #region ManagePayroll
        public async Task<ActionResult> ManagePayroll()
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            await Task.Run(() => model.GetPayrollList());
            return View(model);
        }

        [HttpGet]
        public ActionResult CreatePayroll()
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Monthly = new SelectList(d.GetMonthlyList(), "Value", "Text");
            //DateTime date = DateTime.Now;
            //int totalDay = DateTime.DaysInMonth(date.Year, date.Month-1);

            //DateTime FromDate = new DateTime(date.Year, date.Month-1, 1);
            //DateTime ToDate = new DateTime(date.Year, date.Month-1, totalDay);

            //VM_HRMS_Payroll Payroll = new VM_HRMS_Payroll();
            //Payroll.Monthly = 1;
            //Payroll.HasFastivalBonus = 0;
            //Payroll.FromDate = FromDate;
            //Payroll.ToDate = ToDate;

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> CreatePayroll(VM_HRMS_Payroll model)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Monthly = new SelectList(d.GetMonthlyList(), "Value", "Text");
            model.Entry_By = user.ID.ToString();
            model.Entry_Date = DateTime.Now;

            await Task.Run(() => model.CreatePayroll1(model));
            if (string.IsNullOrEmpty(model.PayrollStatus))
            {
                await Task.Run(() => model.Save());

            }
            return RedirectToAction("ManagePayroll", "Payroll");
        }

        [HttpGet]
        public async Task<ActionResult> ClosePayroll(int id)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            await Task.Run(() => model.ClosePayroll(id));
            await Task.Run(() => model.Save());

            return RedirectToAction("ManagePayroll", "Payroll");
        }

        #endregion

        #region PayrollProcess
        [HttpGet]
        public async Task<ActionResult> ProcessPayroll(int id)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.CardTitleList = new SelectList(d.GetCardTitleList(), "Value", "Text");
            ViewBag.UnitList = new SelectList(d.GetUnitList(), "Value", "Text");
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            await Task.Run(() => model.GetPayrollDetails(id));
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ProcessPayroll(VM_HRMS_Payroll model)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            model.Entry_By = user.ID.ToString();
            model.Update_By = user.ID.ToString();
            model.Entry_Date = DateTime.Now;
            model.Update_Date = DateTime.Now;

            await Task.Run(() => model.GetProcessPayroll(model));
            await Task.Run(() => model.Save());
            return RedirectToAction("PayrollDetails", "Payroll", new { id = model.PayRollID });
        }

        public ActionResult PreviousPayrollView()
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Month = new SelectList(d.GetOpenPayrollList(), "Value", "Text");
            return View();
        }

        [HttpPost]
        public ActionResult PreviousPayrollView(VM_HRMS_Payroll VM_HRMS_Payroll)
        {
            string id = VM_HRMS_Payroll.Month.ToString();
            try
            {
                return RedirectToAction("PayrollDetails", new { ID = id });
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public async Task<ActionResult> PayrollDetails(int id)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.SectionList = new SelectList(d.GetSectionList(), "Value", "Text");
            VM_HRMS_Payroll = new VM_HRMS_Payroll();
            await Task.Run(() => VM_HRMS_Payroll.ListVmPayroll(id));
            //await Task.Run(() => VM_HRMS_Payroll.GetPayrollList(id));
            return View(VM_HRMS_Payroll);
        }

        [HttpGet]
        public async Task<ActionResult> VmPayrollDetails(string id)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int PayrollID = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("X")), out PayrollID);
            int EmployeeID = 0;
            id = id.Replace(PayrollID.ToString() + "X", "");
            int.TryParse(id, out EmployeeID);

            VM_HRMS_Payroll = new VM_HRMS_Payroll();
            VM_HRMS_Payroll.PayRollID = PayrollID;
            await Task.Run(() => VM_HRMS_Payroll.ListVmPayrollDetails(PayrollID, EmployeeID));

            return View(VM_HRMS_Payroll);
        }

        [HttpGet]
        public async Task<ActionResult> VmPayrollEdit(string id)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int PayrollID = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("X")), out PayrollID);
            int EmployeeID = 0;
            id = id.Replace(PayrollID.ToString() + "X", "");
            int.TryParse(id, out EmployeeID);

            VM_HRMS_Payroll = new VM_HRMS_Payroll();
            VM_HRMS_Payroll.EmployeeID = EmployeeID;
            VM_HRMS_Payroll.PayRollID = PayrollID;
            VM_HRMS_Payroll.Update_By = user.ID.ToString();
            VM_HRMS_Payroll.Update_Date = DateTime.Today;
            await Task.Run(() => VM_HRMS_Payroll.GetUpdateEmployeePayroll(VM_HRMS_Payroll));
            return RedirectToAction("PayrollDetails", "Payroll", new { id = VM_HRMS_Payroll.PayRollID });
        }
        #endregion

        #region Payslip
        [HttpGet]
        public ActionResult PaySlip()
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.PayrollList = new SelectList(d.GetPayrollList(), "Value", "Text");
            ViewBag.SectionList = new SelectList(d.GetSectionList(), "Value", "Text");
            return View();
        }

        [HttpPost]
        public ActionResult PaySlip(VM_HRMS_Payroll model)
        {
            DtPayroll ds = new DtPayroll();

            ReportClass cr = new ReportClass();
            if (model.SectionID > 0)
            {
                ds = model.LoadPaySlip(model);
            }
            else
            {
                ds = model.LoadPaySlipAll(model);
            }
            cr.FileName = Server.MapPath("~/Views/Reports/PaySlip.rpt");
            cr.Load();
            cr.SetDataSource(ds);
            cr.SummaryInfo.ReportTitle = "PaySlip";
            Stream stream = cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }

        public ActionResult GeneratePaySlip(VM_HRMS_Payroll model)
        {
            DtPayroll ds = new DtPayroll();

            ReportClass cr = new ReportClass();
            if (model.SectionID > 0)
            {
                ds = model.LoadPaySlip(model);
            }
            else
            {
                ds = model.LoadPaySlipAll(model);
            }
            cr.FileName = Server.MapPath("~/Views/Reports/PaySlip.rpt");
            cr.Load();
            cr.SetDataSource(ds);
            cr.SummaryInfo.ReportTitle = "PaySlip";
            Stream stream = cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }


        public ActionResult PaySlipByEmployeeID(string id)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int PayrollID = 0;
            int.TryParse(id.Substring(0, id.LastIndexOf("X")), out PayrollID);
            int EmployeeID = 0;
            id = id.Replace(PayrollID.ToString() + "X", "");
            int.TryParse(id, out EmployeeID);

            VM_HRMS_Payroll = new VM_HRMS_Payroll();
            VM_HRMS_Payroll.EmployeeID = EmployeeID;
            VM_HRMS_Payroll.ID = PayrollID;

            DtPayroll ds = new DtPayroll();
            ds = VM_HRMS_Payroll.LoadPaySlip(VM_HRMS_Payroll);
            ReportClass cr = new ReportClass();
            cr.FileName = Server.MapPath("~/Views/Reports/PaySlip.rpt");
            cr.Load();
            cr.SetDataSource(ds);
            cr.SummaryInfo.ReportTitle = "PaySlip";
            Stream stream = cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }
        #endregion

        #region CreateForFutureNeed
        public ActionResult EmployeeSalaryCard()
        {
            ViewBag.Emp = new SelectList(d.GetActiveEmployee(), "Value", "Text");
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            model.VMEmployee = new VMEmployee();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> EmployeeSalaryCard(VM_HRMS_Payroll model)
        {
            ViewBag.Emp = new SelectList(d.GetActiveEmployee(), "Value", "Text");
            VMEmployee VMEmployee = new VMEmployee();

            VM_HRMS_Payroll VM_HRMS_Payroll = new VM_HRMS_Payroll();
            VM_HRMS_Payroll.VMEmployee = new VMEmployee();
            await Task.Run(() => VM_HRMS_Payroll.VMEmployee = VMEmployee.GetEmployeeDetails(model.EmpIDFK));
            await Task.Run(() => VM_HRMS_Payroll.GetEmployeeEODRecord(model.EmpIDFK));
            return View(VM_HRMS_Payroll);
        }

        [HttpPost]
        public async Task<ActionResult> EmployeeSalaryCardSave(VM_HRMS_Payroll model)
        {
            try
            {
                ViewBag.Emp = new SelectList(d.GetActiveEmployee(), "Value", "Text");

                //decimal totalSalary = decimal.Zero;
                var all = db.HrmsEodRecords.Where(a => a.EmployeeId == model.EmpIDFK && a.Status == true);
                if (model.EODRecordList.Any())
                {
                    //totalSalary = decimal.Zero;
                    foreach (var v in model.EODRecordList)
                    {
                        var checkAmount = all.FirstOrDefault(a => a.ID == v.EODRecordID);
                        if (all.Any(a => a.ID == v.EODRecordID))
                        {
                            //totalSalary += v.Amount;
                            if (checkAmount.ActualAmount != v.Amount)
                            {

                                var update = db.HrmsEodRecords.FirstOrDefault(a => a.ID == checkAmount.ID);
                                update.Status = false;
                                HRMS_EodRecord record = new HRMS_EodRecord()
                                {
                                    EmployeeId = v.EmployeeID,
                                    Eod_RefFk = v.EODRefFK,
                                    ActualAmount = v.Amount,
                                    Status = true,
                                    EffectiveDate = DateTime.Now,
                                    Update_By = "1",
                                    Entry_Date = DateTime.Now,
                                    Entry_By = "1"
                                };
                                db.HrmsEodRecords.Add(record);
                            }
                        }
                    }
                }
                await db.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;

            }
            return RedirectToAction("EmployeeSalaryCard", "Payroll");
        }

        [HttpGet]
        public ActionResult EmployeeBasicPay()
        {

            ViewBag.Designation = new SelectList(d.GetDesignationList(), "Value", "Text");
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            model.ListEmployee = new List<VMEmployee>();

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> EmployeeBasicPay(VM_HRMS_Payroll model)
        {
            ViewBag.Designation = new SelectList(d.GetDesignationList(), "Value", "Text");
            await Task.Run(() => model.ListEmployee = VMEmployees.GetEmployeeByDesignationID(model.ID, model.Amount));
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> EmployeeBasicPaySave(VM_HRMS_Payroll model)
        {
            try
            {
                ViewBag.Designation = new SelectList(d.GetDesignationList(), "Value", "Text");

                VM_HRMS_Payroll VM_HRMS_Payroll = new VM_HRMS_Payroll();
                VM_HRMS_Payroll.ListEmployee = new List<VMEmployee>();
                await Task.Run(() => VM_HRMS_Payroll.ListEmployee = VMEmployees.GetEmployeeByDesignationID(model.ID, model.Amount));
            }
            catch (Exception ex)
            {
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Error Happened." + ex.Message;

            }
            return View(VM_HRMS_Payroll);
        }
        #endregion
        #region ManageSalary
        [HttpGet]
        public async Task<ActionResult> ManageSalary()
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            await Task.Run(() => model.GetSalaryList());
            return View(model);
        }

        [HttpGet]
        public ActionResult CreateSalary()
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Emp = new SelectList(d.GetSalaryCreateEmployeeList(), "Value", "Text");
            ViewBag.OTAllowance = new SelectList(d.GetOTEligibleList(), "Value", "Text");
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            model.OTAllow = null;
            model.Default = true;
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> CreateSalary(VM_HRMS_Payroll model)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Emp = new SelectList(d.GetActiveEmployee(), "Value", "Text");
            ViewBag.OTAllowance = new SelectList(d.GetOTEligibleList(), "Value", "Text");
            model.Entry_By = user.ID.ToString();
            model.Entry_Date = DateTime.Now;

            await Task.Run(() => model.GetEmployeeInfo(model.EmployeeID));
            await Task.Run(() => model.GetCreateSalary(model));
            await Task.Run(() => model.Save());

            return RedirectToAction("ManageSalary", "Payroll");
        }

        [HttpGet]
        public async Task<ActionResult> UpdateSalary(int Id)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.OTAllowance = new SelectList(d.GetOTEligibleList(), "Value", "Text");
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            await Task.Run(() => model.GetEmployeeInfo(Id));
            await Task.Run(() => model.GetEmployeeGrossSalary(Id));

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateSalary(VM_HRMS_Payroll model)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            model.Update_By = user.ID.ToString();
            model.Update_Date = DateTime.Now;

            await Task.Run(() => model.GetUpdateSalary(model));
            await Task.Run(() => model.Save());

            return RedirectToAction("ManageSalary", "Payroll");
        }

        public ActionResult DeleteSalary(int Id)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VM_HRMS_Payroll = new VM_HRMS_Payroll();
            VM_HRMS_Payroll.GetEmployeeSalaryDelete(Id);

            return RedirectToAction("ManageSalary", "Payroll");
        }

        #endregion

        #region NightAndHolydayBill
        public async Task<ActionResult> ManageBill()
        {
            if (user != null)
            {
                VM_HRMS_Payroll model = new VM_HRMS_Payroll();
                await Task.Run(() => model.GetUnassignBill());
                return View(model);
            }
            return RedirectToAction("Login", "Account");
        }

        public async Task<ActionResult> ManageAssignedBill()
        {
            if (user != null)
            {
                VM_HRMS_Payroll model = new VM_HRMS_Payroll();
                await Task.Run(() => model.GetBillList());
                return View(model);
            }
            return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        public ActionResult CreateBill()
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.DesignationList = new SelectList(d.GetDesignationList(), "Value", "Text");
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> CreateBill(VM_HRMS_Payroll model)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            model.Entry_By = user.ID.ToString();
            model.Entry_Date = DateTime.Now;

            await Task.Run(() => model.GetCreateNightBill(model));
            await Task.Run(() => model.Save());

            return RedirectToAction("ManageBill", "Payroll");
        }

        [HttpGet]
        public async Task<ActionResult> UpdateBill(int Id)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            await Task.Run(() => model.GetEmployeeInfo(Id));
            await Task.Run(() => model.GetEmployeeBill(Id));

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateBill(VM_HRMS_Payroll model)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            model.Update_By = user.ID.ToString();
            model.Update_Date = DateTime.Now;

            await Task.Run(() => model.GetUpdateNightBill(model));
            await Task.Run(() => model.Save());

            return RedirectToAction("ManageBill", "Payroll");
        }

        public ActionResult DeleteBill(int Id)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VM_HRMS_Payroll = new VM_HRMS_Payroll();
            VM_HRMS_Payroll.GetDeleteNightBill(Id);

            return RedirectToAction("ManageBill", "Payroll");
        }
        #endregion

        #region ManageBonus
        [HttpGet]
        public async Task<ActionResult> ManageBonus()
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VMBonus model = new VMBonus();
            await Task.Run(() => model.GetBonusList());
            return View(model);
        }

        [HttpGet]
        public ActionResult CreateBonus()
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            //ViewBag.DesignationList = new SelectList(d.GetNightBillCreateDesignationList(), "Value", "Text");
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> CreateBonus(VMBonus model)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            model.Entry_By = user.ID.ToString();
            model.Entry_Date = DateTime.Now;
            await Task.Run(() => model.GetCreateBonus(model));

            return RedirectToAction("ManageBonus", "Payroll");
        }

        [HttpGet]
        public async Task<ActionResult> UpdateBonus(int Id)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.OTAllowance = new SelectList(d.GetOTEligibleList(), "Value", "Text");
            VM_HRMS_Payroll model = new VM_HRMS_Payroll();
            await Task.Run(() => model.GetEmployeeInfo(Id));
            await Task.Run(() => model.GetEmployeeGrossSalary(Id));

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateBonus(VM_HRMS_Payroll model)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            model.Update_By = user.ID.ToString();
            model.Update_Date = DateTime.Now;

            await Task.Run(() => model.GetUpdateSalary(model));
            await Task.Run(() => model.Save());

            return RedirectToAction("ManageBonus", "Payroll");
        }

        [HttpGet]
        public async Task<ActionResult> ProcessBonus(int id)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.CardTitleList = new SelectList(d.GetCardTitleList(), "Value", "Text");
            ViewBag.UnitList = new SelectList(d.GetUnitList(), "Value", "Text");
            //ViewBag.PayrollList = new SelectList(d.GetOpenPayrollList(), "Value", "Text");
            //ViewBag.Fastival = new SelectList(d.GetFastivalList(), "Value", "Text");
            VMBonus model = new VMBonus();
            await Task.Run(() => model.GetBonusInfo(id));
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ProcessBonus(VMBonus model)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            model.Entry_By = user.ID.ToString();
            model.Update_By = user.ID.ToString();
            model.Entry_Date = DateTime.Now;
            model.Update_Date = DateTime.Now;

            await Task.Run(() => model.GetProcessBonus(model));
            await Task.Run(() => model.Save());
            return RedirectToAction("BonusDetails", "Payroll", new { id = model.BonusId });

        }

        [HttpGet]
        public async Task<ActionResult> BonusDetails(int id)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VMBonus model = new VMBonus();
            await Task.Run(() => model.GetEmployeeBonus(id));
            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> CloseBonus(int id)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            VMBonus model = new VMBonus();
            await Task.Run(() => model.CloseBonus(id));
            await Task.Run(() => model.Save());

            return RedirectToAction("ManageBonus", "Payroll");
        }

        #endregion

        #region SalaryIncrement
        [HttpGet]
        public ActionResult SalaryIncrement()
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.CardTitleList = new SelectList(d.GetCardTitleList(), "Value", "Text");
            VMSalaryIncrement model = new VMSalaryIncrement();
            model.DataList = new List<VMSalaryIncrement>();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> SalaryIncrement(VMSalaryIncrement model)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.CardTitleList = new SelectList(d.GetCardTitleList(), "Value", "Text");
            model.DataList = new List<VMSalaryIncrement>();
            await Task.Run(() => model.GetIncrementListByTitleWise(model.CardTitleId));
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> CreateIncrementSalary(VMSalaryIncrement model)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.CardTitleList = new SelectList(d.GetCardTitleList(), "Value", "Text");
            model.Entry_By = user.ID.ToString();
            model.Entry_Date = DateTime.Now;
            await Task.Run(() => model.CreateYearlyIncrement(model));
            await Task.Run(() => model.Save());
            return RedirectToAction("SalaryIncrement");
        }

        [HttpGet]
        public ActionResult IncrementList()
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.CardTitleList = new SelectList(d.GetCardTitleList(), "Value", "Text");
            VMSalaryIncrement model = new VMSalaryIncrement();
            model.DataList = new List<VMSalaryIncrement>();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> IncrementList(VMSalaryIncrement model)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.CardTitleList = new SelectList(d.GetCardTitleList(), "Value", "Text");
            model.DataList = new List<VMSalaryIncrement>();
            await Task.Run(() => model.GetIncrementedListByTitleWise(model.CardTitleId));
            return View(model);
        }

        #endregion

        #region Report

        [HttpGet]
        public ActionResult GetSalarySheet(int id)
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.SectionList = new SelectList(d.GetSectionList(), "Value", "Text");
            ViewBag.StatusList = new SelectList(d.GetEmployeePayrollStatus(), "Value", "Text");
            VM_HRMS_Payroll Payroll = new VM_HRMS_Payroll();
            Payroll.PayRollID = id;
            return View(Payroll);
        }

        [HttpPost]
        public ActionResult GetSalarySheet(VM_HRMS_Payroll model)
        {
            DsPayrollReport ds = new DsPayrollReport();
            VMReportPayroll vmmodel = new VMReportPayroll();
            ReportClass cr = new ReportClass();
            if (model.PayrollStatusId == 1)
            {
                ds = vmmodel.GetSalarySheetForResign(model.SectionID.Value, model.PayRollID);
            }
            else if (model.PayrollStatusId == 2)
            {
                ds = vmmodel.GetSalarySheetForLefty(model.SectionID.Value, model.PayRollID);
            }
            else
            {
                ds = vmmodel.GetSalarySheet(model.SectionID.Value, model.PayRollID);
            }

            cr.FileName = Server.MapPath("~/Views/Reports/CrpSalarySheet.rpt");
            cr.SummaryInfo.ReportTitle = "SalarySheet";
            cr.Load();
            cr.SetDataSource(ds);
            Stream stream = cr.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }

        #endregion

        #region OSSPayroll
        public ActionResult EmployeeSalary()
        {
            var data = (from t1 in db.EmployeePayroll
                        join t2 in db.Employees on t1.EmployeeFK equals t2.ID
                        join t3 in db.Departments on t2.Department_Id equals t3.ID
                        join t4 in db.Designations on t2.Designation_Id equals t4.ID
                        where t1.Status == true && t1.IsNew == true
                        select new VmEmployeePayroll
                        {
                            EmployeePayroll = t1,
                            Employee = t2,
                            Department = t3,
                            Designation = t4
                        }).ToList();
            return View(data);
        }

        public ActionResult CreateEmployeeSalary()
        {
            if (user == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Emp = new SelectList(d.GetActiveEmployeeListforForm(), "Value", "Text");
            return View();
        }
        [HttpPost]
        public ActionResult CreateEmployeeSalary(EmployeePayroll model)
        {
            db = new HrmsContext();
            db.EmployeePayroll.Add(model);
            db.SaveChanges();
            return RedirectToAction("EmployeeSalary");
        }

        [HttpPost]
        public ActionResult SalaryIncreament(int empid, decimal grossamt, DateTime affdate)
        {
            var last = db.EmployeePayroll.Where(x => x.EmployeeFK == empid && x.IsNew == true && x.Status == true).Single();
            last.IsNew = false;
            last.Status = false;

            db.SaveChanges();
            // to remove  same key already exists in the ObjectStateManager
            ((IObjectContextAdapter)db).ObjectContext.Detach(last);

            var model = new EmployeePayroll
            {
                EmployeeFK = empid,
                GrossAmount = grossamt,
                DateOfPromotion = affdate,
                Status = true,
                Entry_By = user.ID.ToString(),
                Entry_Date = DateTime.Now
            };
            db.EmployeePayroll.Add(model);
            db.SaveChanges();
            return RedirectToAction("EmployeeSalary");
        }

        public ActionResult DeleteEmployeeSalary(int Id)
        {
            var last = db.EmployeePayroll.Where(x => x.ID == Id && x.IsNew == true && x.Status == true).Single();
            last.Status = false;
            db.SaveChanges();
            return RedirectToAction("EmployeeSalary");
        }

        //Salary Process

        public ActionResult ManageEmployeePayroll()
        {
            db = new HrmsContext();
            ViewBag.Employee = new SelectList(d.GetDepartmentListForPayroll(), "Value", "Text");
            ViewBag.Payroll = new SelectList(d.GetPayrollSettings(), "Value", "Text");
            var data = (from t1 in db.PayrollMaster
                        where t1.Status == true
                        select new VmPayrollManage
                        {
                            PayrollMaster = t1,
                            Departments = (from t2 in db.PayrollDepartment
                                           join t3 in db.Departments on t2.DepartmentFk equals t3.ID
                                           where t2.PayrollMasterFk == t1.ID && t2.Status == true && t3.Status == true
                                           select t3).ToList(),
                            TotalMemebers = db.PayrollDetails.Where(x => x.PayrollMasterFk == t1.ID && t1.Status == true).Count()
                        }).OrderBy(x => x.PayrollMaster.PayrollStatus).ThenByDescending(x => x.PayrollMaster.ID).ToList();
            return View(data);
        }

        [HttpPost]
        public ActionResult CreateEmployeePayroll(PayrollMaster model)
        {
            db = new HrmsContext();
            model.PayrollStatus = 1;
            model.Entry_By = user.ID.ToString();
            db.PayrollMaster.Add(model);
            db.SaveChanges();

            foreach (var dept in model.DepartmentFk)
            {
                var payrolldepartment = new PayrollDepartment
                {
                    DepartmentFk = dept,
                    PayrollMasterFk = model.ID,
                    Entry_By = user.ID.ToString(),
                    Status = true
                };
                db.PayrollDepartment.Add(payrolldepartment);
                db.SaveChanges();
                var process = new VmPayrollManage();
                process.PayrollProcess(model, dept);
            }


            return RedirectToAction("ManageEmployeePayroll");
        }

        public ActionResult EmployeePayrollDelete(int id)
        {
            db = new HrmsContext();
            var master = new PayrollMaster() { ID = id, Status = false };
            db.PayrollMaster.Attach(master);
            db.Entry(master).Property(x => x.Status).IsModified = true;

            var details = db.PayrollDetails.Where(f => f.PayrollMasterFk == id).ToList();
            details.ForEach(a => a.Status = false);
            db.SaveChanges();
            return RedirectToAction("ManageEmployeePayroll");
        }

        public ActionResult EmployeePayrollDetails(int id)
        {
            db = new HrmsContext();
            var data = (from t1 in db.PayrollDetails
                        join t2 in db.Employees on t1.EmployeeFK equals t2.ID
                        join t3 in db.Departments on t2.Department_Id equals t3.ID
                        join t4 in db.Designations on t2.Designation_Id equals t4.ID
                        join t5 in db.PayrollMaster on t1.PayrollMasterFk equals t5.ID
                        where t1.PayrollMasterFk == id
                        orderby t2.EmployeeIdentity
                        select new VmPayrollDetails
                        {
                            PayrollDetails = t1,
                            Employee = t2,
                            Department = t3,
                            Designation = t4,
                            PayrollMaster = t5
                        }).ToList();
            return View(data);
        }

        public ActionResult PayrollAdjustement(int DetailsId, int Masterid, decimal OtherDeduction, decimal OtherAdjustment, decimal ExpenseAdjustment, string Remarks)
        {
            db = new HrmsContext();
            var details = db.PayrollDetails.Single(x => x.ID == DetailsId);
            details.OtherDeduction = OtherDeduction;
            details.OtherAdjustment = OtherAdjustment;
            details.ExpenseAdjustment = ExpenseAdjustment;
            details.TotalPayble = details.TotalPayble + OtherAdjustment + ExpenseAdjustment;
            details.TotalDeduction = details.TotalDeduction + OtherDeduction;
            details.Remarks = Remarks;
            details.TotalAmountToBePaid = details.TotalAmountToBePaid + OtherAdjustment + ExpenseAdjustment - OtherDeduction;
            db.SaveChanges();
            return RedirectToAction("EmployeePayrollDetails", new { id = Masterid });
        }

        public ActionResult SpecificEmployeePayrollDetails(string sid)
        {
            int id = Convert.ToInt32(EncryptDecrypt.Decrypt(sid));
            db = new HrmsContext();
            var data = (from t1 in db.PayrollDetails
                        join t2 in db.Employees on t1.EmployeeFK equals t2.ID
                        join t3 in db.Departments on t2.Department_Id equals t3.ID
                        join t4 in db.Designations on t2.Designation_Id equals t4.ID
                        join t5 in db.PayrollMaster on t1.PayrollMasterFk equals t5.ID
                        join t6 in db.EmployeePayroll on t1.EmployeeFK equals t6.EmployeeFK
                        join t7 in db.PayrollSettingMaster on t5.PayrollSettingFk equals t7.ID
                        join t8 in db.PayrollSetting on t7.ID equals t8.PayrollSettingMasterFk
                        where t1.ID == id && t6.IsNew == true && t6.Status == true && t7.Status == true
                        select new VmPayrollDetails
                        {
                            PayrollDetails = t1,
                            Employee = t2,
                            Department = t3,
                            Designation = t4,
                            PayrollMaster = t5,
                            PayrollSetting = t8,
                            GrossSalary = t6.GrossAmount
                        }).FirstOrDefault();
            return View(data);
        }

        public ActionResult SelaryReprocess(int did, int mid, DateTime fromdate, DateTime todate)
        {
            var process = new VmPayrollManage();
            process.SalaryReprocess(did, mid, fromdate, todate);
            return RedirectToAction("EmployeePayrollDetails", new { id = mid });
        }
        public ActionResult ChangePayrollStatus(int payrollid, int status)
        {
            db = new HrmsContext();
            var payroll = new PayrollMaster() { ID = payrollid, PayrollStatus = status };
            db.PayrollMaster.Attach(payroll);
            db.Entry(payroll).Property(x => x.PayrollStatus).IsModified = true;
            db.SaveChanges();
            return RedirectToAction("ManageEmployeePayroll");
        }

        public ActionResult PayrollSettingIndex()
        {
            db = new HrmsContext();
            var data = (from t1 in db.PayrollSettingMaster
                        where t1.Status == true
                        select new VmPayrollSetting
                        {
                            PayrollSettingMaster = t1,
                            PayrollSetting = db.PayrollSetting.Where(x => x.PayrollSettingMasterFk == t1.ID && x.Status == true).FirstOrDefault()
                        }).ToList();
            return View(data);
        }

        public ActionResult CreatePayrollSetting(PayrollSetting model, string reference, string remarks)
        {
            db = new HrmsContext();
            var master = new PayrollSettingMaster
            {
                Reference = reference,
                Remarks = remarks,
                Status = true,
                Entry_By = user.ID.ToString(),
                Entry_Date = DateTime.Today
            };
            db.PayrollSettingMaster.Add(master);
            db.SaveChanges();

            model.PayrollSettingMasterFk = master.ID;
            db.PayrollSetting.Add(model);
            db.SaveChanges();
            return RedirectToAction("PayrollSettingIndex");
        }

        public ActionResult RemoveSetting(int id)
        {
            db = new HrmsContext();
            var master = new PayrollSettingMaster() { ID = id, Status = false };
            db.PayrollSettingMaster.Attach(master);
            db.Entry(master).Property(x => x.Status).IsModified = true;
            db.SaveChanges();
            return RedirectToAction("PayrollSettingIndex");
        }
        #endregion
    }
}

public class EODReference
{
    public int EODRefID { get; set; }

    public int Priority { get; set; }

    public string SalaryHead { get; set; }

    public decimal Amount { get; set; }

    public string Note { get; set; }

    public int EmpID { get; set; }
}