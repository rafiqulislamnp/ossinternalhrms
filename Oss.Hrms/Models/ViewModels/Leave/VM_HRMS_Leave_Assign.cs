﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.Leave
{
    public class VM_HRMS_Leave_Assign:RootModel
    {

        private HrmsContext db = new HrmsContext();

        [DisplayName("Employee")]
        //[StringLength(17, MinimumLength = 5, ErrorMessage = "Shift Name should be minimum 1 characters and Maximum 50 characters")]
        //[Required(ErrorMessage = "Please Shift Name")]
        public int EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        // public HRMS_Employee HrmsEmployee { get; set; }
        [DisplayName("Leave Name")]
        //[StringLength(17, MinimumLength = 5, ErrorMessage = "Shift Name should be minimum 1 characters and Maximum 50 characters")]
        [Required(ErrorMessage = "Please Laeve Name")]
        public int LeaveTypeId { get; set; }
        [ForeignKey("LeaveTypeId")]


        [DisplayName("Year")]
        //[StringLength(17, MinimumLength = 5, ErrorMessage = "Shift Name should be minimum 1 characters and Maximum 50 characters")]
        [Required(ErrorMessage = "Please Select Year")]
        [StringLength(10)]
        public string year { get; set; }

        [DisplayName("Leave Amount")]
        //[StringLength(17, MinimumLength = 5, ErrorMessage = "Shift Name should be minimum 1 characters and Maximum 50 characters")]
        [Required(ErrorMessage = "Please Enter Leave Amount")]
       // [StringLength(10)]
        public decimal Total { get; set; }
        public decimal Taken { get; set; }
        public decimal Remaining { get; set; }
        [DisplayName("Name")]
        public string Name { get; set; }
        [DisplayName("Employee ID")]
        public string EmployeeIdentity { get; set; }

        [DisplayName("Business Unit")]
        //[StringLength(17, MinimumLength = 5, ErrorMessage = "Shift Name should be minimum 1 characters and Maximum 50 characters")]
        //[Required(ErrorMessage = "Please Shift Name")]
        public int BU_Id { get; set; }
        [DisplayName("Business Unit")]
        public string BusinessUnitName { get; set; }
        [DisplayName("Card Title")]
        public string CardTitle { get; set; }

        [DisplayName("Department Name")]
        public int Dept_ID { get; set; }
        [DisplayName("Department Name")]
        public string Dept_Name { get; set; }

        [DisplayName("Section Name")]
        public int Section_ID { get; set; }
        [DisplayName("Section Name")]
        public string Section_Name { get; set; }

        [DisplayName("Unit Name")]
        public int Unit_ID { get; set; }
        [DisplayName("Unit Name")]
        public string Unit_Name { get; set; }
        [DisplayName("Leave Name")]
        public string LeaveName { get; set; }

        public List<string> EmployeeIds { get; set; }

        public IEnumerable<VM_HRMS_Leave_Assign> DataListhrHrmsLeaveAssigns { get; set; }


        public void SearchEmployeeLeaveInformation(VM_HRMS_Leave_Assign vm)
        {
            //var dt1 = fromdate.ToString("yyyy-MM-dd");
            //var dt2 = todate.ToString("yyyy-MM-dd");
            var a = new List<VM_HRMS_Leave_Assign>();
            if (vm.LeaveTypeId == 1000 && vm.EmployeeId== 10000000)
            {
                a = (from t1 in db.HrmsLeaveAssigns
                    join t2 in db.Employees on t1.EmployeeId equals t2.ID
                    join t3 in db.HrmsLeaveTypes on t1.LeaveTypeId equals t3.ID
                    where (t1.Year == vm.year) && t1.Status==true
                    select new VM_HRMS_Leave_Assign()
                    {
                        ID = t1.ID,
                        EmployeeId = t1.EmployeeId,
                        EmployeeIdentity = t2.EmployeeIdentity,
                        CardTitle = t2.CardTitle,
                        Name = t2.Name,
                        LeaveTypeId = t1.LeaveTypeId,
                        LeaveName = t3.Leave_Name,
                        year = t1.Year,
                        Total = t1.Total,
                        Taken = t1.Taken,
                        Remaining = t1.Remaining,
                    }).AsEnumerable().ToList();
            }
            if (vm.LeaveTypeId == 1000 && vm.EmployeeId != 10000000)
            {
                a = (from t1 in db.HrmsLeaveAssigns
                    join t2 in db.Employees on t1.EmployeeId equals t2.ID
                    join t3 in db.HrmsLeaveTypes on t1.LeaveTypeId equals t3.ID
                    where (t1.Year == vm.year && t1.EmployeeId==vm.EmployeeId) && t1.Status == true
                     select new VM_HRMS_Leave_Assign()
                    {
                        ID = t1.ID,
                        EmployeeId = t1.EmployeeId,
                        EmployeeIdentity = t2.EmployeeIdentity,
                        CardTitle = t2.CardTitle,
                        Name = t2.Name,
                        LeaveTypeId = t1.LeaveTypeId,
                        LeaveName = t3.Leave_Name,
                        year = t1.Year,
                        Total = t1.Total,
                        Taken = t1.Taken,
                        Remaining = t1.Remaining,
                    }).AsEnumerable().ToList();
            }
            if (vm.LeaveTypeId != 1000 && vm.EmployeeId != 10000000)
            {
                a = (from t1 in db.HrmsLeaveAssigns
                    join t2 in db.Employees on t1.EmployeeId equals t2.ID
                    join t3 in db.HrmsLeaveTypes on t1.LeaveTypeId equals t3.ID
                    where (t1.Year == vm.year && t1.EmployeeId == vm.EmployeeId && t1.LeaveTypeId == vm.LeaveTypeId) && t1.Status == true
                     select new VM_HRMS_Leave_Assign()
                    {
                        ID = t1.ID,
                        EmployeeId = t1.EmployeeId,
                        EmployeeIdentity = t2.EmployeeIdentity,
                        CardTitle = t2.CardTitle,
                        Name = t2.Name,
                        LeaveTypeId = t1.LeaveTypeId,
                        LeaveName = t3.Leave_Name,
                        year = t1.Year,
                        Total = t1.Total,
                        Taken = t1.Taken,
                        Remaining = t1.Remaining,
                    }).AsEnumerable().ToList();
            }
            if (vm.LeaveTypeId != 1000 && vm.EmployeeId == 10000000)
            {
                a = (from t1 in db.HrmsLeaveAssigns
                    join t2 in db.Employees on t1.EmployeeId equals t2.ID
                    join t3 in db.HrmsLeaveTypes on t1.LeaveTypeId equals t3.ID
                    where (t1.Year == vm.year &&  t1.LeaveTypeId == vm.LeaveTypeId) && t1.Status == true
                     select new VM_HRMS_Leave_Assign()
                    {
                        ID = t1.ID,
                        EmployeeId = t1.EmployeeId,
                        EmployeeIdentity = t2.EmployeeIdentity,
                        CardTitle = t2.CardTitle,
                        Name = t2.Name,
                        LeaveTypeId = t1.LeaveTypeId,
                        LeaveName = t3.Leave_Name,
                        year = t1.Year,
                        Total = t1.Total,
                        Taken = t1.Taken,
                        Remaining = t1.Remaining,
                    }).AsEnumerable().ToList();
            }

            //else
            //{
            //    a = (from t1 in db.HrmsLeaveAssigns
            //        join t2 in db.Employees on t1.EmployeeId equals t2.ID
            //        join t3 in db.HrmsLeaveTypes on t1.LeaveTypeId equals t3.ID
            //        where (t1.Year == vm.year & t1.LeaveTypeId == vm.LeaveTypeId)
            //        select new VM_HRMS_Leave_Assign()
            //        {
            //            ID = t1.ID,
            //            EmployeeId = t1.EmployeeId,
            //            EmployeeIdentity = t2.EmployeeIdentity,
            //            CardTitle = t2.CardTitle,
            //            Name = t2.Name,
            //            LeaveTypeId = t1.LeaveTypeId,
            //            LeaveName = t3.Leave_Name,
            //            year = t1.Year,
            //            Total = t1.Total,
            //            Taken = t1.Taken,
            //            Remaining = t1.Remaining
            //        }).AsEnumerable().ToList();
            //}
            
            DataListhrHrmsLeaveAssigns = a;
        }



        public void GetRemainingLeaveInfo(int id)
        {
            var str = "";
            var htm = "<span>You Have Remaining Leave</span>&nbsp;&nbsp;&nbsp;";
            DateTime d = new DateTime();
            var a = new List<VM_HRMS_Leave_Assign>();
            a = (from t1 in db.HrmsLeaveAssigns
                join t3 in db.HrmsLeaveTypes on t1.LeaveTypeId equals t3.ID
                 where (t1.Year ==d.Year.ToString() && t1.EmployeeId==id)
                select new VM_HRMS_Leave_Assign()
                {
                    ID = t1.ID,
                    EmployeeId = t1.EmployeeId,
                    LeaveTypeId = t1.LeaveTypeId,
                    LeaveName = t3.Leave_Name,
                    year = t1.Year,
                    Total = t1.Total,
                    Taken = t1.Taken,
                    Remaining = t1.Remaining,
                }).AsEnumerable().ToList();
            DataListhrHrmsLeaveAssigns = a;
        }

        public void LeaveStatus()
        {
            throw new NotImplementedException();
        }

        //public string GetRemainingLeaveInfo(int empId)
        //{
        //    List<VM_HRMS_Leave_Assign> list = new List<VM_HRMS_Leave_Assign>();
        //    DateTime dt = DateTime.Now;
        //    string resp = "";
        //    var ds = (from t1 in db.HrmsLeaveAssigns
        //        join t2 in db.Employees on t1.EmployeeId equals t2.ID
        //        join t3 in db.HrmsLeaveTypes on t1.LeaveTypeId equals t3.ID
        //        where t1.EmployeeId ==empId));
        //    //var ds =
        //    //    SqlDataAccess.SQL_ExecuteReader(
        //    //        "Select distinct [LeaveName],[Total],[Taken],[Remaining] from [dbo].[tblLeaveCategoryFinal] where Employee_ID='" + empId +
        //    //        "' and Year='" + dt.Year.ToString() + "'", out resp);
        //    var str = "";
        //    var htm = "<span>You Have Remaining Leave</span>&nbsp;&nbsp;&nbsp;";
        //    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        //    {
        //        //Session["MaxYear"] = ds.Tables[0].Rows[0][0].ToString();
        //        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        //        {
        //            str += "<span class=\"label label-blue-alt\">" +
        //                   ds.Tables[0].Rows[i][0].ToString().Replace("Leave", "")
        //                   + "</span>&nbsp;<span class=\"badge badge-blue-alt\">Total" +
        //                   "</span>&nbsp;<span class=\"badge badge-blue-alt\">" + ds.Tables[0].Rows[i][1].ToString() +
        //                   "</span>&nbsp;&nbsp;<span class=\"badge badge-blue-alt\">Taken</span>&nbsp;<span class=\"badge badge-blue-alt\">" +
        //                   ds.Tables[0].Rows[i][2].ToString() +
        //                   "</span>&nbsp;&nbsp;<span class=\"badge badge-blue-alt\">Remaining</span>&nbsp;<span class=\"badge badge-blue-alt\">" +
        //                   ds.Tables[0].Rows[i][3].ToString() +
        //                   "</span>&nbsp;&nbsp;</br></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        //            //str += "<span class=\"label label-blue-alt\">" +
        //            //       ds.Tables[0].Rows[i][0].ToString().Replace("Leave", "") +
        //            //       "</span>&nbsp;<span class=\"badge badge-blue-alt\">" + ds.Tables[0].Rows[i][1].ToString() +
        //            //       "</span>&nbsp;&nbsp;";
        //        }
        //        htm += str;
        //        return htm;
        //    }
        //    return "<span>No Leave assign yet!</span>";
        //}

    }


}