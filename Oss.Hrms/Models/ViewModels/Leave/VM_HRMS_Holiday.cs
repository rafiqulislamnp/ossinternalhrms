﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.Leave
{
    public class VM_HRMS_Holiday : RootModel
    {
        //[DisplayName("Employee Id")]
        //public int EmployeeId { get; set; }
        //[DisplayName("Employee Name")]
        //public string EmployeeName { get; set; }
        //[DisplayName("Employee Id")]
        //public string EmployeeIdentity { get; set; }

        [DataType(DataType.Date)]
        [DisplayName("Off Day")]
        public DateTime OffDay { get; set; }

        [DisplayName("Description")]
        [Required]
        public string Description { get; set; }

        public ICollection<VM_HRMS_Holiday> Holidays { get; set; }

        private HrmsContext db = new HrmsContext();
        public void GetHolidayList()
        {
            var holiDay = (from h in db.HrmsHolidays
                select new VM_HRMS_Holiday()
                {
                    ID = h.ID,OffDay = h.OffDay, Description = h.Description
                }).ToList();
            Holidays = holiDay;
        }
    }
}