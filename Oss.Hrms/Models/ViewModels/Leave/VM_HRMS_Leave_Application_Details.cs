﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.Leave
{
    public class VM_HRMS_Leave_Application_Details
    {
        public int Leave_ApplicationId { get; set; }

        [DataType(DataType.Date)]
        public DateTime Start_Date { get; set; }

        [DataType(DataType.Date)]
        public DateTime End_Date { get; set; }
        public int TotalDays { get; set; }
        [StringLength(200)]
        public string Comments { get; set; }
        public int LeaveStatus { get; set; }  //--In-View Dropdown----

        public ICollection<VM_HRMS_Leave_Application_Details> LeaveApplicationDetailses { get; set; }
        private HrmsContext db = new HrmsContext();
        public void GetLeaveApplicationDetails()
        {
            
        }
    }
}