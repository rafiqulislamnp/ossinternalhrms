﻿using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;
using Oss.Hrms.Models.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Oss.Hrms.Models.ViewModels.Salary
{
    public class VMSalaryIncrement : RootModel
    {
        HrmsContext db = new HrmsContext();
        public List<VMSalaryIncrement> DataList { get; set; }

        [Display(Name = "Employee"), Required(ErrorMessage = "Employee isRequired.")]
        public int EmployeeID { get; set; }
        [Display(Name = "Designation")]
        public int DesignationId { get; set; }
        [Display(Name = "Section")]
        public int? SectionID { get; set; }
        [Display(Name = "Unit"), Required(ErrorMessage = "Unit is required.")]
        public int UnitID { get; set; }
        public bool IsChecked { get; set; }
        public string CardNo { get; set; }
        public string CardTitle { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeType { get; set; }
        public string Designation { get; set; }
        public string Section { get; set; }
        public string Grade { get; set; }
        public string Note { get; set; }

        public string ViewJoinDate { get; set; }
        public string ViewEfectDate { get; set; }
        public string Title { get; set; }
        public int CardTitleId { get; set; }
        [DisplayName("From Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }
        [DisplayName("To Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }

        [DisplayName("Join Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime JoinDate { get; set; }

        [DisplayName("Efect Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime EfectDate { get; set; }

        #region SalaryModel
        [Display(Name = "Gross Salary")]
        public decimal GrossSalary { get; set; }
        [Display(Name = "Basic Pay"), Required(ErrorMessage = "Basic Pay required.")]
        public decimal BasicAmount { get; set; }
        public decimal HouseAllowance { get; set; }
        public decimal OtherAllowance { get; set; }
        public int IncrementRate { get; set; }
        public decimal IncrementAmount { get; set; }
        public decimal IncrementTotal { get; set; }
        public decimal IncrementBasic { get; set; }
        public decimal IncrementHouse { get; set; }
        public decimal IncrementGross { get; set; }
        public bool OTAllow { get; set; }
        public decimal OTRate { get; set; }
        public bool HDRateAllow { get; set; }
        public decimal HDRate { get; set; }
        public bool IsAlart { get; set; }
        #endregion

        public void GetSalaryIncrementList()
        {
            DateTime today = DateTime.Today;
            string pdate = today.ToString("MMMM");
            string ydate = today.Year.ToString();
            this.Title = "Employee Salary Increment Information For the Month Of "+ pdate+" "+ydate;
            var getEmp = (from o in db.Employees
                          join p in db.Sections on o.Section_Id equals p.ID
                          join q in db.Designations on o.Designation_Id equals q.ID
                          where o.Present_Status==1 && o.Joining_Date.Month==today.Month && o.Joining_Date.Year!=today.Year
                          select new VMSalaryIncrement
                          {
                              EmployeeID = o.ID,
                              EmployeeName=o.Name,
                              CardNo=o.CardTitle +" "+ o.EmployeeIdentity,
                              CardTitle=o.CardTitle,
                              JoinDate = o.Joining_Date,
                              Section=p.SectionName,
                              Designation=q.Name,
                              Grade=o.Grade
                              
                          }).ToList();


            if (getEmp.Any())
            {
                foreach (var v in getEmp)
                {
                    v.EfectDate = new DateTime(today.Year, v.JoinDate.Month, v.JoinDate.Day);
                    var getRecord = db.HrmsEodRecords.Where(a=>a.EmployeeId==v.EmployeeID && a.Status==true);
                    v.ViewJoinDate = Helpers.ShortDateString(v.JoinDate);
                    v.ViewEfectDate = Helpers.ShortDateString(v.EfectDate);
                    if (getRecord.Any())
                    {
                        v.IsAlart = getRecord.Any(a =>a.Eod_RefFk==1 && a.EffectiveDate == v.EfectDate) == true ? false : true;
                        v.BasicAmount = getRecord.FirstOrDefault(a => a.Eod_RefFk == 1).ActualAmount;
                        v.HouseAllowance= getRecord.FirstOrDefault(a => a.Eod_RefFk == 2).ActualAmount;
                        v.OtherAllowance = 1100;
                        v.GrossSalary = v.BasicAmount + v.HouseAllowance + v.OtherAllowance;
                        v.IncrementRate = 5;
                        v.IncrementAmount = Math.Round((v.BasicAmount * v.IncrementRate )/ 100);
                        v.IncrementBasic = v.BasicAmount + v.IncrementAmount;
                        v.IncrementHouse = Math.Round(v.IncrementBasic*(decimal)0.4);
                        v.IncrementGross = v.IncrementBasic + v.IncrementHouse + v.OtherAllowance;

                        if (getRecord.Any(a => a.Eod_RefFk == 11 && a.ActualAmount > 0))
                        {
                            v.OTAllow = true;
                            v.OTRate= (v.IncrementBasic / 208) * 2;
                        }
                        else if (getRecord.Any(a => a.Eod_RefFk == 11 && a.ActualAmount <= 0))
                        {
                            v.OTAllow = false;
                        }
                    }
                }

                this.DataList = getEmp;
            }
        }

        public void GetIncrementListByTitleWise(int CardTitleID)
        {
            DropDownData d = new DropDownData();
            DateTime today = DateTime.Today;
            string pdate = today.ToString("MMMM");
            string ydate = today.Year.ToString();
            this.Title = "Employee Salary Increment Information For the Month Of " + pdate + " " + ydate;
            List<VMSalaryIncrement> lstIncrement = new List<VMSalaryIncrement>();
            if (CardTitleID > 1)
            {
                string CardTitle = d.GetCardTitle(CardTitleId);
                var getEmp = (from o in db.Employees
                              join p in db.Sections on o.Section_Id equals p.ID
                              join q in db.Designations on o.Designation_Id equals q.ID
                              where o.Present_Status == 1 && o.Joining_Date.Month == today.Month && o.Joining_Date.Year != today.Year && o.CardTitle.Contains(CardTitle)
                              select new VMSalaryIncrement
                              {
                                  EmployeeID = o.ID,
                                  EmployeeName = o.Name,
                                  CardNo = o.EmployeeIdentity,
                                  CardTitle = o.CardTitle,
                                  JoinDate = o.Joining_Date,
                                  Section = p.SectionName,
                                  Designation = q.Name,
                                  Grade = o.Grade
                              }).ToList();
                if (getEmp.Any())
                {
                    foreach (var v in getEmp)
                    {
                        DateTime efectDate = new DateTime(today.Year, v.JoinDate.Month, 1);
                        var getRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == v.EmployeeID && a.Status == true);
                        v.IsAlart = getRecord.Any(a => a.Eod_RefFk == 1 && a.EffectiveDate == efectDate) == true ? false : true;
                        if (v.IsAlart)
                        {
                            VMSalaryIncrement item = new VMSalaryIncrement();
                            item.EmployeeID = v.EmployeeID;
                            item.EmployeeName = v.EmployeeName;
                            item.CardNo = v.CardTitle + " " + v.CardNo;
                            item.CardTitle = v.CardTitle;
                            item.JoinDate = v.JoinDate;
                            item.Section = v.Section;
                            item.Designation = v.Designation;
                            item.Grade = v.Grade;
                            item.EfectDate = efectDate;
                            item.ViewJoinDate = Helpers.ShortDateString(v.JoinDate);
                            item.ViewEfectDate = Helpers.ShortDateString(item.EfectDate);
                            if (getRecord.Any())
                            {
                                item.BasicAmount = getRecord.FirstOrDefault(a => a.Eod_RefFk == 1).ActualAmount;
                                item.HouseAllowance = getRecord.FirstOrDefault(a => a.Eod_RefFk == 2).ActualAmount;
                                item.OtherAllowance = 1100;
                                item.GrossSalary = item.BasicAmount + item.HouseAllowance + item.OtherAllowance;
                                item.IncrementRate = 5;
                                item.IncrementAmount = Math.Round((item.BasicAmount * item.IncrementRate) / 100);
                                item.IncrementBasic = item.BasicAmount + item.IncrementAmount;
                                item.IncrementHouse = Math.Round(item.IncrementBasic * (decimal)0.4);
                                item.IncrementGross = item.IncrementBasic + item.IncrementHouse + item.OtherAllowance;
                                item.IncrementTotal = item.IncrementAmount + (item.IncrementHouse - item.HouseAllowance);
                                if (getRecord.Any(a => a.Eod_RefFk == 11 && a.ActualAmount > 0))
                                {
                                    item.OTAllow = true;
                                    item.OTRate = (item.IncrementBasic / 208) * 2;
                                }
                                else if (getRecord.Any(a => a.Eod_RefFk == 11 && a.ActualAmount <= 0))
                                {
                                    item.OTAllow = false;
                                }
                                if (getRecord.Any(a => a.Eod_RefFk == 47 && a.ActualAmount > 0))
                                {
                                    item.HDRateAllow = true;
                                    item.HDRate = item.IncrementBasic / 30;
                                }
                                else if (getRecord.Any(a => a.Eod_RefFk == 47 && a.ActualAmount <= 0))
                                {
                                    item.HDRateAllow = false;
                                }
                            }
                            lstIncrement.Add(item);
                        }

                    }
                }
            }
            else if (CardTitleID==1)
            {
                var getEmp = (from o in db.Employees
                              join p in db.Sections on o.Section_Id equals p.ID
                              join q in db.Designations on o.Designation_Id equals q.ID
                              where o.Present_Status == 1 && o.Joining_Date.Month == today.Month && o.Joining_Date.Year != today.Year
                              select new VMSalaryIncrement
                              {
                                  EmployeeID = o.ID,
                                  EmployeeName = o.Name,
                                  CardNo = o.EmployeeIdentity,
                                  CardTitle = o.CardTitle,
                                  JoinDate = o.Joining_Date,
                                  Section = p.SectionName,
                                  Designation = q.Name,
                                  Grade = o.Grade

                              }).ToList();


                if (getEmp.Any())
                {
                    foreach (var v in getEmp)
                    {
                        DateTime efectDate = new DateTime(today.Year, v.JoinDate.Month, 1);
                        var getRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == v.EmployeeID && a.Status == true);
                        v.IsAlart = getRecord.Any(a => a.Eod_RefFk == 1 && a.EffectiveDate == efectDate) == true ? false : true;
                        if (v.IsAlart)
                        {
                            VMSalaryIncrement item = new VMSalaryIncrement();
                            item.EmployeeID = v.EmployeeID;
                            item.EmployeeName = v.EmployeeName;
                            item.CardNo = v.CardTitle + " " + v.CardNo;
                            item.CardTitle = v.CardTitle;
                            item.JoinDate = v.JoinDate;
                            item.Section = v.Section;
                            item.Designation = v.Designation;
                            item.Grade = v.Grade;
                            item.EfectDate = efectDate;
                            item.ViewJoinDate = Helpers.ShortDateString(v.JoinDate);
                            item.ViewEfectDate = Helpers.ShortDateString(item.EfectDate);
                            if (getRecord.Any())
                            {
                                item.BasicAmount = getRecord.FirstOrDefault(a => a.Eod_RefFk == 1).ActualAmount;
                                item.HouseAllowance = getRecord.FirstOrDefault(a => a.Eod_RefFk == 2).ActualAmount;
                                item.OtherAllowance = 1100;
                                item.GrossSalary = item.BasicAmount + item.HouseAllowance + item.OtherAllowance;
                                item.IncrementRate = 5;
                                item.IncrementAmount = Math.Round((item.BasicAmount * item.IncrementRate) / 100);
                                item.IncrementBasic = item.BasicAmount + item.IncrementAmount;
                                item.IncrementHouse = Math.Round(item.IncrementBasic * (decimal)0.4);
                                item.IncrementGross = item.IncrementBasic + item.IncrementHouse + item.OtherAllowance;
                                item.IncrementTotal = item.IncrementAmount + (item.IncrementHouse - item.HouseAllowance);
                                if (getRecord.Any(a => a.Eod_RefFk == 11 && a.ActualAmount > 0))
                                {
                                    item.OTAllow = true;
                                    item.OTRate = (item.IncrementBasic / 208) * 2;
                                }
                                else if (getRecord.Any(a => a.Eod_RefFk == 11 && a.ActualAmount <= 0))
                                {
                                    item.OTAllow = false;
                                }
                                if (getRecord.Any(a => a.Eod_RefFk == 47 && a.ActualAmount > 0))
                                {
                                    item.HDRateAllow = true;
                                    item.HDRate = item.IncrementBasic / 30;
                                }
                                else if (getRecord.Any(a => a.Eod_RefFk == 47 && a.ActualAmount <= 0))
                                {
                                    item.HDRateAllow = false;
                                }
                            }
                            lstIncrement.Add(item);
                        }
                    }
                }
            }
            this.DataList = lstIncrement;
        }

        public void GetIncrementedListByTitleWise(int CardTitleID)
        {
            DropDownData d = new DropDownData();
            DateTime today = DateTime.Today;
            string pdate = today.ToString("MMMM");
            string ydate = today.Year.ToString();
            this.Title = "Employee Salary Increment Information For the Month Of " + pdate + " " + ydate;
            List<VMSalaryIncrement> lstIncrement = new List<VMSalaryIncrement>();
            if (CardTitleID > 1)
            {
                string CardTitle = d.GetCardTitle(CardTitleId);
                var getEmp = (from o in db.Employees
                              join p in db.Sections on o.Section_Id equals p.ID
                              join q in db.Designations on o.Designation_Id equals q.ID
                              where o.Present_Status == 1 && o.Joining_Date.Month == today.Month && o.Joining_Date.Year != today.Year && o.CardTitle.Contains(CardTitle)
                              select new VMSalaryIncrement
                              {
                                  EmployeeID = o.ID,
                                  EmployeeName = o.Name,
                                  CardNo = o.EmployeeIdentity,
                                  CardTitle = o.CardTitle,
                                  JoinDate = o.Joining_Date,
                                  Section = p.SectionName,
                                  Designation = q.Name,
                                  Grade = o.Grade
                              }).ToList();
                if (getEmp.Any())
                {
                    foreach (var v in getEmp)
                    {
                        DateTime efectDate = new DateTime(today.Year, v.JoinDate.Month, 1);
                        var getRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == v.EmployeeID && a.Status == true || a.ID==47);
                        v.IsAlart = getRecord.Any(a => a.Eod_RefFk == 1 && a.EffectiveDate == efectDate) == true ? true : false;
                        if (v.IsAlart)
                        {
                            VMSalaryIncrement item = new VMSalaryIncrement();
                            item.EmployeeID = v.EmployeeID;
                            item.EmployeeName = v.EmployeeName;
                            item.CardNo = v.CardTitle + " " + v.CardNo;
                            item.CardTitle = v.CardTitle;
                            item.JoinDate = v.JoinDate;
                            item.Section = v.Section;
                            item.Designation = v.Designation;
                            item.Grade = v.Grade;
                            item.EfectDate = efectDate;
                            item.ViewJoinDate = Helpers.ShortDateString(v.JoinDate);
                            item.ViewEfectDate = Helpers.ShortDateString(item.EfectDate);
                            if (getRecord.Any())
                            {
                                item.BasicAmount = getRecord.FirstOrDefault(a => a.Eod_RefFk == 1).ActualAmount;
                                item.HouseAllowance = getRecord.FirstOrDefault(a => a.Eod_RefFk == 2).ActualAmount;
                                item.OtherAllowance = 1100;
                                item.GrossSalary = item.BasicAmount + item.HouseAllowance + item.OtherAllowance;
                                if (getRecord.Any(a => a.Eod_RefFk == 11 && a.ActualAmount > 0))
                                {
                                    item.OTAllow = true;
                                    item.OTRate = (item.IncrementBasic / 208) * 2;
                                }
                                else if (getRecord.Any(a => a.Eod_RefFk == 11 && a.ActualAmount <= 0))
                                {
                                    item.OTAllow = false;
                                }
                                if (getRecord.Any(a => a.Eod_RefFk == 47 && a.ActualAmount > 0))
                                {
                                    item.HDRateAllow = true;
                                    item.HDRate = (item.IncrementBasic / 208) * 2;
                                }
                                else if (getRecord.Any(a => a.Eod_RefFk == 47 && a.ActualAmount <= 0))
                                {
                                    item.HDRateAllow = false;
                                }
                            }
                            lstIncrement.Add(item);
                        }

                    }
                }
            }
            else if (CardTitleID == 1)
            {
                var getEmp = (from o in db.Employees
                              join p in db.Sections on o.Section_Id equals p.ID
                              join q in db.Designations on o.Designation_Id equals q.ID
                              where o.Present_Status == 1 && o.Joining_Date.Month == today.Month && o.Joining_Date.Year != today.Year
                              select new VMSalaryIncrement
                              {
                                  EmployeeID = o.ID,
                                  EmployeeName = o.Name,
                                  CardNo = o.EmployeeIdentity,
                                  CardTitle = o.CardTitle,
                                  JoinDate = o.Joining_Date,
                                  Section = p.SectionName,
                                  Designation = q.Name,
                                  Grade = o.Grade

                              }).ToList();


                if (getEmp.Any())
                {
                    foreach (var v in getEmp)
                    {
                        DateTime efectDate = new DateTime(today.Year, v.JoinDate.Month, 1);
                        var getRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == v.EmployeeID && a.Status == true || a.ID==47);
                        v.IsAlart = getRecord.Any(a => a.Eod_RefFk == 1 && a.EffectiveDate == efectDate) == true ? true : false;
                        if (v.IsAlart)
                        {
                            VMSalaryIncrement item = new VMSalaryIncrement();
                            item.EmployeeID = v.EmployeeID;
                            item.EmployeeName = v.EmployeeName;
                            item.CardNo = v.CardTitle + " " + v.CardNo;
                            item.CardTitle = v.CardTitle;
                            item.JoinDate = v.JoinDate;
                            item.Section = v.Section;
                            item.Designation = v.Designation;
                            item.Grade = v.Grade;
                            item.EfectDate = efectDate;
                            item.ViewJoinDate = Helpers.ShortDateString(v.JoinDate);
                            item.ViewEfectDate = Helpers.ShortDateString(item.EfectDate);
                            if (getRecord.Any())
                            {
                                item.BasicAmount = getRecord.FirstOrDefault(a => a.Eod_RefFk == 1).ActualAmount;
                                item.HouseAllowance = getRecord.FirstOrDefault(a => a.Eod_RefFk == 2).ActualAmount;
                                item.OtherAllowance = 1100;
                                item.GrossSalary = item.BasicAmount + item.HouseAllowance + item.OtherAllowance;
                                if (getRecord.Any(a => a.Eod_RefFk == 11 && a.ActualAmount > 0))
                                {
                                    item.OTAllow = true;
                                    item.OTRate = (item.IncrementBasic / 208) * 2;
                                }
                                else if (getRecord.Any(a => a.Eod_RefFk == 11 && a.ActualAmount <= 0))
                                {
                                    item.OTAllow = false;
                                }
                                if (getRecord.Any(a => a.Eod_RefFk == 47 && a.ActualAmount > 0))
                                {
                                    item.HDRateAllow = true;
                                    item.HDRate = (item.IncrementBasic / 208) * 2;
                                }
                                else if (getRecord.Any(a => a.Eod_RefFk == 47 && a.ActualAmount <= 0))
                                {
                                    item.HDRateAllow = false;
                                }
                            }
                            lstIncrement.Add(item);
                        }
                    }
                }
            }
            this.DataList = lstIncrement;
        }

        public void CreateYearlyIncrement(VMSalaryIncrement model)
        {
            List<EODReference> updateRecordList = new List<EODReference>();

            if (model.DataList.Any(a => a.IsChecked == true))
            {
                var referenceExistEod = db.HrmsEodReferences.Where(a => a.Status == true && a.Start == 1 || a.ID==47).ToList();
                if (referenceExistEod.Any())
                {
                    foreach (var incSal in model.DataList.Where(a => a.IsChecked == true))
                    {
                        #region AddItemInList
                        foreach (var itemRef in referenceExistEod)
                        {
                            if (itemRef.ID != 3)
                            {
                                EODReference item = new EODReference();
                                item.EODRefID = itemRef.ID;
                                item.EmpID = incSal.EmployeeID;
                                if (itemRef.ID == 1)
                                {
                                    item.Amount = incSal.IncrementBasic;
                                }
                                else if (itemRef.ID == 2)
                                {
                                    item.Amount = incSal.IncrementHouse;
                                }
                                else if (itemRef.ID == 11)
                                {
                                    if (incSal.OTAllow)
                                    {
                                        item.Amount = Math.Round(incSal.OTRate, 2);
                                    }
                                }
                                else if (itemRef.ID == 47)
                                {
                                    if (incSal.HDRateAllow)
                                    {
                                        item.Amount = Math.Round(incSal.HDRate, 2);
                                    }
                                }
                                updateRecordList.Add(item);
                            }
                        }
                        #endregion
                    }

                    foreach (var incSal in model.DataList.Where(a => a.IsChecked == true))
                    {
                        #region SaveData

                        var allExistRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == incSal.EmployeeID && a.Status == true || a.ID==47);
                        if (updateRecordList.Any())
                        {
                            foreach (var updateRecord in updateRecordList)
                            {
                                if (allExistRecord.Any(a => a.Eod_RefFk == updateRecord.EODRefID))
                                {
                                    var previousRecord = allExistRecord.FirstOrDefault(a => a.Eod_RefFk == updateRecord.EODRefID);
                                    if (previousRecord.ActualAmount != updateRecord.Amount)
                                    {
                                        previousRecord.Status = false;
                                        previousRecord.Update_By = model.Entry_By;
                                        previousRecord.Update_Date = model.Entry_Date;

                                        HRMS_EodRecord recordAdd = new HRMS_EodRecord()
                                        {
                                            EmployeeId = updateRecord.EmpID,
                                            Eod_RefFk = updateRecord.EODRefID,
                                            ActualAmount = updateRecord.Amount,
                                            Status = true,
                                            EffectiveDate = incSal.EfectDate,
                                            Entry_Date = model.Entry_Date,
                                            Entry_By = model.Entry_By
                                        };
                                        db.HrmsEodRecords.Add(recordAdd);
                                    }
                                }
                            }
                        }

                        #endregion
                    }
                }
            }
        }

        public async Task<int> Save()
        {
            return await db.SaveChangesAsync();
        }
    }
}