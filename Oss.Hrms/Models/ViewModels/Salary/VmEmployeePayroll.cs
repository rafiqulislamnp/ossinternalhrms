﻿
using Oss.Hrms.Models.Entity.Common;
using Oss.Hrms.Models.Entity.HRMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.ViewModels.Salary
{

    public class VmEmployeePayroll
    {
        public HRMS_Employee Employee { get; set; }
        public EmployeePayroll EmployeePayroll { get; set; }
        public Designation Designation { get; set; }
        public Department Department { get; set; }
    }

    public class VmPayrollManage
    {

        public PayrollMaster PayrollMaster { get; set; }
        public List<Department> Departments { get; set; }
        public int TotalMemebers { get; set; }

        public void PayrollProcess(PayrollMaster model, int departemntid)
        {
            HrmsContext db = new HrmsContext();
            var employeelist = db.Employees.Where(x => x.Present_Status == 1 && x.Status == true && x.Department_Id == departemntid).ToList();

            foreach (var emp in employeelist)
            {
                var totaldays = 0;
                decimal totalpayable = 0;
                decimal totuldeduction = 0;
                decimal totalamounttobepaid = 0;
                decimal bonus = 0;
                var absentday = 0;
                decimal absentdeduction = 0;
                var lateday = 0;
                decimal latededuction = 0;
                var informlateday = 0;
                decimal informlatededuction = 0;

                var employeeslaary = db.EmployeePayroll.Where(x => x.Status == true && x.IsNew == true && x.EmployeeFK == emp.ID).SingleOrDefault();
                var employeeattendance = new List<HRMS_Attendance_History>();

                decimal perdaysalary = employeeslaary.GrossAmount / 30;
                //for new joined employee's partial salary


                if ((emp.Joining_Date >= model.FromDate && emp.Joining_Date <= model.ToDate) || (emp.RejoinDate >= model.FromDate && emp.RejoinDate <= model.ToDate))
                {
                    employeeattendance = db.HrmsAttendanceHistory.Where(x => x.Status == true && x.EmployeeId == emp.ID && x.Date >= emp.Joining_Date && x.Date <= model.ToDate).ToList();
                    absentday = employeeattendance.Where(x => x.AttendanceStatus == 3 || (x.LeavePaidType == 2 && x.AttendanceStatus == 4)).Count();
                    absentdeduction = perdaysalary * absentday;


                    lateday = employeeattendance.Where(x => x.AttendanceStatus == 2 && x.Remarks == "Late").Count();
                    var latedeductiondays = (int)Math.Floor((double)lateday / 3);
                    latededuction = perdaysalary * latedeductiondays;

                    informlateday = employeeattendance.Where(x => x.AttendanceStatus == 2 && x.Remarks == "Inform Late").Count();
                    var informlatedeductiondays = (int)Math.Floor((double)informlateday / 4);
                    informlatededuction = (perdaysalary / 2) * informlatedeductiondays;

                    totaldays = employeeattendance.Count();
                    bonus = decimal.Divide(decimal.Divide(employeeslaary.GrossAmount * model.BonusPercent, 100), 30) * totaldays;
                    totalpayable = perdaysalary * totaldays + bonus;
                    totuldeduction = absentdeduction + latededuction + informlatededuction;
                    totalamounttobepaid = totalpayable - totuldeduction;
                }
                else
                {
                    employeeattendance = db.HrmsAttendanceHistory.Where(x => x.Status == true && x.EmployeeId == emp.ID && x.Date >= model.FromDate && x.Date <= model.ToDate).ToList();
                    absentday = employeeattendance.Where(x => x.AttendanceStatus == 3 || (x.LeavePaidType == 2 && x.AttendanceStatus == 4)).Count();
                    absentdeduction = perdaysalary * absentday;


                    lateday = employeeattendance.Where(x => x.AttendanceStatus == 2 && x.Remarks == "Late").Count();
                    var latedeductiondays = (int)Math.Floor((double)lateday / 3);
                    latededuction = perdaysalary * latedeductiondays;

                    informlateday = employeeattendance.Where(x => x.AttendanceStatus == 2 && x.Remarks == "Inform Late").Count();
                    var informlatedeductiondays = (int)Math.Floor((double)informlateday / 4);
                    informlatededuction = (perdaysalary / 2) * informlatedeductiondays;

                    if (!(emp.Joining_Date >= model.FromDate && emp.Joining_Date <= model.ToDate) && employeeslaary.DateOfPromotion > model.FromDate && employeeslaary.DateOfPromotion <= model.ToDate)
                    {
                        var prevgross = db.EmployeePayroll.Where(x => x.EmployeeFK == emp.ID && x.IsNew == false).OrderByDescending(x => x.ID).Select(x => x.GrossAmount).SingleOrDefault();

                        int totalDayinoldStructure = (employeeslaary.DateOfPromotion - model.FromDate).Value.Days + 1;
                        int totalDayinNewStructure = (model.ToDate - employeeslaary.DateOfPromotion).Value.Days;

                        decimal perdaysalaryinprevstructure = prevgross / 30;

                        bonus = decimal.Divide(decimal.Divide(prevgross * model.BonusPercent, 100), 30) * totalDayinoldStructure + decimal.Divide(decimal.Divide(employeeslaary.GrossAmount * model.BonusPercent, 100), 30) * totalDayinNewStructure;
                        totalpayable = perdaysalaryinprevstructure * totalDayinoldStructure + perdaysalary * totalDayinNewStructure + bonus;
                        totuldeduction = absentdeduction + latededuction + informlatededuction;
                        totalamounttobepaid = totalpayable - totuldeduction;

                    }
                    else
                    {

                        totaldays = employeeattendance.Count();
                        bonus = decimal.Divide(employeeslaary.GrossAmount * model.BonusPercent, 100);
                        totalpayable = employeeslaary.GrossAmount + bonus;
                        totuldeduction = absentdeduction + latededuction + informlatededuction;
                        totalamounttobepaid = totalpayable - totuldeduction;
                    }


                }

                var PayrollDetails = new PayrollDetails
                {
                    EmployeeFK = emp.ID,
                    PayrollMasterFk = model.ID,
                    AbsentDays = absentday,
                    AbsentDeduction = absentdeduction,
                    LateDays = lateday,
                    LateDeduction = latededuction,
                    InformLateDays = informlateday,
                    InformLateDeduction = informlatededuction,
                    BonusPercent = model.BonusPercent,
                    BonusAmount = bonus,
                    TotalDays = employeeattendance.Count(),
                    TotalPayble = totalpayable,
                    TotalDeduction = totuldeduction,
                    TotalAmountToBePaid = totalamounttobepaid,
                    Entry_By = model.Entry_By,
                    Status = true
                };
                db.PayrollDetails.Add(PayrollDetails);
                db.SaveChanges();
            }
        }


        public void SalaryReprocess(int did, int mid, DateTime fromedate, DateTime todate)
        {
            HrmsContext db = new HrmsContext();
            var employee = (from t1 in db.PayrollDetails
                            join t2 in db.Employees on t1.EmployeeFK equals t2.ID
                            where t1.ID == did && t2.Present_Status == 1 && t2.Status == true
                            select new
                            {
                                t2
                            }).Single();

            var totaldays = 0;
            decimal totalpayable = 0;
            decimal totuldeduction = 0;
            decimal totalamounttobepaid = 0;

            var employeeslaary = db.EmployeePayroll.Where(x => x.Status == true && x.IsNew == true && x.EmployeeFK == employee.t2.ID).SingleOrDefault();
            var employeeattendance = new List<HRMS_Attendance_History>();

            //for new joined employee's partial salary
            if (employee.t2.Joining_Date >= fromedate && employee.t2.Joining_Date <= todate)
            {
                employeeattendance = db.HrmsAttendanceHistory.Where(x => x.Status == true && x.EmployeeId == employee.t2.ID && x.Date >= employee.t2.Joining_Date && x.Date <= todate).ToList();
            }
            else
            {
                employeeattendance = db.HrmsAttendanceHistory.Where(x => x.Status == true && x.EmployeeId == employee.t2.ID && x.Date >= fromedate && x.Date <= todate).ToList();
            }

            decimal perdaysalary = employeeslaary.GrossAmount / 30;
            var absentday = employeeattendance.Where(x => x.AttendanceStatus == 3 || (x.LeavePaidType == 2 && x.AttendanceStatus == 4)).Count();
            var absentdeduction = perdaysalary * absentday;


            var lateday = employeeattendance.Where(x => x.AttendanceStatus == 2 && x.Remarks == "Late").Count();
            var latedeductiondays = (int)Math.Floor((double)lateday / 3);
            var latededuction = perdaysalary * latedeductiondays;

            var informlateday = employeeattendance.Where(x => x.AttendanceStatus == 2 && x.Remarks == "Inform Late").Count();
            var informlatedeductiondays = (int)Math.Floor((double)informlateday / 4);
            var informlatededuction = (perdaysalary / 2) * informlatedeductiondays;

            var payroll = db.PayrollDetails.Single(x => x.ID == did);

            if (employeeslaary.DateOfPromotion > fromedate && employeeslaary.DateOfPromotion <= todate)
            {
                var prevgross = db.EmployeePayroll.Where(x => x.EmployeeFK == employee.t2.ID && x.IsNew == false).OrderByDescending(x => x.ID).Select(x => x.GrossAmount).SingleOrDefault();

                int totalDayinoldStructure = (employeeslaary.DateOfPromotion - fromedate).Value.Days + 1;
                int totalDayinNewStructure = (todate - employeeslaary.DateOfPromotion).Value.Days;

                decimal perdaysalaryinprevstructure = prevgross / 30;

                //bonus = decimal.Divide(decimal.Divide(prevgross * model.BonusPercent, 100), 30) * totalDayinoldStructure + decimal.Divide(decimal.Divide(employeeslaary.GrossAmount * model.BonusPercent, 100), 30) * totalDayinNewStructure;
                totalpayable = perdaysalaryinprevstructure * totalDayinoldStructure + perdaysalary * totalDayinNewStructure + payroll.BonusAmount;
                totuldeduction = absentdeduction + latededuction + informlatededuction;
                totalamounttobepaid = totalpayable - totuldeduction;

            }
            else
            {

                totaldays = employeeattendance.Count();
                //bonus = decimal.Divide(decimal.Divide(employeeslaary.GrossAmount * model.BonusPercent, 100), 30) * totaldays;
                totalpayable = employeeslaary.GrossAmount + payroll.BonusAmount;
                totuldeduction = absentdeduction + latededuction + informlatededuction;
                totalamounttobepaid = totalpayable - totuldeduction;
            }


            payroll.AbsentDays = absentday;
            payroll.AbsentDeduction = absentdeduction;
            payroll.LateDays = lateday;
            payroll.LateDeduction = latededuction;
            payroll.InformLateDays = informlateday;
            payroll.InformLateDeduction = informlatededuction;
            payroll.TotalPayble = totalpayable;
            payroll.TotalDeduction = totuldeduction;
            payroll.TotalAmountToBePaid = totalamounttobepaid;
            db.SaveChanges();

        }
    }

    public class VmPayrollDetails
    {
        public HRMS_Employee Employee { get; set; }
        public Designation Designation { get; set; }
        public Department Department { get; set; }
        public PayrollDetails PayrollDetails { get; set; }
        public PayrollMaster PayrollMaster { get; set; }
        public PayrollSetting PayrollSetting { get; set; }
        public decimal GrossSalary { get; set; }
    }

    public class VmPayrollSetting
    {
        public PayrollSettingMaster PayrollSettingMaster { get; set; }
        public PayrollSetting PayrollSetting { get; set; }
    }
}
