﻿using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;
using Oss.Hrms.Models.ViewModels.HRMS;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.ViewModels.Promotion
{
    public class VMPromotions : RootModel
    {
        HrmsContext db = new HrmsContext();
        public List<VMPromotions> DataList { get; set; }
        public VMEmployee VMEmployee { get; set; }
        public List<VMEmployee> ListEmployee { get; set; }


        [Display(Name = "Employee"), Required(ErrorMessage = "Employee is Required.")]
        public int EmployeeID { get; set; }
        [Display(Name = "CardTitle"), Required(ErrorMessage = "CardTitle is Required.")]
        public string CardTitle { get; set; }
        [Display(Name = "CardNo"), Required(ErrorMessage = "CardNo is Required.")]
        public string CardNo { get; set; }
        [Display(Name = "Department"), Required(ErrorMessage = "Department is Required.")]
        public int DepartmentId { get; set; }
        [Display(Name = "Section"), Required(ErrorMessage = "Section is Required.")]
        public int SectionId { get; set; }
        [Display(Name = "Grade"), Required(ErrorMessage = "Grade is Required.")]
        public int GradeId { get; set; }
        [Display(Name = "Designation"), Required(ErrorMessage = "Designation is Required.")]
        public int DesignationId { get; set; }
        [Display(Name = "LineNo"), Required(ErrorMessage = "LineNo is Required.")]
        public int LineId { get; set; }
        [Display(Name = "Unit"), Required(ErrorMessage = "Unit is Required.")]
        public int UnitId { get; set; }
        [Display(Name = "Business Unit"), Required(ErrorMessage = "Business Unit is Required.")]
        public int BusinessUnitId { get; set; }
        [Display(Name = "Promotion Date"), Required(ErrorMessage = "Promotion Date is Required.")]
        public DateTime PromotionDate { get; set; }
        public int EodRecordId { get; set; }
        public string Grade { get; set; }

        public string EmployeeName { get; set; }
        public string CCardNo { get; set; }
        public string CCardTitle { get; set; }
        public string CSection { get; set; }
        public string CDesignation { get; set; }
        public decimal CSalary { get; set; }

        public string PCardNo { get; set; }
        public string PCardTitle { get; set; }
        public string PSection { get; set; }
        public string PDesignation { get; set; }
        public decimal PSalary { get; set; }


        public void GetCreatePromotion(VMPromotions model)
        {
            DropDownData d = new DropDownData();
            model.Grade = d.GetGradeById(model.GradeId);
            var getSalary = db.HrmsEodRecords.Where(a =>a.EmployeeId==model.EmployeeID && a.Eod_RefFk == 1 && a.Status == true).OrderBy(a => a.ID);
            if (getSalary.Any())
            {
                model.EodRecordId = getSalary.FirstOrDefault().ID;
            }
            var getHistory = db.HrmsEmploymentHistory.Where(a => a.EmployeeIdFK == model.EmployeeID && a.Current==true);
            if (getHistory.Any())
            {
                var update = getHistory.FirstOrDefault();
                update.Current = false;
            }
            var getEmp = db.Employees.Where(a=>a.ID==model.EmployeeID).FirstOrDefault();
            
            HRMS_EmploymentHistory pro = new HRMS_EmploymentHistory();
            pro.EmployeeIdFK = getEmp.ID;
            pro.CardNo = getEmp.EmployeeIdentity;
            pro.CardTitle = getEmp.CardTitle;
            pro.PunchCardNo = getEmp.CardNo;
            pro.DepartmentFK = getEmp.Department_Id;
            pro.SectionFK = getEmp.Section_Id;
            pro.DesignationFK = getEmp.Designation_Id;
            pro.Grade = getEmp.Grade;
            pro.BusinessUnitFK = getEmp.Business_Unit_Id;
            pro.UnitFK = getEmp.Unit_Id;
            pro.LineFK = getEmp.LineId;
            pro.HistoyType = 1;
            pro.Current = true;
            if (model.EodRecordId>0)
            {
                pro.EodRecordFK = model.EodRecordId;
            }
            pro.Staff_Type = getEmp.Staff_Type;
            pro.PromotionDate = model.PromotionDate;
            pro.Status = true;
            pro.Entry_Date = model.Entry_Date;
            pro.Entry_By = model.Entry_By;
            pro.Update_Date = model.Entry_Date;

            db.HrmsEmploymentHistory.Add(pro);

            db.SaveChanges();

            getEmp.EmployeeIdentity = model.CardNo;
            getEmp.CardTitle = model.CardTitle;
            getEmp.Department_Id = model.DepartmentId;
            getEmp.Section_Id = model.SectionId;
            getEmp.Designation_Id = model.DesignationId;
            getEmp.Grade = model.Grade;
            getEmp.LineId = model.LineId;
            getEmp.Business_Unit_Id = model.BusinessUnitId;
            getEmp.Unit_Id = model.UnitId;
            getEmp.Update_Date = model.PromotionDate;

            db.SaveChanges();
        }

        public void GetPromotionList()
        {
            List<VMPromotions> lstPromotion = new List<VMPromotions>();
            var vData = (from t1 in db.HrmsEmploymentHistory
                         join t3 in db.Sections on t1.SectionFK equals t3.ID
                         join t4 in db.Designations on t1.DesignationFK equals t4.ID
                         where t1.Current == true
                         select new
                         {
                             HID = t1.ID,
                             EmployeeId = t1.EmployeeIdFK,
                             CardTitle = t1.CardTitle,
                             CardNo = t1.CardNo,
                             Section = t3.SectionName,
                             Designation = t4.Name,
                             Salary = db.HrmsEodRecords.Where(a => a.ID == t1.EodRecordFK).ToList(),

                             CurrentInfo = (from o in db.Employees
                                          join p in db.Sections on o.Section_Id equals p.ID
                                          join q in db.Designations on o.Designation_Id equals q.ID
                                          where o.ID == t1.EmployeeIdFK
                                          select new
                                          {
                                              Name=o.Name,
                                              EmployeeId = o.ID,
                                              CardTitle = o.CardTitle,
                                              CardNo = o.EmployeeIdentity,
                                              Section = p.SectionName,
                                              Designation = q.Name,
                                              Salary = db.HrmsEodRecords.Where(a => a.EmployeeId == o.ID && a.Eod_RefFk == 1 && a.Status == true).ToList()
                                          }).ToList()
                         }).OrderByDescending(a => a.HID);

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    VMPromotions pmodel = new VMPromotions();

                    if (v.CurrentInfo.Any())
                    {
                        var proEmp = v.CurrentInfo.FirstOrDefault();

                        pmodel.CCardNo = proEmp.CardTitle +" "+ proEmp.CardNo;
                        pmodel.CCardTitle = proEmp.CardTitle;
                        pmodel.EmployeeName = proEmp.Name;
                        pmodel.CSection = proEmp.Section;
                        pmodel.CDesignation = proEmp.Designation;

                        if (proEmp.Salary.Any())
                        {
                            var getBasic = proEmp.Salary.FirstOrDefault().ActualAmount;
                            decimal totalSalary = getBasic + (getBasic * (decimal)0.4) + 1100;
                            pmodel.CSalary = Math.Round(totalSalary);
                        }
                    }
                    pmodel.PCardNo = v.CardTitle +" "+ v.CardNo;
                    pmodel.PCardTitle = v.CardTitle;
                    pmodel.PSection = v.Section;
                    pmodel.PDesignation = v.Designation;
                    if (v.Salary.Any())
                    {
                        var getBasic = v.Salary.FirstOrDefault().ActualAmount;
                        decimal totalSalary = getBasic + (getBasic * (decimal)0.4) + 1100;
                        pmodel.PSalary = Math.Round(totalSalary);
                    }
                    lstPromotion.Add(pmodel);
                }
            }

            this.DataList = lstPromotion;
                        
        }

    }
}