﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VMDesignation: RootModel
    {
        [DisplayName("Designation")]
        [Required(ErrorMessage = "Please Enter Designation Name")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Designation Name should be minimum 3 characters and maximum 100 characters")]
        public string Name { get; set; }

        [DisplayName("Designation (Bangla)")]
        //[Required(ErrorMessage = "Please Enter Designation Name Bangla")]
        //[StringLength(100, MinimumLength = 3, ErrorMessage = "Designation Name should be minimum 3 characters and maximum 100 characters")]
        public string DesigNameBangla { get; set; }


        public IEnumerable<VMDesignation> DataList { get; set; }
    }
}