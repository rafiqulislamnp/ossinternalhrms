﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Cryptography;
using System.Web;
using System.Web.DynamicData;
using System.Web.Mvc.Html;
using Oss.Hrms.Models.Entity.Common;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VM_PoliceStation : RootModel
    {
        [DisplayName("Police Station Name")]
        [Required(ErrorMessage = "Please Select Police Station")]
        public string PoliceStationName { get; set; }

        [DisplayName("District Name")]
        public int District_ID { get; set; }
        [ForeignKey("District_ID")]
        public District District { get; set; }

        [DisplayName("District Name")]
        [Required(ErrorMessage = "Please Select District")]
        public string DistrictName { get; set; }



        public ICollection<VM_PoliceStation> CollectionVmDistrict { get; set; }
        HrmsContext db = new HrmsContext();

        public void GetPoliceStationList()
        {
            var policeStations = (from t1 in db.District
                select new
                {
                    t1,
                    ps = db.PoliceStation.Where(a => a.District_ID == t1.ID).ToList()
                }).ToList();

            List<VM_PoliceStation>lst=new List<VM_PoliceStation>();
            foreach (var v in policeStations)
            {
                if (v.ps.Any())
                {
                    foreach (var v1 in v.ps)
                    {

                        VM_PoliceStation model = new VM_PoliceStation();
                        model.District_ID = v.t1.ID;
                        model.DistrictName = v.t1.DistrictName;
                        model.PoliceStationName = v1.PoliceStationName;
                        model.ID = v1.ID;
                        lst.Add(model);
                    }
                    
                }
                else
                {
                    VM_PoliceStation model = new VM_PoliceStation();
                    model.District_ID = v.t1.ID;
                    model.DistrictName = v.t1.DistrictName;
                    lst.Add(model);
                }

                
            }



            
            //select new VM_PoliceStation()
            //    {
            //        ID = t1.ID,
            //        PoliceStationName = t1.PoliceStationName,
            //        District_ID = t2.ID,
            //        DistrictName = t2.DistrictName
            //    }).ToList();

            CollectionVmDistrict = lst;
        }

        public VM_PoliceStation GetSpecificPoliceStation(int id)
        {
          var p =(from t1 in db.PoliceStation join t2 in db.District  on t1.District_ID equals t2.ID
                select new VM_PoliceStation()
                {
                  ID = t1.ID,
                  PoliceStationName = t1.PoliceStationName,
                  DistrictName = t2.DistrictName
              }).FirstOrDefault(x => x.ID == id);
            return p;
        }

        public VM_PoliceStation GetSpecificDistrictInfo(int id)
        {
            var p = (from t1 in db.District where t1.ID==id
                     select new VM_PoliceStation()
                     {
                         District_ID = t1.ID,
                         DistrictName = t1.DistrictName
                     }).FirstOrDefault();
            return p;
        }

        //public void GetLineNos()
        //{
        //    var lines = (from l in db.Lines
        //        select new VMLine()
        //        {
        //            ID = l.ID,
        //            Line = l.Line
        //        }).ToList();

        //    CollectionVmLine = lines;
        //}
    }
}