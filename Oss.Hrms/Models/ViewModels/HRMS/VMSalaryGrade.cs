﻿using Oss.Hrms.Models.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VMSalaryGrade : RootModel
    {
        [DisplayName("Grade Name")]
        [Required(ErrorMessage = "Please Enter Grade Name")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Salary Grade Name should be minimum 3 characters and maximum 100 characters")]
        public string Name { get; set; }

        [DisplayName("Grade Amount")]
        [Required(ErrorMessage = "Grade Amount can not null")]
        public decimal Grade_Amount { get; set; }


        public IEnumerable<VMSalaryGrade> DataList { get; set; }
    }
}