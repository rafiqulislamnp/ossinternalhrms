﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VMUserNotication : RootModel
    {
        [DisplayName("Employee Name :")]
        public int  Employee_Id { get; set; }

        [DisplayName()]
        public string Name { get; set; }

        [DisplayName("Email Id:")]
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "Enter Email Id.")]
        public string Email_Id { get; set; }

        [DisplayName("Subject :")]
        public string Subject { get; set; }

        [DisplayName("Business Unit :")]
        public int Business_Id { get; set; }

        public int Status { get; set; }
    }
}