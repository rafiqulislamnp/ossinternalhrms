﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.ViewModels.Reports;
using Oss.Hrms.Models.Services;
using System.ComponentModel.DataAnnotations.Schema;
using Oss.Hrms.Helper;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VMApplications
    {
        public int ID { get; set; }

        public string Name { get; set; }
        public string EngName { get; set; }
        [Display(Name = "Husband Name BN")]
        public string HusbandName { get; set; }
        [Display(Name = "Land Owner BN")]
        public string LandOwner { get; set; }
        [Display(Name = "Home BN")]
        public string Home { get; set; }
        [Display(Name = "PostOffice BN")]
        public string PostOffice { get; set; }
        [Display(Name = "Thana BN")]
        public string Thana { get; set; }
        [Display(Name = "District BN")]
        public string District { get; set; }

        public DateTime JoinningDate  { get; set; }
        public DateTime QuitDate { get; set; }
        public string EmployeeIDNo { get; set; }

        public string Designation { get; set; }
        public string EngDesignation { get; set; }
        public string EmpMail { get; set; }

        public string EmpPhone { get; set; }

        public string Section { get; set; }

        public string Grade{ get; set; }

        public string Department { get; set; }

        public string FatherName { get; set; }

        public string MotherName { get; set; }

        public string PresentAddress { get; set; }

        public string PermanentAddress { get; set; }
        public string Gender { get; set; }

        public string CardNo { get; set; }
        public string Line { get; set; }
        public string Unit { get; set; }

        [Display(Name = "Physical Eligibility")]
        public string PhysicalEligibility { get; set; }

        [Display(Name = "Birth Mark")]
        public string BirthMark { get; set; }


       
        [Display(Name = "Welfare Officer")]
        public string WelfareOfficer { get; set; }

        public string LawStep { get; set; }

        [Display(Name = "EDD Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime EDD { get; set; }
       
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime ResignDate { get; set; }

        [Display(Name = "LMP Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime LMP { get; set; }
                
        [Display(Name = "Designation")]
        public int ChangeDesignation { get; set; }

        [Display(Name = "Section")]
        public int  ChangeSection { get; set; }

        [Display(Name = "Grade")]
        public int ChangeGrade { get; set; }

        [Display(Name = "Department")]
        public int ChangeDepartment { get; set; }

        [Column(TypeName = "NVARCHAR")]
        [Display(Name = "Child Gender")]
        [StringLength(100)]
        public string ChildGender_BN { get; set; }

        [Column(TypeName = "NVARCHAR")]
        [Display(Name = "Nominate Person:")]
        [StringLength(100)]
        public string NominatePerson_BN { get; set; }

        [Column(TypeName = "NVARCHAR")]
        [Display(Name = "Relation:")]
        [StringLength(100)]
        public string Relation_BN { get; set; }

        [Display(Name ="From Date:")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }

        [Display(Name = "To Date:")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Application Date:")]
        public DateTime ApplicationDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date:")]
        public DateTime StartDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date:")]
        public DateTime EndDate { get; set; }

        [Display(Name = "Basic Pay")]
        [Required(ErrorMessage = "Basic Pay is Required")]
        [RegularExpression(@"^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$", ErrorMessage = "Can't be zero")]
        public decimal BasicPay { get; set; }

        public int Age { get; set; }
        public int PregnantWeek { get; set; }
        public DateTime DOB { get; set; }
        
        public decimal ActualAmount { get; set; }
        
        public int ButtonID { get; set; }

        public DateTime  TotalJobAge { get; set; }

        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }


        public int TotalYear { get; set; }
        public int TotalMonth { get; set; }
        public int TotalDay { get; set; }

        [Display(Name = "Total Pay")]
        public string TotalPay { get; set; }

        [Display(Name = "Daily Pay")]
        public string DailyPay { get; set; }

        [Display(Name = "First Installment")]
        public string FirstInstallment { get; set; }

        [Display(Name = "Present Village")]
        public string PresentVill{ get; set; }
        [Display(Name = "Present PostOffice")]
        public string PresentPostOffice { get; set; }
        [Display(Name = "Present Thana")]
        public string PresentThana { get; set; }
        [Display(Name = "Present District")]
        public string PresentDistrict { get; set; }
        [Display(Name = "Permanent Village")]
        public string PermanentVill { get; set; }
        [Display(Name = "Permanent PostOffice")]
        public string PermanentPostOffice { get; set; }
        [Display(Name = "Permanent Thana")]
        public string PermanentThana { get; set; }
        [Display(Name = "Permanent District")]
        public string PermanentDistrict { get; set; }

        public string NomineName { get; set; }
        public string NominePresentAddress { get; set; }
        public string NominePermanentAddress { get; set; }
        public string NomineFatherName { get; set; }
        public string NomineMotherName { get; set; }
        public string NomineRelation { get; set; }

        [Display(Name = "Relation With Nominate Person")]
        public string RelationNoPerson { get; set; }
        public List<ReportDocType> ReportSource { get; set; }

        public decimal BasicAmount { get; set; }

        public decimal HouseAmount { get; set; }

        public decimal IncrementAmount { get; set; }

        public decimal IncrementSalary { get; set; }

        //FinalSatelmantProperty
        public decimal OtherAmount { get; set; }

        public void EmployeeBasicInfo(int EmployeeID)
        {
            HrmsContext db = new HrmsContext();

            var vData = (from t1 in db.Employees
                         join t2 in db.Designations on t1.Designation_Id equals t2.ID
                         join t3 in db.Sections on t1.Section_Id equals t3.ID
                         join t4 in db.Departments on t1.Department_Id equals t4.ID
                         join t5 in db.Lines on t1.LineId equals t5.ID
                         join t6 in db.Units on t1.Unit_Id equals t6.ID
                         where t1.ID == EmployeeID
                         select new
                         {
                             Name = t1.Name_BN,
                             EmployeeIDNo = t1.EmployeeIdentity,
                             Designation = t2.DesigNameBangla,
                             Section = t3.SectionName,
                             Grade = t1.Grade,
                             Department = t4.DeptNameBangla,
                             Line = t5.Line,
                             JoinningDate = t1.Joining_Date,
                             DOB = t1.DOB,
                             EngName = t1.Name,
                             EngDesignation = t2.Name,
                             Unit = t6.UnitName,
                             MotherName = t1.Mother_Name,
                             FatherName = t1.Father_Name,
                             CardNo = t1.EmployeeIdentity,
                             Gender = t1.Gender,
                             ResignDate = t1.QuitDate,
                             QuitDate=t1.QuitDate,
                             PresentVillageName = t1.PresentVillage_ID == null ? string.Empty : db.Village.FirstOrDefault(x => x.ID == t1.PresentVillage_ID).VillageName,
                             PresentPostOfficeName = t1.PresentPostOffice_ID == null ? string.Empty : db.PostOffice.FirstOrDefault(x => x.ID == t1.PresentPostOffice_ID).PostOfficeName,
                             PresentPoliceStationName = t1.PresentPoliceStation_ID == null ? string.Empty : db.PoliceStation.FirstOrDefault(x => x.ID == t1.PresentPoliceStation_ID).PoliceStationName,
                             PresentDistrictName = t1.PresentDistrict_ID == null ? string.Empty : db.District.FirstOrDefault(x => x.ID == t1.PresentDistrict_ID).DistrictName,
                             PermanentVillageName = t1.PermanentVillage_ID == null ? string.Empty : db.Village.FirstOrDefault(x => x.ID == t1.PermanentVillage_ID).VillageName,
                             PermanentPostOfficeName = t1.PermanentPostOffice_ID == null ? string.Empty : db.PostOffice.FirstOrDefault(x => x.ID == t1.PermanentPostOffice_ID).PostOfficeName,
                             PermanentPoliceStationName = t1.PermanentPoliceStation_ID == null ? string.Empty : db.PoliceStation.FirstOrDefault(x => x.ID == t1.PermanentPoliceStation_ID).PoliceStationName,
                             PermanentDistrictName = t1.PermanentDistrict_ID == null ? string.Empty : db.District.FirstOrDefault(x => x.ID == t1.PermanentDistrict_ID).DistrictName,
                             NomineName= db.HrmsReferences.Any(a => a.Employee_Id == t1.ID && a.IsReference == false) == true ? db.HrmsReferences.FirstOrDefault(a => a.Employee_Id == t1.ID).Referee_Name : string.Empty,
                             NominePermanentAddress = db.HrmsReferences.Any(a => a.Employee_Id == t1.ID && a.IsReference==false) == true ? db.HrmsReferences.FirstOrDefault(a => a.Employee_Id == t1.ID).PermanentAddress : string.Empty,
                             NominePresentAddress = db.HrmsReferences.Any(a => a.Employee_Id == t1.ID && a.IsReference == false) == true ? db.HrmsReferences.FirstOrDefault(a => a.Employee_Id == t1.ID).PresentAddress : string.Empty,
                             NomineRelation = db.HrmsReferences.Any(a => a.Employee_Id == t1.ID && a.IsReference == false) == true ? db.HrmsReferences.FirstOrDefault(a => a.Employee_Id == t1.ID).Relation : string.Empty,
                             NomineFatherName = db.HrmsReferences.Any(a => a.Employee_Id == t1.ID && a.IsReference == false) == true ? db.HrmsReferences.FirstOrDefault(a => a.Employee_Id == t1.ID).Referee_FatherName : string.Empty,
                             NomineMotherName = db.HrmsReferences.Any(a => a.Employee_Id == t1.ID && a.IsReference == false) == true ? db.HrmsReferences.FirstOrDefault(a => a.Employee_Id == t1.ID).Referee_MotherName : string.Empty
                         });

            if (vData.Any())
            {
                var a = vData.FirstOrDefault();

                this.Name = a.Name;
                this.EmployeeIDNo = a.EmployeeIDNo;
                this.Section = a.Section;
                this.Designation = a.Designation;
                this.Department = a.Department;
                this.Grade = a.Grade;
                this.Line = a.Line;
                this.JoinningDate = a.JoinningDate;
                this.EngName = a.EngName;
                this.EngDesignation = a.EngDesignation;
                this.Unit = a.Unit;
                this.FatherName = a.FatherName;
                this.MotherName = a.MotherName;
                this.CardNo = a.CardNo;
                this.PresentAddress = a.PresentVillageName+","+a.PresentPostOfficeName+","+a.PresentPoliceStationName+","+a.PresentDistrictName;
                this.PermanentAddress = a.PermanentVillageName + "," + a.PermanentPostOfficeName + "," + a.PermanentPoliceStationName + "," + a.PermanentDistrictName;
                this.PresentVill = a.PresentVillageName;
                this.PresentThana = a.PresentPoliceStationName;
                this.PresentPostOffice = a.PresentPostOfficeName;
                this.PresentDistrict = a.PresentDistrictName;
                this.PermanentVill = a.PermanentVillageName;
                this.PermanentPostOffice = a.PermanentPostOfficeName;
                this.PermanentThana = a.PermanentPoliceStationName;
                this.PermanentDistrict = a.PermanentDistrictName;
                this.DOB = a.DOB;
                this.Gender = a.Gender;
                this.ResignDate = a.ResignDate;
                this.NomineFatherName = a.NomineFatherName;
                this.NomineMotherName = a.NomineMotherName;
                this.NominePermanentAddress = a.NominePermanentAddress;
                this.NominePresentAddress = a.NominePresentAddress;
                this.NomineName = a.NomineName;
                this.NomineRelation=a.NomineRelation;
                EmployeeAge(a.DOB);
            }
        }
        
        public void LoadData(VMApplications model)
        {
            model.BasicPay = GetEmployeeBasic(model.ID);
            EmployeeBasicInfo(model.ID);
            DateTime ZeroDate = new DateTime(1900, 01, 01, 00, 00, 00);
            CalculateJobAge(model.JoinningDate, model.EDD);
            decimal houseRent = (model.BasicPay * 40)/100;
            decimal roundhouseRent = Math.Round(houseRent,0,MidpointRounding.AwayFromZero);
            decimal IncrementTotal = GetIncrementAmount(model.ID);

            if (ZeroDate.Equals(model.ResignDate) || ZeroDate.Equals(model.JoinningDate))
            {
                this.TotalYear =0;
                this.TotalMonth = 0;
                this.TotalDay = 0;
            }
            else
            {
                TotalWorkingAge(model.JoinningDate, ResignDate);
            }
            List<ReportDocType> lst = new List<ReportDocType>();
            ReportDocType report = new ReportDocType();
            report.HeaderTop1 = model.PresentVill;
            report.HeaderTop2 = model.PresentPostOffice;
            report.HeaderTop3 = model.PresentThana;
            report.HeaderTop4 = model.PresentDistrict;
            report.HeaderTop5 = model.PermanentVill;
            report.HeaderTop6 = model.PermanentPostOffice;
            report.HeaderTop7 = model.PermanentThana;
            report.HeaderTop8 = model.PermanentDistrict;
            report.HeaderTop8 = model.PresentAddress;
            report.HeaderTop9 = model.PermanentAddress;
            report.HeaderLeft1 = model.Name;
            report.HeaderLeft2 = model.EmployeeIDNo;
            report.HeaderLeft3 = model.Designation;
            report.HeaderLeft4 = model.Section;
            report.HeaderLeft5 = model.Department;
            report.HeaderLeft6 = model.Grade;
            report.HeaderLeft7 = model.Line;
            report.HeaderLeft8 = model.EngName;
            report.HeaderLeft9 = model.WelfareOfficer;
            report.HeaderLeft10 = model.LawStep;
            report.HeaderLeft11 = model.MotherName;
            report.HeaderLeft12 = model.FatherName;
            report.HeaderLeft13 = model.CardNo;

            report.HeaderMiddle6 = model.Unit;
            report.HeaderMiddle1 = model.Age.ToString();
            report.HeaderMiddle2 = Helpers.ShortDateString(model.EDD);
            report.HeaderMiddle3 = model.PregnantWeek.ToString();
            report.HeaderMiddle4 = model.EngDesignation;
            report.HeaderMiddle5 = Helpers.ShortDateString(model.LMP);

            report.HeaderRight1 = GetName(model.ChangeDesignation, 1);
            report.HeaderRight2 = GetName(model.ChangeSection, 2);
            //report.HeaderRight3 = GetName(model.ChangeDesignation, 1);
            //report.HeaderRight4 = GetName(model.ChangeGrade, 4);

            report.Body1 = Helpers.ShortDateString(model.ApplicationDate);
            report.Body2 = Helpers.ShortDateString(model.FromDate);
            report.Body3 = Helpers.ShortDateString(model.ToDate);
            report.Body4 = Helpers.ShortDateString(model.StartDate);
            report.Body5 = Helpers.ShortDateString(model.EndDate);
            report.Body6 = model.ChildGender_BN;
            report.Body7 = model.NominatePerson_BN;
            report.Body8 = model.Relation_BN;
            report.Body9 = Helpers.ShortDateString(model.JoinningDate);
            report.Body10 = model.EngName;
            report.Body11 = model.Age.ToString();
            report.Body12 = model.Year.ToString();  
            report.Body13 = model.Month.ToString(); 
            report.Body14 = model.Day.ToString();  
            report.Body15 = model.TotalPay;
            report.Body16 = model.DailyPay;
            report.Body17 = model.FirstInstallment;
            report.Body18 = Helpers.ShortDateString(model.DOB);
            report.Body19 = model.Gender;
            report.Body20 = model.PhysicalEligibility;
            report.Body21 = model.BirthMark;
            report.Body22 = model.TotalYear.ToString();
            report.Body23 = model.TotalMonth.ToString();
            report.Body24 = model.TotalDay.ToString();
            report.Body25 = Helpers.ShortDateString(model.ResignDate);
            
            report.SubBody1 = model.BasicPay.ToString();
            report.SubBody2 = roundhouseRent.ToString();
            report.SubBody3 = "250";
            report.SubBody4 = "200";
            report.SubBody5 = "650";
            report.SubBody6 = (model.BasicPay + roundhouseRent + 1100).ToString();
            report.SubBody7= model.Name;
            report.SubBody8= model.EmployeeIDNo;
            report.SubBody9 = model.RelationNoPerson;
            report.SubBody10 = Math.Round(model.BasicPay).ToString();
            report.SubBody11 = roundhouseRent.ToString();
            report.SubBody12 = Math.Round(model.BasicPay + roundhouseRent + 1100).ToString();
            report.SubBody13 = Helpers.IncreamentDate(model.JoinningDate);
            report.SubBody14 = IncrementTotal.ToString();
            report.Footer1 = model.Name;
            report.Footer2 = model.HusbandName;
            report.Footer3 = model.LandOwner;
            report.Footer4 = model.Home;
            report.Footer5 = model.PostOffice;
            report.Footer6 = model.Thana;
            report.Footer7 = model.District;

            report.Duplicate1 = model.Name;
            report.Duplicate2 = model.Designation;
            report.Duplicate3 = model.EmployeeIDNo;
            report.Duplicate4 = model.Section;
            report.Duplicate5 = model.Line;
            report.Duplicate6 = model.WelfareOfficer;

            //Nomine Call AppoinmentLetter
            report.Duplicate7 = model.NomineName;
            report.Duplicate8 = model.NomineFatherName;
            report.Duplicate9 = model.NomineMotherName;
            report.Duplicate10 = model.NominePresentAddress;
            report.Duplicate11 = model.NominePermanentAddress;
            report.Duplicate12 = model.NomineRelation;
            //

            report.Duplicate13 = model.PermanentVill;
            report.Duplicate14 = model.PermanentPostOffice;
            report.Duplicate15 = model.PermanentThana;
            report.Duplicate16 = model.PermanentDistrict;
            report.Duplicate17 = model.Designation;
            report.Duplicate18 = GetName(model.ChangeDesignation, 1);
            report.Duplicate19 = GetName(model.ChangeSection, 2);
            report.Duplicate20 = model.Age.ToString();
            report.Duplicate21 = model.PresentAddress;
            report.Duplicate22 = Helpers.ShortDateString(model.ApplicationDate);
            report.Duplicate23 = model.BirthMark;
            report.Duplicate24 = model.PresentAddress;
            report.Duplicate25 = model.PermanentAddress;
            
            lst.Add(report);
            this.ReportSource = lst;
        }

        public void LoadFinalSettlementData(VMApplications model)
        {
            HrmsContext db = new HrmsContext();
            
            int LastBasic = 0;
            int LastGross = 0;
            int LastHouse = 0;
            int LastOther = 1100;
            decimal EarnLeaveTaka = decimal.Zero;
            decimal CompanyTaka = decimal.Zero;
            decimal OtherTaka = decimal.Zero;
            decimal OTHour = 0;
            decimal OTTaka = 0;
            int PresentDay = 0;
            decimal AttendanceTaka = 0;
            decimal AttendanceBonus = decimal.Zero;
            
            decimal TotalBenefit = 0;
            decimal TotalPayment = 0;
            decimal Deduction = decimal.Zero;
            decimal TotalPayable = 0;

            var takePayroll = db.HrmsPayrolls.OrderByDescending(a=>a.ID).FirstOrDefault();

            model.FromDate = takePayroll.FromDate;
            model.ToDate = takePayroll.ToDate;

            var getPayroll = (from o in db.HrmsPayrolls
                             join p in db.HrmsPayrollInfos on o.ID equals p.PayrollFK
                             where p.EmployeeIdFk==model.ID && p.PayrollFK==takePayroll.ID
                             select new
                             {
                                 o,p
                             }).ToList();

            if (getPayroll.Any())
            {
                LastBasic = (int)getPayroll.FirstOrDefault(a => a.p.Eod_ReferenceFk == 1).p.Amount;
                LastHouse = (int)getPayroll.FirstOrDefault(a => a.p.Eod_ReferenceFk == 2).p.Amount;
                LastGross = LastBasic + LastHouse + LastOther;
                PresentDay = (int)getPayroll.FirstOrDefault(a => a.p.Eod_ReferenceFk == 31).p.Amount;
                AttendanceTaka = (int)getPayroll.FirstOrDefault(a => a.p.Eod_ReferenceFk == 36).p.Amount;
                OTHour =(int)getPayroll.FirstOrDefault(a => a.p.Eod_ReferenceFk == 28).p.Amount;
                OTTaka= getPayroll.FirstOrDefault(a => a.p.Eod_ReferenceFk == 39).p.Amount;
                AttendanceBonus = getPayroll.Any(a => a.p.Eod_ReferenceFk == 7) == true ? getPayroll.FirstOrDefault(a => a.p.Eod_ReferenceFk == 7).p.Amount : 0;
               
            }
            
            EmployeeBasicInfo(model.ID);
            DateTime ZeroDate = new DateTime(1900, 01, 01, 00, 00, 00);
            CalculateJobAge(model.JoinningDate, model.EDD);

            if (ZeroDate.Equals(model.ResignDate) || ZeroDate.Equals(model.JoinningDate))
            {
                this.TotalYear = 0;
                this.TotalMonth = 0;
                this.TotalDay = 0;
            }
            else
            {
                TotalWorkingAge(model.JoinningDate, ResignDate);
            }

            if (model.Year >= 10)
            {
                if (model.Month > 6)
                {
                    TotalBenefit = (model.Year + 1) * 45 * (LastBasic / 30);
                }
                else
                {
                    TotalBenefit = model.Year * 45 * (LastBasic / 30);
                }
            }
            else if (model.Year >= 5 && model.Year < 10)
            {
                if (model.Month > 6)
                {
                    TotalBenefit = (model.Year + 1) * 14 * (LastBasic / 30);
                }
                else
                {
                    TotalBenefit = model.Year * 14 * (LastBasic / 30);
                }
            }

            TotalPayment = AttendanceTaka + AttendanceBonus + OTTaka + TotalBenefit + model.OtherAmount;
            TotalPayable = TotalPayment + Deduction;
            List<ReportDocType> lst = new List<ReportDocType>();
            ReportDocType report = new ReportDocType();

            //EmployeeInformation
            report.HeaderLeft1 = model.Name;
            report.HeaderLeft2 = model.EmployeeIDNo;
            report.HeaderLeft3 = model.Designation;
            report.HeaderLeft4 = model.Section;
            
            report.HeaderRight1 = LastGross.ToString();
            report.HeaderRight2 = LastBasic.ToString();

            if (model.QuitDate.Equals(ZeroDate) && !model.ResignDate.Equals(ZeroDate))
            {
                report.HeaderMiddle1 = Helpers.ShortDateString(model.ResignDate);
                report.HeaderMiddle2 = Helpers.ShortDateString(model.ResignDate);
            }
            else if (!model.QuitDate.Equals(ZeroDate) && model.ResignDate.Equals(ZeroDate))
            {
                report.HeaderMiddle1 = Helpers.ShortDateString(model.QuitDate);
                report.HeaderMiddle2 = Helpers.ShortDateString(model.QuitDate);
            }
            else if (model.QuitDate.Equals(ZeroDate) && model.ResignDate.Equals(ZeroDate))
            {
                report.HeaderMiddle1 = string.Empty;
                report.HeaderMiddle2 = string.Empty;
            }
            
            report.HeaderMiddle3 = Helpers.ShortDateString(model.JoinningDate);
            report.HeaderMiddle4 = model.Year.ToString();
            report.HeaderMiddle5 = model.Month.ToString();
            report.HeaderMiddle6 = model.Day.ToString();
            report.HeaderMiddle7 = model.Age.ToString();
            
            report.Body1 = Helpers.ShortDateString(model.FromDate);
            report.Body2 = Helpers.ShortDateString(model.ToDate);
            report.Body3 = PresentDay.ToString();
            report.Body4 = Math.Round(AttendanceTaka).ToString();
            report.Body5 = Math.Round(AttendanceBonus).ToString();
            report.Body6 = EarnLeaveTaka.ToString();
            report.Body7 = Math.Round(EarnLeaveTaka).ToString();
            report.Body8 = OTHour.ToString();
            report.Body9 = Math.Round(OTTaka).ToString();
            report.Body10 = Math.Round(TotalBenefit).ToString();
            report.Body11 = Math.Round(model.OtherAmount).ToString();
            report.Body12 = Math.Round(TotalPayment).ToString();
            report.Body13 = CompanyTaka.ToString();
            report.Body14 = Math.Round(TotalPayable).ToString();
           
            lst.Add(report);
            this.ReportSource = lst;
        }

        public void JobLoadData(VMApplications model)
        {
            EmployeeBasicInfo(model.ID);
            List<ReportDocType> lst = new List<ReportDocType>();
            ReportDocType report = new ReportDocType();
            report.HeaderLeft1 = model.Name;
            report.HeaderLeft2 = model.EmployeeIDNo;
            report.HeaderLeft3 = model.Designation;
            report.HeaderLeft4 = model.Section;
            report.HeaderLeft5 = model.Department;
            report.HeaderLeft6 = model.Grade;
            report.HeaderLeft7 = model.Line;
            report.HeaderLeft13 = model.CardNo;
            report.Body9 = Helpers.ShortDateString(model.JoinningDate);
            lst.Add(report);
            this.ReportSource = lst;
        }

        #region IDCardGenarate
        /// <summary>
        /// For Specific Id Card For Employee
        /// </summary>
        public DtPayroll IDCardGenarate()
        {
            HrmsContext db = new HrmsContext();

            DtPayroll ds = new DtPayroll();

            var IDData = (from t1 in db.Employees
                          join t2 in db.Designations on t1.Designation_Id equals t2.ID
                          join t3 in db.Sections on t1.Section_Id equals t3.ID
                          join t4 in db.Departments on t1.Department_Id equals t4.ID
                          where t1.Status == true
                          select new
                          {
                              EngName = t1.Name,
                              EngDesignation = t2.Name,
                              Section = t3.SectionName,
                              CardNo = t1.CardNo,
                              JoinningDate = t1.Joining_Date,
                              Grade = t1.Grade,
                              Department=t4.DeptName
                          }).ToList();

            foreach (var v in IDData)
            {
                ds.DtEmployeeID.Rows.Add(
                   v.EngName,
                   v.CardNo,
                   v.Department,
                   v.EngDesignation,
                   v.Section,
                   v.Grade,
                   v.JoinningDate,
                   string.Empty,
                   0
                  );
            }
         return ds;

        }

        #endregion

        #region Methods

        public decimal GetEmployeeBasic(int EmployeeID)
        {
            decimal basicPay = decimal.Zero;
            HrmsContext db = new HrmsContext();
            var Vdata = db.HrmsEodRecords.Where(a => a.EmployeeId == EmployeeID && a.Eod_RefFk == 1 && a.Status == true);
            if (Vdata.Any())
            {
                basicPay = Vdata.OrderByDescending(a => a.ID).FirstOrDefault().ActualAmount;
            }

            return basicPay;
        }

        private decimal GetIncrementAmount(int EmployeeID)
        {
            decimal BasicAmount = decimal.Zero;
            decimal HouseAllowance = decimal.Zero;
            decimal OtherAllowance = decimal.Zero;
            decimal GrossSalary = decimal.Zero;
            decimal IncrementRate = decimal.Zero;
            decimal IncrementAmount = decimal.Zero;
            decimal NewBasic = decimal.Zero;
            decimal NewHouse = decimal.Zero;
            decimal NewGross = decimal.Zero;
            decimal IncrementTotal = decimal.Zero;

            HrmsContext db = new HrmsContext();
            var getRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == EmployeeID && a.Status == true);
            if (getRecord.Any())
            {
                BasicAmount = getRecord.FirstOrDefault(a => a.Eod_RefFk == 1).ActualAmount;
                HouseAllowance = getRecord.FirstOrDefault(a => a.Eod_RefFk == 2).ActualAmount;
                OtherAllowance = 1100;
                GrossSalary = BasicAmount + HouseAllowance + OtherAllowance;
                IncrementRate = 5;
                IncrementAmount = (BasicAmount * IncrementRate) / 100;
                NewBasic = BasicAmount + IncrementAmount;
                NewHouse = NewBasic * (decimal)0.4;
                NewGross = NewBasic + NewHouse + OtherAllowance;
                IncrementTotal = Math.Round(IncrementAmount + (NewHouse - HouseAllowance));
            }

            return IncrementTotal;
        }

        /// <summary>
        /// id means primaryKey and Filterid means 
        /// Designations=1,Sections=2,Departments=3,Grade=4
        /// </summary>
        /// <returns></returns>
        private string GetName(int id, int Filterid)
        {
            HrmsContext db = new HrmsContext();
            string name = string.Empty;
            if (Filterid == 1)
            {
                var c = (from t1 in db.Designations
                         where t1.ID == id
                         select new
                         {
                             Name = t1.DesigNameBangla
                         }).FirstOrDefault();
                name = c.Name;


            }
            else if (Filterid == 2)
            {

                var c = (from t1 in db.Sections
                         where t1.ID == id
                         select new
                         {
                             Name = t1.SectionName
                         }).FirstOrDefault();
                name = c.Name;
            }
            else if (Filterid == 3)
            {

                var c = (from t1 in db.Departments
                         where t1.ID == id
                         select new
                         {
                             Name = t1.DeptNameBangla
                         }).FirstOrDefault();
                name = c.Name;
            }
            //else if (Filterid == 4)
            //{
            //    DropDownData d = new DropDownData();

            //    var c = d.GetGradeList().Where(a => a.Value == id.ToString()).Select(x => new { Name = x.Text }).FirstOrDefault();

            //    name = c.Name;
            //}

            return name;
        }

        private void TotalWorkingAge(DateTime FromDate, DateTime Todate)
        {
            int Years = new DateTime(DateTime.Now.Subtract(FromDate).Ticks).Year - 1;
            DateTime PastYearDate = FromDate.AddYears(Years);
            int Months = 0;
            for (int i = 1; i <= 12; i++)
            {
                if (PastYearDate.AddMonths(i) == Todate)
                {
                    Months = i;
                    break;
                }
                else if (PastYearDate.AddMonths(i) >= Todate)
                {
                    Months = i - 1;
                    break;
                }
            }
            int Days = Todate.Subtract(PastYearDate.AddMonths(Months)).Days;

            this.TotalYear = Years;
            this.TotalMonth = Months;
            this.TotalDay = Days;
        }

        private void CalculateJobAge(DateTime FromDate, DateTime Todate)
        {

            int Years = new DateTime(DateTime.Now.Subtract(FromDate).Ticks).Year - 1;
            DateTime PastYearDate = FromDate.AddYears(Years);
            int Months = 0;
            for (int i = 1; i <= 12; i++)
            {
                if (PastYearDate.AddMonths(i) == Todate)
                {
                    Months = i;
                    break;
                }
                else if (PastYearDate.AddMonths(i) >= Todate)
                {
                    Months = i - 1;
                    break;
                }
            }
            int Days = Todate.Subtract(PastYearDate.AddMonths(Months)).Days;

            this.Year = Years;
            this.Month = Months;
            this.Day = Days;
        }

        private void EmployeeAge(DateTime Birthdate)
        {
            int age = DateTime.Now.Year - Birthdate.Year;
            if (DateTime.Now.DayOfYear < Birthdate.DayOfYear)
            {
                age = age - 1;
            }

            this.Age = age;
        }

        #endregion
    }
}