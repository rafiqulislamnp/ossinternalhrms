﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VmExpense
    {
        public string Name { get; set; }
        public int EmployeeId { get; set; }
        public decimal Amount { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}