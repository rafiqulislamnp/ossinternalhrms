﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VMCountry: RootModel
    {
        [DisplayName("Country Name")]
        [Required(ErrorMessage = "Please Select Country Name")]
        //[StringLength(100, MinimumLength = 3, ErrorMessage = "Name should be minimum 3 characters and Maximum 100 characters")]
        public string Name { get; set; }

        public IEnumerable<VMCountry> DataList { get; set; }
    }
}