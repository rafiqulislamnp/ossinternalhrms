﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VM_HRMS_Facility : RootModel
    {
        // [DisplayName("Employee ID")]
        [DisplayName("Card No")]
        public int Employee_Id { get; set; }
        //[DisplayName("Employee ID")]
        [DisplayName("Card No")]
        public string Employee_Identity { get; set; }
        public string Name { get; set; }
        [DisplayName("Item Name")]
        public string Item_Name { get; set; }
        [DisplayName("Monthly Cost")]
        public decimal Cost_Per_Month { get; set; }

        public ICollection<VM_HRMS_Facility> Facilities { get; set; }


        private HrmsContext db = new HrmsContext();

        public void GetFacilityList()
        {
            var facilities = (from f in db.HrmsFacilities
                join em in db.Employees on f.Employee_Id equals em.ID
                select new VM_HRMS_Facility()
                {
                    ID = f.ID,
                    Employee_Id = em.ID,
                    Employee_Identity = em.EmployeeIdentity,
                    Item_Name = f.Item_Name,
                    Cost_Per_Month = f.Cost_Per_Month
                }).ToList();
            Facilities = facilities;
        }
    }
}