﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VMLine : RootModel
    {
        [DisplayName("Line No")]
        [Required(ErrorMessage = "Please Select Country Name")]
        public string Line { get; set; }

        public ICollection<VMLine> CollectionVmLine { get; set; }
        HrmsContext db = new HrmsContext();

        public void GetLineNos()
        {
            var lines = (from l in db.Lines
                select new VMLine()
                {
                    ID = l.ID, Line = l.Line
                }).ToList();

            CollectionVmLine = lines;
        }
    }
}