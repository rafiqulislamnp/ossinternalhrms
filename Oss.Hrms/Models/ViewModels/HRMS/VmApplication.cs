﻿using Oss.Hrms.Models.Entity.HRMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VmApplication
    {
        public HRMS_Application Application { get; set; }
        public HRMS_Employee Employee { get; set; }
    }
}