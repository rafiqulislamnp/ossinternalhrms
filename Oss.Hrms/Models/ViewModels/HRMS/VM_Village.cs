﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VM_Village:RootModel
    {
        [DisplayName("Village Name")]
        [Required(ErrorMessage = "Enter Village")]
        public string VillageName { get; set; }

        [DisplayName("Post Office Name")]
        [Required(ErrorMessage = "Please Select Post Office")]
        public string PostOfficeName { get; set; }

        [DisplayName("Post Office Name")]
        public int PostOffice_ID { get; set; }


        [DisplayName("Police Station Name")]
        [Required(ErrorMessage = "Please Select Police Station")]
        public string PoliceStationName { get; set; }

        [DisplayName("Police Station Name")]
        // [Required(ErrorMessage = "Please Select Police Station")]
        public int PoliceStation_ID { get; set; }


        [DisplayName("District Name")]
        [Required(ErrorMessage = "Please Select District")]
        public string DistrictName { get; set; }

        [DisplayName("District Name")]
        // [Required(ErrorMessage = "Please Select Police Station")]
        public int District_ID { get; set; }

        public ICollection<VM_Village> CollectionVmDistrict { get; set; }
        HrmsContext db = new HrmsContext();

        public void GetVillageList()
        {
            var village = (from t1 in db.District
                join t2 in db.PoliceStation on t1.ID equals t2.District_ID
                join t3 in db.PostOffice on t2.ID equals t3.PoliceStation_ID
                select new
                {
                    d = t1,
                    po = t2,
                    post =t3,
                    v = db.Village.Where(x => x.PostOffice_ID == t3.ID).ToList()
                }).ToList();
            List<VM_Village> lst = new List<VM_Village>();
            foreach (var vl in village)
            {
                if (vl.v.Any())
                {
                    foreach (var v1 in vl.v)
                    {
                        VM_Village model = new VM_Village();
                        model.ID = v1.ID;
                        model.VillageName = v1.VillageName;
                        model.PostOfficeName = vl.post.PostOfficeName;
                        model.PostOffice_ID = vl.post.ID;
                        model.PoliceStationName = vl.po.PoliceStationName;
                        model.PoliceStation_ID = vl.po.ID;
                        model.DistrictName = vl.d.DistrictName;
                        model.District_ID = vl.d.ID;
                        lst.Add(model);
                    }
                   
                }
                else
                {
                    VM_Village model = new VM_Village();
                    model.PostOfficeName = vl.post.PostOfficeName;
                    model.PostOffice_ID = vl.post.ID;
                    model.PoliceStationName = vl.po.PoliceStationName;
                    model.PoliceStation_ID = vl.po.ID;
                    model.DistrictName = vl.d.DistrictName;
                    model.District_ID = vl.d.ID;
                    lst.Add(model);
                }
                
            }
            //var p = (from t1 in db.Village
            //    join t2 in db.PostOffice on t1.PostOffice_ID equals t2.ID
            //    join t3 in db.PoliceStation on t2.PoliceStation_ID equals t3.ID
            //    join t4 in db.District on t3.District_ID equals t4.ID
            //    Select new
            //{

            //}}).ToList();
            //select new VM_Village()
            //{
            //    ID = t1.ID,
            //    VillageName = t1.VillageName,
            //    PostOfficeName = t2.PostOfficeName,
            //    PostOffice_ID = t1.PostOffice_ID,
            //    PoliceStationName = t3.PoliceStationName,
            //    PoliceStation_ID = t2.PoliceStation_ID,
            //    DistrictName = t4.DistrictName,
            //    District_ID = t3.ID
            //}).ToList();
            CollectionVmDistrict = lst;
        }


        public VM_Village GetSpecificVillage(int id)
        {
            var p = (from t1 in db.Village
                join t2 in db.PostOffice on t1.PostOffice_ID equals t2.ID
                join t3 in db.PoliceStation on t2.PoliceStation_ID equals t3.ID
                join t4 in db.District on t3.District_ID equals t4.ID
                select new VM_Village()
                {
                    ID = t1.ID,
                    VillageName = t1.VillageName,
                    PostOfficeName = t2.PostOfficeName,
                    PoliceStationName = t3.PoliceStationName,
                    DistrictName = t4.DistrictName,
                    District_ID = t3.ID
                }).FirstOrDefault(x => x.ID == id);
            return p;
        }


        public VM_Village GetinformationforVillageAdd(int id)
        {
            var p = (from  t1 in db.PostOffice 
                join t2 in db.PoliceStation on t1.PoliceStation_ID equals t2.ID
                join t3 in db.District on t2.District_ID equals t3.ID where t1.ID==id
                select new VM_Village()
                {
                    PostOffice_ID = t1.ID,
                    PostOfficeName = t1.PostOfficeName,
                    PoliceStationName = t2.PoliceStationName,
                    DistrictName = t3.DistrictName,
                    District_ID = t3.ID
                }).FirstOrDefault();
            return p;
        }
    }
}