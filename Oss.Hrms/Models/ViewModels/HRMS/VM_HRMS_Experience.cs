﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VM_HRMS_Experience : RootModel
    {
        //[DisplayName("Employee ID")]
        [DisplayName("Card No")]
        public int Employee_Id { get; set; }
        // [DisplayName("Employee ID")]
        [DisplayName("Card No")]
        public string Employee_Identity { get; set; }
        public string Name { get; set; }

        [DisplayName("Company Name")]
        public string Company_Name { get; set; }
        [DisplayName("Designation")]
        public string Designation { get; set; }
        [DisplayName("Department")]
        public string Department { get; set; }
        [DisplayName("Responsibility")]
        public string Responsibility { get; set; }
        [DisplayName("Job Start Date")]
        [DataType(DataType.Date)]
        public DateTime Job_Start_Date { get; set; }
        [DisplayName("Job End Date")]
        [DataType(DataType.Date)]
        public DateTime Job_End_Date { get; set; }
        public ICollection<VM_HRMS_Experience> Experiences { get; set; }
        

        private HrmsContext db = new HrmsContext();

        public void GetExperienceList()
        {
            var exp = (from ex in db.HrmsExperiences
                join em in db.Employees on ex.Employee_Id equals em.ID
                select new VM_HRMS_Experience()
                {
                    ID = ex.ID,
                    Employee_Identity = em.EmployeeIdentity,
                    Company_Name = ex.Company_Name,
                    Designation = ex.Designation,
                    Department = ex.Department,
                    Responsibility = ex.Responsibility,
                    Job_Start_Date = ex.Job_Start_Date,
                    Job_End_Date = ex.Job_End_Date
                }).ToList();
            Experiences = exp;
        }
    }

}