﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VM_HRMS_Spouse : RootModel
    {
        [DisplayName("Card No")]
       // [DisplayName("Employee ID")]
        public int Employee_Id { get; set; }

       // [DisplayName("Employee ID")]
        [DisplayName("Card No")]
        public string Employee_Identity { get; set; }
        public string Employee_Name { get; set; }

        [DisplayName("Spouse Name")]
        [Required(ErrorMessage = "Enter Name")]
        [StringLength(100,MinimumLength = 3,ErrorMessage = "Please, Enter Minimum 3 characters.")]
        public string SpouseName { get; set; }

        //[Required(ErrorMessage = "")]
        [DataType(DataType.Date)]
        [DisplayName("Date of Birth")]
        public DateTime SpouseDOB { get; set; }

        [DisplayName("Contact No.")]
        [StringLength(11,ErrorMessage = "Enter Mobile No properly.")]
        [DataType(DataType.PhoneNumber)]
        public string Spouse_Contact_No { get; set; }

        [DisplayName("Blood Group")]
        //[Required(ErrorMessage = "Enter Blood Group")]
        public string Spouse_Blood_Group { get; set; }

        public ICollection<VM_HRMS_Spouse> VmHrmsSpouses { get; set; }


        HrmsContext db = new HrmsContext();

        // Retrieve Spouse List 
        public void GetSpouseList()
        {

            var spouseList = (from sp in db.HrmsSpouses 
                    join em in db.Employees on sp.Employee_Id equals em.ID
                    select new VM_HRMS_Spouse()
                    {
                        ID = sp.ID,
                        Employee_Identity = em.EmployeeIdentity,  Employee_Name = em.Name, SpouseName = sp.SpouseName,
                        SpouseDOB = sp.SpouseDOB,  Spouse_Contact_No = sp.Spouse_Contact_No, 
                        Spouse_Blood_Group = sp.Spouse_Blood_Group
                    }).AsEnumerable().ToList();
            VmHrmsSpouses = spouseList;

        }


        public List<VM_HRMS_Spouse> GetSpouseListWiseId(int? id)
        {
            var spouseList = (from sp in db.HrmsSpouses
                join em in db.Employees on sp.Employee_Id equals em.ID
                select new VM_HRMS_Spouse()
                {
                    ID = sp.ID,
                    Employee_Identity = em.EmployeeIdentity,
                    Employee_Name = em.Name,
                    SpouseName = sp.SpouseName,
                    SpouseDOB = sp.SpouseDOB,
                    Spouse_Contact_No = sp.Spouse_Contact_No,
                    Spouse_Blood_Group = sp.Spouse_Blood_Group
                }).ToList();
            return spouseList;
        }
    }
}