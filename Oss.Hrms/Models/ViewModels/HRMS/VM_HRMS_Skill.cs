﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VM_HRMS_Skill : RootModel
    {
        [DisplayName("Card No")]
        // [DisplayName("Employee ID")]
        public int Employee_Id { get; set; }
        [DisplayName("Card No")]
        //[DisplayName("Employee ID")]
        public string Employee_Identity { get; set; }
        public string Name { get; set; }
        [DisplayName("Skill Type")]
        public string Skill_Type { get; set; }
        [DisplayName("Skill Name")]
        public string Skill_Name { get; set; }
        [DisplayName("Skill Description")]
        public string Skill_Description { get; set; }

        public ICollection<VM_HRMS_Skill> Skills { get; set; }


        private HrmsContext db = new HrmsContext();
        public void GetSkillList()
        {
            var skillInfo = (from s in db.HrmsSkills
                join em in db.Employees on s.Employee_Id equals em.ID
                select new VM_HRMS_Skill()
                {
                    ID = s.ID,
                    Employee_Id = em.ID,
                    Employee_Identity = em.EmployeeIdentity,
                    Skill_Type = s.Skill_Type,
                    Skill_Name = s.Skill_Name,
                    Skill_Description = s.Skill_Description
                }).ToList();
            Skills = skillInfo;

        }
    }
}