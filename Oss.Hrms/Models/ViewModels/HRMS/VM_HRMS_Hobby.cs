﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VM_HRMS_Hobby : RootModel
    {
        // [DisplayName("Employee ID")]
        [DisplayName("Card No")]
        public int Employee_Id { get; set; }
        // [DisplayName("Employee ID")]
        [DisplayName("Card No")]
        public string Employee_Identity { get; set; }
        public string Name { get; set; }
        [DisplayName("Hobby Name")]
        public string Hobby_Name { get; set; }
        [DisplayName("Remarks")]
        [StringLength(100,MinimumLength = 10)]
        public string Remarks { get; set; }

        public ICollection<VM_HRMS_Hobby> Hobbies { get; set; }
        
        private HrmsContext db = new HrmsContext();

        public void getHobbyList()
        {
            var hobbies = (from h in db.HrmsHobbies
                join em in db.Employees on h.Employee_Id equals em.ID
                select new VM_HRMS_Hobby()
                {
                    ID = h.ID, Employee_Id = em.ID, Employee_Identity = em.EmployeeIdentity, Hobby_Name = h.Hobby_Name,
                    Remarks = h.Remarks
                }).ToList();

            Hobbies = hobbies;
        }
    }
}