﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VM_HRMS_Training: RootModel
    {
        [DisplayName("Card No")]
        //[DisplayName("Employee ID")]
        public int Employee_Id { get; set; }
        [DisplayName("Card No")]
        // [DisplayName("Employee ID")]
        public string Employee_Identity { get; set; }
        public string Name { get; set; }
        [DisplayName("Course Name")]
        public string Course_Name { get; set; }
        [DisplayName("Institute")]
        public string Institute { get; set; }
        [DisplayName("Country")]
        public string Country { get; set; }
        [DisplayName("Start Date")]
        [DataType(DataType.Date)]
        public DateTime Course_Start_Date { get; set; }

        [DisplayName("End Date")]
        [DataType(DataType.Date)]
        public DateTime Course_End_Date { get; set; }
        [DisplayName("Duration")]
        public string Course_Duration { get; set; }

        public ICollection<VM_HRMS_Training> trainingInfo { get; set; }


       private HrmsContext db = new HrmsContext();

        public void GetTrainingInfoList()
        {
            var training = (from t in db.HrmsTrainings
                join em in db.Employees on t.Employee_Id equals em.ID
                select new VM_HRMS_Training
                {
                    ID = t.ID,
                    Employee_Id = em.ID,
                    Employee_Identity = em.EmployeeIdentity,
                    Course_Name = t.Course_Name,
                    Institute = t.Institute,
                    Country = t.Country,
                    Course_Start_Date = t.Course_Start_Date,
                    Course_End_Date = t.Course_End_Date,
                    Course_Duration = t.Course_Duration
                }).ToList();

            trainingInfo = training;
        }
    }
}