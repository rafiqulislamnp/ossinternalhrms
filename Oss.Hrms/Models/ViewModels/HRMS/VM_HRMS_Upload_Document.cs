﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.HRMS
{
    public class VM_HRMS_Upload_Document:RootModel

    {
        [DisplayName("Card No")]
        public int Employee_Id { get; set; }

    [ForeignKey("Employee_Id")]
    public HRMS_Employee HrmsEmployee { get; set; }

    [Column(TypeName = "NVARCHAR")]
    [StringLength(100)]
    public string Document_Type { get; set; }

    [Column(TypeName = "NVARCHAR")]
    [StringLength(100)]
    public string Document_Name { get; set; }

    [Column(TypeName = "NVARCHAR")]
    [StringLength(100)]
    public string Document { get; set; }

    [Column(TypeName = "NVARCHAR")]
    [StringLength(100)]
    public string DocumentPath { get; set; }

    }
}