﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Oss.Hrms.Models;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Entity.User;
using Oss.Hrms.Models.Services;
using Oss.Hrms.Models.ViewModels.Attendance;
using System.Security.Cryptography;
using System.Text;

namespace Oss.Hrms.Models.ViewModels
{
    public class VM_User : RootModel
    {

        HrmsContext db = new HrmsContext();
        //public User  User { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int UserRoleFk { get; set; }
        public string LblMessage { get; set; }
        public bool IsSupervisor { get; set; }

        public IEnumerable<VM_User> DataList { get; set; }




        //public void SelectSingle(string  username, string pass)
        //{
        //    var v = (from o in db.Users
        //             where (o.Status == true && UserName == username && Password== pass)
        //             select new VM_User()
        //             {
        //                 ID = o.ID,
        //                 UserName = o.UserName,
        //                Password=o.Password
        //             }).FirstOrDefault();
        //    this.User = v.User;
        //}

        public VM_User LoginUser(bool authorised)
        {
            db = new HrmsContext();
            var pass = GetHashedPassword(this.Password);
            var v = (from t1 in db.User_User
                     where t1.UserName == UserName && t1.Password == pass && t1.Status == true
                     select new VM_User
                     {
                         ID = t1.ID,
                         UserName = t1.UserName,
                         UserRoleFk = t1.User_RoleFK ?? 0
                     }).FirstOrDefault();
            
            if (v != null)
            {
                v.IsSupervisor = false;
                var sup = (from t1 in db.Employees
                           join t2 in db.User_User on t1.SupervisorFk equals t2.HRMS_EmployeeFK
                           where t2.ID == v.ID
                           select new
                           {
                               t1.SupervisorFk
                           }).FirstOrDefault();


                if (sup != null)
                {
                    v.IsSupervisor = true;
                }
            }
            return v;
        }

        //public int LoginUser1(bool authorised)
        //{
        //    db = new HrmsContext();
        //    int flag = 0;

        //    var pass = GetHashedPassword(this.Password);
        //    var v = (from t1 in db.User_User
        //             join t2 in db.User_Role on t1.User_RoleFK equals t2.ID
        //        where t1.Status == true && t1.UserName == UserName && t1.Password == pass
        //             select new VM_User()
        //        {
        //            ID = t1.ID,
        //            UserName = t1.UserName,
        //            Password = t1.Password
        //        }).FirstOrDefault();
        //    if (v != null)
        //    {
        //        flag = v.ID;
        //        //User.ID=flag
        //        //this.User.ID = flag;
        //        //this.User.UserName = v.UserName;
        //        //this.User.Password = v.Password;
        //    }
        //    if (flag > 0)
        //    {
        //        if (v.Status == false)
        //        {
        //            this.LblMessage = "You are not authorised!";
        //            return 0;
        //        }
        //    }
        //    else
        //    {
        //        this.LblMessage = "User / Password does not match!";
        //    }
        //    return flag;
        //}
        private string GetHashedPassword(string password)
        {
            char[] array = password.ToCharArray();
            Array.Reverse(array);
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(array);
            byte[] hash = md5.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("x2"));
            }
            return sb.ToString();
        }
    }
}