﻿using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;
using Oss.Hrms.Models.ViewModels.HRMS;
using Oss.Hrms.Models.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Oss.Hrms.Models.ViewModels.Bonus
{
    public class VMBonus : RootModel
    {
        public List<VMBonus> DataList { get; set; }
        HrmsContext db = new HrmsContext();

        [Display(Name = "Employee"), Required(ErrorMessage = "Employee is Required.")]
        public int EmployeeID { get; set; }

        public string EmployeeName { get; set; }

        [Display(Name = "Card Title"), Required(ErrorMessage = "Card title is required.")]
        public string CardTitle { get; set; }

        public string CardNo { get; set; }
        public string Designation { get; set; }
        public string Section { get; set; }
        public string Grade { get; set; }
        public string EmployeeIdentity { get; set; }
        public string Name { get; set; }

        [Display(Name = "Designation")]
        public int DesignationId { get; set; }
        [Display(Name = "Section")]
        public int? SectionID { get; set; }
        [Display(Name = "Unit"), Required(ErrorMessage = "Unit is required.")]
        public int UnitID { get; set; }

        [Display(Name = "Card Title"), Required(ErrorMessage = "Card title is required.")]
        public int CardID { get; set; }

        [Display(Name = "Bonus Title"), Required(ErrorMessage = "Bonus Title is required.")]
        public int BonusId { get; set; }

        [DisplayName("Bonus Title"), Required(ErrorMessage = "Bonus Title is required.")]
        public string BonusTitle { get; set; }

        public string BonusStatus { get; set; }

        [DisplayName("From Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }

        [DisplayName("To Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }
        
        [DisplayName("Payment Date")]
        [Required(ErrorMessage = "Payment date is required"), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime PaymentDate { get; set; }

        [DisplayName("Join Date")]
        public DateTime JoinDate { get; set; }

        public int JobYear { get; set; }
        public int JobMonth { get; set; }
        public int JobDay { get; set; }

        public decimal BonusRate { get; set; }
        public decimal BonusAmount { get; set; }
        public decimal StampDeduction { get; set; }
        public decimal PayableAmount { get; set; }
        public int EodReferenceId { get; set; }

        #region SalaryModel

        [Display(Name = "Gross Salary")]
        public decimal GrossSalary { get; set; }
        [Display(Name = "Basic Pay"), Required(ErrorMessage = "Basic Pay required.")]
        public decimal BasicAmount { get; set; }
        
        #endregion

        [Display(Name = "Basic")]
        public string BasicPay { get; set; }
        public string PreviewJoinDate { get; set; }
        public string TenureDate { get; set; }
        public string BonusPercentRate { get; set; }


        public HRMS_BonusInfo HRMS_BonusInfo { get; set; }
        public VMEmployee VMEmployee { get; set; }
        public List<VMEmployee> ListEmployee { get; set; }


        public void GetBonusList()
        {
            var vData = db.HrmsBonuses.Where(a=>a.Status==true).OrderByDescending(a=>a.ID).Select(a=>new VMBonus {
                BonusId=a.ID,
                BonusTitle=a.BonusTitle,
                PaymentDate=a.PaymentDate,
                BonusStatus=a.CloseBonus==false?"Running":"Closed"
            }).ToList();

            this.DataList = vData;
        }

        public void GetCreateBonus(VMBonus model)
        {
            HrmsContext db = new HrmsContext();

            HRMS_Bonus bonus = new HRMS_Bonus
            {
                BonusTitle = model.BonusTitle,
                BonusYear = model.PaymentDate.Year,
                PaymentDate=model.PaymentDate,
                CloseBonus=false,
                Entry_Date=model.Entry_Date,
                Update_Date=model.Entry_Date,
                Entry_By=model.Entry_By,

            };

            db.HrmsBonuses.Add(bonus);
            db.SaveChanges();
        }

        public void GetBonusInfo(int BonusId)
        {
            HrmsContext db = new HrmsContext();
            var getBonus = db.HrmsBonuses.Where(a => a.ID == BonusId).Select(o => new { o });
                             
            if (getBonus.Any())
            {
                var takeBonus = getBonus.FirstOrDefault();
                this.BonusTitle = takeBonus.o.BonusTitle;
                this.BonusId = takeBonus.o.ID;
                this.PaymentDate = takeBonus.o.PaymentDate;
            }
        }

        public void GetProcessBonus(VMBonus model)
        {
            DropDownData dr = new DropDownData();
            model.CardTitle = dr.GetCardTitle(model.CardID);

            List<HRMS_BonusInfo> lstBonus = new List<HRMS_BonusInfo>();

            var activeEmployeeList = db.Employees.Where(a => a.Present_Status == 1 && a.CardTitle.Equals(model.CardTitle) && a.Unit_Id == model.UnitID).Select(a => new { ID = a.ID, JoinDate=a.Joining_Date }).ToList();
                                      
            foreach (var emp in activeEmployeeList)
            {
                model.TotalWorkingAge(emp.JoinDate, model.PaymentDate);

                HRMS_BonusInfo item = new HRMS_BonusInfo();
                item.BonusIdFK = model.BonusId;
                item.Status = true;
                item.EmployeeIdFK = emp.ID;
                item.EodReferenceIdFK = 13;
                var eodRecord = db.HrmsEodRecords.Where(a => a.EmployeeId == emp.ID && a.Eod_RefFk==1 && a.Status == true).ToList();
                if (eodRecord.Any())
                {
                    item.BasicAmount = eodRecord.FirstOrDefault().ActualAmount;
                    item.GrossAmount = Math.Round((item.BasicAmount + (item.BasicAmount * (decimal)0.4) + 1100), 0, MidpointRounding.AwayFromZero);
                    item.Year = model.JobYear;
                    item.Month = model.JobMonth;
                    item.Day = model.JobDay;
                    item.Entry_By = model.Entry_By;
                    item.Entry_Date = model.Entry_Date;
                    item.Update_Date = model.Entry_Date;

                    if (model.JobYear >= 1)
                    {
                        item.BonusRate = 100;
                        item.BonusAmount = item.BasicAmount;
                        item.Stamp = 10;
                    }
                    else if (model.JobYear<1 && model.JobMonth>=6)
                    {
                        item.BonusRate = 50;
                        item.BonusAmount = Math.Round((item.BasicAmount * (decimal)0.5), 0, MidpointRounding.AwayFromZero);
                        item.Stamp = 10;
                    }
                    else if (model.JobYear < 1 && model.JobMonth<6 && model.JobMonth >= 3)
                    {
                        item.BonusRate = 25;
                        item.BonusAmount = Math.Round((item.BasicAmount * (decimal)0.25), 0, MidpointRounding.AwayFromZero);
                        item.Stamp = 10;
                    }
                    else if (model.JobYear < 1 && model.JobMonth < 3)
                    {
                        item.BonusRate = 0;
                        item.BonusAmount = 500;
                        item.Stamp = 0;
                    }

                    lstBonus.Add(item);
                }
            }

            if (lstBonus.Any())
            {
                foreach (var bonusInfo in lstBonus)
                {
                    db.HrmsBonusInfos.Add(bonusInfo);
                }
            }
        }

        public async Task<int> Save()
        {
            return await db.SaveChangesAsync();
        }

        public void TotalWorkingAge(DateTime FromDate, DateTime Todate)
        {
            int Years = new DateTime(DateTime.Now.Subtract(FromDate).Ticks).Year - 1;
            DateTime PastYearDate = FromDate.AddYears(Years);
            int Months = 0;
            for (int i = 1; i <= 12; i++)
            {
                if (PastYearDate.AddMonths(i) == Todate)
                {
                    Months = i;
                    break;
                }
                else if (PastYearDate.AddMonths(i) >= Todate)
                {
                    Months = i - 1;
                    break;
                }
            }
            int Days = Todate.Subtract(PastYearDate.AddMonths(Months)).Days;

            this.JobYear = Years;
            this.JobMonth = Months;
            this.JobDay = Days;
        }

        public void GetEmployeeBonus(int BonusId)
        {
            var vData = (from o in db.HrmsBonuses
                         join p in db.HrmsBonusInfos on o.ID equals p.BonusIdFK
                         join q in db.Employees on p.EmployeeIdFK equals q.ID
                         join r in db.Designations on q.Designation_Id equals r.ID
                         join s in db.Sections on q.Section_Id equals s.ID
                         where o.ID == BonusId && o.Status == true
                         select new VMBonus
                         {
                             BonusId = o.ID,
                             EmployeeID = p.EmployeeIdFK,
                             EmployeeName = q.Name,
                             BonusTitle = o.BonusTitle,
                             CardNo = q.CardTitle + " " + q.EmployeeIdentity,
                             CardTitle = q.CardTitle,
                             Designation = r.Name,
                             SectionID = s.ID,
                             Section = s.SectionName,
                             JoinDate = q.Joining_Date,
                             JobYear = p.Year,
                             JobMonth = p.Month,
                             JobDay = p.Day,
                             TenureDate = p.Year + "(Y) "+ p.Month + "(M) " + p.Day + "(D)",
                             BasicAmount = p.BasicAmount,
                             GrossSalary = p.GrossAmount,
                             BonusRate = p.BonusRate,
                             BonusAmount = p.BonusAmount,
                             StampDeduction = p.Stamp,
                             PayableAmount = p.BonusRate > 0 == true ? p.BonusAmount - p.Stamp : p.BonusAmount
                         }).OrderBy(a => a.SectionID).ThenBy(a => a.CardNo).ToList();
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    v.PreviewJoinDate = Helpers.ShortDateString(v.JoinDate);
                    v.BonusPercentRate = v.BonusRate > 0 == true ? (int)v.BonusRate + " %" : "0 %";
                }
            }
            
            this.DataList = vData;
        }

        public void CloseBonus(int ID)
        {
            var closeBonus = db.HrmsBonuses.Where(a => a.ID == ID);
            if (closeBonus.Any())
            {
                closeBonus.FirstOrDefault().CloseBonus = true;
            }
        }
    }
}