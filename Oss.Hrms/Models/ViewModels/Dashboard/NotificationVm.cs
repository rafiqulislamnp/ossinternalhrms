﻿using Oss.Hrms.Models.Entity.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.ViewModels.Dashboard
{
    public class NotificationVm
    {
        public Notification Notification { get; set; }
        public string Author { get; set; }
    }
    public class NotificationViewModel
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Header { get; set; }
        public int flag { get; set; } //1 for notification & 2 for salary notification
    }
}