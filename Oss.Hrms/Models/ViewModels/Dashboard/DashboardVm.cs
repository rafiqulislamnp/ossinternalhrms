﻿using Oss.Hrms.Models.Entity.Common;
using Oss.Hrms.Models.Services;
using Oss.Hrms.Models.ViewModels.Attendance;
using Oss.Hrms.Models.ViewModels.HRMS;
using Oss.Hrms.Models.ViewModels.Leave;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.ViewModels.Dashboard
{
    public class DashboardVm
    {
        private HrmsContext db = new HrmsContext();
        public List<VM_HRMS_Leave_Assign> VM_HRMS_Leave_Assign { get; set; }
        public List<NotificationViewModel> Notification { get; set; }
        public List<VM_HRMS_Leave_Application> LeaveApplication { get; set; }
        public List<VmApplication> Applications { get; set; }
        public string Intime { get; set; }
        public List<VM_HRMS_Daily_Instant_Attendance> AttendanceHistory { get; set; }
        public DashboardVm Data { get; set; }


        public void InitialDataLoad(int empid)
        {
            Data = new DashboardVm
            {
                VM_HRMS_Leave_Assign = LeaveStatus(empid),
                Notification = Notifications(empid),
                LeaveApplication = LeaveApplications(empid),
                Applications = Application(empid),
                Intime = GetInTime(empid),
                AttendanceHistory = GetAttendanceHistory(empid)
            };

        }
        public List<VM_HRMS_Leave_Assign> LeaveStatus(int empid)
        {
            db = new HrmsContext();
            var a = (from t1 in db.HrmsLeaveAssigns
                     join t2 in db.Employees on t1.EmployeeId equals t2.ID
                     join t4 in db.User_User on t2.ID equals t4.HRMS_EmployeeFK
                     join t3 in db.HrmsLeaveTypes on t1.LeaveTypeId equals t3.ID
                     where (t1.Year == DateTime.Now.Year.ToString() && t4.ID == empid) && t1.Status == true
                     select new VM_HRMS_Leave_Assign
                     {
                         ID = t1.ID,
                         EmployeeId = t1.EmployeeId,
                         EmployeeIdentity = t2.EmployeeIdentity,
                         CardTitle = t2.CardTitle,
                         Name = t2.Name,
                         LeaveTypeId = t1.LeaveTypeId,
                         LeaveName = t3.Leave_Name,
                         year = t1.Year,
                         Total = t1.Total,
                         Taken = t1.Taken,
                         Remaining = t1.Remaining,
                     }).ToList();

            if (a.Any())
            {
                return a;
            }
            return null;
        }
        public List<NotificationViewModel> Notifications(int empid)
        {
            db = new HrmsContext();
            DateTime now = DateTime.Now;
            var startDate = new DateTime(now.Year, now.Month, 1);
            var endDate = startDate.AddMonths(1).AddDays(-1);
            var ndata = db.Notification.Where(x => x.Status == true).Select(x => new NotificationViewModel { Id = x.ID, Date = x.Date, Header = x.HeadLine, flag = 1 }).ToList();


            var sdata = (from t1 in db.PayrollDetails
                         join t2 in db.PayrollMaster on t1.PayrollMasterFk equals t2.ID
                         join t3 in db.Employees on t1.EmployeeFK equals t3.ID
                         join t4 in db.User_User on t3.ID equals t4.HRMS_EmployeeFK
                         where t1.Status == true && t2.Status == true && t4.ID == empid
                         && t2.Entry_Date >= startDate && t2.Entry_Date <= endDate
                         select new NotificationViewModel
                         {
                             Id = t1.ID,
                             Date = t2.Entry_Date,
                             Header = "",
                             flag = 2
                         }).ToList();
            var data = ndata.Union(sdata).OrderByDescending(x => x.Date).ToList();
            if (data.Any())
            {
                return data;
            }
            return null;
        }
        public List<VM_HRMS_Leave_Application> LeaveApplications(int empid)
        {
            db = new HrmsContext();
            DateTime now = DateTime.Now;
            var startDate = new DateTime(now.Year, now.Month, 1);
            var endDate = startDate.AddMonths(1).AddDays(-1);
            var role = db.User_User.Single(x => x.ID == empid).User_RoleFK;
            if (role == 1)
            {
                var Leavelist = db.Database
                .SqlQuery<VM_HRMS_Leave_Application>(
                    $@"Exec [dbo].[sp_ViewDateRangeWiseLeaveInfo] '{startDate}', '{
                            endDate
                        }'").Where(x => x.SupervisorApprovalStatus == 1 && x.LeaveStatus == 0)
               .OrderByDescending(x => x.ID).ToList();
                return Leavelist;
            }
            else
            {

                var employeeid = (from t1 in db.Employees
                                  join t2 in db.User_User on t1.ID equals t2.HRMS_EmployeeFK
                                  where t2.ID == empid
                                  select new { t1.ID }).FirstOrDefault();
                var Leavelist = db.Database
                      .SqlQuery<VM_HRMS_Leave_Application>(
                          $@"Exec [dbo].[sp_ViewDateRangeWiseSpecificLeaveInfo] '{startDate}', '{
                                  endDate
                              }', '{employeeid.ID}'").OrderBy(x => x.SupervisorApprovalStatus).ThenByDescending(x => x.ID).ToList();
                return Leavelist;
            }
        }
        public List<VmApplication> Application(int empid)
        {
            db = new HrmsContext();

            var role = db.User_User.Single(x => x.ID == empid).User_RoleFK;
            if (role == 1)
            {
                var model = (from t1 in db.HRMS_Application
                             join t2 in db.Employees on t1.EmployeeFk equals t2.ID
                             where t1.Status == true
                             orderby t1.ID descending
                             select new VmApplication
                             {
                                 Application = t1,
                                 Employee = t2
                             }).ToList();
                return model;
            }
            else
            {
                var employeeid = (from t1 in db.Employees
                                  join t2 in db.User_User on t1.ID equals t2.HRMS_EmployeeFK
                                  where t2.ID == empid
                                  select new { t1.ID }).FirstOrDefault();
                var model = (from t1 in db.HRMS_Application
                             join t2 in db.Employees on t1.EmployeeFk equals t2.ID
                             where t1.EmployeeFk == employeeid.ID
                             orderby t1.ID descending
                             select new VmApplication
                             {
                                 Application = t1,
                                 Employee = t2
                             }).ToList();
                return model;
            }

        }
        public string GetInTime(int empid)
        {
            db = new HrmsContext();
            var employeeid = (from t1 in db.Employees
                              join t2 in db.User_User on t1.ID equals t2.HRMS_EmployeeFK
                              where t2.ID == empid
                              select new { t1.ID }).FirstOrDefault();
            var today = DateTime.Today;
            var time = db.HrmsDailyAttendances.Where(x => x.EmployeeId == employeeid.ID && DbFunctions.TruncateTime(x.Date) == DbFunctions.TruncateTime(today)).Select(x => x.InTime).FirstOrDefault();
            if (time != DateTime.MinValue)
            {
                return time.ToLongTimeString();
            }
            return "";
        }
        public List<VM_HRMS_Daily_Instant_Attendance> GetAttendanceHistory(int empid)
        {
            db = new HrmsContext();
            var employeeid = (from t1 in db.Employees
                              join t2 in db.User_User on t1.ID equals t2.HRMS_EmployeeFK
                              where t2.ID == empid
                              select new { t1.ID }).FirstOrDefault();
            DateTime today = DateTime.Today;
            DateTime lastday = DateTime.Today.AddDays(-3);
            var data = (from t1 in db.HrmsAttendanceHistory
                        where t1.EmployeeId == employeeid.ID && (t1.Date >= lastday && t1.Date <= today)
                        orderby t1.Date descending
                        select new VM_HRMS_Daily_Instant_Attendance
                        {
                            Date = t1.Date,
                            InTime = t1.InTime,
                            OutTime = t1.OutTime,
                            Remarks = t1.Remarks
                        }).ToList();
            return data;
        }
    }
}