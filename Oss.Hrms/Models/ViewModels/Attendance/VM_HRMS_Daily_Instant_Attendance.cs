﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity.SqlServer;
using System.Web;
using Oss.Hrms.Models;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Entity.Common;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.ViewModels.Attendance
{

    public class VM_HRMS_Daily_Instant_Attendance : RootModel
    {
        public HrmsContext db = new HrmsContext();

        [DisplayName("Employee ID")]
        [Required(ErrorMessage = "Employee ID is Required")]
        //[StringLength(80, MinimumLength = 2, ErrorMessage = "Job Title should be minimum 2 characters and Maximum 100 characters")]
        public int EmployeeId { get; set; }

        [DisplayName("Date")]
        [Required(ErrorMessage = "Date is Required")]
        //[StringLength(80, MinimumLength = 2, ErrorMessage = "Job Title should be minimum 2 characters and Maximum 100 characters")]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [DisplayName("In Time")]
        // [Required(ErrorMessage = "In Time is Required")]
        public string InTime2 { get; set; }

        [DisplayName("Out Time")]
        // [Required(ErrorMessage = "Out Time is Required")]
        public string OutTime2 { get; set; }

        [DisplayName("In Time")]
        // [Required(ErrorMessage = "In Time is Required")]
        //[StringLength(80, MinimumLength = 2, ErrorMessage = "Job Title should be minimum 2 characters and Maximum 100 characters")]
        //  [DataType(DataType.Time)]

        public TimeSpan InTime1 { get; set; }
        public DateTime InTime { get; set; }
        [DisplayName("Out Time")]
        //  [Required(ErrorMessage = "Out Time is Required")]
        //[StringLength(80, MinimumLength = 2, ErrorMessage = "Job Title should be minimum 2 characters and Maximum 100 characters")]
        //  [DataType(DataType.Time)]
        public DateTime OutTime { get; set; }
        public TimeSpan OutTime1 { get; set; }

        [DisplayName("Total Time")]
        // [Required(ErrorMessage = "In Time is Required")]
        //[StringLength(80, MinimumLength = 2, ErrorMessage = "Job Title should be minimum 2 characters and Maximum 100 characters")]
        public int TotalTime { get; set; }
        [DisplayName("Late")]
        public int Late { get; set; }
        [DisplayName("Early")]
        public int Early { get; set; }

        //[DisplayName("Over Time")]
        //public int OverTime { get; set; }
        [DisplayName("Status")]
        public int AttendanceStatus { get; set; }
        [DisplayName("Card No")]
        [StringLength(maximumLength: 50)]
        public string CardNo { get; set; }
        [DisplayName("Card Title")]
        public string CardTitle { get; set; }
        [StringLength(maximumLength: 100)]
        public string Remarks { get; set; }

        [DisplayName("Shift Name")]
        public int ShiftTypeId { get; set; }

        [DisplayName("Holiday")]
        public int HolidayType { get; set; }
        [DisplayName("Leave Paid Type")]
        public int LeavePaidType { get; set; }
        [DisplayName("Freeze Status")]
        public int IsFreeze { get; set; }


        [DisplayName("End Date")]
        [Required(ErrorMessage = "End Date is Required")]
        //[StringLength(80, MinimumLength = 2, ErrorMessage = "Job Title should be minimum 2 characters and Maximum 100 characters")]
        [DataType(DataType.Date)]
        public DateTime toDate { get; set; }

        [DisplayName("Employee ID")]
        public string EmployeeIdentity { get; set; }


        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Shift Name")]
        public string ShiftName { get; set; }

        public string TotalDuration { get; set; }

        [DisplayName("Over Time")]
        public int Overtime1 { get; set; }

        [DisplayName("Over Time")]
        public string Overtime { get; set; }

        [DisplayName("Break Time")]
        public string OvertimeBreak { get; set; }

        [DisplayName("Payable Overtime")]
        public int CalculatedOT { get; set; }
        [DisplayName("Section")]
        public string Section_Name { get; set; }
        public string Designation_Name { get; set; }
        public IEnumerable<VM_HRMS_Daily_Instant_Attendance> DataListHrmsDailyInstantAttendances { get; set; }
        //public ICollection<VM_HRMS_Daily_Instant_Attendance> InstantAttendances { get; set; }
        public VM_HRMS_Daily_Instant_Attendance VmHrmsDailyInstantAttendance { get; set; }


        public void GetInstandAttendaceList()
        {

        }
        //var ledger = db.Database.SqlQuery<StoreLedger>( $@"EXEC dbo.StoreItemLedgerItemWise '{itemid}', '{fromdate}', '{todate}'").ToList();
        public void LoadEmployeeDailyAttendanceData(VM_HRMS_Daily_Instant_Attendance vm)
        {
            // DateDiff dd = new DateDiff();

            var a = new List<VM_HRMS_Daily_Instant_Attendance>();
            if (vm.ShiftTypeId == 100)
            {
                a = db.Database.SqlQuery<VM_HRMS_Daily_Instant_Attendance>(
                   "Exec [dbo].[sp_ViewDateRangeWiseAttendance] @fromDate='" + vm.Date + "', @todate='" + vm.toDate +
                   "'").ToList();

                //a = (from t1 in db.HrmsDailyAttendances
                //     join t2 in db.Employees on t1.EmployeeId equals t2.ID
                //     join t3 in db.HrmsShiftTypes on t1.ShiftTypeId equals t3.ID
                //     where (t1.Date >= vm.Date && t1.Date <= vm.toDate)
                //     select new VM_HRMS_Daily_Instant_Attendance()
                //     {
                //         ID = t1.ID,
                //         EmployeeId = t1.EmployeeId,
                //         EmployeeIdentity = t2.EmployeeIdentity,
                //         Name = t2.Name,
                //         ShiftName = t3.ShiftName,
                //         Date = t1.Date,
                //         InTime = t1.InTime,
                //         OutTime = t1.OutTime,
                //         TotalDuration = dd.GetTotalTimeDurationonly(t1.InTime.ToString(), t1.OutTime.ToString()),
                //         AttendanceStatus = t1.AttendanceStatus,
                //         Remarks = t1.Remarks
                //     }).AsEnumerable().ToList();
            }
            else
            {
                //a = (from t1 in db.HrmsDailyAttendances
                //     join t2 in db.Employees on t1.EmployeeId equals t2.ID
                //     join t3 in db.HrmsShiftTypes on t1.ShiftTypeId equals t3.ID
                //     where (t1.Date >= vm.Date && t1.Date <= vm.toDate) & t1.ShiftTypeId == vm.ShiftTypeId
                //     select new VM_HRMS_Daily_Instant_Attendance()
                //     {
                //         ID = t1.ID,
                //         EmployeeId = t1.EmployeeId,
                //         EmployeeIdentity = t2.EmployeeIdentity,
                //         Name = t2.Name,
                //         ShiftName = t3.ShiftName,
                //         Date = t1.Date,
                //         InTime = t1.InTime,
                //         OutTime = t1.OutTime,
                //         TotalDuration = dd.GetTotalTimeDurationonly(t1.InTime.ToString(), t1.OutTime.ToString()),
                //         AttendanceStatus = t1.AttendanceStatus,
                //         Remarks = t1.Remarks
                //     }).AsEnumerable().ToList();
            }

            this.DataListHrmsDailyInstantAttendances = a;
        }


        //public void LoadEmployeeDailyAttendance(VM_HRMS_Daily_Instant_Attendance vm)
        //{
        //   // DateDiff dd =new DateDiff();

        //    var a= new List<VM_HRMS_Daily_Instant_Attendance>();
        //    if (vm.ShiftTypeId == 100)
        //    {
        //         a = (from t1 in db.HrmsDailyAttendances
        //            join t2 in db.Employees on t1.EmployeeId equals t2.ID
        //            join t3 in db.HrmsShiftTypes on t1.ShiftTypeId equals t3.ID
        //            where (t1.Date >= vm.Date && t1.Date <= vm.toDate)
        //            select new VM_HRMS_Daily_Instant_Attendance()
        //            {
        //                ID = t1.ID,
        //                EmployeeId = t1.EmployeeId,
        //                EmployeeIdentity = t2.EmployeeIdentity,
        //                Name = t2.Name,
        //                ShiftName = t3.ShiftName,
        //                Date = t1.Date,
        //                InTime = t1.InTime,
        //                OutTime = t1.OutTime,
        //                TotalDuration = dd.GetTotalTimeDurationonly(t1.InTime.ToString(), t1.OutTime.ToString()),
        //                AttendanceStatus = t1.AttendanceStatus,
        //                Remarks = t1.Remarks
        //            }).AsEnumerable().ToList();
        //    }
        //    else
        //    {
        //         a = (from t1 in db.HrmsDailyAttendances
        //            join t2 in db.Employees on t1.EmployeeId equals t2.ID
        //            join t3 in db.HrmsShiftTypes on t1.ShiftTypeId equals t3.ID
        //            where (t1.Date >= vm.Date && t1.Date <= vm.toDate) & t1.ShiftTypeId == vm.ShiftTypeId
        //            select new VM_HRMS_Daily_Instant_Attendance()
        //            {
        //                ID = t1.ID,
        //                EmployeeId = t1.EmployeeId,
        //                EmployeeIdentity = t2.EmployeeIdentity,
        //                Name = t2.Name,
        //                ShiftName = t3.ShiftName,
        //                Date = t1.Date,
        //                InTime = t1.InTime,
        //                OutTime = t1.OutTime,
        //                TotalDuration = dd.GetTotalTimeDurationonly(t1.InTime.ToString(), t1.OutTime.ToString()),
        //                AttendanceStatus = t1.AttendanceStatus,
        //                Remarks = t1.Remarks
        //            }).AsEnumerable().ToList();
        //    }

        //    this.DataListHrmsDailyInstantAttendances = a;
        //}


        public VM_HRMS_Daily_Instant_Attendance LoadSpecificEmployeeDailyAttendance(int? id)
        {
            var att = new VM_HRMS_Daily_Instant_Attendance();
            var Attendancelist = db.Database
                .SqlQuery<VM_HRMS_Daily_Instant_Attendance>(
                    $@"Exec [dbo].[sp_ViewSpecificEmployeeAttendance] '{id}'").ToList();
            att = (from t1 in Attendancelist
                   where t1.ID == id
                   select new VM_HRMS_Daily_Instant_Attendance
                   {
                       ID = t1.ID,
                       EmployeeId = t1.EmployeeId,
                       EmployeeIdentity = t1.EmployeeIdentity,
                       Name = t1.Name,
                       ShiftTypeId = t1.ShiftTypeId,
                       ShiftName = t1.ShiftName,
                       Date = t1.Date,
                       InTime2 = t1.InTime == new DateTime(1900, 01, 01, 00, 00, 00) ? DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") : t1.InTime.ToString("dd/MM/yyyy HH:mm:ss"),
                       OutTime2 = t1.OutTime == new DateTime(1900, 01, 01, 00, 00, 00) ? DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") : t1.OutTime.ToString("dd/MM/yyyy HH:mm:ss"),
                       TotalDuration = t1.TotalDuration,
                       AttendanceStatus = t1.AttendanceStatus,
                       Overtime1 = t1.Overtime1,
                       Remarks = t1.Remarks
                   }).SingleOrDefault();

            return att;
        }

        // Load Specific Employee Attedance History from HRMS_Attendance_History
        public VM_HRMS_Daily_Instant_Attendance LoadSpecificEmployeeDailyAttendanceRecord(int? id)
        {
            var att = new VM_HRMS_Daily_Instant_Attendance();
            var Attendancelist = db.Database
                .SqlQuery<VM_HRMS_Daily_Instant_Attendance>(
                    $@"Exec [dbo].[sp_ViewSpecificEmployeeAttendanceRecord] '{id}'").ToList();
            att = (from t1 in Attendancelist
                   where t1.ID == id
                   select new VM_HRMS_Daily_Instant_Attendance
                   {
                       ID = t1.ID,
                       EmployeeId = t1.EmployeeId,
                       EmployeeIdentity = t1.EmployeeIdentity,
                       Name = t1.Name,
                       CardTitle = t1.CardTitle,
                       Designation_Name = t1.Designation_Name,
                       ShiftTypeId = t1.ShiftTypeId,
                       ShiftName = t1.ShiftName,
                       Date = t1.Date,
                       InTime2 = t1.InTime2,
                       OutTime2 = t1.OutTime2,
                       TotalDuration = t1.TotalDuration,
                       AttendanceStatus = t1.AttendanceStatus,
                       Overtime1 = t1.Overtime1,
                       Remarks = t1.Remarks
                   }).SingleOrDefault();

            return att;
        }


        //
        //public void SelectSingle(string iD)
        //{
        //    int id = 0;
        //    iD = this.VmControllerHelper.Decrypt(iD);
        //    Int32.TryParse(iD, out id);

        //    db = new xOssContext();
        //    var v = (from rc in db.Mkt_Buyer
        //        join t2 in db.Common_Country
        //            on rc.Common_CountryFk equals t2.ID
        //        select new VmMkt_Buyer
        //        {
        //            Common_Country = t2,
        //            Mkt_Buyer = rc
        //        }).Where(c => c.Mkt_Buyer.ID == id).SingleOrDefault();
        //    this.Mkt_Buyer = v.Mkt_Buyer;
        //    this.Common_Country = v.Common_Country;
        //}
        //public void SelectSingle(string iD)
        //{
        //    int id = 0;
        //    iD = this.VmControllerHelper.Decrypt(iD);
        //    Int32.TryParse(iD, out id);

        //    db = new xOssContext();
        //    var v = (from rc in db.Mkt_Buyer
        //        join t2 in db.Common_Country
        //            on rc.Common_CountryFk equals t2.ID
        //        select new VmMkt_Buyer
        //        {
        //            Common_Country = t2,
        //            Mkt_Buyer = rc
        //        }).Where(c => c.Mkt_Buyer.ID == id).SingleOrDefault();
        //    this.Mkt_Buyer = v.Mkt_Buyer;
        //    this.Common_Country = v.Common_Country;
        //}

        //public static List<string> TimeSpansInRange(TimeSpan start, TimeSpan end, TimeSpan interval)
        //{
        //    List<string> timeSpans = new List<string>();
        //    while (start.Add(interval) <= end)
        //    {
        //        timeSpans.Add(start.Hours.ToString() + ":" + start.Minutes.ToString());
        //        start = start.Add(interval);
        //    }
        //    return timeSpans;
        //}

        public void EmployeeAttendanceProcess(DateTime date, int late, int shifttypeId, string pby)
        {

            var dt1 = date.ToString("yyyy-MM-dd");
            var dt2 = DateTime.Now.ToString("yyyy-MM-dd");
            var ds = 0;
            if (dt2 == dt1)
            {
                ds = db.Database.ExecuteSqlCommand("[dbo].[Shift_Wise_Employee_Instant_Attendance_Process] @cDT='" + dt1 + "', @LateTime='" + late + "', @shfttypeId='" + shifttypeId + "', @processedby='" + pby + "'");
            }
            else
            {
                ds = db.Database.ExecuteSqlCommand("[dbo].[Sp_Attendance_Process] @cDT='" + dt1 + "', @LateTime='" + late + "', @shfttypeId='" + shifttypeId + "', @processedby='" + pby + "'");
            }
        }


        public void SingleEmployeeAttendanceProcess(DateTime date, string cardno, int shifttypeId, string pby)
        {

            var dt1 = date.ToString("yyyy-MM-dd");
            var ds = 0;
            ds = db.Database.ExecuteSqlCommand("[dbo].[Sp_Specific_Employee_Attendance_Process] @attendanceDate='" + dt1 + "', @LateTime='0', @shfttypeId='" + shifttypeId + "', @processedby='" + pby + "',@cardno='" + cardno + "'");

        }
    }
}