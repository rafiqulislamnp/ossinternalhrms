﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Oss.Hrms.Models.Entity.User;

namespace Oss.Hrms.Models.ViewModels.User
{
   
    public class VmUser_MenuItem
    {
        private HrmsContext db;
        public User_MenuItem User_MenuItem { get; set; }
        public User_Menu User_Menu { get; set; }
        public User_SubMenu User_SubMenu { get; set; }

        public IEnumerable<VmUser_MenuItem> DataList { get; set; }

        public void InitialDataLoad()
        {

            db = new HrmsContext();
            var a = (from userMenuItem in db.User_MenuItem
                     join userMenu in db.User_Menu on userMenuItem.User_MenuFk equals userMenu.ID
                     join userSubMenu in db.User_SubMenu on userMenuItem.User_SubMenuFk equals userSubMenu.ID

                     where userMenuItem.Active == true

                     select new VmUser_MenuItem
                     {
                         User_MenuItem = userMenuItem,
                         User_Menu = userMenu,
                         User_SubMenu = userSubMenu
                     }).OrderByDescending(x => x.User_MenuItem.ID).AsEnumerable();
            this.DataList = a;

        }
        public void SelectSingle(int iD)
        {
            db = new HrmsContext();
            var v = (from userMenuItem in db.User_MenuItem
                     join userMenu in db.User_Menu on userMenuItem.User_MenuFk equals userMenu.ID
                     join userSubMenu in db.User_SubMenu on userMenuItem.User_MenuFk equals userSubMenu.ID

                     select new VmUser_MenuItem
                     {
                         User_MenuItem = userMenuItem,
                         User_Menu = userMenu,
                         User_SubMenu = userSubMenu
                     }).FirstOrDefault(c => c.User_MenuItem.ID == iD);
            if (v != null) this.User_MenuItem = v.User_MenuItem;
            if (v != null) this.User_Menu = v.User_Menu;
            if (v != null) this.User_SubMenu = v.User_SubMenu;
        }



        public int Add(int userID)
        {
            User_MenuItem.AddReady(userID);
            return User_MenuItem.Add();

        }
        public bool Edit(int userID)
        {
            return User_MenuItem.Edit(userID);
        }
        public bool Delete(int userID)
        {
            return User_MenuItem.Delete(userID);
        }
    }
}