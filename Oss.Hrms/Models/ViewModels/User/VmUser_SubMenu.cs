﻿using Oss.Hrms.Models.Entity.User;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.ViewModels.User
{
    public class VmUser_SubMenu
    {       
        public User_RoleMenuItem User_RoleMenuItem { get; set; }
        public VmUser_MenuItem VmUser_MenuItem { get; set; }
        public IEnumerable<VmUser_SubMenu> DataList { get; set; }



    }
}