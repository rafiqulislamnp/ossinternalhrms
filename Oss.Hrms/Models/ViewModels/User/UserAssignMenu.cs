﻿using System;
using System.Collections.Generic;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.User
{
    public class UserAssignMenu: BaseModel
    {
        public int HRMS_EmployeeFK { get; set; }
        public int User_RoleFK { get; set; }
        public int User_RoleMenuItemFK { get; set; }
    }
   
}