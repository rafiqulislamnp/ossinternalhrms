﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.User
{
    public class User_SubMenu : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("SubMenu")]
        [Required(ErrorMessage = "SubMenu Name is required")]
        [StringLength(30, ErrorMessage = "SubMenu Name upto 30 Chracter")]
        public string Name { get; set; }

        public int? User_MenuFk { get; set; }

        //...........................New Added Code...................................//
        //public User_Menu User_Menu { get; set; }
        //public List<User_MenuItem> User_MenuItems { get; set; }
    }
}