﻿
using Oss.Hrms.Models.Entity.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.ViewModels.User
{
    public class VmUser_Team
    {
        private HrmsContext db;        
        public User_Team User_Team { get; set; }
        public IEnumerable<VmUser_Team> DataList { get; set; }

        public void InitialDataLoad()
        {
             
            db = new HrmsContext();
            var a = (from User_Team in db.User_Team
                     where User_Team.Active == true

                     select new VmUser_Team
                     {
                         User_Team = User_Team
                     }).OrderByDescending(x => x.User_Team.ID).AsEnumerable();
            this.DataList = a;

        }
        public void SelectSingle(int iD)
        {
            db = new HrmsContext();
            var v = (from User_Team in db.User_Team

                     select new VmUser_Team
                     {
                         User_Team = User_Team
                     }).Where(c => c.User_Team.ID == iD).SingleOrDefault();
            this.User_Team = v.User_Team;

        }



        public int Add(int userID)
        {
            User_Team.AddReady(userID);
            return User_Team.Add();

        }
        public bool Edit(int userID)
        {
            return User_Team.Edit(userID);
        }

    }
}