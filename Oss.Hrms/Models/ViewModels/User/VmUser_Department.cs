﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Oss.Hrms.Models.Entity.User;

namespace Oss.Hrms.Models.ViewModels.User
{
   
    public class VmUser_Department
    {
        private HrmsContext db;
        public User_Department User_Department { get; set; }
        public User_Menu User_Menu { get; set; }
        public User_SubMenu User_SubMenu { get; set; }

        public IEnumerable<VmUser_Department> DataList { get; set; }

        public void InitialDataLoad()
        {

            db = new HrmsContext();
            var a = (from userDepartment in db.User_Department
                   

                     select new VmUser_Department
                     {
                         User_Department = userDepartment,
                       
                     }).OrderByDescending(x => x.User_Department.ID).AsEnumerable();
            this.DataList = a;

        }
        public void SelectSingle(int iD)
        {
            db = new HrmsContext();
            var v = (from userDepartment in db.User_Department
                     
                     select new VmUser_Department
                     {
                         User_Department = userDepartment,
                        
                     }).FirstOrDefault(c => c.User_Department.ID == iD);
            this.User_Department = v.User_Department;

        }



        public int Add(int userID)
        {
            User_Department.AddReady(userID);
            return User_Department.Add();

        }
        public bool Edit(int userID)
        {
            return User_Department.Edit(userID);
        }
        public bool Delete(int userID)
        {
            return User_Department.Delete(userID);
        }
    }
}