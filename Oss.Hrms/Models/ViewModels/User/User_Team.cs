﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Romo.Models.User
{
    public class User_Team : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("Group")]
        [Required(ErrorMessage = "User name is required")]
      //  [MaxLength(10, ErrorMessage = "Upto 10 Chracter")]
        public string Name { get; set; }
    }
}