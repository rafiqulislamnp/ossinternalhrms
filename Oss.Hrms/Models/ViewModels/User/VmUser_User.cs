﻿using Oss.Hrms.Models.Entity.Common;
using Oss.Hrms.Models.Entity.User;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace Oss.Hrms.Models.ViewModels.User
{
    //[MetadataType(typeof(VmUser_User))]
    [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
    public class VmUser_User
    {
        private HrmsContext db;
        public User_Role User_Role { get; set; }
        public User_User User_User { get; set; }
        // public User_Team User_Team { get; set; }
        public string LblError { get; set; }
        [DisplayName("User Name")]
        public string UserNameFromEdit { get; set; }
        public IEnumerable<VmUser_User> DataList { get; set; }
        public List<VmRoleDetails> DataListVmRoleDetails { get; set; }
        public Department User_Department { get; set; }
        public User_AccessLevel User_AccessLevel { get; set; }
        //public VmControllerHelper VmControllerHelper { get; set; }
        public User_MenuItem User_MenuItem { get; set; }
        public User_SubMenu User_SubMenu { get; set; }
        public User_Menu User_Menu { get; set; }
        public int EmployeeId { get; set; }
        public bool IsSupperUser { get; set; }

        public string Section { get; set; }
        public string Designation { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }

        public string Email { get; set; }
        public string Address { get; set; }
        public string Mobile { get; set; }

        public VmUser_User()
        {
            //VmControllerHelper = new VmControllerHelper();
        }

        [Required(ErrorMessage = "Password is Required")]
        [StringLength(12, MinimumLength = 6, ErrorMessage = "Minimum 6 upto 12 Chracter")]
        [DisplayName("New Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm Password is Required")]
        [StringLength(12, MinimumLength = 6, ErrorMessage = "Minimum 6 upto 12 Chracter")]
        [DisplayName("Confirm Password")]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Password and Confirm Password Not Match")]

        public string ConfirmPassword { get; set; }

        //[StringLength(12, MinimumLength = 6, ErrorMessage = "Insert Current Password")]
        //[DisplayName("Current Password")]
        //[DataType(DataType.Password)]
        //public string CurrentPassword { get; set; }
        public HttpPostedFileBase Photo { get; set; }

        public void InitialDataLoad()
        {
            db = new HrmsContext();

            var v = (from User_User in db.User_User
                         //join User_Team in db.User_Team
                         //on User_User.User_TeamFK equals User_Team.ID
                     join User_Department in db.Departments
                     on User_User.User_DepartmentFK equals User_Department.ID
                     //join User_AccessLevel in db.User_AccessLevel
                     //on User_User.User_AccessLevelFK equals User_AccessLevel.ID

                     //join User_Department in db.User_Department
                     //on User_User.User_DepartmentFK equals User_Department.ID

                     select new VmUser_User
                     {
                         // User_Team = User_Team,
                         User_Department = User_Department,
                         //User_AccessLevel = User_AccessLevel,
                         User_User = User_User,

                         //User_Department= User_Department

                     }).Where(x => x.User_User.Status == true).OrderByDescending(x => x.User_User.ID).ToList();
            foreach (var c in v)
            {
                if (!c.User_User.IsSupperUser)
                {
                    string img = "nopic.png";
                    if (c.User_User.HRMS_EmployeeFK != null)
                    {
                        var d = GetEmployeeDetails((int)c.User_User.HRMS_EmployeeFK);
                        c.User_User.Name = d.Name;
                        c.User_User.Address = d.Address;
                        c.User_User.Email = d.Email;
                        c.User_User.Mobile = d.Mobile;
                        if (string.IsNullOrEmpty(d.Image))
                        {
                            c.User_User.Photo = "~/Assets/Images/EmpImages/" + img;
                        }
                        else
                        {
                            c.User_User.Photo = "~/Assets/Images/EmpImages/" + d.Image;
                        }
                    }
                    else
                    {
                        var d = GetNullEmployeeDetails((int)c.User_User.ID);
                        c.User_User.Name = d.Name;
                        c.User_User.Address = d.Address;
                        c.User_User.Email = d.Email;
                        c.User_User.Mobile = d.Mobile;
                        c.User_User.Photo = d.Image;
                    }
                }
                else
                {
                    var d = GetNullEmployeeDetails((int)c.User_User.ID);
                    c.User_User.Name = d.Name;
                    c.User_User.Address = d.Address;
                    c.User_User.Email = d.Email;
                    c.User_User.Mobile = d.Mobile;
                    c.User_User.Photo = d.Image;
                }
            }

            this.DataList = v;




            //List<VmUser_User> tempData = v.ToList();

        }
        public void SelectSingle(int id)
        {
            db = new HrmsContext();
            var v = (from User_User in db.User_User
                         //join User_Team in db.User_Team
                         //on User_User.User_TeamFK equals User_Team.ID
                     join User_Department in db.Departments
                     on User_User.User_DepartmentFK equals User_Department.ID
                     //join User_AccessLevel in db.User_AccessLevel
                     //on User_User.User_AccessLevelFK equals User_AccessLevel.ID

                     where User_User.Status == true 
                     //join User_Department in db.User_Department
                     //on User_User.User_DepartmentFK equals User_Department.ID

                     select new VmUser_User
                     {

                         User_Department = User_Department,
                         //User_AccessLevel = User_AccessLevel,
                         User_User = User_User
                    }).Where(c => c.User_User.ID ==id).SingleOrDefault();
            this.User_User = v.User_User;
            this.User_Department = v.User_Department;
            //this.User_AccessLevel = v.User_AccessLevel;
            this.UserNameFromEdit = v.User_User.UserName;
            this.IsSupperUser = v.User_User.IsSupperUser;
            string img = "nopic.png";
            if (!this.User_User.IsSupperUser)
            {
                if (this.User_User.HRMS_EmployeeFK != null)
                {

                    var d = GetEmployeeDetails((int)v.User_User.HRMS_EmployeeFK);

                    this.User_User.Name = d.Name;
                    this.User_User.Address = d.Address;
                    this.User_User.Mobile = d.Mobile;
                    this.User_User.Email = d.Email;
                    if (string.IsNullOrEmpty(d.Image))
                    {
                        this.User_User.Photo = "~/CustomeFile/ProfilePic/" + img;
                    }
                    else
                    {
                        this.User_User.Photo = "~/Assets/Images/EmpImages/" + d.Image;
                    }
                }
                else
                {
                    var d = GetNullEmployeeDetails((int)v.User_User.ID);

                    this.User_User.Name = d.Name;
                    this.User_User.Address = d.Address;
                    this.User_User.Mobile = d.Mobile;
                    this.User_User.Email = d.Email;
                    this.User_User.Photo = d.Image;
                }
            }
            else
            {
                this.User_User.Photo = "~/CustomeFile/ProfilePic/" + img;
            }
        }
        //public int Add(int userID, string imagePath)
        //{

        //    User_User.AddReady(0);
        //    User_User.Password = GetHashedPassword(this.Password);
        //    Upload(imagePath);
        //    return User_User.Add();
        //}
        public int Add(int userID)
        {
            db = new HrmsContext();
            //User_User.AddReady(0);
            User_User.Password = GetHashedPassword(this.Password);
            User_User.Name = db.Employees.FirstOrDefault(c => c.ID == User_User.HRMS_EmployeeFK).Name;
            db.User_User.Add(User_User);
            return db.SaveChanges();
            //return User_User.Add();
        }
        public void EditPassword(int userID, bool IsSupperUser=false)
        {
            db = new HrmsContext();
            User_User.Password = GetHashedPassword(this.Password);
            User_User.IsSupperUser = IsSupperUser;
            var user= db.User_User.Where(x=>x.ID==User_User.ID).FirstOrDefault();
            user.Password = GetHashedPassword(this.Password);
            user.IsSupperUser = IsSupperUser;
            db.SaveChanges();

            //return User_User.Edit(userID);
        }
        public void EditUser(int userID, string imagePath)
        {
            db = new HrmsContext();
            var user = db.User_User.FirstOrDefault(x=>x.ID==User_User.ID);

            user.UserName = this.UserNameFromEdit;
            user.Name = this.User_User.Name;
            user.Address = this.User_User.Address;
            user.Email = this.User_User.Email;
            user.Mobile = this.User_User.Mobile;
            user.User_DepartmentFK = this.User_User.User_DepartmentFK;
            user.User_RoleFK = this.User_User.User_RoleFK;
            Upload(imagePath);
            db.SaveChanges();
           //return User_User.Edit(userID);
        }
        public void Edit(int userID,bool active)
        {
            db = new HrmsContext();
            var user = db.User_User.Where(x=>x.ID== userID).FirstOrDefault();
            user.IsActive = active;
            db.SaveChanges();
            //return User_User.Edit(userID);
        }
        //public bool EditSelfPassword(VmUser_User VmUser_User)
        //{
        //    db = new HrmsContext();
        //    string hashedPassword = GetHashedPassword(VmUser_User.User_User.Password);
        //    int flag = 0;
        //    var v = (from t1 in db.User_User
        //             where t1.ID == VmUser_User.User_User.ID
        //             &&
        //             t1.Password == hashedPassword
        //             select new
        //             {
        //                 ID = t1.ID
        //             }).Count();

        //    if (v > 0)
        //    {
        //        User_User.Password = GetHashedPassword(VmUser_User.Password);
        //        User_User.IsSupperUser = VmUser_User.IsSupperUser;
        //        return User_User.Edit(VmUser_User.User_User.ID);
        //    }
        //    return false;

        //}
        //public bool Delete(int userID)
        //{
        //    return User_User.Delete(userID);
        //}

        internal string GetUserName(string userName)
        {
            //db = new xOssContext();
            //var v = (from userTable in db.User_User
            //         select new
            //         {

            //         });
            return userName;
        }

        public string Upload(string imagePath)
        {
            string s = "";
            try
            {
                string exten = Path.GetFileName(Photo.FileName);
                string fName = Guid.NewGuid() + exten.Substring(exten.IndexOf("."), exten.Length - exten.IndexOf("."));
                string filePath = Path.Combine(imagePath + fName);
                Photo.SaveAs(filePath);
                User_User.Photo = fName;

            }
            catch (Exception ex)
            {

                s = ex.Message;

            }
            return s;

        }

        public int LoginUser(bool hasCokiee)
        {
            db = new HrmsContext();
            string hashedPassword = GetHashedPassword(User_User.Password);
            int flag = 0;
            var v = (from t1 in db.User_User
                     where t1.UserName == User_User.UserName
                     &&
                     t1.Password == hashedPassword
                     select new
                     {
                         UserName = t1.UserName,
                         Name = t1.Name,
                         ID = t1.ID,
                         IsActive = t1.IsActive,
                         User_AccessLevel = t1.User_AccessLevelFK,
                         User_Department = t1.User_DepartmentFK,
                         EmpId = t1.HRMS_EmployeeFK,
                         IsSupperUser = t1.IsSupperUser

                     }).FirstOrDefault();
            string img = "nopic.png";
            if (v != null)
            {
                flag = v.ID;
                this.User_User.ID = flag;
                this.User_User.Name = v.Name;
                this.User_User.UserName = v.UserName;
                this.User_User.User_AccessLevelFK = v.User_AccessLevel;
                this.User_User.User_DepartmentFK = v.User_Department;
                this.User_User.HRMS_EmployeeFK = v.EmpId;
                this.User_User.IsSupperUser = v.IsSupperUser;
                if (!v.IsSupperUser)
                {
                    var d = GetEmployeeDetails((int)v.EmpId);
                    if (string.IsNullOrEmpty(d.Image))
                    {
                        this.User_User.Photo = "~/CustomeFile/ProfilePic/" + img;
                    }
                    else
                    {
                        this.User_User.Photo = "~/Assets/Images/EmpImages/" + d.Image;
                    }


                }
                else
                {
                    this.User_User.Photo = "~/CustomeFile/ProfilePic/" + img;
                }
            }
            if (flag > 0)
            {
                if (v.IsActive == false)
                {
                    this.LblError = "You are not authorised!";
                    return 0;
                }
                if (!hasCokiee)
                {
                    DefinePermission();
                }
            }
            else
            {
                this.LblError = "User / Password does not match!";
            }
            //throw new NotImplementedException();

            return flag;
        }

        private void DefinePermission()
        {
            //GET data from database
            //Insert in to cookie   
            //Merchandising-Orders-In Progress

            //HttpCookie MenuC1 = new HttpCookie("Menu1");
            //HttpCookie MenuC2 = new HttpCookie("Menu2");
            //HttpCookie MenuC3 = new HttpCookie("Menu3");
            //HttpCookie MenuC4 = new HttpCookie("Menu4");
            //HttpCookie MenuC5 = new HttpCookie("Menu5");
            //HttpCookie MenuC6 = new HttpCookie("Menu6");
            //HttpCookie MenuC7 = new HttpCookie("Menu7");
            //HttpCookie MenuC8 = new HttpCookie("Menu8");
            //HttpCookie MenuC9 = new HttpCookie("Menu9");
            //HttpCookie MenuC10 = new HttpCookie("Menu10");

            //HttpCookie AloneC1 = new HttpCookie("Alone1");
            //HttpCookie AloneC2 = new HttpCookie("Alone2");
            //HttpCookie AloneC3 = new HttpCookie("Alone3");
            //HttpCookie AloneC4 = new HttpCookie("Alone4");
            //HttpCookie AloneC5 = new HttpCookie("Alone5");
            //HttpCookie AloneC6 = new HttpCookie("Alone6");
            //HttpCookie AloneC7 = new HttpCookie("Alone7");
            //HttpCookie AloneC8 = new HttpCookie("Alone8");
            //HttpCookie AloneC9 = new HttpCookie("Alone9");
            //HttpCookie AloneC10 = new HttpCookie("Alone10");

            //HttpCookie UserID = new HttpCookie("UserID");
            //HttpCookie UserName = new HttpCookie("UserName");

            string tempMenu = GetMenu();
            int tml = tempMenu.Length / 10;

            HttpContext.Current.Session["Menu1"] = tempMenu.Substring(0, tml);
            HttpContext.Current.Session["Menu2"] = tempMenu.Substring(tml, tml);
            HttpContext.Current.Session["Menu3"] = tempMenu.Substring(tml * 2, tml);
            HttpContext.Current.Session["Menu4"] = tempMenu.Substring(tml * 3, tml);
            HttpContext.Current.Session["Menu5"] = tempMenu.Substring(tml * 4, tml);
            HttpContext.Current.Session["Menu6"] = tempMenu.Substring(tml * 5, tml);
            HttpContext.Current.Session["Menu7"] = tempMenu.Substring(tml * 6, tml);
            HttpContext.Current.Session["Menu8"] = tempMenu.Substring(tml * 7, tml);
            HttpContext.Current.Session["Menu9"] = tempMenu.Substring(tml * 8, tml);
            HttpContext.Current.Session["Menu10"] = tempMenu.Substring(tml * 9, tempMenu.Length - (tml * 9));

            string tempAMenu = GetAloneMethod();
            tml = tempAMenu.Length / 10;

            HttpContext.Current.Session["Alone1"] = tempAMenu.Substring(0, tml);
            HttpContext.Current.Session["Alone2"] = tempAMenu.Substring(tml, tml);
            HttpContext.Current.Session["Alone3"] = tempAMenu.Substring(tml * 2, tml);
            HttpContext.Current.Session["Alone4"] = tempAMenu.Substring(tml * 3, tml);
            HttpContext.Current.Session["Alone5"] = tempAMenu.Substring(tml * 4, tml);
            HttpContext.Current.Session["Alone6"] = tempAMenu.Substring(tml * 5, tml);
            HttpContext.Current.Session["Alone7"] = tempAMenu.Substring(tml * 6, tml);
            HttpContext.Current.Session["Alone8"] = tempAMenu.Substring(tml * 7, tml);
            HttpContext.Current.Session["Alone9"] = tempAMenu.Substring(tml * 8, tml);
            HttpContext.Current.Session["Alone10"] = tempAMenu.Substring(tml * 9, tempAMenu.Length - (tml * 9));

            //HttpContext.Current.Response.Cookies.Add(MenuC1);
            //HttpContext.Current.Response.Cookies.Add(MenuC2);
            //HttpContext.Current.Response.Cookies.Add(MenuC3);
            //HttpContext.Current.Response.Cookies.Add(MenuC4);
            //HttpContext.Current.Response.Cookies.Add(MenuC5);
            //HttpContext.Current.Response.Cookies.Add(MenuC6);
            //HttpContext.Current.Response.Cookies.Add(MenuC7);
            //HttpContext.Current.Response.Cookies.Add(MenuC8);
            //HttpContext.Current.Response.Cookies.Add(MenuC9);
            //HttpContext.Current.Response.Cookies.Add(MenuC10);

            //HttpContext.Current.Response.Cookies.Add(AloneC1);
            //HttpContext.Current.Response.Cookies.Add(AloneC2);
            //HttpContext.Current.Response.Cookies.Add(AloneC3);
            //HttpContext.Current.Response.Cookies.Add(AloneC4);
            //HttpContext.Current.Response.Cookies.Add(AloneC5);
            //HttpContext.Current.Response.Cookies.Add(AloneC6);
            //HttpContext.Current.Response.Cookies.Add(AloneC7);
            //HttpContext.Current.Response.Cookies.Add(AloneC8);
            //HttpContext.Current.Response.Cookies.Add(AloneC9);
            //HttpContext.Current.Response.Cookies.Add(AloneC10);

            //UserID["UserID"] = this.User_User.ID.ToString();
            //UserName["UserName"] = this.User_User.UserName;

            //HttpContext.Current.Response.Cookies.Add(UserID);
            //HttpContext.Current.Response.Cookies.Add(UserName);
        }

        public string GetMenu()
        {
            int id = this.User_User.ID;
            db = new HrmsContext();
            string str = "";
            var v = (from t1 in db.User_RoleMenuItem
                     join t2 in db.User_MenuItem
                     on t1.User_MenuItemFk equals t2.ID
                     join t3 in db.User_SubMenu
                     on t2.User_SubMenuFk equals t3.ID
                     join t4 in db.User_Menu
                     on t3.User_MenuFk equals t4.ID
                     where t1.User_RoleFK == (from ttt in db.User_User where ttt.ID == id select ttt.User_RoleFK).FirstOrDefault()
                     && t2.IsAlone == false
                       && t2.Active == true
                       && t1.IsAllowed == true
                     select new
                     {
                         Menu = t4.Name,
                         SubMenu = t3.Name,
                         ItemMenu = t2.Name,
                         Method = t2.Method,
                         SUbMenuFK = t2.User_SubMenuFk
                     }).OrderBy(x => x.SUbMenuFK).ToList().Distinct();
            string TopMenu = "", SubMenu = "", Menuitem = "", Method = "";
            foreach (var a in v)
            {
                if (TopMenu != a.Menu)
                {
                    TopMenu = a.Menu;
                    str += "!" + a.Menu;
                }
                if (SubMenu != a.SubMenu)
                {
                    SubMenu = a.SubMenu;
                    str += "$" + a.SubMenu;
                }
                Menuitem = a.ItemMenu;
                Method = a.Method;
                str += "#" + Menuitem + "|" + Method + "^";
                //str += a.Menu + "!" + a.SubMenu + "$" + a.ItemMenu + "#" + a.Method + "|";
            }
            return str + "=";
        }

        public string GetAloneMethod()
        {
            int id = this.User_User.ID;
            db = new HrmsContext();
            string str = "";
            var v = (from t1 in db.User_RoleMenuItem
                     join t2 in db.User_MenuItem
                     on t1.User_MenuItemFk equals t2.ID
                     // join t3 in db.User_Menu
                     //on t2.User_MenuFk equals t3.ID
                     where t1.User_RoleFK == (from ttt in db.User_User where ttt.ID == id select ttt.User_RoleFK).FirstOrDefault()
                     && t2.IsAlone == true
                     && t2.Active == true
                       && t1.IsAllowed == true
                     select new
                     {
                         Method = t2.Method,
                         //Name = t3.Name

                     }).ToList();
            //this.DataList = u;
            foreach (var a in v)
            {
                string temp = a.Method.Replace("/", "@(");
                str += "@" + temp + ")|";
            }
            return str;
        }

        public bool CheckPermission(string Controller, string Method)
        {

            bool flag = false;
            Controller = Controller.ToLower();
            Method = Method.ToLower();
            var v = (from User_MenuItem in db.User_MenuItem
                     join User_SubMenu in db.User_SubMenu
                     on User_MenuItem.User_SubMenuFk equals User_SubMenu.ID
                     join User_Menu in db.User_Menu
                     on User_MenuItem.User_MenuFk equals User_Menu.ID
                     select new VmUser_User
                     {
                         User_MenuItem = User_MenuItem,
                         User_SubMenu = User_SubMenu,
                         User_Menu = User_Menu
                     }).Where(x => x.User_MenuItem.Active == true).OrderByDescending(x => x.User_MenuItem.ID).AsEnumerable();

            //HttpCookie MenuC1 = HttpContext.Current.Request.Cookies["Menu1"];
            //HttpCookie MenuC2 = HttpContext.Current.Request.Cookies["Menu2"];
            //HttpCookie MenuC3 = HttpContext.Current.Request.Cookies["Menu3"];
            //HttpCookie MenuC4 = HttpContext.Current.Request.Cookies["Menu4"];
            //HttpCookie MenuC5 = HttpContext.Current.Request.Cookies["Menu5"];
            //HttpCookie MenuC6 = HttpContext.Current.Request.Cookies["Menu6"];
            //HttpCookie MenuC7 = HttpContext.Current.Request.Cookies["Menu7"];
            //HttpCookie MenuC8 = HttpContext.Current.Request.Cookies["Menu8"];
            //HttpCookie MenuC9 = HttpContext.Current.Request.Cookies["Menu9"];
            //HttpCookie MenuC10 = HttpContext.Current.Request.Cookies["Menu10"];

            //HttpCookie AloneC1 = HttpContext.Current.Request.Cookies["Alone1"];
            //HttpCookie AloneC2 = HttpContext.Current.Request.Cookies["Alone2"];
            //HttpCookie AloneC3 = HttpContext.Current.Request.Cookies["Alone3"];
            //HttpCookie AloneC4 = HttpContext.Current.Request.Cookies["Alone4"];
            //HttpCookie AloneC5 = HttpContext.Current.Request.Cookies["Alone5"];
            //HttpCookie AloneC6 = HttpContext.Current.Request.Cookies["Alone6"];
            //HttpCookie AloneC7 = HttpContext.Current.Request.Cookies["Alone7"];
            //HttpCookie AloneC8 = HttpContext.Current.Request.Cookies["Alone8"];
            //HttpCookie AloneC9 = HttpContext.Current.Request.Cookies["Alone9"];
            //HttpCookie AloneC10 = HttpContext.Current.Request.Cookies["Alone10"];

            var MenuC1 = HttpContext.Current.Session["Menu1"];
            var MenuC2 = HttpContext.Current.Session["Menu2"];
            var MenuC3 = HttpContext.Current.Session["Menu3"];
            var MenuC4 = HttpContext.Current.Session["Menu4"];
            var MenuC5 = HttpContext.Current.Session["Menu5"];
            var MenuC6 = HttpContext.Current.Session["Menu6"];
            var MenuC7 = HttpContext.Current.Session["Menu7"];
            var MenuC8 = HttpContext.Current.Session["Menu8"];
            var MenuC9 = HttpContext.Current.Session["Menu9"];
            var MenuC10 = HttpContext.Current.Session["Menu10"];
            var AloneC1 = HttpContext.Current.Session["Alone1"];
            var AloneC2 = HttpContext.Current.Session["Alone2"];
            var AloneC3 = HttpContext.Current.Session["Alone3"];
            var AloneC4 = HttpContext.Current.Session["Alone4"];
            var AloneC5 = HttpContext.Current.Session["Alone5"];
            var AloneC6 = HttpContext.Current.Session["Alone6"];
            var AloneC7 = HttpContext.Current.Session["Alone7"];
            var AloneC8 = HttpContext.Current.Session["Alone8"];
            var AloneC9 = HttpContext.Current.Session["Alone9"];
            var AloneC10 = HttpContext.Current.Session["Alone10"];

            //string Alone = AloneC1["Alone1"].ToString().ToLower() + AloneC2["Alone2"].ToString().ToLower() + AloneC3["Alone3"].ToString().ToLower() + AloneC4["Alone4"].ToString().ToLower() + AloneC5["Alone5"].ToString().ToLower() + AloneC6["Alone6"].ToString().ToLower() + AloneC7["Alone7"].ToString().ToLower() + AloneC8["Alone8"].ToString().ToLower() + AloneC9["Alone9"].ToString().ToLower() + AloneC10["Alone10"].ToString().ToLower()
            //             + MenuC1["Menu1"].ToString().ToLower() + MenuC2["Menu2"].ToString().ToLower() + MenuC3["Menu3"].ToString().ToLower() + MenuC4["Menu4"].ToString().ToLower() + MenuC5["Menu5"].ToString().ToLower() + MenuC6["Menu6"].ToString().ToLower() + MenuC7["Menu7"].ToString().ToLower() + MenuC8["Menu8"].ToString().ToLower() + MenuC9["Menu9"].ToString().ToLower() + MenuC10["Menu10"].ToString().ToLower();
            string Alone = AloneC1.ToString().ToLower() + AloneC2.ToString().ToLower() + AloneC3.ToString().ToLower() + AloneC4.ToString().ToLower() + AloneC5.ToString().ToLower() + AloneC6.ToString().ToLower() + AloneC7.ToString().ToLower() + AloneC8.ToString().ToLower() + AloneC9.ToString().ToLower() + AloneC10.ToString().ToLower()
                       + MenuC1.ToString().ToLower() + MenuC2.ToString().ToLower() + MenuC3.ToString().ToLower() + MenuC4.ToString().ToLower() + MenuC5.ToString().ToLower() + MenuC6.ToString().ToLower() + MenuC7.ToString().ToLower() + MenuC8.ToString().ToLower() + MenuC9.ToString().ToLower() + MenuC10.ToString().ToLower();

            string temp = "@" + Controller + "@(" + Method + ")";

            if (Alone.Contains("@" + Controller + "@(" + Method + ")"))
            {
                flag = true;
            }
            else if (Alone.Contains(Controller + "/" + Method + "^"))
            {
                flag = true;
            }


            return flag;
        }

        public string GetHashedPassword(string password)
        {
            char[] array = password.ToCharArray();
            Array.Reverse(array);
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(array);
            byte[] hash = md5.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("x2"));
            }
            return sb.ToString();
        }

        //public static implicit operator VmUser_User(Models.User.User_User v)
        //{
        //    throw new NotImplementedException();
        //}

        public VmUser_User GetEmployeeDetails(int Id)
        {
            db = new HrmsContext();
            var vData = (from o in db.Employees
                         join p in db.Sections on o.Section_Id equals p.ID
                         join q in db.Designations on o.Designation_Id equals q.ID
                         join r in db.District on o.PresentDistrict_ID equals r.ID
                         join s in db.Village on o.PresentVillage_ID equals s.ID
                         join t in db.PostOffice on o.PresentPostOffice_ID equals t.ID
                         join u in db.PoliceStation on o.PresentPoliceStation_ID equals u.ID
                         where o.ID == Id
                         select new VmUser_User
                         {
                             Section = p.SectionName,
                             Designation = q.Name,
                             Name = o.Name,
                             Image = o.Photo,
                             Address = o.Present_Address,
                             Mobile = o.Mobile_No,
                             Email = o.Email
                         }).FirstOrDefault();
            return vData;
        }

        public VmUser_User GetNullEmployeeDetails(int UserId)
        {
            db = new HrmsContext();
            var vData = (from o in db.User_User
                         where o.ID == UserId
                         select new VmUser_User
                         {
                             Section = "",
                             Designation = "",
                             Name = o.UserName,
                             Image = "~/CustomeFile/ProfilePic/nopic.png",
                             Address = o.Address,
                             Mobile = o.Mobile,
                             Email = o.Email
                         }).FirstOrDefault();
            return vData;
        }

        public VmUser_User GetEmployeeRole(int id)
        {
            db = new HrmsContext();

            var a = (from t1 in db.User_User
                     join t2 in db.User_Role
                     on t1.User_RoleFK equals t2.ID
                     where t1.ID == id
                     select new VmUser_User
                     {
                         User_User = t1,
                         User_Role = t2

                     }).AsEnumerable();
            if (a.Any())
            {
                var c = a.FirstOrDefault();

                if (c.User_User.HRMS_EmployeeFK != null)
                {

                    var b = db.Employees.Where(e => e.ID == c.User_User.HRMS_EmployeeFK).FirstOrDefault();
                    c.Name = b.CardTitle + " " + b.Name;
                }
                else
                {
                    c.Name = c.User_User.Name;
                }

                return c;
            }

            return a.FirstOrDefault();

        }


        public void GetRoleDetailsById(int id)
        {
            db = new HrmsContext();
            var v = (from t1 in db.User_RoleMenuItem
                     join t2 in db.User_MenuItem on t1.User_MenuItemFk equals t2.ID
                     join t3 in db.User_Menu on t2.User_MenuFk equals t3.ID
                     join t4 in db.User_SubMenu on t3.ID equals t4.User_MenuFk
                     where t1.User_RoleFK == id
                     select new VmRoleDetails
                     {
                         MenuID = t1.User_MenuItemFk,
                         RoleID = t1.User_RoleFK,
                         Top = t3.Name,
                         Menu = t4.Name,
                         Description = t2.Description,
                         Item = t2.Name,
                         IsSelected = t1.IsAllowed,
                         Priority = t2.Priority,
                         ID = t1.ID,
                         UserSubMenuID = t4.ID,
                     }).OrderBy(x => x.Priority).OrderBy(x => x.UserSubMenuID).ToList();
                 DataListVmRoleDetails = v;



            //if (DataListVmRoleDetails.Count == 0)
            //{
            //    CreateNewRole(id);
            //}
        }

        //private void CreateNewRole(string id)
        //{
        //    int rId = 0;
        //    Int32.TryParse(id, out rId);
        //    db = new xOssContext();
        //    object[] parameter = { new SqlParameter("@id", rId) };
        //    db.Database.ExecuteSqlCommand("EXEC dbo.SP_CreateNewRole @id", parameter);
        //    string s = rId.ToString();
        //    s = this.VmControllerHelper.Encrypt(s);
        //    GetRoleDetailsById(s);
        //}
    }


}