﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.CoreProcess.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Oss.Romo.Models.User
{
    public class User_User : BaseModel
    {
        public User_User()
        {
            //IsSupperUser = true;
            //this.SupperUser = true;

        }
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }

        //unique user name
        [Index("IX_UniqueUserName", 1, IsUnique = true)]
        [Required(ErrorMessage = "User name is required")]
        [MaxLength(25, ErrorMessage = "Upto 25 Chracter")]
        [RegularExpression(@"(\S)+", ErrorMessage = "White space is not allowed.")]
        //[Editable(true)]
        //[Remote("CheckName", "User", HttpMethod = "POST", ErrorMessage = "User name already exists. Please enter a different user name.")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [StringLength(40, ErrorMessage = "Minimum 6 upto 12 Chracter")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "Minimum 3 upto 30 Chracter")]
        public string Name { get; set; }


        [StringLength(200)]
        public string Address { get; set; }


        [StringLength(200)]
        public string Photo { get; set; }


        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }


        //[DataType(DataType.PhoneNumber)]
        public string Mobile { get; set; }

        [Required(ErrorMessage = "User Access Level is required")]
        [DisplayName("Access Level")]
        public int? User_AccessLevelFK { get; set; }

        [Required(ErrorMessage = "User Department is required")]
        [DisplayName("Department")]
        public int? User_DepartmentFK { get; set; }

        [DisplayName("Role")]
        [DefaultValue(0)]
        public int? User_RoleFK { get; set; }

        //[DisplayName("User Team")]
        //public int? User_TeamFK { get; set; }


        [DefaultValue(true)]
        public bool IsActive { get; set; }

        [DisplayName("Remember Me")]
        public bool RememberMe { get; set; }

        public int? HRMS_EmployeeFK { get; set; }

   
        public bool IsSupperUser { get; set; }

    
       

        //...........................New Added Code...................................//

        //public User_AccessLevel User_AccessLevel { get; set; }
        //public User_Department User_Department { get; set; }
        //public User_Role User_Role { get; set; }




    }


}