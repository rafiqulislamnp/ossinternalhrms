﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.ViewModels.Reports
{
    public class VMFilter
    {
        public int? SectionId { get; set; }

        public int? PayrollId { get; set; }
    }
}