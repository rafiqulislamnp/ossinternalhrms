﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.ViewModels.Reports
{
    public static class Helpers
    {
        public static string ShortDateString(DateTime Date)
        {
            string datestring = string.Empty;

            string day = Date.Day < 10 ? "0"+ Date.Day : Date.Day.ToString();
            string month = Date.Month < 10 ? "0" + Date.Month : Date.Month.ToString();
            int year = Date.Year;

            datestring = day + "-" + month + "-" + year;

            return datestring;
        }

        public static string ShortDateStringSlash(DateTime Date)
        {
            string datestring = string.Empty;

            string day = Date.Day < 10 ? "0" + Date.Day : Date.Day.ToString();
            string month = Date.Month < 10 ? "0" + Date.Month : Date.Month.ToString();
            int year = Date.Year;

            datestring = day + "/" + month + "/" + year;

            return datestring;
        }

        public static string PeriodicShortDateString(DateTime FromDate,DateTime ToDate)
        {
            string datestring = string.Empty;

            string from = ShortDateString(FromDate);
            string to= ShortDateString(ToDate);

            datestring = from + " To " + to;

            return datestring;
        }

        public static string ShortMonthYear(DateTime Date)
        {
            string datestring = string.Empty;

            

            string day = Date.Day < 10 ? "0" + Date.Day : Date.Day.ToString();
            string month = Date.Month < 10 ? "0" + Date.Month : Date.Month.ToString();
            int year = Date.Year;

            datestring = day + "/" + month + "/" + year;

            return datestring;
        }

        public static string DateString(DateTime Date)
        {
            string datestring = string.Empty;

            string day = Date.Day < 10 ? "0" + Date.Day : Date.Day.ToString();
            string month = Date.Month < 10 ? "0" + Date.Month : Date.Month.ToString();
            int year = Date.Year;

            datestring = month  + "-" + day + "-" + year;

            return datestring;
        }

        public static string IncreamentDate(DateTime JoinDate)
        {
            string IncrementDate = string.Empty;

            //DateTime efDate = new DateTime(DateTime.Today.Year,JoinDate.Month,JoinDate.Day);
            DateTime efDate = new DateTime(DateTime.Today.Year, JoinDate.Month, 1);
            IncrementDate = ShortDateString(efDate);

            return IncrementDate;
        }

        //public static string GetShortDateString(DateTime date)
        //{
        //    string monthName = string.Empty;

        //    List<string> monthname = new List<string>() { "January","February","March","April","May","Jun","July","August","September","October","November","December"};

        //    int year = date.Year;
        //    int month = date.Month;

        //    for (int i=1;i<monthName.Count();i++)
        //    {
        //        if (i==monthName[i]) { }
        //    }

        //    foreach (var v in monthname)
        //    {

        //    }

        //    return monthName;

        //}


        private static IEnumerable<DateTime> GetDaysBetween(DateTime start, DateTime end)
        {
            for (DateTime i = start; i < end; i = i.AddDays(1))
            {
                yield return i;
            }
        }


        public static int GetNoOfOffDays(DateTime FromDate,DateTime ToDate, string OffDay)
        {
            int countday = 0;
            if (OffDay.Equals("Friday"))
            {
                countday = GetDaysBetween(FromDate, ToDate).Where(d => d.DayOfWeek == DayOfWeek.Friday).Count();
            }
            else if(OffDay.Equals("Saturday"))
            {
                countday = GetDaysBetween(FromDate, ToDate).Where(d => d.DayOfWeek == DayOfWeek.Saturday).Count();
            }
            else if (OffDay.Equals("Sunday"))
            {
                countday = GetDaysBetween(FromDate, ToDate).Where(d => d.DayOfWeek == DayOfWeek.Sunday).Count();
            }
            else if (OffDay.Equals("Monday"))
            {
                countday = GetDaysBetween(FromDate, ToDate).Where(d => d.DayOfWeek == DayOfWeek.Monday).Count();
            }
            else if (OffDay.Equals("Tuesday"))
            {
                countday = GetDaysBetween(FromDate, ToDate).Where(d => d.DayOfWeek == DayOfWeek.Tuesday).Count();
            }
            else if (OffDay.Equals("Wednesday"))
            {
                countday = GetDaysBetween(FromDate, ToDate).Where(d => d.DayOfWeek == DayOfWeek.Wednesday).Count();
            }
            else if (OffDay.Equals("Thursday"))
            {
                countday = GetDaysBetween(FromDate, ToDate).Where(d => d.DayOfWeek == DayOfWeek.Thursday).Count();
            }
            
            return countday;
        }
    }
}