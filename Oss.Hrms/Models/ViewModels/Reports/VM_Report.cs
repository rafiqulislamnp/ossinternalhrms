﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.ViewModels.Attendance;

namespace Oss.Hrms.Models.ViewModels.Reports
{
    public class VM_Report
    {

        private HrmsContext db = new HrmsContext();
        //public List<SelectListItem> ReportNameList { get; set; }

        [Display(Name = "Select Report")]
        public string ReportName { get; set; }
        [DisplayName("Report")]
        public int ReportId { get; set; }
        public int SectionID { get; set; }
        public int? CardID { get; set; }
        public string Designation { get; set; }
        public int Posted { get; set; }
        public int Present { get; set; }
        public int Late { get; set; }
        public int Absent { get; set; }
        public int Leave { get; set; }
       // public decimal Total_Leave { get; set; }
        public int Holiday { get; set; }
        public int Offday { get; set; }
        public string Remarks { get; set; }
        [DisplayName("From Date")]
        public DateTime Fromdate { get; set; }
        [DisplayName("To Date")]
        public DateTime Todate { get; set; }
        public int EmployeeId { get; set; }
        public int EmployeeId1 { get; set; }
        public string Employee_ID { get; set; }
        public string CardTitle { get; set; }
        public string Name { get; set; }
        public string Designation_Name { get; set; }
        public string UnitName { get; set; }
        public string Line { get; set; }
        public string SectionName { get; set; }
        public string Date { get; set; }
        public TimeSpan InTime { get; set; }
        public TimeSpan OutTime { get; set; }
        public string TotalDuration { get; set; }
        public string Status { get; set; }
        public int TotalOT { get; set; }
        public int BOT { get; set; }
        public int EOT { get; set; }
        public int OT { get; set; }
        public string Joining_Date { get; set; }
        public string Leave_Name { get; set; }
        public string Leave_From { get; set; }
        public string Leave_To { get; set; }
        public decimal Approved_Days { get; set; }
        public decimal Total_Leave { get; set; }
        
        public decimal Total_Approved_Leave { get; set; }
        public decimal Leave_Balance { get; set; }
        public string CardNo { get; set; }
        public string DrawnSalary { get; set; }
        public int? EmployeeStatus { get; set; }
        // public string TotalDuration { get; set; }

        public int PayrollID { get; set; }
        public int Total_Month { get; set; }
        public int Working_Days { get; set; }
        public int F_Leave { get; set; }
        public int TotalDeductionDays { get; set; }
        public int TotalWorkingDays { get; set; }
        public decimal TotalEarnLeave { get; set; }
        public int EnjoyableLeave { get; set; }
        public decimal PayableEarnLeave { get; set; }
        public int Stamp { get; set; }
        public decimal Present_Salary { get; set; }
        public decimal OneDaySalary { get; set; }
        public decimal PayableEarnLeaveTK { get; set; }
        public string Signature { get; set; }
        public string Unit { get; set; }
        public string Name_CardNo { get; set; }
        public string Designation_JoinDate { get; set; }
        public string One { get; set; }
        public string Two { get; set; }
        public string Three { get; set; }
        public string Four { get; set; }
        public string Five { get; set; }
        public string Six { get; set; }
        public string Seven { get; set; }
        public string Eight { get; set; }
        public string Nine { get; set; }
        public string Ten { get; set; }
        public string Eleven { get; set; }
        public string Twelve { get; set; }
        public string Thirteen { get; set; }
        public string Forteen { get; set; }
        public string Fifteen { get; set; }
        public string Sixteen { get; set; }
        public string Seventeen { get; set; }
        public string Eighteen { get; set; }
        public string Nineteen { get; set; }
        public string Twenty { get; set; }
        public string TwentyOne { get; set; }
        public string TwentyTwo { get; set; }
        public string TwentyThree { get; set; }
        public string TwentyFour { get; set; }
        public string TwentyFive { get; set; }
        public string TwentySix { get; set; }
        public string TwentySeven { get; set; }
        public string TwentyEight { get; set; }
        public string TwentyNine { get; set; }
        public string Thirty { get; set; }
        public string ThirtyOne { get; set; }
        public int Working_Day { get; set; }
        public int Lunch_Leave { get; set; }
        public int CL_EL_ML { get; set; }
        public int FestivalLeave_SpecialLeave { get; set; }
        public int TotalLeave { get; set; }
        public int Invalid { get; set; }
        public int TotalPresent { get; set; }
        public int GrandTotal { get; set; }
       

        public IEnumerable<VM_Report> DataListReports { get; set; }

        public DataTable TotalMonthlyAttendnanceReprt(DateTime fromdate, DateTime todate)
        {

            DataTable table = new DataTable();
            table.Columns.Add("Unit", typeof(string));
            table.Columns.Add("Card Title", typeof(string));
            table.Columns.Add("Name CardNo", typeof(string));
            table.Columns.Add("Designation JoinDate", typeof(string));
            table.Columns.Add("1", typeof(string));
            table.Columns.Add("2", typeof(string));
            table.Columns.Add("3", typeof(string));
            table.Columns.Add("4", typeof(string));
            table.Columns.Add("5", typeof(string));
            table.Columns.Add("6", typeof(string));
            table.Columns.Add("7", typeof(string));
            table.Columns.Add("8", typeof(string));
            table.Columns.Add("9", typeof(string));
            table.Columns.Add("10", typeof(string));
            table.Columns.Add("11", typeof(string));
            table.Columns.Add("12", typeof(string));
            table.Columns.Add("13", typeof(string));
            table.Columns.Add("14", typeof(string));
            table.Columns.Add("15", typeof(string));
            table.Columns.Add("16", typeof(string));
            table.Columns.Add("17", typeof(string));
            table.Columns.Add("18", typeof(string));
            table.Columns.Add("19", typeof(string));
            table.Columns.Add("20", typeof(string));
            table.Columns.Add("21", typeof(string));
            table.Columns.Add("22", typeof(string));
            table.Columns.Add("23", typeof(string));
            table.Columns.Add("24", typeof(string));
            table.Columns.Add("25", typeof(string));
            table.Columns.Add("26", typeof(string));
            table.Columns.Add("27", typeof(string));
            table.Columns.Add("28", typeof(string));
            table.Columns.Add("29", typeof(string));
            table.Columns.Add("30", typeof(string));
            table.Columns.Add("31", typeof(string));
            table.Columns.Add("Working Days", typeof(string));
            table.Columns.Add("Present", typeof(string));
            table.Columns.Add("Late", typeof(string));
           // table.Columns.Add("Leave", typeof(string));
            table.Columns.Add("Lunch Leave", typeof(string));
            table.Columns.Add("CL_EL_ML", typeof(string));
            table.Columns.Add("Total_Leave", typeof(string));
            table.Columns.Add("FestivalLeave_SpecialLeave", typeof(string));
            table.Columns.Add("Holiday", typeof(string));
            table.Columns.Add("Absent", typeof(string));
            table.Columns.Add("Invalid", typeof(string));
            table.Columns.Add("Total Present", typeof(string));
            table.Columns.Add("GrandTotal", typeof(string));
            table.Columns.Add("TotalOT", typeof(string));

            //Unit	CardTitle	EmployeeId	Name_CardNo	Designation_JoinDate	1	2	3	4	5	6	7	8	9	10	11	12	13	14	15	16	17	18	19	20	21	22	23	24	25	26	27	28	29	30	31	
            //Working_Day	Present	Late	Lunch_Leave	CL_EL_ML	Total_Leave	FestivalLeave_SpecialLeave	Holiday	Absent	
            //Invalid	TotalPresent	GrandTotal	TotalOT
            table.Rows.Clear();

            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var dt2 = todate.ToString("yyyy-MM-dd");
            var monthname = fromdate.ToString("MMMM");
            // DateTime dt = DateTime.Now;
            // Console.WriteLine(dt.ToString("MMMM"));

            var all_rows = db.Database.SqlQuery<VM_Report>("Exec [dbo].[sp_EmployeeMonthly_Attendance_Report] '" + dt1 + "','" + dt2 + "' ").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].Unit, all_rows[i].CardTitle, all_rows[i].Name_CardNo, all_rows[i].Designation_JoinDate,
                    all_rows[i].One, all_rows[i].Two,all_rows[i].Three, all_rows[i].Four, all_rows[i].Five, all_rows[i].Six,
                    all_rows[i].Seven, all_rows[i].Eight,all_rows[i].Nine, all_rows[i].Ten, all_rows[i].Eleven, all_rows[i].Twelve,
                    all_rows[i].Thirteen, all_rows[i].Forteen,all_rows[i].Fifteen, all_rows[i].Sixteen, all_rows[i].Seventeen, all_rows[i].Eighteen,
                    all_rows[i].Nineteen, all_rows[i].Twenty,all_rows[i].TwentyOne, all_rows[i].TwentyTwo, all_rows[i].TwentyThree, all_rows[i].TwentyFour,
                    all_rows[i].TwentyFive, all_rows[i].TwentySix,all_rows[i].TwentySeven, all_rows[i].TwentyEight, all_rows[i].TwentyNine, all_rows[i].Thirty, all_rows[i].ThirtyOne,
                    all_rows[i].Working_Day, all_rows[i].Present, all_rows[i].Late, all_rows[i].Lunch_Leave,
                    all_rows[i].CL_EL_ML, all_rows[i].TotalLeave, all_rows[i].FestivalLeave_SpecialLeave, all_rows[i].Holiday,
                    all_rows[i].Absent, all_rows[i].Invalid, all_rows[i].TotalPresent, all_rows[i].GrandTotal, all_rows[i].TotalOT);
            }
            return table;
        }


        public DataTable DailyAttendanceSummery(DateTime fromdate)
        {
            DataTable table = new DataTable();
           // table.Columns.Add("Unit", typeof(string));
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Posted", typeof(string));
            table.Columns.Add("Present", typeof(string));
            table.Columns.Add("Late", typeof(string));
            table.Columns.Add("Absent", typeof(string));
            table.Columns.Add("Leave", typeof(string));
            table.Columns.Add("Holiday", typeof(string));
            table.Columns.Add("Offday", typeof(string));
            table.Columns.Add("Invalid", typeof(string));
            table.Columns.Add("Remarks", typeof(string));
            //var dt = "2018-05-01";
            var dt1 = fromdate.ToString("yyyy-MM-dd");

            var all_rows = db.Database.SqlQuery<VM_Report>($@"Select COALESCE(SectionName,'Grand Total') as SectionName, 
                            COALESCE(Name,'Total')  as  [Designation],
                            Sum(Posted) as Posted,Sum(Present)as Present,Sum(Late)as Late,
                            Sum([Absent])as [Absent],Sum(Leave)as Leave,Sum(Holiday)as Holiday, Sum(Offday)as Offday,Sum(Invalid)as Invalid
                            from(
                            Select  SectionName, Name ,Posted,Present,Late,[Absent],Leave,Holiday,Remarks,Offday,Invalid
                            from  (Select (Select count(1) as Posted from HRMS_Employee e where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID ) as Posted,
                            (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Attendance_History b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 1 and b.[Date]='{dt1}') as Present,
                            (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Attendance_History b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 2 and b.[Date]='{dt1}') as Late,
                            (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Attendance_History b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 3 and b.[Date]='{dt1}') as [Absent],
                            (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Attendance_History b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 4 and b.[Date]='{dt1}') as Leave,
                            (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Attendance_History b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 6 and b.[Date]='{dt1}') as Holiday,
                            (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Attendance_History b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 5 and b.[Date]='{dt1}') as Offday,
                            (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Attendance_History b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 9 and b.[Date]='{dt1}') as Invalid,
                                u.ID as Unit_Id,u.UnitName, '' as Remarks, d.Name,s.SectionName from HRMS_Attendance_History a 
                                join HRMS_Employee e on a.EmployeeId = e.ID 
                                inner Join Designations d on e.Designation_Id = d.ID 
                                join Units u on e.Unit_Id = u.ID 
                                join Sections s on e.Section_Id=s.ID
                                where a.[Date]= '{dt1}' and e.Present_Status = 1) t  
                            group by Posted, Present, Late,[Absent], Leave, Holiday, Offday, Invalid, Remarks, Name, SectionName 
                            ) as a 
                            group by SectionName, name  WITH ROLLUP").ToList();
            // and s.ID=1


            //var all_rows = db.Database.SqlQuery<VM_Report>($@"Select COALESCE(SectionName,'Grand Total') as SectionName, 
            //COALESCE(Name,'Total')  as  [Designation],
            //SUM(Posted) AS Posted,Sum(Present)as Present,Sum(Late)as Late,
            //Sum([Absent])as [Absent],Sum(Leave)as Leave,Sum(Holiday)as Holiday, Sum(Offday)as Offday,
            //case when SUM(Posted) = (Sum(Present)+Sum(Late)+Sum([Absent])+Sum(Leave)+Sum(Holiday)+Sum(Offday)+Sum(Invalid)) 
            //then Sum(Invalid) else SUM(Posted) - (Sum(Present)+Sum(Late)+Sum([Absent])+Sum(Leave)+Sum(Holiday)+Sum(Offday)+Sum(Invalid))
            //end as Invalid from(
            //Select  SectionName, Name ,Posted,Present,Late,[Absent],Leave,Holiday,Remarks,Offday,Invalid
            //from  (Select  (Select count(1) from HRMS_Employee where designation_Id = d.ID and Present_Status = 1) as Posted,
            //(Select count(1) from HRMS_Attendance_History b Join HRMS_Employee ee on b.EmployeeId = ee.ID where[Date] = '{dt1}'  and[AttendanceStatus] = 1 and ee.Designation_Id = e.Designation_Id and e.Present_Status = 1) as Present, 
            //(Select count(1) from HRMS_Attendance_History b Join HRMS_Employee ee on b.EmployeeId = ee.ID where[Date] = '{dt1}'  and[AttendanceStatus] = 2 and ee.Designation_Id = e.Designation_Id and e.Present_Status = 1) as Late,  
            //(Select count(1) from HRMS_Attendance_History b Join HRMS_Employee ee on b.EmployeeId = ee.ID where[Date] = '{dt1}'  and[AttendanceStatus] = 3 and ee.Designation_Id = e.Designation_Id and e.Present_Status = 1)as [Absent],  
            //(Select count(1) from HRMS_Attendance_History b Join HRMS_Employee ee on b.EmployeeId = ee.ID where[Date] = '{dt1}'  and[AttendanceStatus] = 4 and ee.Designation_Id = e.Designation_Id and e.Present_Status = 1)as Leave,  
            //(Select count(1) from HRMS_Attendance_History b Join HRMS_Employee ee on b.EmployeeId = ee.ID where[Date] = '{dt1}'  and[AttendanceStatus] = 6 and ee.Designation_Id = e.Designation_Id and e.Present_Status = 1)as Holiday, 
            //(Select count(1) from HRMS_Attendance_History b Join HRMS_Employee ee on b.EmployeeId = ee.ID where[Date] = '{dt1}'  and[AttendanceStatus] = 5 and ee.Designation_Id = e.Designation_Id and e.Present_Status = 1)as Offday, 
            //(Select count(1) from HRMS_Attendance_History b Join HRMS_Employee ee on b.EmployeeId = ee.ID where[Date] = '{dt1}' and[AttendanceStatus] = 9 and ee.Designation_Id = e.Designation_Id and e.Present_Status = 1)as Invalid, 
            // u.ID as Unit_Id,u.UnitName, '' as Remarks, d.Name,s.SectionName from HRMS_Attendance_History a 
            // join HRMS_Employee e on a.EmployeeId = e.ID 
            // inner Join Designations d on e.Designation_Id = d.ID 
            // join Units u on e.Unit_Id = u.ID 
            // join Sections s on e.Section_Id=s.ID
            // where a.[Date]= '{dt1}') t  and e.Present_Status = 1
            //group by Posted, Present, Late,[Absent], Leave, Holiday, Offday, Invalid, Remarks, Name, SectionName 
            //) as a 
            //group by SectionName, name  WITH ROLLUP ").ToList(); //ORDER BY SectionName

            for (int i = 0; i < all_rows.Count; i++)
            {
               table.Rows.Add(all_rows[i].SectionName, all_rows[i].Designation, all_rows[i].Posted, all_rows[i].Present, all_rows[i].Late, all_rows[i].Absent, all_rows[i].Leave, all_rows[i].Holiday, all_rows[i].Offday, all_rows[i].Invalid, all_rows[i].Remarks);
            }
            return table;

        }


        public DataTable InstantDailyAttendanceSummery(DateTime fromdate)
        {
            DataTable table = new DataTable();
            // table.Columns.Add("Unit", typeof(string));
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Posted", typeof(string));
            table.Columns.Add("Present", typeof(string));
            table.Columns.Add("Late", typeof(string));
            table.Columns.Add("Absent", typeof(string));
            table.Columns.Add("Leave", typeof(string));
            table.Columns.Add("Holiday", typeof(string));
            table.Columns.Add("Offday", typeof(string));
            table.Columns.Add("Invalid", typeof(string));
            table.Columns.Add("Remarks", typeof(string));
            //var dt = "2018-05-01";
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            //case when SUM(Posted) = (Sum(Present)+Sum(Late)+Sum([Absent])+Sum(Leave)+Sum(Holiday)+Sum(Offday)+Sum(Invalid)) 
            //then Sum(Invalid) else SUM(Posted) - (Sum(Present)+Sum(Late)+Sum([Absent])+Sum(Leave)+Sum(Holiday)+Sum(Offday)+Sum(Invalid)) end as Invalid
            var all_rows = db.Database.SqlQuery<VM_Report>($@"Select COALESCE(SectionName,'Grand Total') as SectionName, 
                                                            COALESCE(Name,'Total')  as  [Designation],
                                                            Sum(Posted) as Posted,Sum(Present)as Present,Sum(Late)as Late,
                                                            Sum([Absent])as [Absent],Sum(Leave)as Leave,Sum(Holiday)as Holiday, Sum(Offday)as Offday,Sum(Invalid)as Invalid

                                                            from(
                                                            Select  SectionName, Name ,Posted,Present,Late,[Absent],Leave,Holiday,Remarks,Offday,Invalid
                                                            from  (Select (Select count(1) as Posted from HRMS_Employee e where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID ) as Posted,
                                                            (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Daily_Attendance b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 1 and b.[Date]='{dt1}') as Present,
                                                            (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Daily_Attendance b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 2 and b.[Date]='{dt1}') as Late,
                                                            (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Daily_Attendance b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 3 and b.[Date]='{dt1}') as [Absent],
                                                            (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Daily_Attendance b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 4 and b.[Date]='{dt1}') as Leave,
                                                            (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Daily_Attendance b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 6 and b.[Date]='{dt1}') as Holiday,
                                                            (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Daily_Attendance b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 5 and b.[Date]='{dt1}') as Offday,
                                                            (Select count(1) as Posted from HRMS_Employee e inner join HRMS_Daily_Attendance b on b.EmployeeId=e.ID where e.Present_Status=1 and e.section_Id=s.ID and e.designation_Id = d.ID and b.[AttendanceStatus] = 9 and b.[Date]='{dt1}') as Invalid,
                                                                u.ID as Unit_Id,u.UnitName, '' as Remarks, d.Name,s.SectionName from HRMS_Daily_Attendance a 
                                                                join HRMS_Employee e on a.EmployeeId = e.ID 
                                                                inner Join Designations d on e.Designation_Id = d.ID 
                                                                join Units u on e.Unit_Id = u.ID 
                                                                join Sections s on e.Section_Id=s.ID
                                                                where a.[Date]= '{dt1}' and e.Present_Status = 1) t  
                                                            group by Posted, Present, Late,[Absent], Leave, Holiday, Offday, Invalid, Remarks, Name, SectionName 
                                                            ) as a 
                                                            group by SectionName, name  WITH ROLLUP").ToList();
            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SectionName, all_rows[i].Designation, all_rows[i].Posted, all_rows[i].Present, all_rows[i].Late, all_rows[i].Absent, all_rows[i].Leave, all_rows[i].Holiday, all_rows[i].Offday, all_rows[i].Invalid, all_rows[i].Remarks);
            }
            return table;

        }




        public DataTable PersonalMonthlyAttendanceSummery(DateTime fromdate,DateTime todate, int employeeId)
        {
            DataTable table = new DataTable();
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("Employee_ID", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation_Name", typeof(string));
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("UnitName", typeof(string));
            table.Columns.Add("Line", typeof(string));
            table.Columns.Add("Date", typeof(string));
            table.Columns.Add("InTime", typeof(string));
            table.Columns.Add("OutTime", typeof(string));
            table.Columns.Add("Status", typeof(string));
            table.Columns.Add("TotalOT", typeof(string));
            table.Columns.Add("BOT", typeof(string));
            table.Columns.Add("EOT", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var dt2 = todate.ToString("yyyy-MM-dd");
            var all_rows = db.Database.SqlQuery<VM_Report>($@"Select e.EmployeeIdentity as Employee_ID,e.CardTitle,e.Name,d.Name as [Designation_Name],u.UnitName,l.Line,s.SectionName,
                                                         convert(varchar(10),a.[Date],126) as [Date],CAST(a.Intime AS time) as InTime,CAST(a.[OutTime]  AS time)as OutTime,
                                                         (SELECT [dbo].[fn_BuyerStatus](a.AttendanceStatus,e.ID, a.[Date])) as [Status],
                                                         --(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' 
                                                         --WHEN a.AttendanceStatus = 2 THEN 'Late' 
                                                         --WHEN a.AttendanceStatus = 3 THEN 'Absent' 
                                                         --WHEN a.AttendanceStatus = 4 THEN 'Leave' 
                                                         --WHEN a.AttendanceStatus = 5 THEN 'OffDay' 
                                                         --WHEN a.AttendanceStatus = 6 THEN 'Holiday' 
                                                         --ELSE 'Invalid' END) as [Status], 
                                                         PayableOverTime AS [TotalOT], 
                                                         (SELECT [dbo].[fn_BuyerOffdayOT]((CASE WHEN a.PayableOverTime >= 2 THEN 2 ELSE 0 END),e.ID, a.[Date])) as [BOT],
                                                         (SELECT [dbo].[fn_EmployeeOffdayEOT](a.PayableOverTime,e.ID, a.[Date])) as [EOT]
                                                         --[dbo].[fn_EmployeeOffdayEOT]
                                                         --(CASE WHEN a.PayableOverTime >= 2 THEN 2 ELSE 0 END) as [BOT], 
                                                         ---(CASE WHEN a.PayableOverTime > 2 THEN PayableOverTime-2 ELSE  0 END ) as [EOT]
                                                          from HRMS_Attendance_History a 
                                                         join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID 
                                                         join Units u on e.Unit_Id = u.ID join Designations d on e.Designation_Id = d.ID 
                                                         join Sections s on e.Section_Id = s.ID join LineNoes l on e.LineId = l.ID Where[Date] between '" + dt1 + "' and '" + dt2 + "' and e.ID =" + employeeId + " ").ToList();
            //"Select e.EmployeeIdentity as Employee_ID,e.CardTitle,e.Name,d.Name as [Designation_Name]," +
            //                                           "u.UnitName,l.Line,s.SectionName, convert(varchar(10),a.[Date],126) as [Date],CAST(a.Intime AS time) as InTime,CAST(a.[OutTime]  AS time)as OutTime, " +
            //                                           "(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' " +
            //                                           "WHEN a.AttendanceStatus = 2 THEN 'Late' " +
            //                                           "WHEN a.AttendanceStatus = 3 THEN 'Absent' " +
            //                                           "WHEN a.AttendanceStatus = 4 THEN 'Leave' " +
            //                                           "WHEN a.AttendanceStatus = 5 THEN 'OffDay' " +
            //                                           "WHEN a.AttendanceStatus = 6 THEN 'Holiday' " +
            //                                           "ELSE 'Invalid' END) as [Status], " +
            //                                           "PayableOverTime AS[TotalOT], " +
            //                                           "(CASE WHEN a.PayableOverTime >= 2 THEN 2 ELSE 0 END) as [BOT], " +
            //                                           "(CASE WHEN a.PayableOverTime > 2 THEN PayableOverTime-2 ELSE 0 END) as [EOT] " +
            //                                           " from HRMS_Attendance_History a " +
            //                                           "join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID " +
            //                                           "join Units u on e.Unit_Id = u.ID join Designations d on e.Designation_Id = d.ID " +
            //                                           "join Sections s on e.Section_Id = s.ID join LineNoes l on e.LineId = l.ID " +
            //                                           "Where[Date] between '"+ dt1 + "' and '"+ dt2+ "' and e.ID ="+ employeeId + " ").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].CardTitle, all_rows[i].Employee_ID, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].SectionName, all_rows[i].UnitName, all_rows[i].Line, all_rows[i].Date
                    , all_rows[i].InTime, all_rows[i].OutTime, all_rows[i].Status, all_rows[i].TotalOT,all_rows[i].BOT, all_rows[i].EOT);
            }
            return table;
        }



        public DataTable PersonalMonthlyAttendanceSummeryBuyerReprt(DateTime fromdate, DateTime todate, int employeeId)
        {
            DataTable table = new DataTable();
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("Employee_ID", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation_Name", typeof(string));
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("UnitName", typeof(string));
            table.Columns.Add("Line", typeof(string));
            table.Columns.Add("Date", typeof(string));
            table.Columns.Add("InTime", typeof(string));
            table.Columns.Add("OutTime", typeof(string));
            table.Columns.Add("Status", typeof(string));
            table.Columns.Add("OT", typeof(string));
            //table.Columns.Add("BOT", typeof(string));
            //table.Columns.Add("EOT", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var dt2 = todate.ToString("yyyy-MM-dd");
            var all_rows = db.Database.SqlQuery<VM_Report>($@"Select e.EmployeeIdentity as Employee_ID,e.CardTitle,e.Name,d.Name as [Designation_Name],u.UnitName,l.Line,s.SectionName,
                                         convert(varchar(10),a.[Date],126) as [Date],
                                         --CAST(a.Intime AS time) as InTime,
                                         (SELECT [dbo].[fn_BuyerinTime] (CAST(a.[Intime]  AS time),a.[Date],e.ID)) as InTime,
                                         (SELECT [dbo].[fn_BuyerOutTime] (CAST(a.[OutTime]  AS time),
                                         (CASE WHEN a.PayableOverTime > 2 THEN PayableOverTime-2 ELSE 0 END),a.[Date],e.ID))as OutTime, 
                                         (SELECT [dbo].[fn_BuyerStatus](a.AttendanceStatus,e.ID, a.[Date])) as [Status],
                                         --(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' WHEN a.AttendanceStatus = 2 THEN 'Late'
                                         -- WHEN a.AttendanceStatus = 3 THEN 'Absent' WHEN a.AttendanceStatus = 4 THEN 'Leave'
                                         --  WHEN a.AttendanceStatus = 5 THEN 'OffDay' WHEN a.AttendanceStatus = 6 THEN 'Holiday' 
                                         --  ELSE 'Invalid' END) as [Status], 
                                           PayableOverTime AS[TotalOT], 
                                           --[dbo].[fn_BuyerOffdayOT]
                                           (SELECT [dbo].[fn_BuyerOffdayOT]((CASE WHEN a.PayableOverTime >= 2 THEN 2 ELSE 0 END),e.ID, a.[Date])) as [OT],
                                           --(CASE WHEN a.PayableOverTime >= 2 THEN 2 ELSE 0 END) as [OT], 
                                           (CASE WHEN a.PayableOverTime > 2 THEN PayableOverTime-2 ELSE 0 END) as [EOT]  
                                           from HRMS_Attendance_History a join HRMS_Employee e on a.EmployeeId = e.ID join 
                                           HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID join 
                                           Units u on e.Unit_Id = u.ID join 
                                           Designations d on e.Designation_Id = d.ID join 
                                           Sections s on e.Section_Id = s.ID join LineNoes l on e.LineId = l.ID 
                                            Where[Date] between '" + dt1 + "' and '" + dt2 + "' and e.ID =" + employeeId + " ").ToList();
            // Where[Date] between '2018-07-01' and '2018-07-31' and e.ID =2943
                //"Select e.EmployeeIdentity as Employee_ID,e.CardTitle,e.Name,d.Name as [Designation_Name]," +
                //                                           "u.UnitName,l.Line,s.SectionName, convert(varchar(10),a.[Date],126) as [Date],CAST(a.Intime AS time) as InTime,(SELECT [dbo].[fn_BuyerOutTime] (CAST(a.[OutTime]  AS time),(CASE WHEN a.PayableOverTime > 2 THEN PayableOverTime-2 ELSE 0 END)))as OutTime, " +
                //                                           "(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' " +
                //                                           "WHEN a.AttendanceStatus = 2 THEN 'Late' " +
                //                                           "WHEN a.AttendanceStatus = 3 THEN 'Absent' " +
                //                                           "WHEN a.AttendanceStatus = 4 THEN 'Leave' " +
                //                                           "WHEN a.AttendanceStatus = 5 THEN 'OffDay' " +
                //                                           "WHEN a.AttendanceStatus = 6 THEN 'Holiday' " +
                //                                           "ELSE 'Invalid' END) as [Status], " +
                //                                           "PayableOverTime AS[TotalOT], " +
                //                                           "(CASE WHEN a.PayableOverTime >= 2 THEN 2 ELSE 0 END) as [OT], " +
                //                                           "(CASE WHEN a.PayableOverTime > 2 THEN PayableOverTime-2 ELSE 0 END) as [EOT] " +
                //                                           " from HRMS_Attendance_History a " +
                //                                           "join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID " +
                //                                           "join Units u on e.Unit_Id = u.ID join Designations d on e.Designation_Id = d.ID " +
                //                                           "join Sections s on e.Section_Id = s.ID join LineNoes l on e.LineId = l.ID " +
                //                                           "Where[Date] between '" + dt1 + "' and '" + dt2 + "' and e.ID =" + employeeId + " ").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].CardTitle, all_rows[i].Employee_ID, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].SectionName, all_rows[i].UnitName, all_rows[i].Line, all_rows[i].Date
                    , all_rows[i].InTime, all_rows[i].OutTime, all_rows[i].Status, all_rows[i].OT);
            }
            return table;
        }





        public DataTable DailyInvalidAttendance(DateTime fromdate)
        {
            DataTable table = new DataTable();
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation_Name", typeof(string));
            table.Columns.Add("Date", typeof(string));
            table.Columns.Add("InTime", typeof(string));
            table.Columns.Add("OutTime", typeof(string));
            table.Columns.Add("Status", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var all_rows = db.Database.SqlQuery<VM_Report>("Select s.SectionName, (e.CardTitle  +' '+ e.EmployeeIdentity) as CardTitle, e.Name, d.Name as [Designation_Name], " +
                                                           "convert(varchar(10), a.[Date], 126) as [Date], CAST(a.Intime AS time) as InTime, CAST(a.[OutTime]  AS time) as OutTime, " +
                                                           "(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' " +
                                                           "WHEN a.AttendanceStatus = 2 THEN 'Late' " +
                                                           "WHEN a.AttendanceStatus = 3 THEN 'Absent' " +
                                                           "WHEN a.AttendanceStatus = 4 THEN 'Leave' " +
                                                           "WHEN a.AttendanceStatus = 5 THEN 'OffDay'  " +
                                                           "WHEN a.AttendanceStatus = 6 THEN 'Holiday' " +
                                                           "ELSE 'Invalid' END) as [Status] from HRMS_Attendance_History a " +
                                                           "join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID " +
                                                           "join Designations d on e.Designation_Id = d.ID join Sections s on e.Section_Id = s.ID " +
                                                           "Where convert(varchar(10),[Date], 126) = '"+dt1+"' and a.Intime = a.[OutTime] " +
                                                           "Group by s.SectionName,e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,a.[Date],a.Intime,a.Outtime, a.AttendanceStatus").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardTitle, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Date
                    , all_rows[i].InTime, all_rows[i].OutTime, all_rows[i].Status);
            }
            return table;
        }


        public DataTable DailyAttendanceReport(DateTime fromdate)
        {
            DataTable table = new DataTable();
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation_Name", typeof(string));
            table.Columns.Add("Date", typeof(string));
            table.Columns.Add("InTime", typeof(string));
            table.Columns.Add("OutTime", typeof(string));
            table.Columns.Add("Status", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var all_rows = db.Database.SqlQuery<VM_Report>("Select s.SectionName, (e.CardTitle  +' '+ e.EmployeeIdentity) as CardTitle, e.Name, d.Name as [Designation_Name], " +
                                                           "convert(varchar(10), a.[Date], 126) as [Date], CAST(a.Intime AS time) as InTime, CAST(a.[OutTime]  AS time) as OutTime, " +
                                                           "(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' " +
                                                           "WHEN a.AttendanceStatus = 2 THEN 'Late' " +
                                                           "WHEN a.AttendanceStatus = 3 THEN 'Absent' " +
                                                           "WHEN a.AttendanceStatus = 4 THEN 'Leave' " +
                                                           "WHEN a.AttendanceStatus = 5 THEN 'OffDay'  " +
                                                           "WHEN a.AttendanceStatus = 6 THEN 'Holiday' " +
                                                           "ELSE 'Invalid' END) as [Status] from HRMS_Attendance_History a " +
                                                           "join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID " +
                                                           "join Designations d on e.Designation_Id = d.ID join Sections s on e.Section_Id = s.ID " +
                                                           "Where convert(varchar(10),[Date], 126) = '" + dt1 + "'" +
                                                           "Group by s.SectionName,e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,a.[Date],a.Intime,a.Outtime, a.AttendanceStatus").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardTitle, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Date
                    , all_rows[i].InTime, all_rows[i].OutTime, all_rows[i].Status);
            }
            return table;
        }


        public DataTable InstantDailyAttendanceReport(DateTime fromdate)
        {
            DataTable table = new DataTable();
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation_Name", typeof(string));
            table.Columns.Add("Date", typeof(string));
            table.Columns.Add("InTime", typeof(string));
            table.Columns.Add("OutTime", typeof(string));
            table.Columns.Add("Status", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            //CAST(a.[OutTime]  AS time) as OutTime
            var all_rows = db.Database.SqlQuery<VM_Report>("Select s.SectionName, (e.CardTitle  +' '+ e.EmployeeIdentity) as CardTitle, e.Name, d.Name as [Designation_Name], " +
                                                           "convert(varchar(10), a.[Date], 126) as [Date], CAST(a.Intime AS time) as InTime, '00:00:00' as OutTime, " +
                                                           "(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' " +
                                                           "WHEN a.AttendanceStatus = 2 THEN 'Late' " +
                                                           "WHEN a.AttendanceStatus = 3 THEN 'Absent' " +
                                                           "WHEN a.AttendanceStatus = 4 THEN 'Leave' " +
                                                           "WHEN a.AttendanceStatus = 5 THEN 'OffDay'  " +
                                                           "WHEN a.AttendanceStatus = 6 THEN 'Holiday' " +
                                                           "ELSE 'Invalid' END) as [Status] from HRMS_Daily_Attendance a " +
                                                           "join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID " +
                                                           "join Designations d on e.Designation_Id = d.ID join Sections s on e.Section_Id = s.ID " +
                                                           "Where convert(varchar(10),[Date], 126) = '" + dt1 + "'" +
                                                           "Group by s.SectionName,e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,a.[Date],a.Intime,a.Outtime, a.AttendanceStatus").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardTitle, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Date
                    , all_rows[i].InTime, all_rows[i].OutTime, all_rows[i].Status);
            }
            return table;
        }

        public DataTable DailyLateAttendanceReport(DateTime fromdate)
        {
            DataTable table = new DataTable();
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation_Name", typeof(string));
            table.Columns.Add("Date", typeof(string));
            table.Columns.Add("InTime", typeof(string));
            table.Columns.Add("OutTime", typeof(string));
            table.Columns.Add("Status", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var all_rows = db.Database.SqlQuery<VM_Report>("Select s.SectionName, (e.CardTitle  +' '+ e.EmployeeIdentity) as CardTitle, e.Name, d.Name as [Designation_Name], " +
                                                           "convert(varchar(10), a.[Date], 126) as [Date], CAST(a.Intime AS time) as InTime, CAST(a.[OutTime]  AS time) as OutTime, " +
                                                           "(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' " +
                                                           "WHEN a.AttendanceStatus = 2 THEN 'Late' " +
                                                           "WHEN a.AttendanceStatus = 3 THEN 'Absent' " +
                                                           "WHEN a.AttendanceStatus = 4 THEN 'Leave' " +
                                                           "WHEN a.AttendanceStatus = 5 THEN 'OffDay'  " +
                                                           "WHEN a.AttendanceStatus = 6 THEN 'Holiday' " +
                                                           "ELSE 'Invalid' END) as [Status] from HRMS_Attendance_History a " +
                                                           "join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID " +
                                                           "join Designations d on e.Designation_Id = d.ID join Sections s on e.Section_Id = s.ID " +
                                                           "Where convert(varchar(10),[Date], 126) = '" + dt1 + "'and a.AttendanceStatus = 2" +
                                                           "Group by s.SectionName,e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,a.[Date],a.Intime,a.Outtime, a.AttendanceStatus").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardTitle, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Date
                    , all_rows[i].InTime, all_rows[i].OutTime, all_rows[i].Status);
            }
            return table;
        }

        public DataTable InstantDailyLateAttendanceReport(DateTime fromdate)
        {
            DataTable table = new DataTable();
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation_Name", typeof(string));
            table.Columns.Add("Date", typeof(string));
            table.Columns.Add("InTime", typeof(string));
           // table.Columns.Add("OutTime", typeof(string));
            table.Columns.Add("Status", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var all_rows = db.Database.SqlQuery<VM_Report>("Select s.SectionName, (e.CardTitle  +' '+ e.EmployeeIdentity) as CardTitle, e.Name, d.Name as [Designation_Name], " +
                                                           "convert(varchar(10), a.[Date], 126) as [Date], CAST(a.Intime AS time) as InTime, " + //, CAST(a.[OutTime]  AS time) as OutTime
                                                           "(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' " +
                                                           "WHEN a.AttendanceStatus = 2 THEN 'Late' " +
                                                           "WHEN a.AttendanceStatus = 3 THEN 'Absent' " +
                                                           "WHEN a.AttendanceStatus = 4 THEN 'Leave' " +
                                                           "WHEN a.AttendanceStatus = 5 THEN 'OffDay'  " +
                                                           "WHEN a.AttendanceStatus = 6 THEN 'Holiday' " +
                                                           "ELSE 'Invalid' END) as [Status] from HRMS_Daily_Attendance a " +
                                                           "join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID " +
                                                           "join Designations d on e.Designation_Id = d.ID join Sections s on e.Section_Id = s.ID " +
                                                           "Where convert(varchar(10),[Date], 126) = '" + dt1 + "'and a.AttendanceStatus = 2" +
                                                           "Group by s.SectionName,e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,a.[Date],a.Intime, a.AttendanceStatus").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardTitle, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Date
                    , all_rows[i].InTime, all_rows[i].Status);
            }
            return table;
        }


        public DataTable DailyAbsentAttendanceReport(DateTime fromdate)
        {
            DataTable table = new DataTable();
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation_Name", typeof(string));
            table.Columns.Add("Date", typeof(string));
            table.Columns.Add("InTime", typeof(string));
            table.Columns.Add("OutTime", typeof(string));
            table.Columns.Add("Status", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var all_rows = db.Database.SqlQuery<VM_Report>("Select s.SectionName, (e.CardTitle  +' '+ e.EmployeeIdentity) as CardTitle, e.Name, d.Name as [Designation_Name], " +
                                                           "convert(varchar(10), a.[Date], 126) as [Date], CAST(a.Intime AS time) as InTime, CAST(a.[OutTime]  AS time) as OutTime, " +
                                                           "(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' " +
                                                           "WHEN a.AttendanceStatus = 2 THEN 'Late' " +
                                                           "WHEN a.AttendanceStatus = 3 THEN 'Absent' " +
                                                           "WHEN a.AttendanceStatus = 4 THEN 'Leave' " +
                                                           "WHEN a.AttendanceStatus = 5 THEN 'OffDay'  " +
                                                           "WHEN a.AttendanceStatus = 6 THEN 'Holiday' " +
                                                           "ELSE 'Invalid' END) as [Status] from HRMS_Attendance_History a " +
                                                           "join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID " +
                                                           "join Designations d on e.Designation_Id = d.ID join Sections s on e.Section_Id = s.ID " +
                                                           "Where convert(varchar(10),[Date], 126) = '" + dt1 + "'and a.AttendanceStatus = 3" +
                                                           "Group by s.SectionName,e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,a.[Date],a.Intime,a.Outtime, a.AttendanceStatus").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardTitle, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Date
                    , all_rows[i].InTime, all_rows[i].OutTime, all_rows[i].Status);
            }
            return table;
        }

        public DataTable InstantDailyAbsentAttendanceReport(DateTime fromdate)
        {
            DataTable table = new DataTable();
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation_Name", typeof(string));
            table.Columns.Add("Date", typeof(string));
            table.Columns.Add("InTime", typeof(string));
            table.Columns.Add("OutTime", typeof(string));
            table.Columns.Add("Status", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var all_rows = db.Database.SqlQuery<VM_Report>("Select s.SectionName, (e.CardTitle  +' '+ e.EmployeeIdentity) as CardTitle, e.Name, d.Name as [Designation_Name], " +
                                                           "convert(varchar(10), a.[Date], 126) as [Date], CAST(a.Intime AS time) as InTime, CAST(a.[OutTime]  AS time) as OutTime, " +
                                                           "(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' " +
                                                           "WHEN a.AttendanceStatus = 2 THEN 'Late' " +
                                                           "WHEN a.AttendanceStatus = 3 THEN 'Absent' " +
                                                           "WHEN a.AttendanceStatus = 4 THEN 'Leave' " +
                                                           "WHEN a.AttendanceStatus = 5 THEN 'OffDay'  " +
                                                           "WHEN a.AttendanceStatus = 6 THEN 'Holiday' " +
                                                           "ELSE 'Invalid' END) as [Status] from HRMS_Daily_Attendance a " +
                                                           "join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID " +
                                                           "join Designations d on e.Designation_Id = d.ID join Sections s on e.Section_Id = s.ID " +
                                                           "Where convert(varchar(10),[Date], 126) = '" + dt1 + "'and a.AttendanceStatus = 3" +
                                                           "Group by s.SectionName,e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,a.[Date],a.Intime,a.Outtime, a.AttendanceStatus").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardTitle, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Date
                    , all_rows[i].InTime, all_rows[i].OutTime, all_rows[i].Status);
            }
            return table;
        }


        public DataTable DailyInvalidAttendanceReport(DateTime fromdate)
        {
            DataTable table = new DataTable();
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation_Name", typeof(string));
            table.Columns.Add("Date", typeof(string));
            table.Columns.Add("InTime", typeof(string));
            table.Columns.Add("OutTime", typeof(string));
            table.Columns.Add("Status", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var all_rows = db.Database.SqlQuery<VM_Report>("Select s.SectionName, (e.CardTitle  +' '+ e.EmployeeIdentity) as CardTitle, e.Name, d.Name as [Designation_Name], " +
                                                           "convert(varchar(10), a.[Date], 126) as [Date], CAST(a.Intime AS time) as InTime, CAST(a.[OutTime]  AS time) as OutTime, " +
                                                           "(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' " +
                                                           "WHEN a.AttendanceStatus = 2 THEN 'Late' " +
                                                           "WHEN a.AttendanceStatus = 3 THEN 'Absent' " +
                                                           "WHEN a.AttendanceStatus = 4 THEN 'Leave' " +
                                                           "WHEN a.AttendanceStatus = 5 THEN 'OffDay'  " +
                                                           "WHEN a.AttendanceStatus = 6 THEN 'Holiday' " +
                                                           "ELSE 'Invalid' END) as [Status] from HRMS_Attendance_History a " +
                                                           "join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID " +
                                                           "join Designations d on e.Designation_Id = d.ID join Sections s on e.Section_Id = s.ID " +
                                                           "Where convert(varchar(10),[Date], 126) = '" + dt1 + "'and a.AttendanceStatus = 9" +
                                                           "Group by s.SectionName,e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,a.[Date],a.Intime,a.Outtime, a.AttendanceStatus").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardTitle, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Date
                    , all_rows[i].InTime, all_rows[i].OutTime, all_rows[i].Status);
            }
            return table;
        }

        public DataTable InstantDailyInvalidAttendanceReport(DateTime fromdate)
        {
            DataTable table = new DataTable();
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation_Name", typeof(string));
            table.Columns.Add("Date", typeof(string));
            table.Columns.Add("InTime", typeof(string));
            table.Columns.Add("OutTime", typeof(string));
            table.Columns.Add("Status", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var all_rows = db.Database.SqlQuery<VM_Report>("Select s.SectionName, (e.CardTitle  +' '+ e.EmployeeIdentity) as CardTitle, e.Name, d.Name as [Designation_Name], " +
                                                           "convert(varchar(10), a.[Date], 126) as [Date], CAST(a.Intime AS time) as InTime, CAST(a.[OutTime]  AS time) as OutTime, " +
                                                           "(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' " +
                                                           "WHEN a.AttendanceStatus = 2 THEN 'Late' " +
                                                           "WHEN a.AttendanceStatus = 3 THEN 'Absent' " +
                                                           "WHEN a.AttendanceStatus = 4 THEN 'Leave' " +
                                                           "WHEN a.AttendanceStatus = 5 THEN 'OffDay'  " +
                                                           "WHEN a.AttendanceStatus = 6 THEN 'Holiday' " +
                                                           "ELSE 'Invalid' END) as [Status] from HRMS_Daily_Attendance a " +
                                                           "join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID " +
                                                           "join Designations d on e.Designation_Id = d.ID join Sections s on e.Section_Id = s.ID " +
                                                           "Where convert(varchar(10),[Date], 126) = '" + dt1 + "'and a.AttendanceStatus = 9" +
                                                           "Group by s.SectionName,e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,a.[Date],a.Intime,a.Outtime, a.AttendanceStatus").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardTitle, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Date
                    , all_rows[i].InTime, all_rows[i].OutTime, all_rows[i].Status);
            }
            return table;
        }



        public DataTable SectionWiseTiffinList(DateTime fromdate)
        {
            DataTable table = new DataTable();
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation_Name", typeof(string));
            table.Columns.Add("Date", typeof(string));
            table.Columns.Add("InTime", typeof(string));
            table.Columns.Add("OutTime", typeof(string));
            table.Columns.Add("Status", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var all_rows = db.Database.SqlQuery<VM_Report>("Select s.SectionName, (e.CardTitle  +' '+ e.EmployeeIdentity) as CardTitle, e.Name, d.Name as [Designation_Name], " +
                                                           "convert(varchar(10), a.[Date], 126) as [Date], CAST(a.Intime AS time) as InTime, CAST(a.[OutTime]  AS time) as OutTime, " +
                                                           "(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' " +
                                                           "WHEN a.AttendanceStatus = 2 THEN 'Late' " +
                                                           "WHEN a.AttendanceStatus = 3 THEN 'Absent' " +
                                                           "WHEN a.AttendanceStatus = 4 THEN 'Leave' " +
                                                           "WHEN a.AttendanceStatus = 5 THEN 'OffDay'  " +
                                                           "WHEN a.AttendanceStatus = 6 THEN 'Holiday' " +
                                                           "ELSE 'Invalid' END) as [Status] from HRMS_Attendance_History a " +
                                                           "join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID " +
                                                           "join Designations d on e.Designation_Id = d.ID join Sections s on e.Section_Id = s.ID " +
                                                           "Where convert(varchar(10),[Date], 126) = '" + dt1 + "'and a.PayableOverTime >=5" +
                                                           "Group by s.SectionName,e.EmployeeIdentity,e.CardTitle,e.Name,d.Name,a.[Date],a.Intime,a.Outtime, a.AttendanceStatus").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardTitle, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Date
                    , all_rows[i].InTime, all_rows[i].OutTime, all_rows[i].Status);
            }
            return table;
        }





        public DataTable SectionWiseTiffinListSummery(DateTime fromdate)
        {
            DataTable table = new DataTable();
            table.Columns.Add("Section Name", typeof(string));
            table.Columns.Add("Designation Name", typeof(string));
            table.Columns.Add("Posted", typeof(string));
            table.Columns.Add("Tiffin", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var all_rows = db.Database.SqlQuery<VM_Report>($@"Select COALESCE(SectionName,'Z. Grand Total') as SectionName,  
            COALESCE(Designation_Name, 'Total') as  [Designation_Name], Sum(Posted) as Posted, Sum(Tiffinlist) as Present from
            (Select  SectionName,[Designation_Name], Posted, Tiffinlist from(Select(Select count(1) from HRMS_Employee where designation_Id = d.ID and Present_Status = 1) as Posted,
            (Select Count(1) from HRMS_Attendance_History at inner join HRMS_Employee e1 on at.EmployeeId = e1.ID
            Where convert(varchar(10), at.[Date], 126) = '{dt1}' and at.PayableOverTime >= 5 and e1.Designation_Id = e.Designation_Id and e1.Section_Id = e.Section_Id) as Tiffinlist,
            d.Name as [Designation_Name], u.ID as Unit_Id, u.UnitName, '' as Remarks, d.Name, s.SectionName from HRMS_Attendance_History a
            join HRMS_Employee e on a.EmployeeId = e.ID inner Join Designations d on e.Designation_Id = d.ID
            join Units u on e.Unit_Id = u.ID join Sections s on e.Section_Id = s.ID
            where a.[Date] = '{dt1}') t group by Posted, SectionName,[Designation_Name], Tiffinlist ) as a
            group by SectionName, Designation_Name WITH ROLLUP ORDER BY SectionName").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SectionName, all_rows[i].Designation_Name, all_rows[i].Posted, all_rows[i].Present);
            }
            return table;
        }


        public DataTable OvertimeReport(DateTime fromdate)
        {
            DataTable table = new DataTable();
            table.Columns.Add("CardTitle", typeof(string));
            table.Columns.Add("Employee_ID", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation_Name", typeof(string));
            table.Columns.Add("SectionName", typeof(string));
            table.Columns.Add("UnitName", typeof(string));
            table.Columns.Add("Line", typeof(string));
            table.Columns.Add("Date", typeof(string));
            table.Columns.Add("InTime", typeof(string));
            table.Columns.Add("OutTime", typeof(string));
            table.Columns.Add("Status", typeof(string));
            table.Columns.Add("TotalOT", typeof(string));
            table.Columns.Add("BOT", typeof(string));
            table.Columns.Add("EOT", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
           // var dt2 = todate.ToString("yyyy-MM-dd");
            var all_rows = db.Database.SqlQuery<VM_Report>("Select e.EmployeeIdentity as Employee_ID,e.CardTitle,e.Name,d.Name as [Designation_Name]," +
                                                           "u.UnitName,l.Line,s.SectionName, convert(varchar(10),a.[Date],126) as [Date],CAST(a.Intime AS time) as InTime,CAST(a.[OutTime]  AS time)as OutTime, " +
                                                           "(CASE WHEN a.AttendanceStatus = 1 THEN 'Present' " +
                                                           "WHEN a.AttendanceStatus = 2 THEN 'Late' " +
                                                           "WHEN a.AttendanceStatus = 3 THEN 'Absent' " +
                                                           "WHEN a.AttendanceStatus = 4 THEN 'Leave' " +
                                                           "WHEN a.AttendanceStatus = 5 THEN 'OffDay' " +
                                                           "WHEN a.AttendanceStatus = 6 THEN 'Holiday' " +
                                                           "ELSE 'Invalid' END) as [Status], " +
                                                           "PayableOverTime AS[TotalOT], " +
                                                           "(CASE WHEN a.PayableOverTime >= 2 THEN 2 ELSE 0 END) as [BOT], " +
                                                           "(CASE WHEN a.PayableOverTime > 2 THEN PayableOverTime-2 ELSE 0 END) as [EOT] " +
                                                           " from HRMS_Attendance_History a " +
                                                           "join HRMS_Employee e on a.EmployeeId = e.ID join HRMS_Shift_Type hst on a.ShiftTypeId = hst.ID " +
                                                           "join Units u on e.Unit_Id = u.ID join Designations d on e.Designation_Id = d.ID " +
                                                           "join Sections s on e.Section_Id = s.ID join LineNoes l on e.LineId = l.ID " +
                                                           "Where[Date] = '" + dt1 + "'").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].CardTitle, all_rows[i].Employee_ID, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].SectionName, all_rows[i].UnitName, all_rows[i].Line, all_rows[i].Date
                    , all_rows[i].InTime, all_rows[i].OutTime, all_rows[i].Status, all_rows[i].TotalOT, all_rows[i].BOT, all_rows[i].EOT);
            }
            return table;
        }

        #region Leave Report


        public DataTable MonthlyLeaveReprt(DateTime fromdate, DateTime todate,int eid)
        {
            DataTable table = new DataTable();
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Card No", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Joining Date", typeof(string));
            table.Columns.Add("Leave Name", typeof(string));
            table.Columns.Add("Leave From", typeof(string));
            table.Columns.Add("Leave To", typeof(string));
            table.Columns.Add("Approved Leave", typeof(string));
            table.Columns.Add("Total Leave", typeof(string));
            table.Columns.Add("Total Approved Leave", typeof(string));
            table.Columns.Add("Leave Balance", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var dt2 = todate.ToString("yyyy-MM-dd");
            if (eid == 10000000)
            {
                var all_rows = db.Database.SqlQuery<VM_Report>("Select e.Section_Name as SectionName,e.CardTitle +' ' +e.EmployeeIdentity as CardNo,e.Name," +
                                                           "d.Name as Designation_Name,convert(varchar(10),e.Joining_date,126) as Joining_Date,lp.Leave_Name, convert(varchar(10),l.[From],126)as Leave_From,convert(varchar(10),l.[To],126) as Leave_To," +
                                                            "l.[Days] as Approved_Days,ls.Total as Total_Leave,ls.Taken as Total_Approved_Leave," +
                                                           "ls.Remaining as Leave_Balance from HRMS_Leave_Application l " +
                                                           "inner Join HRMS_Leave_Assign ls on ls.EmployeeId = l.EmployeeId " +
                                                           "And ls.LeaveTypeId = l.LeaveTypeId " +
                                                           "inner Join HRMS_Employee e on l.EmployeeId = e.ID " +
                                                           "inner Join HRMS_Leave_Type lp on l.LeaveTypeId = lp.ID " +
                                                           "inner Join Designations d on e.Designation_Id = d.ID " +
                                                           "where  l.Entry_Date between '" + dt1 + "' and '" + dt2 + "' " +
                                                           "group by l.EmployeeId, l.[From], l.[To], l.[Days], ls.Total, ls.Taken," +
                                                           "ls.Remaining, e.Section_Name, e.Name, e.CardTitle, e.EmployeeIdentity, e.Joining_date, lp.Leave_Name, d.Name ").ToList();

                for (int i = 0; i < all_rows.Count; i++)
                {
                    table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardNo, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Joining_Date, all_rows[i].Leave_Name, all_rows[i].Leave_From, all_rows[i].Leave_To
                        , all_rows[i].Approved_Days, all_rows[i].Total_Leave, all_rows[i].Total_Approved_Leave, all_rows[i].Leave_Balance);
                }
            }
            else
            {
                var all_rows = db.Database.SqlQuery<VM_Report>("Select e.Section_Name as SectionName,e.CardTitle +' ' +e.EmployeeIdentity as CardNo,e.Name," +
                                                           "d.Name as Designation_Name,convert(varchar(10),e.Joining_date,126) as Joining_Date,lp.Leave_Name, convert(varchar(10),l.[From],126)as Leave_From,convert(varchar(10),l.[To],126) as Leave_To," +
                                                            "l.[Days] as Approved_Days,ls.Total as Total_Leave,ls.Taken as Total_Approved_Leave," +
                                                           "ls.Remaining as Leave_Balance from HRMS_Leave_Application l " +
                                                           "inner Join HRMS_Leave_Assign ls on ls.EmployeeId = l.EmployeeId " +
                                                           "And ls.LeaveTypeId = l.LeaveTypeId " +
                                                           "inner Join HRMS_Employee e on l.EmployeeId = e.ID " +
                                                           "inner Join HRMS_Leave_Type lp on l.LeaveTypeId = lp.ID " +
                                                           "inner Join Designations d on e.Designation_Id = d.ID " +
                                                           "where  l.Entry_Date between '" + dt1 + "' and '" + dt2 + "' and e.ID="+eid+" " +
                                                           "group by l.EmployeeId, l.[From], l.[To], l.[Days], ls.Total, ls.Taken," +
                                                           "ls.Remaining, e.Section_Name, e.Name, e.CardTitle, e.EmployeeIdentity, e.Joining_date, lp.Leave_Name, d.Name ").ToList();

                for (int i = 0; i < all_rows.Count; i++)
                {
                    table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardNo, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Joining_Date, all_rows[i].Leave_Name, all_rows[i].Leave_From, all_rows[i].Leave_To
                        , all_rows[i].Approved_Days, all_rows[i].Total_Leave, all_rows[i].Total_Approved_Leave, all_rows[i].Leave_Balance);
                }

            }
            return table;

            //var all_rows = db.Database.SqlQuery<VM_Report>("Select e.Section_Name as SectionName,e.CardTitle +' ' +e.EmployeeIdentity as CardNo,e.Name," +
            //                                                   "d.Name as Designation_Name,convert(varchar(10),e.Joining_date,126) as Joining_Date,lp.Leave_Name, convert(varchar(10),l.[From],126)as Leave_From,convert(varchar(10),l.[To],126) as Leave_To," +
            //                                                    "l.[Days] as Approved_Days,ls.Total as Total_Leave,ls.Taken as Total_Approved_Leave," +
            //                                                   "ls.Remaining as Leave_Balance from HRMS_Leave_Application l " +
            //                                                   "inner Join HRMS_Leave_Assign ls on ls.EmployeeId = l.EmployeeId " +
            //                                                   "And ls.LeaveTypeId = l.LeaveTypeId " +
            //                                                   "inner Join HRMS_Employee e on l.EmployeeId = e.ID " +
            //                                                   "inner Join HRMS_Leave_Type lp on l.LeaveTypeId = lp.ID " +
            //                                                   "inner Join Designations d on e.Designation_Id = d.ID " +
            //                                                   "where  l.Entry_Date between '"+dt1+"' and '"+dt2+"' " +
            //                                                   "group by l.EmployeeId, l.[From], l.[To], l.[Days], ls.Total, ls.Taken," +
            //                                                   "ls.Remaining, e.Section_Name, e.Name, e.CardTitle, e.EmployeeIdentity, e.Joining_date, lp.Leave_Name, d.Name ").ToList();

            //    for (int i = 0; i < all_rows.Count; i++)
            //    {
            //        table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardNo, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Joining_Date, all_rows[i].Leave_Name, all_rows[i].Leave_From, all_rows[i].Leave_To
            //            , all_rows[i].Approved_Days, all_rows[i].Total_Leave, all_rows[i].Total_Approved_Leave, all_rows[i].Leave_Balance);
            //    }

        }




        public DataTable SectionWiseMonthlyLeaveReprt(DateTime fromdate, DateTime todate, int sid)
        {
            DataTable table = new DataTable();
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Card No", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Joining Date", typeof(string));
            table.Columns.Add("Leave Name", typeof(string));
            table.Columns.Add("Leave From", typeof(string));
            table.Columns.Add("Leave To", typeof(string));
            table.Columns.Add("Approved Leave", typeof(string));
            table.Columns.Add("Total Leave", typeof(string));
            table.Columns.Add("Total Approved Leave", typeof(string));
            table.Columns.Add("Leave Balance", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var dt2 = todate.ToString("yyyy-MM-dd");
            if (sid == 25000000)
            {
                var all_rows = db.Database.SqlQuery<VM_Report>("Select e.Section_Name as SectionName,e.CardTitle +' ' +e.EmployeeIdentity as CardNo,e.Name," +
                                                           "d.Name as Designation_Name,convert(varchar(10),e.Joining_date,126) as Joining_Date,lp.Leave_Name, convert(varchar(10),l.[From],126)as Leave_From,convert(varchar(10),l.[To],126) as Leave_To," +
                                                            "l.[Days] as Approved_Days,ls.Total as Total_Leave,ls.Taken as Total_Approved_Leave," +
                                                           "ls.Remaining as Leave_Balance from HRMS_Leave_Application l " +
                                                           "inner Join HRMS_Leave_Assign ls on ls.EmployeeId = l.EmployeeId " +
                                                           "And ls.LeaveTypeId = l.LeaveTypeId " +
                                                           "inner Join HRMS_Employee e on l.EmployeeId = e.ID " +
                                                           "inner Join HRMS_Leave_Type lp on l.LeaveTypeId = lp.ID " +
                                                           "inner Join Designations d on e.Designation_Id = d.ID " +
                                                           "where  l.Entry_Date between '" + dt1 + "' and '" + dt2 + "'" +
                                                           "group by l.EmployeeId, l.[From], l.[To], l.[Days], ls.Total, ls.Taken," +
                                                           "ls.Remaining, e.Section_Name, e.Name, e.CardTitle, e.EmployeeIdentity, e.Joining_date, lp.Leave_Name, d.Name ").ToList();

                for (int i = 0; i < all_rows.Count; i++)
                {
                    table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardNo, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Joining_Date, all_rows[i].Leave_Name, all_rows[i].Leave_From, all_rows[i].Leave_To
                        , all_rows[i].Approved_Days, all_rows[i].Total_Leave, all_rows[i].Total_Approved_Leave, all_rows[i].Leave_Balance);
                }
            }
            else
            {
                var all_rows = db.Database.SqlQuery<VM_Report>("Select e.Section_Name as SectionName,e.CardTitle +' ' +e.EmployeeIdentity as CardNo,e.Name," +
                                                           "d.Name as Designation_Name,convert(varchar(10),e.Joining_date,126) as Joining_Date,lp.Leave_Name, convert(varchar(10),l.[From],126)as Leave_From,convert(varchar(10),l.[To],126) as Leave_To," +
                                                            "l.[Days] as Approved_Days,ls.Total as Total_Leave,ls.Taken as Total_Approved_Leave," +
                                                           "ls.Remaining as Leave_Balance from HRMS_Leave_Application l " +
                                                           "inner Join HRMS_Leave_Assign ls on ls.EmployeeId = l.EmployeeId " +
                                                           "And ls.LeaveTypeId = l.LeaveTypeId " +
                                                           "inner Join HRMS_Employee e on l.EmployeeId = e.ID " +
                                                           "inner Join HRMS_Leave_Type lp on l.LeaveTypeId = lp.ID " +
                                                           "inner Join Designations d on e.Designation_Id = d.ID " +
                                                           "where  l.Entry_Date between '" + dt1 + "' and '" + dt2 + "' and e.Section_Id=" + sid + " " +
                                                           "group by l.EmployeeId, l.[From], l.[To], l.[Days], ls.Total, ls.Taken," +
                                                           "ls.Remaining, e.Section_Name, e.Name, e.CardTitle, e.EmployeeIdentity, e.Joining_date, lp.Leave_Name, d.Name ").ToList();

                for (int i = 0; i < all_rows.Count; i++)
                {
                    table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardNo, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Joining_Date, all_rows[i].Leave_Name, all_rows[i].Leave_From, all_rows[i].Leave_To
                        , all_rows[i].Approved_Days, all_rows[i].Total_Leave, all_rows[i].Total_Approved_Leave, all_rows[i].Leave_Balance);
                }

            }
            return table;
            

        }




        public DataTable MeternityMonthlyLeaveReprt(DateTime fromdate, DateTime todate)
        {
            DataTable table = new DataTable();
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Card No", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Joining Date", typeof(string));
            table.Columns.Add("Leave Name", typeof(string));
            table.Columns.Add("Leave From", typeof(string));
            table.Columns.Add("Leave To", typeof(string));
            table.Columns.Add("Approved Leave", typeof(string));
            table.Columns.Add("Drawn Salary", typeof(string));
            var dt1 = fromdate.ToString("yyyy-MM-dd");
            var dt2 = todate.ToString("yyyy-MM-dd");
           
                var all_rows = db.Database.SqlQuery<VM_Report>("Select e.Section_Name, e.CardTitle, e.EmployeeIdentity as CardNo, e.Name, d.Name, convert(varchar(10), e.Joining_date, 126), lp.Leave_Name," +
                                                               " convert(varchar(10), l.[From], 126) as Leave_From, convert(varchar(10), l.[To], 126) as Leave_To, l.[Days] as Approved_Days,'' as DrawnSalary from HRMS_Leave_Application l " +
                                                               "inner Join HRMS_Leave_Assign ls on ls.EmployeeId = l.EmployeeId " +
                                                               "And ls.LeaveTypeId = l.LeaveTypeId " +
                                                               "inner Join HRMS_Employee e on l.EmployeeId = e.ID " +
                                                               "inner Join HRMS_Leave_Type lp on l.LeaveTypeId = lp.ID " +
                                                               "inner Join Designations d on e.Designation_Id = d.ID " +
                                                               "where  l.Entry_Date between '"+dt1+"' and '"+dt2+ "' and l.LeaveTypeId=196  " +
                                                               "group by l.EmployeeId, l.[From], l.[To], l.[Days], e.Section_Name, " +
                                                               "e.Name, e.CardTitle, e.EmployeeIdentity, e.Joining_date, lp.Leave_Name, d.Name").ToList();

                for (int i = 0; i < all_rows.Count; i++)
                {
                    table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardTitle, all_rows[i].CardNo, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Joining_Date, all_rows[i].Leave_From, all_rows[i].Leave_To
                        , all_rows[i].Approved_Days, all_rows[i].DrawnSalary);
                }
           
            return table;


        }




        public DataTable ResignationEarnLeaveReprt(int eid)
        {
            DataTable table = new DataTable();
            table.Columns.Add("Section", typeof(string));
            table.Columns.Add("Card Title", typeof(string));
            table.Columns.Add("Card No", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Designation", typeof(string));
            table.Columns.Add("Joining Date", typeof(string));
            table.Columns.Add("Total Month", typeof(string));
            table.Columns.Add("Working Days", typeof(string));
            table.Columns.Add("F_Leave", typeof(string));
            table.Columns.Add("Holiday", typeof(string));
            table.Columns.Add("Leave", typeof(string));
            table.Columns.Add("Absent", typeof(string));
            table.Columns.Add("TotalDeductionDays", typeof(string));
            table.Columns.Add("TotalWorkingDays", typeof(string));
            table.Columns.Add("TotalEarnLeave", typeof(string));
            table.Columns.Add("EnjoyableLeave", typeof(string));
            table.Columns.Add("PayableEarnLeave", typeof(string));
            table.Columns.Add("Stamp", typeof(string));
            table.Columns.Add("OneDaySalary", typeof(string));
            table.Columns.Add("Present_Salary", typeof(string));
            table.Columns.Add("PayableEarnLeaveTK", typeof(string));
            table.Columns.Add("Signature", typeof(string));
            //SectionName, CardTitle, Name, Designation, Joining_Date, QuitDate,
            //Total_Month, Working_Days, F_Leave, Holiday, Leave,[Absent],TotalDeductionDays,
            //TotalWorkingDays,TotalEarnLeave,EnjoyableLeave,PayableEarnLeave,Stamp,OneDaySalary,PayableEarnLeaveTK,Signature,Present_Salary
            //var dt1 = fromdate.ToString("yyyy-MM-dd");
            //var dt2 = todate.ToString("yyyy-MM-dd");

            var all_rows = db.Database.SqlQuery<VM_Report>($@"Select SectionName, CardTitle,EmployeeIdentity as CardNo, Name, Designation, Joining_Date, QuitDate,
                                                        cast(Total_Month as int) as Total_Month,cast(Working_Days as int) as Working_Days, 
                                                        cast(F_Leave as int) as F_Leave,cast(Holiday as int) as Holiday, 
                                                        cast(Leave as decimal(18,2)) as Total_Leave,
                                                        cast(Absent as int) as [Absent],cast(TotalDeductionDays as int) as TotalDeductionDays, 
                                                        cast(TotalWorkingDays as int) as TotalWorkingDays, cast(sum(TotalWorkingDays / 18)as decimal(18,2)) as TotalEarnLeave, 
                                                        cast(0 as int) as EnjoyableLeave, cast(sum(TotalWorkingDays / 18) as decimal(18,2)) as PayableEarnLeave,
                                                        cast(Stamp as int)as Stamp,cast(Present_Salary as decimal(18,2))as Present_Salary,
                                                        cast(OneDaySalary as decimal(18,2))as OneDaySalary, 
                                                        cast(sum(TotalWorkingDays / 18 * OneDaySalary) as decimal(18,2)) as PayableEarnLeaveTK, '' As [Signature] 
                                                        from( Select SectionName, CardTitle,EmployeeIdentity, Name, Designation, Joining_Date, QuitDate, 
                                                        Total_Month, Working_Days, F_Leave, Holiday, Leave,[Absent], 
                                                        sum(F_Leave + Holiday + Leave + [Absent]) as TotalDeductionDays, 
                                                        sum(Working_Days - (F_Leave + Holiday + Leave + [Absent])) as TotalWorkingDays,Present_Salary, 
                                                        sum(Present_Salary / 30) as OneDaySalary, Stamp from 
                                                        (Select s.SectionName, e.CardTitle,e.EmployeeIdentity, e.Name, d.Name as Designation, 
                                                        convert(varchar(10), Joining_Date, 126) as Joining_Date,
                                                        Case when  convert(varchar(10), QuitDate, 126)='1900-01-01' then convert(varchar(10),getdate(),126) 
                                                        else convert(varchar(10), QuitDate, 126) end as  QuitDate,
                                                         ((SELECT DATEDIFF(mm, e.Joining_Date, Case when  convert(varchar(10), QuitDate, 126)='1900-01-01' then convert(varchar(10),getdate(),126) 
                                                        else convert(varchar(10), QuitDate, 126) end ) + 1 from HRMS_Employee ee where ee.ID = e.ID)) as Total_Month, 
                                                         ((SELECT DATEDIFF(dd, e.Joining_Date, Case when  convert(varchar(10), QuitDate, 126)='1900-01-01' then convert(varchar(10),getdate(),126) 
                                                        else convert(varchar(10), QuitDate, 126) end) + 1 from HRMS_Employee ee where ee.ID = e.ID)) as Working_Days, 
                                                         (Select count(1) from HRMS_Holiday hd, HRMS_Employee eee where hd.[Description] Like '%Festival%' and hd.OffDay Between eee.Joining_date and Case when  convert(varchar(10), eee.QuitDate, 126)='1900-01-01' then convert(varchar(10),getdate(),126) 
                                                        else convert(varchar(10), eee.QuitDate, 126) end and eee.ID= e.ID) as F_Leave, 
                                                         (Select count(1) from HRMS_Attendance_History att inner join HRMS_Employee ate on att.EmployeeId=ate.ID where att.AttendanceStatus=5 and att.[Date] Between ate.Joining_date and Case when  convert(varchar(10), ate.QuitDate, 126)='1900-01-01' then convert(varchar(10),getdate(),126) 
                                                        else convert(varchar(10), ate.QuitDate, 126) end and ate.ID=e.ID) as Holiday, 
                                                         (Select Isnull(sum(Taken),0) from HRMS_Leave_Assign l where  l.EmployeeId=e.ID) as Leave, 
                                                         (Select count(1) from HRMS_Attendance_History att inner join HRMS_Employee ate on att.EmployeeId=ate.ID where att.AttendanceStatus=3 and att.[Date] Between ate.Joining_date and Case when  convert(varchar(10), ate.QuitDate, 126)='1900-01-01' then convert(varchar(10),getdate(),126) 
                                                        else convert(varchar(10), ate.QuitDate, 126) end and ate.ID= e.ID) as [Absent], 
                                                         (Select isnull(sum(Basic+House+Health+Food+Transport),0) from HRMS_Salary s where  s.Employee_Id=e.ID) as Present_Salary,10 as Stamp 
                                                         from HRMS_Employee e join Sections s on e.Section_Id=s.Id inner join Designations d on e.Designation_Id=d.ID where e.ID="+eid+" )t " +
                                                           "group by SectionName, CardTitle,EmployeeIdentity, Name, Designation, Joining_Date, QuitDate, Total_Month, Working_Days, F_Leave, Holiday, Leave,[Absent], Present_Salary, Stamp ) tbl  " +
                                                           "group by SectionName, CardTitle,EmployeeIdentity, Name, Designation, Joining_Date, QuitDate, Total_Month, Working_Days, F_Leave, Holiday, Leave,[Absent], Present_Salary, Stamp, TotalDeductionDays, TotalWorkingDays, OneDaySalary ").ToList();
                //"Select SectionName, CardTitle,EmployeeIdentity as CardNo, Name, Designation, Joining_Date, QuitDate," +
                //                                           "cast(Total_Month as int) as Total_Month,cast(Working_Days as int) as Working_Days, cast(F_Leave as int) as F_Leave,cast(Holiday as int) as Holiday, cast(Leave as decimal(18,2)) as Total_Leave,cast(Absent as int) as [Absent],cast(TotalDeductionDays as int) as TotalDeductionDays," +
                //                                           " cast(TotalWorkingDays as int) as TotalWorkingDays, cast(sum(TotalWorkingDays / 18)as decimal(18,2)) as TotalEarnLeave, cast(0 as int) as EnjoyableLeave, cast(sum(TotalWorkingDays / 18) as decimal(18,2)) as PayableEarnLeave," +
                //                                           "cast(Stamp as int)as Stamp,cast(Present_Salary as decimal(18,2))as Present_Salary,cast(OneDaySalary as decimal(18,2))as OneDaySalary, cast(sum(TotalWorkingDays / 18 * OneDaySalary) as decimal(18,2)) as PayableEarnLeaveTK, '' As [Signature] " +
                //                                           "from( Select SectionName, CardTitle,EmployeeIdentity, Name, Designation, Joining_Date, QuitDate," +
                //                                           " Total_Month, Working_Days, F_Leave, Holiday, Leave,[Absent], " +
                //                                           "sum(F_Leave + Holiday + Leave + [Absent]) as TotalDeductionDays, sum(Working_Days - (F_Leave + Holiday + Leave + [Absent])) as TotalWorkingDays," +
                //                                           "Present_Salary, sum(Present_Salary / 30) as OneDaySalary, Stamp from " +
                //                                           "(Select s.SectionName, e.CardTitle,e.EmployeeIdentity, e.Name, d.Name as Designation, convert(varchar(10), Joining_Date, 126) as Joining_Date, " +
                //                                           " convert(varchar(10), QuitDate, 126) as QuitDate, " +
                //                                           "((SELECT DATEDIFF(mm, e.Joining_Date, e.QuitDate) + 1 from HRMS_Employee ee where ee.ID = e.ID)) as Total_Month, " +
                //                                           "((SELECT DATEDIFF(dd, e.Joining_Date, e.QuitDate) + 1 from HRMS_Employee ee where ee.ID = e.ID)) as Working_Days, " +
                //                                           "(Select count(1) from HRMS_Holiday hd, HRMS_Employee eee where hd.[Description] Like '%Festival%' and hd.OffDay Between eee.Joining_date and eee.QuitDate and eee.ID= e.ID) as F_Leave, " +
                //                                           "(Select count(1) from HRMS_Attendance_History att, HRMS_Employee ate where att.AttendanceStatus=5 and att.[Date] Between ate.Joining_date and ate.QuitDate and ate.ID= e.ID) as Holiday, " +
                //                                           "(Select Isnull(sum(Taken),0) from HRMS_Leave_Assign l where  l.EmployeeId=e.ID) as Leave, " +
                //                                           "(Select count(1) from HRMS_Attendance_History att, HRMS_Employee ate where att.AttendanceStatus=3 and att.[Date] Between ate.Joining_date and ate.QuitDate and ate.ID= e.ID) as [Absent], " +
                //                                           "(Select isnull(sum(Basic+House+Health+Food+Transport),0) from HRMS_Salary s where  s.Employee_Id=e.ID) as Present_Salary,10 as Stamp " +
                //                                           "from HRMS_Employee e join Sections s on e.Section_Id=s.Id " +
                //                                           "inner join Designations d on e.Designation_Id=d.ID " +
                //                                           "where e.ID="+eid+")t " +
                //                                           "group by SectionName, CardTitle,EmployeeIdentity, Name, Designation, Joining_Date," +
                //                                           " QuitDate, Total_Month, Working_Days, F_Leave, Holiday, Leave,[Absent], Present_Salary, Stamp ) tbl " +
                //                                           "group by SectionName, CardTitle,EmployeeIdentity, Name, Designation, Joining_Date, QuitDate, Total_Month," +
                //                                           " Working_Days, F_Leave, Holiday, Leave,[Absent], Present_Salary, Stamp, TotalDeductionDays, TotalWorkingDays, OneDaySalary ").ToList();

            for (int i = 0; i < all_rows.Count; i++)
            {
                table.Rows.Add(all_rows[i].SectionName, all_rows[i].CardTitle, all_rows[i].CardNo, all_rows[i].Name, all_rows[i].Designation_Name, all_rows[i].Joining_Date, 
                    all_rows[i].Total_Month, all_rows[i].Working_Days, all_rows[i].F_Leave, all_rows[i].Holiday, 
                    all_rows[i].Leave, all_rows[i].Absent, all_rows[i].TotalDeductionDays, all_rows[i].TotalWorkingDays, 
                    all_rows[i].EnjoyableLeave, all_rows[i].PayableEarnLeave, all_rows[i].Stamp, all_rows[i].Present_Salary, 
                    all_rows[i].OneDaySalary, all_rows[i].PayableEarnLeaveTK, all_rows[i].Signature);
            }
            return table;


        }

        #endregion



       
    }
}