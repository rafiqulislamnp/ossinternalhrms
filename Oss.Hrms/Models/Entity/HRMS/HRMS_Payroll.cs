﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.Entity.HRMS
{
    public class HRMS_Payroll:RootModel
    {
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }
        public string Note { get; set; }
        public bool ClosePayroll { get; set; }
    }
}