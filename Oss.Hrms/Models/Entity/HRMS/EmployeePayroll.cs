﻿using Oss.Hrms.Models.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.Entity.HRMS
{
    public class EmployeePayroll : RootModel
    {
        public int EmployeeFK { get; set; }
        public decimal GrossAmount { get; set; }
        public decimal BasicAmount { get; set; } = 0;
        public decimal OtherAmount { get; set; } = 0;
        public bool IsNew { get; set; } = true;
        public string Remarks { get; set; }
        public DateTime? DateOfPromotion { get; set; }
    }

    public class PayrollMaster : RootModel
    {
        public string PayrollTitle { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime PaymentDate { get; set; }
        public int PayrollStatus { get; set; }
        public string Remarks { get; set; }
        public int PayrollSettingFk { get; set; }
        [NotMapped]
        public List<int> DepartmentFk { get; set; }
        [NotMapped]
        public decimal BonusPercent { get; set; }
    }

    public class PayrollDepartment : RootModel
    {
        public int PayrollMasterFk { get; set; }
        public int DepartmentFk { get; set; }
    }

    public class PayrollDetails : RootModel
    {
        public int PayrollMasterFk { get; set; }
        public int EmployeeFK { get; set; }
        public int AbsentDays { get; set; }
        public decimal AbsentDeduction { get; set; }
        public int LateDays { get; set; }
        public decimal LateDeduction { get; set; }
        public int InformLateDays { get; set; }
        public decimal InformLateDeduction { get; set; }
        public decimal BonusPercent { get; set; }
        public decimal BonusAmount { get; set; }
        public int TotalDays { get; set; }
        public decimal TotalPayble { get; set; }
        public decimal TotalDeduction { get; set; }
        public decimal TotalAmountToBePaid { get; set; }
        public decimal OtherDeduction { get; set; }
        public decimal OtherAdjustment { get; set; }
        public decimal ExpenseAdjustment { get; set; }
        public string Remarks { get; set; }
    }
}