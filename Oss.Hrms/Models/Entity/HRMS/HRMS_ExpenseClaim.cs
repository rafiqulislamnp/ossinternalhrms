﻿using Oss.Hrms.Models.Services;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Oss.Hrms.Models.Entity.HRMS
{
    public class HRMS_ExpenseClaim: RootModel
    {
        
        public DateTime Date { get; set; }
        public int EmployeeFk { get; set; }
        [DisplayName("Claim Type")]
        public int ExpenseTypeId { get; set; }
        [DisplayName("Amount")]
        public decimal Expense { get; set; }
        public string Description { get; set; }
        public string Remarks { get; set; }
        public int ApprovalStatus { get; set; }
    }
}