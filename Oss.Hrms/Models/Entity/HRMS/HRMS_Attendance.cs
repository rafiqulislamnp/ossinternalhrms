﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.Entity.Common;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Services;

namespace Oss.Hrms.Models.Entity.HRMS
{
    public class HRMS_Attendance
    {

    }
    /// <summary>
    /// Employee's Daily Attendance, It keeps track of an employee's daily attendance
    /// </summary>
    public class HRMS_Daily_Attendance : RootModel
    {
        public int EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public HRMS_Employee HrmsEmployee { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [DataType(DataType.Time)]
        public DateTime InTime { get; set; }

        [DataType(DataType.Time)]
        public DateTime OutTime { get; set; }
        public int TotalTime { get; set; }
        public int? Late { get; set; }
        public int? Early { get; set; }
        public int? OverTime { get; set; }
        public int AttendanceStatus { get; set; }

        [StringLength(maximumLength: 50)]
        public string CardNo { get; set; }
        [StringLength(maximumLength: 100)]
        public string Remarks { get; set; }

        public int ShiftTypeId { get; set; }
        [ForeignKey("ShiftTypeId")]
        public HRMS_Shift_Type HrmsShiftType { get; set; }
        public int? HolidayType { get; set; }
        public int? LeavePaidType { get; set; }
        //[StringLength(maximumLength: 100)]
        // public string AbsentDaysInOutTime { get; set; }
        public int? IsFreeze { get; set; }
        public int? OverTimeDeduction { get; set; }
        public int? PayableOverTime { get; set; }
        public bool IsEdited { get; set; }
    }


    public class HRMS_Attendance_History : RootModel
    {
        public int EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public HRMS_Employee HrmsEmployee { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [DataType(DataType.Time)]
        public DateTime InTime { get; set; }

        [DataType(DataType.Time)]
        public DateTime OutTime { get; set; }
        public int TotalTime { get; set; }
        public int? Late { get; set; }
        public int? Early { get; set; }
        public int? OverTime { get; set; }
        public int AttendanceStatus { get; set; }

        [StringLength(maximumLength: 50)]
        public string CardNo { get; set; }
        [StringLength(maximumLength: 100)]
        public string Remarks { get; set; }

        public int ShiftTypeId { get; set; }
        [ForeignKey("ShiftTypeId")]
        public HRMS_Shift_Type HrmsShiftType { get; set; }
        public int? HolidayType { get; set; }
        public int? LeavePaidType { get; set; }
        //[StringLength(maximumLength: 100)]
        // public string AbsentDaysInOutTime { get; set; }
        public int? IsFreeze { get; set; }
        public int? OverTimeDeduction { get; set; }
        public int? PayableOverTime { get; set; }
    }

    public class HRMS_Daily_Instant_Attendance : RootModel
    {
        public int EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public HRMS_Employee HrmsEmployee { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [DataType(DataType.Time)]
        public DateTime InTime { get; set; }

        [DataType(DataType.Time)]
        public DateTime OutTime { get; set; }
        public int? TotalTime { get; set; }
        public int? Late { get; set; }
        public int? Early { get; set; }
        public int? OverTime { get; set; }
        public int AttendanceStatus { get; set; }

        [StringLength(maximumLength: 50)]
        public string CardNo { get; set; }
        [StringLength(maximumLength: 100)]
        public string Remarks { get; set; }

        public int ShiftTypeId { get; set; }
        [ForeignKey("ShiftTypeId")]
        public HRMS_Shift_Type HrmsShiftType { get; set; }
        public int? HolidayType { get; set; }
        public int? LeavePaidType { get; set; }
        //[StringLength(maximumLength: 100)]
        // public string AbsentDaysInOutTime { get; set; }
        public int? IsFreeze { get; set; }
    }


    /// <summary>
    /// this is used for assigning shift to employee. 
    /// </summary>
    public class HRMS_Shift_Type : RootModel
    {
        public int BU_Id { get; set; }     //Business Unit Id as Foreign Key
        [ForeignKey("BU_Id")]
        public BusinessUnit BusinessUnit { get; set; }
        [StringLength(50)]
        public string ShiftName { get; set; }
        [DataType(DataType.Time)]
        public DateTime StartTime { get; set; }
        [DataType(DataType.Time)]
        public DateTime EndTime { get; set; }
        [StringLength(50)]
        public string ShiftNameWithTime { get; set; }
        public int DeductionTime { get; set; }
        public int ToleranceTime { get; set; }
    }

    public class HRMS_Shift_Rules : RootModel
    {
        [StringLength(50)]
        public string RuleName { get; set; }

        public int Hour { get; set; }

        public int DeductionMinute { get; set; }

        public int ShiftTypeId { get; set; }

        [ForeignKey("ShiftTypeId")]
        public HRMS_Shift_Type HRMSShiftType { get; set; }

    }
    

    /// <summary>
    /// Assign Shift to Employee for taking daily attendance
    /// </summary>
    public class HRMS_Employee_Shift_Assign : RootModel
    {
        public int ShiftTypeId { get; set; }                //Shift Type Id as Foreign Key
        [ForeignKey("ShiftTypeId")]
        public HRMS_Shift_Type HRMSShiftType { get; set; }
        public int BU_Id { get; set; }
        [ForeignKey("BU_Id")]
        public BusinessUnit BusinessUnit { get; set; }

        public int EmployeeId { get; set; }            // Employee Id Foreign key

        [ForeignKey("EmployeeId")]
        public HRMS_Employee HrmsEmployee { get; set; }

        [DataType(DataType.Date)]
        public DateTime AffectDate { get; set; }

        //[DataType(DataType.Date)]
        //public DateTime EndDate { get; set; }

        [DataType(DataType.Time)]
        public DateTime InTime { get; set; }

        [DataType(DataType.Time)]
        public DateTime OutTime { get; set; }
        //  public int ActiveStatus { get; set; }

        public string identification { get; set; }


    }

    /// <summary>
    /// 
    /// </summary>
    public class HRMS_Employee_Shift_Assign_History : RootModel
    {
        public int ShiftTypeId { get; set; }            //Shift Type Id as Foreign Key
        [ForeignKey("ShiftTypeId")]
        public HRMS_Shift_Type HRMSShiftType { get; set; }
        public int BU_Id { get; set; }
        [ForeignKey("BU_Id")]
        public BusinessUnit BusinessUnit { get; set; }

        public int EmployeeId { get; set; }            // Employee Id as Foreign key

        [ForeignKey("EmployeeId")]
        public HRMS_Employee HrmsEmployee { get; set; }

        [DataType(DataType.Date)]
        public DateTime AffectDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }

        [DataType(DataType.Time)]
        public DateTime InTime { get; set; }

        [DataType(DataType.Time)]
        public DateTime OutTime { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string ActiveStatus { get; set; }
    }

    

    public class HRMS_Attendance_Remarks : RootModel
    {
        [StringLength(50)]
        public string Remarks { get; set; }
    }
}