﻿using Oss.Hrms.Models.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.Entity.HRMS
{
    public class HRMS_BonusInfo : RootModel
    {
        public int BonusIdFK { get; set; }
        [ForeignKey("BonusIdFK")]
        public HRMS_Bonus HrmsBonus { get; set; }
        public int EmployeeIdFK { get; set; }
        [ForeignKey("EmployeeIdFK")]
        public HRMS_Employee HrmsEmployee { get; set; }
        public int? EodReferenceIdFK { get; set; }
        [ForeignKey("EodReferenceIdFK")]
        public HRMS_EodReference HrmsEodReference { get; set; }
        public decimal BasicAmount { get; set; }
        public decimal GrossAmount { get; set; }
        public decimal BonusRate { get; set; }
        public decimal BonusAmount { get; set; }
        public decimal Stamp { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }
        public string Note { get; set; }
    }
}