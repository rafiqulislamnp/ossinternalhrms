﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.Services;
using System.ComponentModel;

namespace Oss.Hrms.Models.Entity.HRMS
{
    public class HRMS_PayrollInfo:RootModel
    {
        public int PayrollFK { get; set; }
        [ForeignKey("PayrollFK")]
        public HRMS_Payroll HrmsPayroll { get; set; }
        public int EmployeeIdFk { get; set; }
        [ForeignKey("EmployeeIdFk")]
        public HRMS_Employee HrmsEmployee { get; set; }
        public int? Eod_ReferenceFk { get; set; }
        [ForeignKey("Eod_ReferenceFk")]
        public HRMS_EodReference HrmsEodReference { get; set; }
        public decimal Amount { get; set; }

        [Column(TypeName = "NVARCHAR")]
        [StringLength(100)]
        public string Note { get; set; }

        [DefaultValue(false)]
        public bool AlertActive { get; set; }
        [DataType(DataType.Date)]
        public DateTime PaymentDate { get; set; }
     
    }
}