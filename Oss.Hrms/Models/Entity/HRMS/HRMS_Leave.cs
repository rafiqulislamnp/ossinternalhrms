﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oss.Hrms.Models.Services;
using Oss.Hrms.Models.Entity.HRMS;
using System.Web.Mvc;

namespace Oss.Hrms.Models.Entity.HRMS
{
    public class HRMS_Leave_Assign : RootModel
    {
        public int EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public HRMS_Employee HrmsEmployee { get; set; }
       
        public int LeaveTypeId { get; set; }
        [ForeignKey("LeaveTypeId")]
        public HRMS_Leave_Type HrmsLeaveType { get; set; }

        [StringLength(10)]
        public string Year { get; set; }
       
        public decimal Total { get; set; }
        public decimal Taken { get; set; }
        public decimal Remaining { get; set; }
    }

    public class HRMS_Leave_Repository :RootModel
    {
        public int EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public HRMS_Employee HrmsEmployee { get; set; }

        public int LeaveAppId { get; set; }
        [ForeignKey("LeaveAppId")]
        public HRMS_Leave_Application HrmsLeaveApplication { get; set; }

        public int LeaveTypeId { get; set; }
        [ForeignKey("LeaveTypeId")]
        public HRMS_Leave_Type HrmsLeaveType { get; set; }

        public string LeaveName { get; set; }

        public DateTime Date { get; set; }
        public int LeavePaidType { get; set; }

    }

    /// <summary>
    /// Employee Leave Application. Keeps track of employee's leave details..... 
    /// </summary>
    public class HRMS_Leave_Application : RootModel
    {
        [DisplayName("Employee ID")]
        public int EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public HRMS_Employee HrmsEmployee { get; set; }

        [DisplayName("Leave Type")]
        public int  LeaveTypeId { get; set; }
        [ForeignKey("LeaveTypeId")]
        public HRMS_Leave_Type HrmsLeaveType { get; set; }

        [DisplayName("From")]
        public DateTime From { get; set; }

        [DisplayName("To")]
        public DateTime To { get; set; }

        [DisplayName("Total Days")]
        public decimal Days { get; set; }
        //[DisplayName("Project Name")]
        //[StringLength(50)]
        //public string ProjectName { get; set; }

        //[StringLength(50)]
        //public string WorkStationName { get; set; }
        [DisplayName("Leave Purpose")]
        [StringLength(50)]
        public string Purpose { get; set; }
        [DisplayName("Stay During Leave")]
        [StringLength(50)]
        public string StayDuringLeave { get; set; }

        //[DisplayName("Mobile No")]
        //[StringLength(50)]
        //public string MobileNo { get; set; }

        //[StringLength(50)]
        //public string AltMobileNo { get; set; }
        [DisplayName("Comment")]
        [StringLength(50)]
        public string Comment { get; set; }

        [DisplayName("Joining Work After Leave")]
        public DateTime JoiningWorkAfterLeave { get; set; }

        [DisplayName("Status")]
        //[StringLength(50)]
        public int LeaveStatus { get; set; }

        //[StringLength(50)]
        //public string During_LeaveWhoTakeOver { get; set; }

        //[StringLength(50)]
        //public string Latest_Period_of_leave_Enjoyed { get; set; }
        [DisplayName("Paid Type")]
        public int LeavePaidType { get; set; }

        //[StringLength(50)]
        //public string Alt_Leave_Dates { get; set; }

        //[StringLength(50)]
        //public string Acknowledgement_Status { get; set; }
        [DisplayName("HR Department")]
        [StringLength(100)]
        public string HRDepartment { get; set; }
        //[DataType(DataType.DateTime)]
        //public DateTime HRAcknowledgeDate { get; set; }

        [DisplayName("Recommended By")]
        [StringLength(50)]
        public string Recommended { get; set; }

        [DisplayName("Section Incharge")]
        [StringLength(50)]
        public string SectionIncharge { get; set; }

        [DisplayName("A.P.M/P.M")]
        [StringLength(50)]
        public string APMorPM { get; set; }

        [DisplayName("Approved By")]
        [StringLength(50)]
        public string ApprovedBy { get; set; }

        [DisplayName("FM")]
        [StringLength(50)]
        public string FM { get; set; }

        public int SupervisorApprovalStatus { get; set; }

        public DateTime ApplicationSubmitDate { get; set; }
    }

    /// <summary>
    /// leave application details......
    ///// </summary>
    //public class HRMS_Leave_Application_Details : RootModel
    //{
    //    public int Leave_ApplicationId { get; set; }
    //    [ForeignKey("Leave_ApplicationId")]
    //    public HRMS_Leave_Application HrmsLeaveApplication { get; set; }

    //    [DataType(DataType.Date)]
    //    public DateTime Start_Date { get; set; }

    //    [DataType(DataType.Date)]
    //    public DateTime End_Date { get; set; }
    //    public int TotalDays { get; set; }
    //    [StringLength(200)]
    //    public string Comments { get; set; }
    //    public int LeaveStatus { get; set; }  //--In-View Dropdown----
    //}

    /// <summary>
    /// Leave Type
    /// </summary>
    public class HRMS_Leave_Type  : RootModel
    {
        [Index("UX_LeaveType", 1, IsUnique = true)]
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string Leave_Name { get; set; }
        public int Leave_Amount { get; set; }
    }
   
    
    /// <summary>
    /// Employee Offdays.....
    /// </summary>
    public class HRMS_Employee_OffDay :RootModel
    {
        public int EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public HRMS_Employee HrmsEmployee { get; set; }
        [StringLength(50)]
        public string OffDay { get; set; }
    }

    /// <summary>
    ///Government declared holidays....
    /// </summary>
    public class HRMS_Holiday : RootModel
    {
       // public int EmployeeId { get; set; }
        //[ForeignKey("EmployeeId")]
        //public HRMS_Employee HrmsEmployee { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime OffDay { get; set; }

        [StringLength(200)]
        public string Description { get; set; }
    }

    public class HRMS_Application:RootModel
    {

        public DateTime Date { get; set; }
        [Required]
        public string Subject { get; set; }
        [Required]
        [AllowHtml]
        public string Details { get; set; }
        public int EmployeeFk { get; set; }
    }
}