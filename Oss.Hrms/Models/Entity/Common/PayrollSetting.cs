﻿using Oss.Hrms.Models.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.Entity.Common
{

    public class PayrollSettingMaster : RootModel
    {
        public string Reference { get; set; }
        public string Remarks { get; set; }
    }

    public class PayrollSetting:RootModel
    {
        public int PayrollSettingMasterFk { get; set; }
        public decimal Basic { get; set; } //
        public decimal HouseRent { get; set; }
        public decimal Medical { get; set; }
        public decimal Conveyance { get; set; }
        public decimal ProvidentFund { get; set; }
        public decimal Others { get; set; }
    }
}