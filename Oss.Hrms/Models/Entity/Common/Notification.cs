﻿using Oss.Hrms.Models.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Oss.Hrms.Models.Entity.Common
{
    public class Notification : RootModel
    {
        public DateTime Date { get; set; }
        [Required]
        public string HeadLine { get; set; }
        [Required]
        [AllowHtml]
        public string Details { get; set; }
    }
}