﻿using Oss.Hrms.Models.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.Entity.User
{
    public class User_Role : RootModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("Role")]
        [Required(ErrorMessage = "Role Name is required")]
        [StringLength(50, ErrorMessage = "Role Name upto 50 Chracter")]
        public string Name { get; set; }

        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }



        //...........................New Added Code...................................//

        //public List<User_RoleMenuItem> User_RoleMenuItems { get; set; }
  
       // public List<User_User> User_Users { get; set; }
    }
}