﻿using Oss.Hrms.Models.Services;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.Entity.User
{
    public class UserAssignMenu: RootModel
    {
        public int HRMS_EmployeeFK { get; set; }
        public int User_RoleFK { get; set; }
        public int User_RoleMenuItemFK { get; set; }
    }
   
}