﻿using Oss.Hrms.Models.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.Entity.User
{
    public class User_Department : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("Department")]
     
        [StringLength(100, ErrorMessage = "Department Name upto 100 Chracter")]
        public string Name { get; set; }
       
        [DefaultValue(false)]
        public bool IsStore { get; set; }


        //...........................New Added Code...................................//
        //public List<User_User> User_Users { get; set; }
    }
}