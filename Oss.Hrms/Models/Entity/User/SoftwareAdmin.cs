﻿using Oss.Hrms;
using Oss.Hrms.Models.ViewModels.User;

using System;
using System.Runtime.InteropServices.ComTypes;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
namespace Oss.Hrms.Models.Entity.User
{
    public class SoftwareAdmin : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            

            if (MvcApplication.IsReleased)
            {
                var actionNameTemp = filterContext.ActionDescriptor.ActionName;
                var iD = filterContext.RouteData.Values["id"];
                string param = "";
                if (iD != null)
                {
                    actionNameTemp+= "/"+ iD.ToString();

                }

                VmUser_User U = (VmUser_User)HttpContext.Current.Session["One"];

                if (U != null)
                {
                    string actionName = filterContext.HttpContext.Request.FilePath;
                    actionName = actionName.Replace("/Romo/" + filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToLower() + "/", "");
                    bool flg = false;
                    if (U.CheckPermission(filterContext.ActionDescriptor.ControllerDescriptor.ControllerName, actionNameTemp))
                    {
                        flg = true;
                    }
                    else if (U.CheckPermission(filterContext.ActionDescriptor.ControllerDescriptor.ControllerName, filterContext.ActionDescriptor.ActionName))
                    {
                        flg = true;
                    }

                    if (U != null && !flg)
                    {
                        if (!actionName.Contains("/Romo/"))
                        {
                            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "action", "UnAuthorized" }, { "controller", "Home" } });
                        }
                        else
                        {
                            if (!U.CheckPermission(filterContext.ActionDescriptor.ControllerDescriptor.ControllerName, actionName))
                            {
                                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "action", "UnAuthorized" }, { "controller", "Home" } });
                            }
                        }


                    }

                   }
                else
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "action", "Login" }, { "controller", "Home" } });

                }

            }
        }
    }
}
