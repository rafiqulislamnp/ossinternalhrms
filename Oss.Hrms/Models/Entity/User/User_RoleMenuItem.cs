﻿using Oss.Hrms.Models.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.Entity.User
{
    public class User_RoleMenuItem : BaseModel
    {
        [DefaultValue(0)]
        public int? FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int? LastEditeddBy { get; set; }
        public int User_RoleFK { get; set; }
        public int User_MenuItemFk { get; set; }
        public bool IsAllowed { get; set; }



        //...........................New Added Code...................................//

        //public User_Role User_Role { get; set; }
        //public User_MenuItem User_MenuItem { get; set; }
    }
}