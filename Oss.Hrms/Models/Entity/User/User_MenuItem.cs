﻿using Oss.Hrms.Models.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.Entity.User
{
    public class User_MenuItem : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("Link Name")]
        [Required(ErrorMessage = "Link Name is required")]
        [StringLength(30, ErrorMessage = "Link Name upto 30 Chracter")]
        public string Name { get; set; }

        [DefaultValue(0)]
        //[Index("IX_Priority", 1, IsUnique = true)]

        public int? Priority { get; set; }
        [DisplayName("Menu")]
        [Required(ErrorMessage = "Menu is required")]
        public int? User_MenuFk { get; set; }
        [DisplayName("Submenu")]
        [Required(ErrorMessage = "Submenu is required")]
        public int? User_SubMenuFk { get; set; }

        [DisplayName("Method")]
        [Required(ErrorMessage = "Name is required")]
        [StringLength(100, ErrorMessage = "Method upto 100 Chracter")]
        public string Method { get; set; }
        [MaxLength(200)]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        [DefaultValue(false)]
        public bool IsAlone { get; set; }



        //...........................New Added Code...................................//

        //public User_Menu User_Menu { get; set; }
        //public User_SubMenu User_SubMenu { get; set; }

        //public List<User_RoleMenuItem> User_RoleMenuItems { get; set; }
    }
}