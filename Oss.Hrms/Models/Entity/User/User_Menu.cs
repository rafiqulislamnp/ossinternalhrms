﻿using Oss.Hrms.Models.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.Entity.User
{
    public class User_Menu : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("Menu")]
        [Required(ErrorMessage = "Menu Name is required")]
        [StringLength(30, ErrorMessage = "Menu Name upto 30 Chracter")]
        public string Name { get; set; }

        //...........................New Added Code...................................//

        //public List<User_MenuItem> User_MenuItems { get; set; }
     
        //public List<User_Menu> User_Menus { get; set; }
    }
}