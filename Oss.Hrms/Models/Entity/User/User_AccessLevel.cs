﻿using Oss.Hrms.Models.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.CoreProcess.Data;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.Entity.User
{
    public class User_AccessLevel : BaseModel
    {
        [DefaultValue(0)]
        public int FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int LastEditeddBy { get; set; }
        [DisplayName("Access Level")]
        public string Name { get; set; }

        public int Level { get; set; }

        public string Description { get; set; }


        //...........................New Added Code...................................//

        //public List<User_User> User_Users { get; set; }
    }
}