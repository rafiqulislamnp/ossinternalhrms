﻿using System.Web.Mail;

namespace Oss.Hrms.Models.Services
{
    public  class MailMessage
    {
        public void SendEmail(string to, string subject, string body)
        {
            System.Web.Mail.MailMessage msg = new System.Web.Mail.MailMessage();
            msg.Body = body;

            string smtpServer = "mail.oss.com.bd";
            string userName = "kabir.fahad@oss.com.bd";
            string password = "1qa2ws3ed";
            int cdoBasic = 1;
            int cdoSendUsingPort = 2;
            if (userName.Length > 0)
            {
                msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserver", smtpServer);
                msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", 25);
                msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", cdoSendUsingPort);
                msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", cdoBasic);
                msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", userName);
                msg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", password);
            }
            msg.To = to;
            msg.From = "kabir.fahad@oss.com.bd";
            msg.Subject = subject;
            msg.BodyEncoding = System.Text.Encoding.UTF8;
            msg.BodyFormat = MailFormat.Html;
            SmtpMail.SmtpServer = smtpServer;
            SmtpMail.Send(msg);
        }
    }
}