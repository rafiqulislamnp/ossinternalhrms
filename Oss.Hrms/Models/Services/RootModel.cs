﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.Services
{
    public class RootModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [DefaultValue(true)]
        public bool Status { get; set; }
        [DataType(DataType.Date)]
        public DateTime Entry_Date { get; set; } = DateTime.Now;
        [DataType(DataType.Date)]
        public DateTime Update_Date { get; set; } = DateTime.Now;
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string Entry_By { get; set; }
        [Column(TypeName = "NVARCHAR")]
        [StringLength(50)]
        public string Update_By { get; set; }



        public RootModel()
        {
            Status = true;
        }
    }
}

