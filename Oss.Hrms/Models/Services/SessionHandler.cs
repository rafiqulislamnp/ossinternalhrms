﻿using Oss.Hrms.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oss.Hrms.Models.Services
{
    public class SessionHandler
    {
        public CurrentUser CurrentUser { get; set; }
        public void Adjust()
        {
            if (System.Web.HttpContext.Current.Session["User"] == null)
            {               
                var xxx = System.Web.HttpContext.Current.Request.Cookies["UserLogin"];
                if (System.Web.HttpContext.Current.Request.Cookies["UserLogin"] != null && System.Web.HttpContext.Current.Request.Cookies["UserLogin"].Value != null)
                {
                    var s = System.Web.HttpContext.Current.Request.Cookies["UserLogin"];
                    VM_User vM_User = new VM_User();

                    vM_User.ID = Convert.ToInt32(s.Values["ID"]);
                    vM_User.UserName = s.Values["Name"];
                    vM_User.Password = s.Values["Password"];
                    vM_User.UserRoleFk =Convert.ToInt32(s.Values["UserRole"]);
                    vM_User.IsSupervisor = Convert.ToBoolean(s.Values["IsSupervisor"]);
                    int theId = 0;
                    //theId = vM_User.UserRoleFk;
                    if (theId == 0)
                    { 
                        CurrentUser cu = new CurrentUser { ID = vM_User.ID, UserName = vM_User.UserName,UserRole= vM_User.UserRoleFk,IsSupervisor=vM_User.IsSupervisor };
                        System.Web.HttpContext.Current.Session["User"] = cu;
                        HttpCookie cookie = new HttpCookie("UserLogin");
                        cookie.Values.Add("ID", cu.ID.ToString());
                        cookie.Values.Add("Name", cu.UserName);
                        cookie.Values.Add("Password", cu.Password);
                        cookie.Values.Add("UserRole", cu.UserRole.ToString());
                        cookie.Values.Add("IsSupervisor", cu.IsSupervisor.ToString());
                        cookie.Expires.AddMinutes(300);
                        this.CurrentUser = cu;
                    }
                }

            }
            else
            {
                this.CurrentUser = (CurrentUser)System.Web.HttpContext.Current.Session["User"];
            }

        }

    }
}