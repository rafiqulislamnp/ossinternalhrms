﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Oss.Hrms.Models.Entity;
using Oss.Hrms.Models.Entity.Common;
using Oss.Hrms.Models.Entity.HRMS;
using Oss.Hrms.Models.Entity.User;
using Oss.Hrms.Models.ViewModels.HRMS;
using Oss.Hrms.Models.Entity.User;

namespace Oss.Hrms.Models
{
    public class HrmsContext : DbContext
    {
        public HrmsContext() : base("name=Oss.Hrms")
        {
            // this.SetCommandTimeOut(300);
        }
        //public void SetCommandTimeOut(int Timeout)
        //{
        //    var objectContext = (this as IObjectContextAdapter).ObjectContext;
        //    objectContext.CommandTimeout = Timeout;
        //}

        // public static object ContextHelper { get; private set; }

        #region report dbset

        //public DbSet<Reports> Reports { get; set; }

        #endregion

        #region  User
        public DbSet<User> Users { get; set; }

        public DbSet<CheckInOut> CheckInOut { get; set; }

        public DbSet<User_User> User_User { get; set; }
        public DbSet<User_Role> User_Role { get; set; }

        public DbSet<User_RoleMenuItem> User_RoleMenuItem { get; set; }

        public DbSet<User_SubMenu> User_SubMenu { get; set; }
        public DbSet<User_Team> User_Team { get; set; }
        public DbSet<User_MenuItem> User_MenuItem { get; set; }
        public DbSet<User_Department> User_Department { get; set; }
        public DbSet<User_AccessLevel> User_AccessLevel { get; set; }
        public DbSet<User_AccessMangment> User_AccessMangment { get; set; }
        public DbSet<User_Menu> User_Menu { get; set; }

        public DbSet<UserAssignMenu> UserAssignMenu { get; set; }
        //  User

        #endregion

        #region Common Setting

        public DbSet<LineNo> Lines { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<BusinessUnit> BusinessUnits { get; set; }
        public DbSet<Designation> Designations { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Section> Sections { get; set; }
        public DbSet<NotificationUser> NotificationUsers { get; set; }
        public DbSet<SalaryGrade> SalaryGrade { get; set; }
        public DbSet<District> District { get; set; }
        public DbSet<PoliceStation> PoliceStation { get; set; }
        public DbSet<PostOffice> PostOffice { get; set; }
        public DbSet<Village> Village { get; set; }
        public DbSet<Notification> Notification { get; set; }
        #endregion


        #region HRMS Context

        //Employee-----
        //   public DbSet<User> Users { get; set; }
        public DbSet<HRMS_Employee> Employees { get; set; }
        public DbSet<HRMS_Spouse> HrmsSpouses { get; set; }
        public DbSet<HRMS_Child> HrmsChildren { get; set; }
        public DbSet<HRMS_Education> HrmsEducations { get; set; }
        public DbSet<HRMS_Skill> HrmsSkills { get; set; }
        public DbSet<HRMS_Training> HrmsTrainings { get; set; }
        public DbSet<HRMS_Reference> HrmsReferences { get; set; }
        public DbSet<HRMS_Salary> HrmsSalaries { get; set; }
        public DbSet<HRMS_Salary_History> HrmsSalaryHistories { get; set; }
        public DbSet<HRMS_Experience> HrmsExperiences { get; set; }
        public DbSet<HRMS_Facility> HrmsFacilities { get; set; }
        public DbSet<HRMS_Facility_History> HrmsFacilityHistories { get; set; }
        public DbSet<HRMS_Hobby> HrmsHobbies { get; set; }
        public DbSet<HRMS_Upload_Document> HrmsUploadDocument { get; set; }
        public DbSet<HRMS_CardTitle> HrmsCardTitle { get; set; }

        //Link 
        public DbSet<HRMS_LineManagerToEmployeeLink> ManagerToEmployeeLinks { get; set; }
        public DbSet<HRMS_AdminToEmployeeLink> AdminToEmployeeLinks { get; set; }
        public DbSet<HRMS_DepartmentHeadToEmployeeLink> HeadToEmployeeLinks { get; set; }
        public DbSet<HRMS_LinkSuperior> HrmsLinkSuperior { get; set; }
        public DbSet<HRMS_ExpenseClaim> ExpenseClaim { get; set; }
        #endregion

        #region Promotion
        public DbSet<HRMS_EmploymentHistory> HrmsEmploymentHistory { get; set; }
        #endregion

        #region Attendance

        public DbSet<HRMS_Daily_Attendance> HrmsDailyAttendances { get; set; }
        public DbSet<HRMS_Attendance_History> HrmsAttendanceHistory { get; set; }
        public DbSet<HRMS_Daily_Instant_Attendance> HrmsDailyInstantAttendances { get; set; }
        public DbSet<HRMS_Shift_Type> HrmsShiftTypes { get; set; }
        public DbSet<HRMS_Employee_Shift_Assign> HrmsEmployeeShiftAssigns { get; set; }
        public DbSet<HRMS_Attendance_Remarks> HrmsAttendanceRemarks { get; set; }
        public DbSet<HRMS_Shift_Rules> HrmsShiftRules { get; set; }

        #endregion

        #region Leave 

        //Leave and Holiday
        public DbSet<HRMS_Leave_Assign> HrmsLeaveAssigns { get; set; }
        public DbSet<HRMS_Leave_Repository> HrmsLeaveRepositories { get; set; }
        public DbSet<HRMS_Leave_Application> HrmsLeaveApplications { get; set; }
        //public DbSet<HRMS_Leave_Application_Details> HrmsLeaveApplicationDetails { get; set; }
        public DbSet<HRMS_Leave_Type> HrmsLeaveTypes { get; set; }
        public DbSet<HRMS_Employee_OffDay> HrmsEmployeeOffDays { get; set; }
        public DbSet<HRMS_Holiday> HrmsHolidays { get; set; }
        public DbSet<HRMS_Application> HRMS_Application { get; set; }

        #endregion

        #region Payroll 

        public DbSet<HRMS_EodReference> HrmsEodReferences { get; set; }
        public DbSet<HRMS_EodRecord> HrmsEodRecords { get; set; }
        public DbSet<HRMS_EodCurrentInfo> HrmsEodCurrentInfos { get; set; }
        public DbSet<HRMS_Payroll> HrmsPayrolls { get; set; }
        public DbSet<HRMS_PayrollInfo> HrmsPayrollInfos { get; set; }
        public DbSet<HRMS_Bonus> HrmsBonuses { get; set; }
        public DbSet<HRMS_BonusInfo> HrmsBonusInfos { get; set; }

        //OSS Payroll

        public DbSet<EmployeePayroll> EmployeePayroll { get; set; }
        public DbSet<PayrollMaster> PayrollMaster { get; set; }
        public DbSet<PayrollDepartment> PayrollDepartment { get; set; }
        public DbSet<PayrollDetails> PayrollDetails { get; set; }
        public DbSet<PayrollSetting> PayrollSetting { get; set; }
        public DbSet<PayrollSettingMaster> PayrollSettingMaster { get; set; }
        
        #endregion

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            SetDecimalPrecisions(modelBuilder);
            SetForeignKeys(modelBuilder);
            //modelBuilder.Entity<HRMS_Employee_Shift_Assign>().MapToStoredProcedures();
            //modelBuilder.Entity<HRMS_Leave_Assign>()
            //    .Property(p => p.Remaining)
            //    .HasComputedColumnSql("[Total]-[Taken]");

            base.OnModelCreating(modelBuilder);

        }

        private void SetDecimalPrecisions(DbModelBuilder modelBuilder)
        {
            //  modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<HRMS_Leave_Assign>().Property(x => x.Total).HasPrecision(18, 2);
            modelBuilder.Entity<HRMS_Leave_Assign>().Property(x => x.Taken).HasPrecision(18, 2);
            modelBuilder.Entity<HRMS_Leave_Assign>().Property(x => x.Remaining).HasPrecision(18, 2);
        }

        private void SetForeignKeys(DbModelBuilder modelBuilder)
        {
            modelBuilder.Properties<DateTime>()
                .Configure(c => c.HasColumnType("datetime2"));


            #region Common Setting

            modelBuilder.Entity<BusinessUnit>()
                .HasRequired<Country>(x => x.Country)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Unit>()
                .HasRequired<BusinessUnit>(x => x.BusinessUnit)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Department>()
                .HasRequired<Unit>(x => x.Unit)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Section>()
                .HasRequired<Department>(x => x.Department)
                .WithMany()
                .WillCascadeOnDelete(false);
            #endregion

            #region HRMS Employee Record


            modelBuilder.Entity<HRMS_Employee>()
                .HasRequired<Country>(x => x.Country)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Employee>()
                .HasRequired<BusinessUnit>(x => x.BusinessUnit)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Employee>()
                .HasRequired<Unit>(x => x.Unit)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Employee>()
                .HasRequired<Department>(x => x.Department)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Employee>()
                .HasRequired<Section>(x => x.Section)
                .WithMany()
                .WillCascadeOnDelete(false);

            //foreign key from LineNo domain model
            modelBuilder.Entity<HRMS_Employee>()
                .HasRequired<LineNo>(x => x.LineNo)
                .WithMany()
                .WillCascadeOnDelete(false);



            modelBuilder.Entity<HRMS_Employee>()
                .HasOptional(m => m.PresentDistrict)
                .WithMany(t => t.PresentDistrict)
                .HasForeignKey(m => m.PresentDistrict_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Employee>()
                .HasOptional(m => m.PermanentDistrict)
                .WithMany(t => t.PermanentDistrict)
                .HasForeignKey(m => m.PermanentDistrict_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Employee>()
                .HasOptional(m => m.PresentPoliceStation)
                .WithMany(t => t.PresentPoliceStation)
                .HasForeignKey(m => m.PresentPoliceStation_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Employee>()
                .HasOptional(m => m.PermanentPoliceStation)
                .WithMany(t => t.PermanentPoliceStation)
                .HasForeignKey(m => m.PermanentPoliceStation_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Employee>()
                .HasOptional(m => m.PresentPostOffice)
                .WithMany(t => t.PresentPostOffice)
                .HasForeignKey(m => m.PresentPostOffice_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Employee>()
                .HasOptional(m => m.PermanentPostOffice)
                .WithMany(t => t.PermanentPostOffice)
                .HasForeignKey(m => m.PermanentPostOffice_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Employee>()
                .HasOptional(m => m.PresentVillage)
                .WithMany(t => t.PresentVillage)
                .HasForeignKey(m => m.PresentVillage_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Employee>()
                .HasOptional(m => m.PermanentVillage)
                .WithMany(t => t.PermanentVillage)
                .HasForeignKey(m => m.PermanentVillage_ID)
                .WillCascadeOnDelete(false);
            //modelBuilder.Entity<HRMS_Employee>()
            //    .HasRequired<District>(x => x.PresentDistrict)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<HRMS_Employee>()
            //    .HasRequired<District>(x => x.PermanentDistrict)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Employee>()
               .HasRequired<Designation>(x => x.Designation)
               .WithMany()
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Spouse>()
              .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
              .WithMany()
              .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Child>()
              .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
              .WithMany()
              .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Education>()
              .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
              .WithMany()
              .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Experience>()
              .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
              .WithMany()
              .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Training>()
              .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
              .WithMany()
              .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Skill>()
              .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
              .WithMany()
              .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Reference>()
              .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
              .WithMany()
              .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Salary>()
             .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
             .WithMany()
             .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Facility>()
             .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
             .WithMany()
             .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Hobby>()
             .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
             .WithMany()
             .WillCascadeOnDelete(false);

            #endregion

            #region HRMS EmploymentHistory

            modelBuilder.Entity<HRMS_EmploymentHistory>()
                .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_EmploymentHistory>()
                .HasRequired<Department>(x => x.Department)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_EmploymentHistory>()
                .HasRequired<Section>(x => x.Section)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_EmploymentHistory>()
              .HasRequired<Designation>(x => x.Designation)
              .WithMany()
              .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_EmploymentHistory>()
                .HasRequired<LineNo>(x => x.LineNo)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_EmploymentHistory>()
                 .HasRequired<BusinessUnit>(x => x.BusinessUnit)
                 .WithMany()
                 .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_EmploymentHistory>()
                .HasRequired<Unit>(x => x.Unit)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_EmploymentHistory>()
                .HasOptional<HRMS_EodRecord>(x => x.EodRecord)
                .WithMany()
                .WillCascadeOnDelete(false);

            #endregion

            #region Attendance

            modelBuilder.Entity<HRMS_Employee_Shift_Assign>()
                .HasRequired<HRMS_Shift_Type>(x => x.HRMSShiftType)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Employee_Shift_Assign>()
                .HasRequired<BusinessUnit>(x => x.BusinessUnit)
                .WithMany()
                .WillCascadeOnDelete(false);

            //Relationship between HrmsEmployee to HrmsEmployeeShiftAssign

            modelBuilder.Entity<HRMS_Employee_Shift_Assign>()
                .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Daily_Attendance>()
                .HasRequired<HRMS_Shift_Type>(x => x.HrmsShiftType)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Daily_Instant_Attendance>()
                .HasRequired<HRMS_Shift_Type>(x => x.HrmsShiftType)
                .WithMany()
                .WillCascadeOnDelete(false);

            #endregion

            #region Payroll Processing

            modelBuilder.Entity<HRMS_EodRecord>()
                .HasRequired<HRMS_EodReference>(x => x.HrmsEodReference)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_EodRecord>()
                .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_PayrollInfo>()
                .HasRequired<HRMS_Payroll>(x => x.HrmsPayroll)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_PayrollInfo>()
                .HasOptional<HRMS_EodReference>(x => x.HrmsEodReference)
                .WithMany()
                .WillCascadeOnDelete(false);

            //modelBuilder.Entity<HRMS_PayrollInfo>()
            //    .HasRequired<HRMS_EodCurrentInfo>(x => x.HrmsEodCurrentInfo)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_PayrollInfo>()
                .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_BonusInfo>()
                .HasRequired<HRMS_Bonus>(x => x.HrmsBonus)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_BonusInfo>()
                .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_BonusInfo>()
                .HasOptional<HRMS_EodReference>(x => x.HrmsEodReference)
                .WithMany()
                .WillCascadeOnDelete(false);
            #endregion

            #region Leave


            modelBuilder.Entity<HRMS_Leave_Application>()
                .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
                .WithMany()
                .WillCascadeOnDelete(false);

            //modelBuilder.Entity<HRMS_Leave_Application_Details>()
            //    .HasRequired<HRMS_Leave_Application>(x => x.HrmsLeaveApplication)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Leave_Application>()
                .HasRequired<HRMS_Leave_Type>(x => x.HrmsLeaveType)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Leave_Repository>()
                .HasRequired<HRMS_Leave_Type>(x => x.HrmsLeaveType)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Leave_Repository>()
                .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Leave_Repository>()
                .HasRequired<HRMS_Leave_Application>(x => x.HrmsLeaveApplication)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Leave_Assign>()
                .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<HRMS_Leave_Assign>()
                .HasRequired<HRMS_Leave_Type>(x => x.HrmsLeaveType)
                .WithMany()
                .WillCascadeOnDelete(false);

            //Employee_Offday
            modelBuilder.Entity<HRMS_Employee_OffDay>()
                .HasRequired<HRMS_Employee>(x => x.HrmsEmployee)
                .WithMany()
                .WillCascadeOnDelete(false);





            #endregion

        }
    }
}